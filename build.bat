@echo off

echo Running on %ComputerName%
IF EXIST "build*" (
	echo Moving into build folder
	cd build
)

python --version 2>NUL
if errorlevel 1 (
	echo Python isn't installed on this computer
	set PythonInstalled=0
) else (
	set PythonInstalled=1
)

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64

set TimeString=%date:~-4,4%%date:~-10,2%%date:~-7,2%%time:~0,2%%time:~3,2%%time:~6,2%

rem mkdir build > NUL 2> NUL
rem echo Running from %cd%
rem echo Time is %TimeString%	

set ProjectName=GameJam
set LibDirectory=..\lib\include
set SourceDirectory=..\code
set DataDirectory=..\data

set CompilePlatform=1
set CompileApplication=1
set DebugBuild=1
set DeveloperBuild=1
set CopyToDataDirectory=1
set AudioEnabled=1
set CrashDumpAsserts=0

if "%DebugBuild%"=="1" (
	set DebugDependantFlags=/MDd /Od /Zi -DDEBUG=1
	set DebugDependantPaths=/LIBPATH:"..\lib\debug"
	set DebugDependantLibraries=glew32d.lib
) else (
	set DebugDependantFlags=/MD /O2 /Ot /Oy -DDEBUG=0
	set DebugDependantPaths=/LIBPATH:"..\lib\release"
	set DebugDependantLibraries=glew32.lib
)

if "%AudioEnabled%"=="1" (
	set AudioLibraries=OpenAL32.lib libogg_static.lib libvorbis_static.lib
) else (
	set AudioLibraries=
)

set CompilerFlags=%DebugDependantFlags% -DWINDOWS_COMPILATION -DAUDIO_ENABLED=%AudioEnabled% -DCRASH_DUMP_ASSERTS=%CrashDumpAsserts% -DDEVELOPER=%DeveloperBuild% /FC /EHsc /nologo /GS- /Gm- -GR- /EHa- /Fm /Oi /WX /W4 /wd4201 /wd4100 /wd4189 /wd4996 /wd4127 /wd4505 /wd4101 /wd4702 /wd4458 /wd4324 /wd4065
set LinkerFlags=-incremental:no
set IncludeDirectories=/I"%SourceDirectory%" /I"%LibDirectory%"
set LibraryDirectories=%DebugDependantPaths%
set Libraries=gdi32.lib User32.lib Shell32.lib opengl32.lib glfw3.lib Shlwapi.lib %AudioLibraries% %DebugDependantLibraries%
set AppExports=/EXPORT:App_GetStartupOptions /EXPORT:App_GetVersion /EXPORT:App_Initialize /EXPORT:App_Update /EXPORT:App_Closing /EXPORT:App_Reloaded /EXPORT:App_FileChanged

rem echo [Building...]

REM Compile the resources file to generate resources.res which defines our program icon
rc /nologo %SourceDirectory%\resources.rc

del *.pdb > NUL 2> NUL

if "%CompilePlatform%"=="1" (
	echo[
	
	if "%PythonInstalled%"=="1" (
		python ..\IncrementVersionNumber.py %SourceDirectory%\win32\win32_version.h
	)
	
	cl /Fe%ProjectName%.exe %CompilerFlags% %IncludeDirectories% %SourceDirectory%\win32\win32_main.cpp /link %LibraryDirectories% %LinkerFlags% %Libraries% kernel32.lib %SourceDirectory%\resources.res
	
	if "%CopyToDataDirectory%"=="1" (
		echo [Copying %ProjectName%.exe to data directory]
		XCOPY ".\%ProjectName%.exe" "%DataDirectory%\" /Y > NUL
	) else (
		echo [Platform Build Finished!]
	)
)

if "%CompileApplication%"=="1" (
	echo[
	
	if "%PythonInstalled%"=="1" (
		python ..\IncrementVersionNumber.py %SourceDirectory%\app\app_version.h
	)
	
	cl /Fe%ProjectName%.dll %CompilerFlags% %IncludeDirectories% %SourceDirectory%\app\app.cpp /link %LibraryDirectories% %LinkerFlags% %Libraries% %AppExports% /DLL /PDB:"%ProjectName%_%TimeString%.pdb"
	
	if "%CopyToDataDirectory%"=="1" (
		echo [Copying %ProjectName%.dll to data directory]
		XCOPY ".\%ProjectName%.dll" "%DataDirectory%\" /Y > NUL
	) else (
		echo [Application Build Finished!]
	)
)

echo[

REM Delete the object files that were created during compilation
del *.obj > NUL 2> NUL

rem echo [Done!]