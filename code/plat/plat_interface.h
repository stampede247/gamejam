/*
File:   plat_interface.h
Author: Taylor Robbins
Date:   07\12\2019
Description:
	** The plat_interface file is the only file (besides external libraries) that is
	** included by both the applicaiton and the platform layer. It defines the backbone
	** of structures that are used to communicate between the two layers. It also provides
	** a space for some platform specific header files to be included in order to provide
	** structures and definitions for the application that change when running on different
	** operating systems.
*/

#ifndef _PLAT_INTERFACE_H
#define _PLAT_INTERFACE_H

//NOTE: mylib.h checks for WIN32_COMPILATION, OSX_COMPILATION, and LINUX_COMPILATION defines
#include "mylib/mylib.h"

#if WINDOWS_COMPILATION
	#include <windows.h>
	#include <openal/al.h>
	#include <openal/alc.h>
	
	#include "win32/win32_files.h"
	
	#define EXPORT __declspec(dllexport)
	#define IMPORT __declspec(dllimport)
#endif

#if OSX_COMPILATION
	#include "osx/osx_helpers.h"
	
	#ifdef HAVE_AL_AL_H
	#include <AL/al.h>
	#elif defined(__APPLE__)
	#include <OpenAL/al.h>
	#else
	#include <al.h>
	#endif
	#ifdef HAVE_AL_ALC_H
	#include <AL/alc.h>
	#elif defined(__APPLE__)
	#include <OpenAL/alc.h>
	#else
	#include <alc.h>
	#endif
	
	#define EXPORT extern "C" __attribute__((visibility("default")))
	#define IMPORT
#endif

#if LINUX_COMPILATION
	#include "linux/linux_helpers.h"
	
	#include <AL/al.h>
	#include <AL/alc.h>
	
	#define EXPORT extern "C" __attribute__((visibility("default")))
	#define IMPORT
#endif

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "plat/plat_structs.h"

// +--------------------------------------------------------------+
// |                   Platform Layer Functions                   |
// +--------------------------------------------------------------+
#define DoesFolderExist_DEFINITION(functionName) bool functionName(const char* folderPath)
typedef DoesFolderExist_DEFINITION(DoesFolderExist_f);
#define CreateFolder_DEFINITION(functionName) bool functionName(const char* folderPath)
typedef CreateFolder_DEFINITION(CreateFolder_f);
#define DoesFileExist_DEFINITION(functionName) bool functionName(const char* filePath)
typedef DoesFileExist_DEFINITION(DoesFileExist_f);
#define FreeFileMemory_DEFINITION(functionName)  void functionName(FileInfo_t* fileInfo)
typedef FreeFileMemory_DEFINITION(FreeFileMemory_f);
#define ReadEntireFile_DEFINITION(functionName)  FileInfo_t functionName(const char* filePath)
typedef ReadEntireFile_DEFINITION(ReadEntireFile_f);
#define WriteEntireFile_DEFINITION(functionName) bool functionName(const char* filePath, void* memory, u32 memorySize)
typedef WriteEntireFile_DEFINITION(WriteEntireFile_f);
#define DeleteFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef DeleteFile_DEFINITION(DeleteFile_f);
#define OpenFile_DEFINITION(functionName) bool functionName(const char* filePath, OpenFile_t* openFileOut)
typedef OpenFile_DEFINITION(OpenFile_f);
#define AppendFile_DEFINITION(functionName) bool functionName(OpenFile_t* filePntr, const void* newData, u32 newDataSize)
typedef AppendFile_DEFINITION(AppendFile_f);
#define CloseFile_DEFINITION(functionName) void functionName(OpenFile_t* filePntr)
typedef CloseFile_DEFINITION(CloseFile_f);
#define LaunchFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef LaunchFile_DEFINITION(LaunchFile_f);
#define GetNumFilesInDirectory_DEFINITION(functionName) u32 functionName(const char* folderPath)
typedef GetNumFilesInDirectory_DEFINITION(GetNumFilesInDirectory_f);
#define GetFileInDirectory_DEFINITION(functionName) char* functionName(MemoryArena_t* arenaPntr, const char* folderPath, u32 index)
typedef GetFileInDirectory_DEFINITION(GetFileInDirectory_f);
#define ShowSourceFile_DEFINITION(functionName) bool functionName(const char* filePath, u32 lineNumber)
typedef ShowSourceFile_DEFINITION(ShowSourceFile_f);

#define DebugOutput_DEFINITION(functionName) void functionName(const char* functionStr, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* message)
typedef DebugOutput_DEFINITION(DebugOutput_f);
#define DebugPrint_DEFINITION(functionName) void functionName(const char* functionStr, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...)
typedef DebugPrint_DEFINITION(DebugPrint_f);

#define CopyToClipboard_DEFINITION(functionName) void functionName(const void* dataPntr, u32 dataSize)
typedef CopyToClipboard_DEFINITION(CopyToClipboard_f);
#define CopyFromClipboard_DEFINITION(functionName) void* functionName(MemoryArena_t* arenaPntr, u32* dataLengthOut)
typedef CopyFromClipboard_DEFINITION(CopyFromClipboard_f);

#define WatchFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef WatchFile_DEFINITION(WatchFile_f);
#define UnwatchFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef UnwatchFile_DEFINITION(UnwatchFile_f);
#define IsWatchingFile_DEFINITION(functionName) bool functionName(const char* filePath)
typedef IsWatchingFile_DEFINITION(IsWatchingFile_f);

#define AllocateMemory_DEFINITION(functionName) void* functionName(u32 numBytes)
typedef AllocateMemory_DEFINITION(AllocateMemory_f);
#define FreeMemory_DEFINITION(functionName) void functionName(void* memPntr)
typedef FreeMemory_DEFINITION(FreeMemory_f);

#define UpdateLoadingBar_DEFINITION(functionName) void functionName(r32 percentDone)
typedef UpdateLoadingBar_DEFINITION(UpdateLoadingBar_f);

// +--------------------------------------------------------------+
// |                          Structures                          |
// +--------------------------------------------------------------+
struct StartupInfo_t
{
	PlatformType_t platformType;
	Version_t version;
	GlfwVersion_t glfwVersion;
	u32 numMonitors;
	u32 primaryMonitorIndex;
	const MonitorInfo_t* monitors;
	RealTime_t systemTime; //world time (UTC timezone)
	RealTime_t localTime; //world time (Local timezone)
};

#define MAX_STARTUP_OP_PATH_LENGTH 256
struct StartupOptions_t
{
	v2i windowSize;
	v2i minWindowSize;
	v2i maxWindowSize;
	v2i windowPosition;
	bool fullscreen;
	u32 fullscreenMonitorIndex;
	bool topmostWindow;
	bool decoratedWindow;
	bool resizableWindow;
	bool forceAspectRatio;
	v2i aspectRatio;
	u32 antialiasingNumSamples;
	u32 temporaryMemorySize;
	u32 permanantMemorySize;
	
	Color_t loadingColor;
	char iconFilePaths[4][MAX_STARTUP_OP_PATH_LENGTH]; //16, 24, 32, and 64 pixel icons
	char loadingImagePath[MAX_STARTUP_OP_PATH_LENGTH];
};

struct PlatformInfo_t
{
	PlatformType_t platformType;
	Version_t version;
	GlfwVersion_t glfwVersion;
	
	bool steamInitialized;
	u32 steamAppId;
	
	GLFWwindow* window;
	
	ALCdevice* alDevice;
	ALCcontext* alContext;
	
	u32 numMonitors;
	u32 primaryMonitorIndex;
	const MonitorInfo_t* monitors;
	
	DoesFolderExist_f* DoesFolderExist;
	CreateFolder_f*    CreateFolder;
	
	DoesFileExist_f*   DoesFileExist;
	FreeFileMemory_f*  FreeFileMemory;
	ReadEntireFile_f*  ReadEntireFile;
	WriteEntireFile_f* WriteEntireFile;
	DeleteFile_f*      DeleteFile;
	
	OpenFile_f*   OpenFile;
	AppendFile_f* AppendFile;
	CloseFile_f*  CloseFile;
	LaunchFile_f* LaunchFile;
	
	GetNumFilesInDirectory_f* GetNumFilesInDirectory;
	GetFileInDirectory_f*     GetFileInDirectory;
	
	ShowSourceFile_f* ShowSourceFile;
	
	DebugOutput_f*    DebugOutput;
	DebugPrint_f*     DebugPrint;
	
	CopyToClipboard_f*   CopyToClipboard;
	CopyFromClipboard_f* CopyFromClipboard;
	
	WatchFile_f*      WatchFile;
	UnwatchFile_f*    UnwatchFile;
	IsWatchingFile_f* IsWatchingFile;
	
	AllocateMemory_f* AllocateMemory;
	FreeMemory_f*     FreeMemory;
	
	UpdateLoadingBar_f* UpdateLoadingBar;
};

struct AppMemory_t
{
	u32 permanantSize;
	u32 transientSize;
	
	void* permanantPntr;
	void* transientPntr;
};

#define MAX_NUM_GAMEPADS        GLFW_JOYSTICK_LAST
#define MAX_NUM_INPUT_EVENTS    32
#define MAX_INPUT_TEXT_LENGTH   64

struct AppInput_t
{
	bool mouseInsideWindow;
	bool mouseInsideChanged;
	v2 mousePos;
	bool mouseMoved;
	v2 mouseStartPos[3];
	r32 mouseMaxDist[3];
	v2 scrollDelta;
	v2 scrollValue;
	bool scrollChanged;
	
	v2i windowPosition;
	bool windowMoved;
	v2i screenSize;
	bool windowResized;
	bool windowHasFocus;
	bool windowFocusChanged;
	bool windowIsMinimized;
	bool windowMinimizedChanged;
	bool steamOverlayOpen;
	bool steamOverlayOpenChanged;
	
	r64 programTimeF; //ms (since start of program)
	u64 programTime; //ms (since start of program)
	r64 elapsedMs; //ms
	r64 avgElapsedMs; //ms
	r64 framerate; //frames/sec
	r64 avgFramerate; //frames/sec
	r64 targetFramerate; //frames/sec
	r64 timeDelta; //stays near 1.0f if framerate is close to targetFramerate
	r64 lastUpdateElapsedMs; //ms
	
	RealTime_t systemTime; //world time (UTC timezone)
	RealTime_t localTime; //world time (Local timezone)
	
	u8 modifiers;
	ButtonState_t buttons[Buttons_NumButtons];
	
	GamepadState_t gamepads[MAX_NUM_GAMEPADS];
	
	u32 numInputEvents;
	InputEvent_t inputEvents[MAX_NUM_INPUT_EVENTS];
	
	u8 textInputLength;
	char textInput[MAX_INPUT_TEXT_LENGTH];
};

#define MAX_WINDOW_TITLE_LENGTH 64

struct AppOutput_t
{
	bool recenterMouse;
	bool showMouse;
	bool closeWindow;
	
	CursorType_t cursorType;
	
	char windowTitle[MAX_WINDOW_TITLE_LENGTH];
};


//+================================================================+
//|                  Game Function Definitions                     |
//+================================================================+

#define AppGetVersion_DEFINITION(functionName)                   Version_t functionName(bool* resetApplication)
typedef AppGetVersion_DEFINITION(AppGetVersion_f);

#define AppGetStartupOptions_DEFINITION(functionName)            void functionName(const StartupInfo_t* StartupInfo, StartupOptions_t* StartupOptions)
typedef AppGetStartupOptions_DEFINITION(AppGetStartupOptions_f);

#define AppInitialize_DEFINITION(functionName)                   void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput)
typedef AppInitialize_DEFINITION(AppInitialize_f);

#define AppReloaded_DEFINITION(functionName)                     void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
typedef AppReloaded_DEFINITION(AppReloaded_f);

#define AppFileChanged_DEFINITION(functionName)                  void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const char* filePath)
typedef AppFileChanged_DEFINITION(AppFileChanged_f);

#define AppUpdate_DEFINITION(functionName)                       void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
typedef AppUpdate_DEFINITION(AppUpdate_f);

#define AppClosing_DEFINITION(functionName)                      void functionName(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
typedef AppClosing_DEFINITION(AppClosing_f);

#endif //  _PLAT_INTERFACE_H
