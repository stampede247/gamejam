/*
File:   plat_callbacks.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** Contains all the callback functions that we register with GLFW 
*/

void GlfwErrorCallback(i32 errorCode, const char* description)
{
	PrintLine_E("GlfwErrorCallback: %X \"%s\"", errorCode, description);
	if (platform.glfwLastErrorStr != nullptr) { free(platform.glfwLastErrorStr); platform.glfwLastErrorStr = nullptr; }
	platform.glfwLastErrorCode = errorCode;
	if (description != nullptr)
	{
		u32 descriptionLength = (u32)strlen(description);
		platform.glfwLastErrorStr = (char*)malloc(descriptionLength+1);
		memcpy(platform.glfwLastErrorStr, description, descriptionLength);
		platform.glfwLastErrorStr[descriptionLength] = '\0';
	}
}

void GlfwWindowCloseCallback(GLFWwindow* window)
{
	WriteLine_W("Window Closing");
	//TODO: Should we dissallow the close in some scenarios?
	// glfwSetWindowShouldClose(window, false);
}

void GlfwWindowSizeCallback(GLFWwindow* window, i32 screenWidth, i32 screenHeight)
{
	Assert(platform.currentInput != nullptr);
	v2i newScreenSize = NewVec2i(screenWidth, screenHeight);
	if (platform.currentInput->screenSize != newScreenSize)
	{
		platform.currentInput->screenSize = newScreenSize;
		platform.currentInput->windowResized = true;
	}
}

void GlfwWindowMoveCallback(GLFWwindow* window, i32 posX, i32 posY)
{
	Assert(platform.currentInput != nullptr);
	v2i newWindowPos = NewVec2i(posX, posY);
	if (platform.currentInput->windowPosition != newWindowPos)
	{
		platform.currentInput->windowPosition = newWindowPos;
		platform.currentInput->windowMoved = true;
		//TODO: Should we keep track of which monitor the window is on?
	}
}

void GlfwWindowMinimizeCallback(GLFWwindow* window, i32 isMinimized)
{
	Assert(platform.currentInput != nullptr);
	bool newMinimizedState = (isMinimized > 0);
	if (platform.currentInput->windowIsMinimized != newMinimizedState)
	{
		platform.currentInput->windowIsMinimized = newMinimizedState;
		platform.currentInput->windowMinimizedChanged = true;
	}
}

void GlfwWindowFocusCallback(GLFWwindow* window, i32 isFocused)
{
	Assert(platform.currentInput != nullptr);
	bool newFocusState = (isFocused > 0);
	if (platform.currentInput->windowHasFocus != newFocusState)
	{
		platform.currentInput->windowHasFocus = (isFocused > 0);
		platform.currentInput->windowFocusChanged = true;
	}
}

void GlfwKeyPressedCallback(GLFWwindow* window, i32 key, i32 scanCode, i32 action, i32 modifiers)
{
	Assert(platform.currentInput != nullptr);
	Plat_HandleKeyEvent(window, platform.currentInput, key, action, modifiers);
}

void GlfwCharPressedCallback(GLFWwindow* window, u32 codepoint)
{
	// PrintLine_D("Text Input: 0x%X", codepoint);
	Assert(platform.currentInput != nullptr);
	if (platform.currentInput->textInputLength < ArrayCount(platform.currentInput->textInput))
	{
		platform.currentInput->textInput[platform.currentInput->textInputLength] = (u8)codepoint;
		platform.currentInput->textInputLength++;
	}
}

void GlfwCursorPosCallback(GLFWwindow* window, r64 mouseX, r64 mouseY)
{
	Assert(platform.currentInput != nullptr);
	
	v2 newMousePos = NewVec2((r32)mouseX, (r32)mouseY);
	#if DOUBLE_MOUSE_POS
	newMousePos = newMousePos * 2;
	#endif
	
	if (platform.currentInput->mousePos != newMousePos)
	{
		platform.currentInput->mousePos = newMousePos;
		platform.currentInput->mouseMoved = true;
		
		if (platform.currentInput->buttons[MouseButton_Left].isDown)
		{
			r32 distance = Vec2Length(newMousePos - platform.currentInput->mouseStartPos[MouseButton_Left]);
			if (distance > platform.currentInput->mouseMaxDist[MouseButton_Left])
			{
				platform.currentInput->mouseMaxDist[MouseButton_Left] = distance;
			}
		}
		if (platform.currentInput->buttons[MouseButton_Right].isDown)
		{
			r32 distance = Vec2Length(newMousePos - platform.currentInput->mouseStartPos[MouseButton_Right]);
			if (distance > platform.currentInput->mouseMaxDist[MouseButton_Right])
			{
				platform.currentInput->mouseMaxDist[MouseButton_Right] = distance;
			}
		}
		if (platform.currentInput->buttons[MouseButton_Middle].isDown)
		{
			r32 distance = Vec2Length(newMousePos - platform.currentInput->mouseStartPos[MouseButton_Middle]);
			if (distance > platform.currentInput->mouseMaxDist[MouseButton_Middle])
			{
				platform.currentInput->mouseMaxDist[MouseButton_Middle] = distance;
			}
		}
	}
}

void GlfwMousePressCallback(GLFWwindow* window, i32 button, i32 action, i32 modifiers)
{
	Assert(platform.currentInput != nullptr);
	Plat_HandleMouseEvent(window, platform.currentInput, button, action, modifiers);
}

void GlfwMouseScrollCallback(GLFWwindow* window, r64 deltaX, r64 deltaY)
{
	Assert(platform.currentInput != nullptr);
	platform.currentInput->scrollValue += NewVec2((r32)deltaX, (r32)deltaY);
	platform.currentInput->scrollDelta += NewVec2((r32)deltaX, (r32)deltaY);
	if (deltaX != 0 || deltaY != 0)
	{
		platform.currentInput->scrollChanged = true;
	}
}

void GlfwCursorEnteredCallback(GLFWwindow* window, i32 entered)
{
	Assert(platform.currentInput != nullptr);
	bool newEnteredState = (entered > 0);
	if (platform.currentInput->mouseInsideWindow != newEnteredState)
	{
		platform.currentInput->mouseInsideWindow = newEnteredState;
		platform.currentInput->mouseInsideChanged = true;
	}
}
