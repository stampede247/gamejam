/*
File:   settings_menu.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Handles the settings menu that can be opened in the main menu or from the in-game menu
*/

// +--------------------------------------------------------------+
// |                   Initialize Settings Menu                   |
// +--------------------------------------------------------------+
void InitializeSettingsMenu()
{
	Assert(settMenu != nullptr);
	
	settMenu->initialized = true;
}

// +--------------------------------------------------------------+
// |                     Start Settings Menu                      |
// +--------------------------------------------------------------+
void StartSettingsMenu(AppMenu_t oldAppMenu, AppState_t appStateBelow, TransInfoType_t transInfoType, void* transInfoPntr)
{
	Assert(settMenu != nullptr);
	
	settMenu->openAnimProgress = 0.0f;
	settMenu->closing = false;
	settMenu->showingKeyboard = false;
	settMenu->showingGamepad = false;
	
}

// +--------------------------------------------------------------+
// |                  Deinitialize Settings Menu                  |
// +--------------------------------------------------------------+
void DeinitializeSettingsMenu()
{
	Assert(settMenu != nullptr);
	
	ClearPointer(settMenu);
}

// +--------------------------------------------------------------+
// |                     Update Settings Menu                     |
// +--------------------------------------------------------------+
void UpdateSettingsMenu(AppState_t appStateBelow, AppMenu_t appMenuAbove)
{
	Assert(settMenu != nullptr);
	if (appMenuAbove == AppMenu_None && (GameBtnPressed(GameBtn_Cancel) || GameBtnPressed(GameBtn_Start)) && !settMenu->showingKeyboard && !settMenu->showingGamepad)
	{
		HandleGameBtn(GameBtn_Cancel);
		HandleGameBtn(GameBtn_Start);
		WriteLine_D("Closing settings menu because of escape");
		PopAppMenu();
	}
	
	if (appMenuAbove == AppMenu_None)
	{
		// +==================================+
		// | Show Input Visualization Hotkey  |
		// +==================================+
		if (ButtonPressed(Button_Pipe))
		{
			HandleButton(Button_Pipe);
			if (settMenu->showingKeyboard)
			{
				settMenu->showingKeyboard = false;
				settMenu->showingGamepad = true;
			}
			else if (settMenu->showingGamepad)
			{
				settMenu->showingGamepad = false;
			}
			else
			{
				settMenu->showingKeyboard = true;
			}
		}
		
		// +==============================+
		// |    Update Open Animation     |
		// +==============================+
		if (!settMenu->closing && settMenu->openAnimProgress < 1.0f)
		{
			settMenu->openAnimProgress += ((r32)appInput->elapsedMs / SETT_MENU_OPEN_TIME);
			if (settMenu->openAnimProgress >= 1.0f) { settMenu->openAnimProgress = 1.0f; }
		}
		if (settMenu->closing && settMenu->openAnimProgress > 0.0f)
		{
			settMenu->openAnimProgress -= ((r32)appInput->elapsedMs / SETT_MENU_CLOSE_TIME);
			if (settMenu->openAnimProgress <= 0.0f) { settMenu->openAnimProgress = 0.0f; }
		}
		
		if (settMenu->closing && settMenu->openAnimProgress <= 0.0f)
		{
			PopAppMenu();
			return;
		}
	}
}

// +--------------------------------------------------------------+
// |                     Render Settings Menu                     |
// +--------------------------------------------------------------+
void RenderSettingsMenu(FrameBuffer_t* renderBuffer, AppState_t appStateBelow, AppMenu_t appMenuAbove)
{
	RcBegin(&resources->mainShader, RenderArea, &resources->pixelFont, renderBuffer);
	RcClearDepthBuffer(1.0f);
	
	// +==============================+
	// |   Blur Existing Background   |
	// +==============================+
	{
		RcBindShader(&resources->gaussianHoriShader);
		RcSetShaderValue0(LerpR32(0, 10, settMenu->openAnimProgress));
		RcBindFrameBuffer(&app->secondaryBuffer);
		RcClear(Transparent, 1.0f);
		RcBindTexture(&renderBuffer->texture);
		RcDrawTexturedRec(RenderArea, White);
		
		RcBindShader(&resources->gaussianVertShader);
		RcSetShaderValue0(LerpR32(0, 10, settMenu->openAnimProgress));
		RcBindFrameBuffer(renderBuffer);
		RcClear(Transparent, 1.0f);
		RcBindTexture(&app->secondaryBuffer.texture);
		RcDrawTexturedRec(RenderArea, ColorLerp(White, NewColor(Color_Gray), settMenu->openAnimProgress));
		
		RcBindShader(&resources->mainShader);
	}
	
	RcBindFont(&resources->pixelFont);
	
	RcDrawBorderedRec(settMenu->drawRec, ColorTransparent(plt->target.dark, 0.7f), plt->target.light, 9.0f);
	
	// +==============================+
	// |  Draw Debug Input Overlays   |
	// +==============================+
	if (settMenu->showingKeyboard)
	{
		RenderCurrentKeyboardInputDisplay();
	}
	if (settMenu->showingGamepad)
	{
		RenderCurrentGamepadInputDisplay();
	}
}
