/*
File:   settings_menu.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _SETTINGS_MENU_H
#define _SETTINGS_MENU_H

struct SettingsMenuData_t
{
	bool initialized;
	
	r32 openAnimProgress;
	bool closing;
	bool showingKeyboard;
	bool showingGamepad;
	
	rec drawRec;
};

#endif //  _SETTINGS_MENU_H
