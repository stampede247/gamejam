/*
File:   mess.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _MESS_H
#define _MESS_H

struct MessData_t
{
	bool initialized;
	
	v2 viewPos;
	v2 targetPos;
	
	r32 metaBallValues[10][10];
	
	DynArray_t polygonVerts;
};

#endif //  _MESS_H
