/*
File:   mess.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Handles the "Mess" AppState which is basically just a dumping ground for various visualizations
	** and things that we want to test.
*/

// +--------------------------------------------------------------+
// |                          Initialize                          |
// +--------------------------------------------------------------+
void InitializeMess()
{
	Assert(mess != nullptr);
	
	mess->viewPos = Vec2_Zero;
	mess->targetPos = mess->viewPos;
	
	CreateDynamicArray(&mess->polygonVerts, mainHeap, sizeof(Vertex_t));
	 
	mess->initialized = true;
}

// +--------------------------------------------------------------+
// |                            Start                             |
// +--------------------------------------------------------------+
bool StartMess(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	u32 numColumns = ArrayCount(mess->metaBallValues[0]);
	u32 numRows = ArrayCount(mess->metaBallValues);
	for (u32 xPos = 0; xPos < numColumns; xPos++)
	{
		for (u32 yPos = 0; yPos < numRows; yPos++)
		{
			mess->metaBallValues[yPos][xPos] = 1.0f;
		}
	}
	
	return true;
}

// +--------------------------------------------------------------+
// |                         Deinitialize                         |
// +--------------------------------------------------------------+
void DeinitializeMess()
{
	Assert(mess != nullptr);
	Assert(mess->initialized == true);
	
	DestroyDynamicArray(&mess->polygonVerts);
	
	mess->initialized = false;
	ClearPointer(mess);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesMessCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                         Update Mess                          |
// +--------------------------------------------------------------+
void UpdateMess(AppMenu_t appMenuAbove)
{
	if (appMenuAbove == AppMenu_None)
	{
		// +==============================+
		// |      Main Menu Shortcut      |
		// +==============================+
		if (ButtonPressed(Button_Tilde)) { PopAppState(); }
		
		// +==============================+
		// |         Update View          |
		// +==============================+
		r32 viewMoveSpeed = 5.0f;
		if (ButtonDown(Button_Shift))   { viewMoveSpeed = 15.0f; }
		if (ButtonDown(Button_Control)) { viewMoveSpeed = 2.0f; }
		if (ButtonDown(Button_W)) { mess->targetPos.y -= viewMoveSpeed; }
		if (ButtonDown(Button_A)) { mess->targetPos.x -= viewMoveSpeed; }
		if (ButtonDown(Button_S)) { mess->targetPos.y += viewMoveSpeed; }
		if (ButtonDown(Button_D)) { mess->targetPos.x += viewMoveSpeed; }
		if (mess->viewPos != mess->targetPos)
		{
			v2 offset = mess->targetPos - mess->viewPos;
			if (Vec2Length(offset) >= 0.01f)
			{
				mess->viewPos += offset / 10.0f;
			}
			else
			{
				mess->viewPos = mess->targetPos;
			}
		}
		
		// rec ballArea = RecSquarify(RenderArea);
		// u32 numColumns = ArrayCount(mess->metaBallValues[0]);
		// u32 numRows = ArrayCount(mess->metaBallValues);
		// r32 cursorRadius = 200;
		
		// r32 newMetaRadius = RenderMousePos.y / RenderArea.height;
		// PrintLine_I("Setting meta balls to %f", newMetaRadius);
		// for (u32 xPos = 0; xPos < numColumns; xPos++)
		// {
		// 	for (u32 yPos = 0; yPos < numRows; yPos++)
		// 	{
		// 		mess->metaBallValues[yPos][xPos] = newMetaRadius;
		// 	}
		// }
		
		// for (u32 xPos = 0; xPos < numColumns; xPos++)
		// {
		// 	for (u32 yPos = 0; yPos < numRows; yPos++)
		// 	{
		// 		r32* ballValue = &mess->metaBallValues[yPos][xPos];
		// 		v2 ballPos = ballArea.topLeft + Vec2Multiply(ballArea.size, NewVec2((r32)xPos / numColumns, (r32)yPos / numRows));
		// 		r32 distance = Vec2Length(RenderMousePos - ballPos);
		// 		if (distance <= cursorRadius)
		// 		{
		// 			if (ButtonDown(MouseButton_Left))
		// 			{
		// 				if (distance < cursorRadius/4) { *ballValue = 0.0f; }
		// 				else
		// 				{
		// 					*ballValue -= 10.0f / distance;
		// 					*ballValue = ClampR32(*ballValue, 0, 1);
		// 				}
		// 			}
		// 			else if (ButtonDown(MouseButton_Right))
		// 			{
		// 				if (distance < cursorRadius/4) { *ballValue = 1.0f; }
		// 				else
		// 				{
		// 					*ballValue += 10.0f / distance;
		// 					*ballValue = ClampR32(*ballValue, 0, 1);
		// 				}
		// 			}
		// 		}
				
		// 		// if (*ballValue < 0.35f)
		// 		// {
		// 		// 	*ballValue -= 0.01f;
		// 		// 	*ballValue = ClampR32(*ballValue, 0, 1);
		// 		// }
		// 		if (*ballValue > 0.0f && *ballValue < 1.0f)
		// 		{
		// 			*ballValue += 0.005f;
		// 			*ballValue = ClampR32(*ballValue, 0, 1);
		// 		}
		// 		if (*ballValue == 0.0f)
		// 		{
		// 			r32 valueUp    = (yPos > 0)            ? mess->metaBallValues[yPos-1][xPos] : 1.0f;
		// 			r32 valueDown  = (yPos < numRows-1)    ? mess->metaBallValues[yPos+1][xPos] : 1.0f;
		// 			r32 valueRight = (xPos < numColumns-1) ? mess->metaBallValues[yPos][xPos+1] : 1.0f;
		// 			r32 valueLeft  = (xPos > 0)            ? mess->metaBallValues[yPos][xPos-1] : 1.0f;
		// 			if (valueUp >= 1.0f || valueDown >= 1.0f || valueRight >= 1.0f || valueLeft >= 1.0f)
		// 			{
		// 				*ballValue += 0.001f;
		// 				*ballValue = ClampR32(*ballValue, 0, 1);
		// 			}
		// 		}
		// 	}
		// }
		
		if (ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			if (mess->polygonVerts.length < 10)//RC_MAX_DYNAMIC_VERTICES)
			{
				Vertex_t* newVert = DynArrayAdd(&mess->polygonVerts, Vertex_t);
				ClearPointer(newVert);
				newVert->position = NewVec3(RenderMousePos.x, RenderMousePos.y, 0.0f);
				newVert->color = NewVec4(NewColor(GetColorByIndex(RandU32(0, 16))));
				newVert->texCoord = NewVec2(RenderMousePos.x / RenderArea.width, RenderMousePos.y / RenderArea.height);
			}
			else
			{
				mess->polygonVerts.length = 0;
			}
		}
	}
}

// +--------------------------------------------------------------+
// |                         Render Mess                          |
// +--------------------------------------------------------------+
void RenderMess(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	RcBegin(&resources->mainShader, RenderArea, &resources->pixelFont, renderBuffer);
	RcClear(VisBlueGray, 1.0f);
	RcSetViewMatrix(Mat4Translate2(-mess->viewPos.x, -mess->viewPos.y));
	
	r32 rotation = ((ProgramTime%10000) / 10000.0f) * Pi32*2;
	r32 scale = Oscillate(0, 1.0f, 1000);
	
	// RcDrawRectangle(NewRec(0, 0, 100, 100), NewColor(Color_Red));
	
	// RcBindTexture(&resources->testTexture);
	// RcDrawTexturedRec(NewRec(0, 0, 200, 200), White, NewRec(Vec2_Zero, NewVec2(resources->testTexture.size)));
	
	// RcDrawSpriteCentered(&resources->testSprite, RenderMousePos + mess->viewPos, White, 5.0f, ((ProgramTime%10000) / 10000.0f) * Pi32 * 2);
	// RcDrawSpriteOffset(&resources->testSprite, RenderMousePos + mess->viewPos, NewVec2(10), White, 5.0f, ((ProgramTime%10000) / 10000.0f) * Pi32 * 2);
	
	// for (u32 x = 0; x < 30; x++)
	// {
	// 	for (u32 y = 0; y < 50; y++)
	// 	{
	// 		// RcDrawRotatedRectangle(NewVec2((r32)x - 2, (r32)y - 2) * 100, NewVec2(500, 800), rotation, (x%2 != 0) ? VisRed : VisBlue);
	// 		RcDrawRotatedRectangle(NewVec2((r32)x - 1, (r32)y + x - 30) * 100, NewVec2(250, 250), rotation, NewColor(GetColorByIndex((y*30 + x) % (6))));
	// 	}
	// }
	
	// for (u32 x = 0; x < 100; x++)
	// {
	// 	for (u32 y = 0; y < 100; y++)
	// 	{
	// 		RcDrawTriangle(NewVec2((r32)x - 10, (r32)y - 10) * 100, rotation, 400, (x%2 != 0) ? VisRed : VisBlue);
	// 	}
	// }
	
	// RcDrawTriangleBordered(RenderMousePos, rotation, 400, VisDarkRed, VisRed, 100);
	
	// RcDrawBorderedRec(NewRec(100, 100, Oscillate(100, 700, 1000), OscillateSaw(100, 800, 1400)), VisRed, VisBlue, Oscillate(4, 80, 10000));
	
	// RcDrawBorderedRecClipped(NewRec(100, 100, 800, 500), VisRed, VisBlue, Oscillate(4, 80, 10000));
	
	// RcDrawGradient(RenderArea, ColorLerp(VisPurple, VisBlue, Oscillate(0, 1, 1333)), ColorLerp(VisYellow, VisRed, Oscillate(0, 1, 1000)), Dir2_Right);
	
	// RcDrawLine(NewVec2(100, 100), RenderMousePos, Oscillate(1, 10, 1000), VisYellow);
	
	// RcDrawCircle(RenderMousePos, Oscillate(10, 200, 10000), VisYellow);
	
	// RcDrawDonut(RenderMousePos, Oscillate(10, 200, 10000), Oscillate(0, 100, 10000) , VisYellow);
	
	// RcDrawSheetFrame(&resources->testSheet, NewVec2i((ProgramTime%(100*(u32)resources->testSheet.numFrames.x))/100, 0), NewRec(Vec2_Zero, NewVec2(resources->testSheet.innerFrameSize) * 5), White, Dir2_Down);
	
	// RcDrawBezierCurve3(NewVec2(100), NewVec2(400, 500), RenderMousePos, VisYellow, 8, 50, true, false);
	// RcDrawBezierArrow3(NewVec2(100), NewVec2(400, 500), RenderMousePos, 8, 150, VisYellow);
	// RcDrawBezierArrowBordered3(NewVec2(100), NewVec2(400, 500), RenderMousePos, 8, 150, VisYellow, VisRed, 15);
	
	// v2 textPos = mess->viewPos + NewVec2(5, RenderArea.height - 5 - RcMaxExtendDown());
	// RcDrawCircle(textPos, 5, VisRed);
	
	// RcBindFontDef(&resources->emojiFont);
	// RcSetFontStyle(FontStyle_Default|FontStyle_RawData);
	// RcDrawNtString("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", Vec2_Zero, VisWhite);
	
	// rec ballArea = RecSquarify(RenderArea);
	// u32 numColumns = ArrayCount(mess->metaBallValues);
	// u32 numRows = ArrayCount(mess->metaBallValues[0]);
	// r32 cursorRadius = 200;
	// RcDrawRectangle(ballArea, VisRed);
	
	// RcBindShader(&resources->metaBallShader);
	// GLint ballValueUniformLocation = glGetUniformLocation(resources->metaBallShader.programId, "MetaBallValues");
	// if (ButtonPressed(Button_1)) { PrintLine_I("Location: %d", ballValueUniformLocation); }
	// glUniform1fv(ballValueUniformLocation, ArrayCount(mess->metaBallValues)*ArrayCount(mess->metaBallValues), &mess->metaBallValues[0][0]);
	// RcSetSecondaryColor(NewColor(Color_Black));
	// RcDrawRectangle(ballArea, ColorTransparent(NewColor(Color_Black), 0.0f));
	// RcBindShader(&resources->mainShader);
	
	// RcDrawDonut(RenderMousePos, cursorRadius, cursorRadius - 2, VisPurple);
	// for (u32 xPos = 0; xPos < numColumns; xPos++)
	// {
	// 	for (u32 yPos = 0; yPos < numRows; yPos++)
	// 	{
	// 		r32* ballValue = &mess->metaBallValues[yPos][xPos];
	// 		v2 ballPos = ballArea.topLeft + Vec2Multiply(ballArea.size, NewVec2((r32)xPos / numColumns, (r32)yPos / numRows));
	// 		r32 distance = Vec2Length(RenderMousePos - ballPos);
	// 		// if (distance < cursorRadius)
	// 		// {
	// 		// 	RcDrawCircle(ballPos, 10, VisPurple);
	// 		// }
	// 	}
	// }
	
	// for (u32 mIndex = 0; mIndex < platform->numMonitors; mIndex++)
	// {
	// 	bool isPrimary = (mIndex == platform->primaryMonitorIndex);
	// 	Assert(platform->monitors != nullptr);
	// 	const MonitorInfo_t* info = &platform->monitors[mIndex];
	// 	Assert(info->modes != nullptr);
	// 	const MonitorDisplayMode_t* displayMode = &info->modes[info->currentMode];
	// 	r32 displayScale = 1/4.0f;
	// 	rec monitorRec = NewRec(info->offset.x * displayScale, info->offset.y * displayScale, displayMode->resolution.width * displayScale, displayMode->resolution.height * displayScale);
	// 	RcDrawBorderedRec(monitorRec, isPrimary ? VisYellow : VisRed, VisWhite, 2);
	// }
	
	// static Vertex_t testTriangle[] = {
	// 	{ {  0.0f,  0.0f,  0.0f }, NewVec4(0, 1, 1, 1), { 0.0f, 0.0f } },
	// 	{ { 10.0f,  0.0f,  0.0f }, NewVec4(1, 1, 0, 1), { 1.0f, 0.0f } },
	// 	{ {  0.0f, 10.0f,  0.0f }, NewVec4(1, 1, 0, 1), { 0.0f, 1.0f } },
		
	// 	{ { 10.0f,  0.0f,  0.0f }, NewVec4(1, 1, 0, 1), { 1.0f, 0.0f } },
	// 	{ {  0.0f, 10.0f,  0.0f }, NewVec4(1, 1, 0, 1), { 0.0f, 1.0f } },
	// 	{ {  8.0f,  8.0f,  0.0f }, NewVec4(1, 0, 1, 1), { 1.0f, 1.0f } }
	// };
	// if (ButtonDown(MouseButton_Left))
	// {
	// 	for (u32 vIndex = 0; vIndex < ArrayCount(testTriangle); vIndex++)
	// 	{
	// 		Vertex_t* vert = &testTriangle[vIndex];
	// 		vert->position.x = (r32)RandU32(0, 10);
	// 		vert->position.y = (r32)RandU32(0, 10);
	// 	}
	// }
	// RcBindTexture(&resources->testTexture);
	// RcDefaultSourceRectangle();
	// RcDrawVerticesEx(ArrayCount(testTriangle), &testTriangle[0], RenderMousePos, NewVec2(3, 3), White, 10.0f, rotation);
	
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	if (mess->polygonVerts.length >= 1 && mess->polygonVerts.length < 10)
	{
		u32 numTempVerts = mess->polygonVerts.length+1;
		Vertex_t* tempVerts = TempArray(Vertex_t, numTempVerts);
		memcpy(tempVerts, mess->polygonVerts.items, sizeof(Vertex_t) * mess->polygonVerts.length);
		Vertex_t* mouseVert = &tempVerts[numTempVerts-1];
		ClearPointer(mouseVert);
		mouseVert->position = NewVec3(RenderMousePos.x, RenderMousePos.y, 0.0f);
		mouseVert->color = NewVec4(White);
		mouseVert->texCoord = NewVec2(RenderMousePos.x / RenderArea.width, RenderMousePos.y / RenderArea.height);
		RcDrawPolygon(numTempVerts, tempVerts);
	}
	else
	{
		// RcDrawVertices(mess->polygonVerts.length, (Vertex_t*)mess->polygonVerts.items);
		// RcDrawConvexPolygon(mess->polygonVerts.length, (Vertex_t*)mess->polygonVerts.items);
		RcDrawPolygon(mess->polygonVerts.length, (Vertex_t*)mess->polygonVerts.items);
	}
	
	#if 0
	if (mess->polygonVerts.length > 0 && mess->polygonVerts.length < 6)
	{
		Vertex_t* vert1 = DynArrayGet(&mess->polygonVerts, Vertex_t, 0);
		RcDrawLine(NewVec2(vert1->position.x, vert1->position.y), RenderMousePos, 2, VisWhite);
		if (mess->polygonVerts.length > 1)
		{
			Vertex_t* vert2 = DynArrayGet(&mess->polygonVerts, Vertex_t, mess->polygonVerts.length-1);
			RcDrawLine(NewVec2(vert2->position.x, vert2->position.y), RenderMousePos, 2, VisWhite);
			if (mess->polygonVerts.length < 3)
			{
				RcDrawLine(NewVec2(vert2->position.x, vert2->position.y), NewVec2(vert1->position.x, vert1->position.y), 2, VisWhite);
			}
		}
	}
	#endif
}
