/*
File:   win32_main.h
Author: Taylor Robbins
Date:   07\12\2019
*/

#ifndef _WIN_32_MAIN_H
#define _WIN_32_MAIN_H

// +--------------------------------------------------------------+
// |                           Globals                            |
// +--------------------------------------------------------------+
struct PlatformData_t
{
	PlatformInfo_t info;
	Version_t version;
	StartupInfo_t startupInfo;
	StartupOptions_t startupOptions;
	AppMemory_t appMemory;
	AppOutput_t appOutput;
	AppInput_t appInputs[2];
	AppInput_t* currentInput;
	
	MemoryArena_t tempArena;
	MemoryArena_t mainHeap;
	MemoryArena_t stdHeap;
	void* heapSpace;
	void* tempSpace;
	DynArray_t appAllocations;
	
	Win32Mutex_t threadsMutex;
	Win32Thread_t threads[MAX_NUM_THREADS];
	
	WatchFilesInfo_t watchFilesInfo = {};
	// Win32Thread_t* FileWatchingThread = nullptr;
	
	bool glfwInitialized;
	GLFWwindow* window;
	v2i screenSize;
	v2i windowPosition;
	CursorType_t currentCursor;
	GLFWcursor* glfwCursors[Cursor_NumTypes];
	u32 primaryMonitorIndex;
	DynArray_t glfwMonitors;
	bool windowResized;
	char* windowTitle;
	
	char* exeDirectory;
	char* exeFileName;
	char* workingDirectory;
	char* appDllPath;
	char* appDllTempPath;
	
	#if DEBUG
	Win32Thread_t* dllWatchThread;
	DllWatchingInfo_t dllWatchInfo;
	#endif
	
	i32 glfwLastErrorCode;
	char* glfwLastErrorStr;
	
	Win32_Texture_t loadingImage;
	Win32_Texture_t dotTexture;
	Win32_VertexBuffer_t squareBuffer;
	Win32_Shader_t mainShader;
	bool appLoadingFinished;
	r32 appLoadingPercent;
};

#endif //  _WIN_32_MAIN_H
