/*
File:   win32_main.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** This is the file that actually get's compiled in order to produce the platform layer executable.
	** All other platform layer code is included either directly or indirectly from this file
	** This file also contains the main entry point for the application which will allocate memory,
	** create the window, load the game DLL, and run the game update loop
	** In order to keep the size of this file manageable all other functions are put in other files
	** This file acts as a platform layer for the game on windows platforms and makes calls to the Win32 API provided by windows to accomplish many of it's tasks
*/

//Standard Libraries
#include <windows.h>
#include "Shlwapi.h"
#include <stdio.h>
#include <iostream>
#include <string.h>
#include <stdbool.h>
#include <ctime>
#include <sys/timeb.h>

#define USE_ASSERT_FAILURE_FUNCTION true

//Project Headers
#include "plat/plat_interface.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#include "win32/win32_version.h"

//Extra my libraries
#include "mylib/my_tempMemory.h"
#include "mylib/my_dynamicArray.h"

// +--------------------------------------------------------------+
// |                 Platform Layer Header Files                  |
// +--------------------------------------------------------------+
#include "win32/win32_defines.h"
#include "win32/win32_memory.h"
#include "win32/win32_threads.h"
#include "win32/win32_file_watching.h"
#include "win32/win32_app_loading.h"
#include "win32/win32_loading_render.h"
#include "win32/win32_main.h"

// +--------------------------------------------------------------+
// |                    Platform Layer Globals                    |
// +--------------------------------------------------------------+
static PlatformData_t platform = {};
static LoadedApp_t app = {};
static u32 numCrashDumps = 0;
#include "mylib/my_tempMemory.cpp" //Defines TempArena global

// +--------------------------------------------------------------+
// |                 Platform Layer Source Files                  |
// +--------------------------------------------------------------+
#include "win32/win32_debug.cpp"
#include "win32/win32_helpers.cpp"
#include "win32/win32_memory.cpp"
#include "win32/win32_files.cpp"
#include "win32/win32_threads.cpp"
#include "win32/win32_app_loading.cpp"
#include "win32/win32_clipboard.cpp"
#include "win32/win32_file_watching.cpp"
#include "win32/win32_loading_render.cpp"

#include "plat/plat_input.cpp"
#include "plat/plat_callbacks.cpp"

// +--------------------------------------------------------------+
// |                     Windows Entry Point                      |
// +--------------------------------------------------------------+
#if OPEN_CONSOLE_WINDOW
int main()
#else
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
#endif
{
	ClearStruct(platform);
	platform.version.major = PLATFORM_VERSION_MAJOR;
	platform.version.minor = PLATFORM_VERSION_MINOR;
	platform.version.build = PLATFORM_VERSION_BUILD;
	struct timeb initStartTime;
	ftime(&initStartTime);
	
	// +===============================+
	// | Create platform memory arenas |
	// +===============================+
	//TODO: Should we use something different than malloc for platform memory?
	{
		InitializeMemoryArenaStdHeap(&platform.stdHeap);
		
		platform.heapSpace = malloc(PLATFORM_HEAP_SIZE);
		if (platform.heapSpace == nullptr)
		{
			Win32_ExitOnError("Couldn't allocate platform heap memory with malloc");
		}
		InitializeMemoryArenaHeap(&platform.mainHeap, platform.heapSpace, PLATFORM_HEAP_SIZE);
		
		platform.tempSpace = malloc(PLATFORM_TEMP_SIZE);
		if (platform.tempSpace == nullptr)
		{
			Win32_ExitOnError("Couldn't allocate platform temp memory malloc");
		}
		InitializeMemoryArenaTemp(&platform.tempArena, platform.tempSpace, PLATFORM_TEMP_SIZE, PLATFORM_TEMP_MAX_NUM_MARKS);
		TempArena = &platform.tempArena;
		TempPushMark();
	}
	//NOTE: All debug printouts have to happen after the TempArena has been initialized
	
	PrintLine_I("Starting Win32 Platform v%u.%u(%u)...", platform.version.major, platform.version.minor, platform.version.build);
	
	//TODO: Parse the command line arguments if there are any. Store them somewhere where the application can query/parse them
	
	// +==============================+
	// |  Fill Directory Information  |
	// +==============================+
	{
		TempPushMark();
		char* tempExePath = Win32_GetExecutablePath(TempArena);
		if (tempExePath == nullptr || strlen(tempExePath) == 0)
		{
			Win32_ExitOnError("Failed to get executable directory");
		}
		u32 tempExePathLength = (u32)strlen(tempExePath);
		char* exeFileName = GetFileNamePart(tempExePath);
		u32 exeDirectoryLength = (u32)(exeFileName - tempExePath);
		u32 exeFileNameLength = tempExePathLength - exeDirectoryLength;
		platform.exeDirectory = ArenaString(&platform.mainHeap, tempExePath, exeDirectoryLength);
		platform.exeFileName = ArenaString(&platform.mainHeap, exeFileName, exeFileNameLength);
		PrintLine_I("Our exe is in folder \"%s\" named \"%s\"", platform.exeDirectory, platform.exeFileName);
		TempPopMark();
		
		platform.workingDirectory = Win32_GetWorkingDirectory(&platform.mainHeap);
		if (platform.workingDirectory == nullptr)
		{
			Win32_ExitOnError("Failed to get working directory");
		}
		PrintLine_I("Our working directory is \"%s\"", platform.workingDirectory);
		
		platform.appDllPath = ArenaPrint(&platform.mainHeap, "%s%s", platform.exeDirectory, APPLICATION_DLL_NAME);
		platform.appDllTempPath = ArenaPrint(&platform.mainHeap, "%s%s", platform.exeDirectory, APPLICATION_DLL_TEMP_NAME);
	}
	
	// +==============================+
	// |   Initialize Win32 Threads   |
	// +==============================+
	{
		_Win32_CreateMutex(&platform.threadsMutex);
		ClearArray(platform.threads);
	}
	
	// +==============================+
	// |   Loading Application Code   |
	// +==============================+
	{
		if (PathFileExists(platform.appDllPath) == false)
		{
			Win32_ExitOnError("Could not find application DLL");
		}
		
		Print_R("Loading application DLL%s...", DEBUG ? " (DEBUG)" : "");
		if (Win32_LoadDllCode(platform.appDllPath, platform.appDllTempPath, &app))
		{
			PrintLine_I("Found Application v%u.%u(%u)", app.version.major, app.version.minor, app.version.build);
		}
		else
		{
			Win32_ExitOnError("Could not load application DLL!");
		}
	}
	
	// +==============================+
	// |     Steam Initialization     |
	// +==============================+
	#if STEAM_BUILD
	{
		WriteLine_I("Initializing Steamworks...");
		bool initResult = SteamAPI_Init();
		if (initResult)
		{
			SteamworksCreateHandle();
		}
		else
		{
			WriteLine_E("Failed to initialize Steamworks");
		}
		platform.info.steamAppId = SteamUtils()->GetAppID();
		
		//TODO: Should we force restart the game if it wasn't started from steam?
	}
	#endif
	
	#if AUDIO_ENABLED
	// +==============================+
	// |      Initialize OpenAL       |
	// +==============================+
	{
		WriteLine_R("Initializing OpenAL...");
		platform.info.alDevice = alcOpenDevice(NULL);
		if (!platform.info.alDevice)
		{
			Win32_ExitOnError("Could not create OpenAL device");
		}
		platform.info.alContext = alcCreateContext(platform.info.alDevice, NULL);
		if (!alcMakeContextCurrent(platform.info.alContext))
		{
			Win32_ExitOnError("Failed to make OpenAL context current!");
		}
		ALfloat listenerOri[] = {0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f};
		alListener3f(AL_POSITION, 0, 0, 1.0f);
		alListener3f(AL_VELOCITY, 0, 0, 0);
		alListenerfv(AL_ORIENTATION, listenerOri);
		WriteLine_R("Done!");
	}
	#endif
	
	// +==============================+
	// |     GLFW Initialization      |
	// +==============================+
	{
		WriteLine_R("Initializing GLFW...");
		
		struct timeb glfwInitStartTime; ftime(&glfwInitStartTime);
		glfwSetErrorCallback(GlfwErrorCallback);
		if (!glfwInit())
		{
			Win32_ExitOnError("GLFW Initialization Failed!");
		}
		platform.glfwInitialized = true;
		struct timeb glfwInitEndTime; ftime(&glfwInitEndTime);
		u32 glfwInitElapsedTime = (u32)(1000.0 * (glfwInitEndTime.time - glfwInitStartTime.time) + (glfwInitEndTime.millitm - glfwInitStartTime.millitm));
		PrintLine_D("glfwInit took %s", FormattedMilliseconds(glfwInitElapsedTime));
		
		WriteLine_R("Done!");
		
		glfwGetVersion(&platform.info.glfwVersion.major, &platform.info.glfwVersion.minor, &platform.info.glfwVersion.revision);
		PrintLine_D("Compiled with GLFW v%d.%d(%d)", platform.info.glfwVersion.major, platform.info.glfwVersion.minor, platform.info.glfwVersion.revision);
	}
	
	// +==============================+
	// |     Monitor Info Lookup      |
	// +==============================+
	{
		WriteLine_R("Scanning for monitors...");
		
		int numMonitors = 0;
		GLFWmonitor** monitors = glfwGetMonitors(&numMonitors);
		if (numMonitors <= 0)
		{
			Win32_ExitOnError("Couldn't find information about the available monitors through GLFW");
		}
		GLFWmonitor* primaryMonitor = glfwGetPrimaryMonitor();
		if (primaryMonitor == nullptr) { primaryMonitor = monitors[0]; }
		
		platform.primaryMonitorIndex = 0;
		CreateDynamicArray(&platform.glfwMonitors, &platform.mainHeap, sizeof(MonitorInfo_t), 4, (u32)numMonitors);
		for (u32 mIndex = 0; mIndex < (u32)numMonitors; mIndex++)
		{
			GLFWmonitor* monitor = monitors[mIndex];
			MonitorInfo_t* info = DynArrayAdd(&platform.glfwMonitors, MonitorInfo_t);
			Assert(info != nullptr);
			ClearPointer(info);
			info->monitor = monitor;
			if (monitor == primaryMonitor) { platform.primaryMonitorIndex = mIndex; }
			glfwGetMonitorPos(monitor, &info->offset.x, &info->offset.y);
			glfwGetMonitorPhysicalSize(monitor, &info->physicalSize.width, &info->physicalSize.height);
			
			int numVideoModes = 0;
			const GLFWvidmode* videoModes = glfwGetVideoModes(monitor, &numVideoModes);
			Assert(videoModes != nullptr);
			Assert(numVideoModes > 0);
			
			const GLFWvidmode* currentVideoMode = glfwGetVideoMode(monitor);
			if (currentVideoMode == nullptr) { currentVideoMode = &videoModes[0]; }
			
			info->numModes = (u32)numVideoModes;
			info->modes = PushArray(&platform.mainHeap, MonitorDisplayMode_t, info->numModes);
			Assert(info->modes != nullptr);
			
			info->currentMode = 0;
			for (u32 dIndex = 0; dIndex < info->numModes; dIndex++)
			{
				MonitorDisplayMode_t* mode = &info->modes[dIndex];
				const GLFWvidmode* vidMode = &videoModes[dIndex];
				ClearPointer(mode);
				if (Win32_AreGlfwVideoModesEqual(vidMode, currentVideoMode)) { info->currentMode = dIndex; }
				mode->resolution = NewVec2i(vidMode->width, vidMode->height);
				mode->bitDepth = vidMode->redBits + vidMode->greenBits + vidMode->blueBits;
				mode->refreshRate = (r32)vidMode->refreshRate;
			}
		}
		
		PrintLine_D("%u Monitors Found", platform.glfwMonitors.length);
		for (u32 mIndex = 0; mIndex < platform.glfwMonitors.length; mIndex++)
		{
			MonitorInfo_t* info = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, mIndex);
			MonitorDisplayMode_t* currentMode = &info->modes[info->currentMode];
			bool isPrimary = (mIndex == platform.primaryMonitorIndex);
			PrintLine_D("  %s[%u]: \"%s\" pos (%d, %d) resolution (%d, %d) @%.1fHz size (%dmm, %dmm)",
				isPrimary ? "Primary" : "Monitor", mIndex, glfwGetMonitorName(info->monitor),
				info->offset.x, info->offset.y,
				currentMode->resolution.x, currentMode->resolution.y, currentMode->refreshRate,
				info->physicalSize.x, info->physicalSize.y
			);
		}
	}
	
	// +==============================+
	// |     Get Startup Options      |
	// +==============================+
	{
		WriteLine_I("Getting startup options from application...");
		
		ClearStruct(platform.startupInfo);
		
		platform.startupInfo.platformType = Platform_Windows;
		platform.startupInfo.version = platform.version;
		platform.startupInfo.glfwVersion = platform.info.glfwVersion;
		platform.startupInfo.numMonitors = platform.glfwMonitors.length;
		platform.startupInfo.primaryMonitorIndex = platform.primaryMonitorIndex;
		platform.startupInfo.monitors = (const MonitorInfo_t*)platform.glfwMonitors.items;
		
		time_t timeStruct = time(NULL);
		u64 timestamp = (u64)timeStruct; //TODO: Will this work on other platforms?
		GetRealTime(timestamp, false, &platform.startupInfo.systemTime);
		SYSTEMTIME localTime = {};
		GetLocalTime(&localTime);
		//TODO: Fill the timestamp
		platform.startupInfo.localTime.year   = localTime.wYear;
		platform.startupInfo.localTime.month  = (u8)(localTime.wMonth-1);
		platform.startupInfo.localTime.day    = (u8)(localTime.wDay-1);
		platform.startupInfo.localTime.hour   = (u8)localTime.wHour;
		platform.startupInfo.localTime.minute = (u8)localTime.wMinute;
		platform.startupInfo.localTime.second = (u8)localTime.wSecond;
		// platform.startupInfo.millisecond      = localTime.wMilliseconds;
		
		ClearStruct(platform.startupOptions);
		platform.startupOptions.windowSize             = NewVec2i(960, 540);
		platform.startupOptions.minWindowSize          = NewVec2i(-1, -1);
		platform.startupOptions.maxWindowSize          = NewVec2i(-1, -1);
		platform.startupOptions.windowPosition         = Vec2i_Zero;
		platform.startupOptions.fullscreen             = false;
		platform.startupOptions.fullscreenMonitorIndex = platform.startupInfo.primaryMonitorIndex;
		platform.startupOptions.topmostWindow          = (false && DEBUG);
		platform.startupOptions.decoratedWindow        = true;
		platform.startupOptions.resizableWindow        = true;
		platform.startupOptions.forceAspectRatio       = false;
		platform.startupOptions.aspectRatio            = NewVec2i(16, 9);
		platform.startupOptions.antialiasingNumSamples = 8;
		platform.startupOptions.temporaryMemorySize    = Megabytes(16);
		platform.startupOptions.permanantMemorySize    = Megabytes(1);
		
		platform.startupOptions.loadingColor = NewColor(Color_Black);
		BufferPrint(platform.startupOptions.iconFilePaths[0], "Resources/Sprites/icon16.png");
		BufferPrint(platform.startupOptions.iconFilePaths[1], "Resources/Sprites/icon24.png");
		BufferPrint(platform.startupOptions.iconFilePaths[2], "Resources/Sprites/icon32.png");
		BufferPrint(platform.startupOptions.iconFilePaths[3], "Resources/Sprites/icon64.png");
		BufferPrint(platform.startupOptions.loadingImagePath, "Resources/Sprites/loading.png");
		
		app.AppGetStartupOptions(&platform.startupInfo, &platform.startupOptions);
		
		Assert(platform.startupOptions.fullscreenMonitorIndex < platform.startupInfo.numMonitors);
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[0], ArrayCount(platform.startupOptions.iconFilePaths[0])));
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[1], ArrayCount(platform.startupOptions.iconFilePaths[1])));
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[2], ArrayCount(platform.startupOptions.iconFilePaths[2])));
		Assert(BufferIsNullTerminated(platform.startupOptions.iconFilePaths[3], ArrayCount(platform.startupOptions.iconFilePaths[3])));
		Assert(BufferIsNullTerminated(platform.startupOptions.loadingImagePath, ArrayCount(platform.startupOptions.loadingImagePath)));
		
		WriteLine_I("Done!");
	}
	
	// +==============================+
	// |     GLFW Window Creation     |
	// +==============================+
	{
		bool fullscreen = platform.startupOptions.fullscreen;
		MonitorInfo_t* monitorChoice = nullptr;
		v2i requestSize = NewVec2i(platform.startupOptions.windowSize.width, platform.startupOptions.windowSize.height);
		i32 requestRefreshRate = 60;
		if (fullscreen)
		{
			monitorChoice = DynArrayGet(&platform.glfwMonitors, MonitorInfo_t, platform.startupOptions.fullscreenMonitorIndex);
			Assert(monitorChoice != nullptr);
			MonitorDisplayMode_t* currentDisplayMode = &monitorChoice->modes[monitorChoice->currentMode];
			requestSize = currentDisplayMode->resolution;
			requestRefreshRate = (i32)RoundR32(currentDisplayMode->refreshRate);
		}
		
		WriteLine_R("Creating GLFW window...");
		
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OPENGL_REQUEST_VERSION_MAJOR);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OPENGL_REQUEST_VERSION_MINOR);
		glfwWindowHint(GLFW_CLIENT_API,            GLFW_OPENGL_API);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, OPENGL_FORCE_FORWARD_COMPAT ? GLFW_TRUE : GLFW_FALSE); //Makes MacOSX happy?
		glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT,  OPENGL_DEBUG_CONTEXT ? GLFW_TRUE : GLFW_FALSE);
		glfwWindowHint(GLFW_OPENGL_PROFILE,        GLFW_OPENGL_ANY_PROFILE);//GLFW_OPENGL_CORE_PROFILE
		
		glfwWindowHint(GLFW_RESIZABLE,      platform.startupOptions.resizableWindow ? GL_TRUE : GL_FALSE);
		glfwWindowHint(GLFW_FLOATING,      	platform.startupOptions.topmostWindow ? GL_TRUE : GL_FALSE);
		glfwWindowHint(GLFW_DECORATED,      platform.startupOptions.decoratedWindow ? GL_TRUE : GL_FALSE);
		glfwWindowHint(GLFW_FOCUSED,        GL_TRUE);
		glfwWindowHint(GLFW_DOUBLEBUFFER,   GL_TRUE);
		glfwWindowHint(GLFW_RED_BITS,       8);
		glfwWindowHint(GLFW_GREEN_BITS,     8);
		glfwWindowHint(GLFW_BLUE_BITS,      8);
		glfwWindowHint(GLFW_ALPHA_BITS,     8);
		glfwWindowHint(GLFW_DEPTH_BITS,     8);
		glfwWindowHint(GLFW_STENCIL_BITS,   8);
		glfwWindowHint(GLFW_SAMPLES,        (i32)platform.startupOptions.antialiasingNumSamples);
		glfwWindowHint(GLFW_REFRESH_RATE,   requestRefreshRate);
		
		platform.windowTitle = ArenaNtString(&platform.mainHeap, WINDOW_TITLE);
		
		struct timeb glfwCreateWindowStartTime; ftime(&glfwCreateWindowStartTime);
		
		platform.window = glfwCreateWindow(requestSize.width, requestSize.height, platform.windowTitle, (monitorChoice != nullptr) ? monitorChoice->monitor : nullptr, NULL);
		
		struct timeb glfwCreateWindowEndTime; ftime(&glfwCreateWindowEndTime);
		u32 glfwCreateWindowElapsedTime = (u32)(1000.0 * (glfwCreateWindowEndTime.time - glfwCreateWindowStartTime.time) + (glfwCreateWindowEndTime.millitm - glfwCreateWindowStartTime.millitm));
		PrintLine_D("glfwCreateWindow took %s", FormattedMilliseconds(glfwCreateWindowElapsedTime));
		
		if (platform.window == nullptr)
		{
			Win32_ExitOnError("GLFW window creation failed!");
		}
		glfwMakeContextCurrent(platform.window);
		
		glfwSetWindowSizeLimits(platform.window, platform.startupOptions.minWindowSize.width, platform.startupOptions.minWindowSize.height, platform.startupOptions.maxWindowSize.width, platform.startupOptions.maxWindowSize.height);
		if (platform.startupOptions.forceAspectRatio)
		{
			glfwSetWindowAspectRatio(platform.window, platform.startupOptions.aspectRatio.width, platform.startupOptions.aspectRatio.height);
		}
		glfwGetFramebufferSize(platform.window, &platform.screenSize.width, &platform.screenSize.height);
		glfwGetWindowPos(platform.window, &platform.windowPosition.x, &platform.windowPosition.y);
		
		//TODO: Fill the monitor info structures in platform.info
		const GLFWvidmode* currentVideoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		PrintLine_I("OpenGL Version %s", glGetString(GL_VERSION)); //TODO: Put these in the platform info structure?
		PrintLine_I("Rendering with \"%s\"", glGetString(GL_RENDERER)); //TODO: Put these in the platform info structure?
		PrintLine_I("Monitor Refresh Rate: %dHz", currentVideoMode->refreshRate);
		PrintLine_I("Screen Size: %dx%d", platform.screenSize.width, platform.screenSize.height);
		PrintLine_I("Window Position: %dx%d", platform.windowPosition.x, platform.windowPosition.y);
		
		glfwSwapInterval(WINDOW_SWAP_INTERVAL);
		glViewport(0, 0, platform.screenSize.width, platform.screenSize.height);
		
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |     GLEW Initialization      |
	// +==============================+
	{
		WriteLine_R("Initializing GLEW...");
		struct timeb glewInitStartTime; ftime(&glewInitStartTime);
		
		glewExperimental = GL_TRUE;
		GLenum glewInitError = glewInit();
		if (glewInitError != GLEW_OK)
		{
			Win32_ExitOnError("Could not initialize GLEW.");
		}
		
		struct timeb glewInitEndTime; ftime(&glewInitEndTime);
		u32 glewInitElapsedTime = (u32)(1000.0 * (glewInitEndTime.time - glewInitStartTime.time) + (glewInitEndTime.millitm - glewInitStartTime.millitm));
		PrintLine_D("glewInit took %s", FormattedMilliseconds(glewInitElapsedTime));
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |    Render Loading Screen     |
	// +==============================+
	{
		WriteLine_I("Preparing for loading screen render...");
		Win32_PrepareForRendering();
		WriteLine_I("Done!");
		
		// while (glfwWindowShouldClose(platform.window) == false) //Enable this to loop forever on the loading screen
		{
			glfwPollEvents();
			Win32_RenderLoadingScreen(0.01f);
			glfwSwapBuffers(platform.window);
		}
	}
	
	// +==============================+
	// |         Window Icon          |
	// +==============================+
	{
		WriteLine_R("Loading window icons...");
		
		FileInfo_t iconFiles[4];
		iconFiles[0] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[0]);
		iconFiles[1] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[1]);
		iconFiles[2] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[2]);
		iconFiles[3] = Win32_ReadEntireFile(platform.startupOptions.iconFilePaths[3]);
		
		if (iconFiles[0].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[0])); }
		if (iconFiles[1].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[1])); }
		if (iconFiles[2].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[2])); }
		if (iconFiles[3].content == nullptr) { Win32_ExitOnError(TempPrint("Couldn't load icon file from \"%s\"", platform.startupOptions.iconFilePaths[3])); }
		
		i32 numChannels[4];
		i32 widths[4], heights[4];
		u8* imageData[4];
		imageData[0] = stbi_load_from_memory((u8*)iconFiles[0].content, iconFiles[0].size, &widths[0], &heights[0], &numChannels[0], 4);
		imageData[1] = stbi_load_from_memory((u8*)iconFiles[1].content, iconFiles[1].size, &widths[1], &heights[1], &numChannels[1], 4);
		imageData[2] = stbi_load_from_memory((u8*)iconFiles[2].content, iconFiles[2].size, &widths[2], &heights[2], &numChannels[2], 4);
		imageData[3] = stbi_load_from_memory((u8*)iconFiles[3].content, iconFiles[3].size, &widths[3], &heights[3], &numChannels[3], 4);
		
		GLFWimage images[4];
		images[0].width = widths[0]; images[0].height = heights[0]; images[0].pixels = imageData[0];
		images[1].width = widths[1]; images[1].height = heights[1]; images[1].pixels = imageData[1];
		images[2].width = widths[2]; images[2].height = heights[2]; images[2].pixels = imageData[2];
		images[3].width = widths[3]; images[3].height = heights[3]; images[3].pixels = imageData[3];
		
		glfwSetWindowIcon(platform.window, ArrayCount(images), images);
		
		stbi_image_free(imageData[0]);
		stbi_image_free(imageData[1]);
		stbi_image_free(imageData[2]);
		stbi_image_free(imageData[3]);
		Win32_FreeFileMemory(&iconFiles[0]);
		Win32_FreeFileMemory(&iconFiles[1]);
		Win32_FreeFileMemory(&iconFiles[2]);
		Win32_FreeFileMemory(&iconFiles[3]);
		
		WriteLine_R("Done!");
	}
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.05f);
	glfwSwapBuffers(platform.window);
	
	// +==============================+
	// |           Cursors            |
	// +==============================+
	{
		WriteLine_R("Creating cursors...");
		platform.glfwCursors[Cursor_Default]          = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
		platform.glfwCursors[Cursor_Text]             = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
		platform.glfwCursors[Cursor_Pointer]          = glfwCreateStandardCursor(GLFW_HAND_CURSOR);
		platform.glfwCursors[Cursor_ResizeHorizontal] = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
		platform.glfwCursors[Cursor_ResizeVertical]   = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
		glfwSetCursor(platform.window, platform.glfwCursors[Cursor_Default]);
		platform.currentCursor = Cursor_Default;
		WriteLine_R("Done!");
	}
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.10f);
	glfwSwapBuffers(platform.window);
	
	// +==============================+
	// |      Register Callbacks      |
	// +==============================+
	{
		Write_R("Registering GLFW callbacks...");
		glfwSetWindowCloseCallback(platform.window,     GlfwWindowCloseCallback);
		glfwSetFramebufferSizeCallback(platform.window, GlfwWindowSizeCallback);
		glfwSetWindowPosCallback(platform.window,       GlfwWindowMoveCallback);
		glfwSetWindowIconifyCallback(platform.window,   GlfwWindowMinimizeCallback);
		glfwSetWindowFocusCallback(platform.window,     GlfwWindowFocusCallback);
		glfwSetKeyCallback(platform.window,             GlfwKeyPressedCallback);
		glfwSetCharCallback(platform.window,            GlfwCharPressedCallback);
		glfwSetCursorPosCallback(platform.window,       GlfwCursorPosCallback);
		glfwSetMouseButtonCallback(platform.window,     GlfwMousePressCallback);
		glfwSetScrollCallback(platform.window,          GlfwMouseScrollCallback);
		glfwSetCursorEnterCallback(platform.window,     GlfwCursorEnteredCallback);
		// glfwSetJoystickCallback(               GlfwJoystickCallback);
		WriteLine_R("Done!");
	}
	
	// +==============================+
	// |      Fill Platform Info      |
	// +==============================+
	{
		platform.info.platformType   = Platform_Windows;
		platform.info.version        = platform.version;
		platform.info.window         = platform.window;
		
		platform.info.numMonitors         = platform.glfwMonitors.length;
		platform.info.primaryMonitorIndex = platform.primaryMonitorIndex;
		platform.info.monitors            = (const MonitorInfo_t*)platform.glfwMonitors.items;
		
		platform.info.DebugOutput            = Win32_DebugOutput;
		platform.info.DebugPrint             = Win32_DebugPrint;
		platform.info.DoesFolderExist        = Win32_DoesFolderExist;
		platform.info.CreateFolder           = Win32_CreateFolder;
		platform.info.DoesFileExist          = Win32_DoesFileExist;
		platform.info.FreeFileMemory         = Win32_FreeFileMemory;
		platform.info.ReadEntireFile         = Win32_ReadEntireFile;
		platform.info.WriteEntireFile        = Win32_WriteEntireFile;
		platform.info.DeleteFile             = Win32_DeleteFile;
		platform.info.OpenFile               = Win32_OpenFile;
		platform.info.AppendFile             = Win32_AppendFile;
		platform.info.CloseFile              = Win32_CloseFile;
		platform.info.LaunchFile             = Win32_LaunchFile;
		platform.info.GetNumFilesInDirectory = Win32_GetNumFilesInDirectory;
		platform.info.GetFileInDirectory     = Win32_GetFileInDirectory;
		platform.info.ShowSourceFile         = Win32_ShowSourceFile;
		platform.info.CopyToClipboard        = Win32_CopyToClipboard;
		platform.info.CopyFromClipboard      = Win32_CopyFromClipboard;
		platform.info.WatchFile              = Win32_WatchFile;
		platform.info.UnwatchFile            = Win32_UnwatchFile;
		platform.info.IsWatchingFile         = Win32_IsWatchingFile;
		platform.info.AllocateMemory         = Win32_AllocateMemory;
		platform.info.FreeMemory             = Win32_FreeMemory;
		platform.info.UpdateLoadingBar       = Win32_UpdateLoadingBar;
	}
	
	// +==============================+
	// | Allocate Application Memory  |
	// +==============================+
	{
		WriteLine_R("Allocating application memory...");
		platform.appMemory.permanantSize = platform.startupOptions.permanantMemorySize;
		platform.appMemory.transientSize = platform.startupOptions.temporaryMemorySize;
		
		PrintLine_D("Allocating %s for permanant application memory...", FormattedSizeStr(platform.appMemory.permanantSize));
		platform.appMemory.permanantPntr = (void*)VirtualAlloc(0, platform.appMemory.permanantSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
		
		PrintLine_D("Allocating %s for temporary application memory...", FormattedSizeStr(platform.appMemory.transientSize));
		platform.appMemory.transientPntr = (void*)VirtualAlloc(0, platform.appMemory.transientSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
		
		if (platform.appMemory.permanantPntr == nullptr || platform.appMemory.transientPntr == nullptr)
		{
			Win32_ExitOnError("Could not allocate enough memory for application!");
		}
		WriteLine_R("Done!");
		
		CreateDynamicArray(&platform.appAllocations, &platform.mainHeap, sizeof(Win32Allocation_t));
	}
	
	#if DEBUG
	// +==============================+
	// |      Start DLL Watching      |
	// +==============================+
	{
		Write_R("Starting application DLL watching...");
		_Win32_CreateMutex(&platform.dllWatchInfo.mutex);
		platform.dllWatchInfo.dllFilePath = platform.appDllPath;
		platform.dllWatchInfo.lastWriteTime = app.lastWriteTime;
		platform.dllWatchThread = _Win32_CreateThread(DllWatchingFunction, &platform.dllWatchInfo);
		if (platform.dllWatchThread == nullptr)
		{
			Win32_ExitOnError("Could not start DLL watching thread!");
		}
		WriteLine_R("Done!");
	}
	#endif
	
	// +==============================+
	// |      Init File Watching      |
	// +==============================+
	_Win32_InitFileWatching();
	
	// +==============================+
	// |   Fill Initial AppInput_t    |
	// +==============================+
	{
		platform.currentInput = &platform.appInputs[0];
		ClearPointer(platform.currentInput);
		
		//TODO: Should we figure out which monitor the window was created on?
		const GLFWvidmode* currentVideoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		platform.currentInput->targetFramerate = (r64)currentVideoMode->refreshRate;
		
		glfwGetFramebufferSize(platform.window, &platform.currentInput->screenSize.width, &platform.currentInput->screenSize.height);
		glfwGetWindowPos(platform.window, &platform.currentInput->windowPosition.x, &platform.currentInput->windowPosition.y);
		platform.currentInput->windowHasFocus = (glfwGetWindowAttrib(platform.window, GLFW_FOCUSED) == GLFW_TRUE);
		platform.currentInput->windowIsMinimized = (glfwGetWindowAttrib(platform.window, GLFW_ICONIFIED) == GLFW_TRUE);
		
		r64 mousePosX, mousePosY;
		glfwGetCursorPos(platform.window, &mousePosX, &mousePosY);
		platform.currentInput->mousePos = NewVec2((r32)mousePosX, (r32)mousePosY);
		platform.currentInput->mouseInsideWindow = (mousePosX >= 0 && mousePosY >= 0 && mousePosX < platform.currentInput->screenSize.width && mousePosY < platform.currentInput->screenSize.height);
		
		for (i32 gIndex = 0; gIndex < MAX_NUM_GAMEPADS; gIndex++)
		{
			GamepadState_t* gamepad = &platform.currentInput->gamepads[gIndex];
			gamepad->isConnected = (glfwJoystickPresent(gIndex) == GLFW_TRUE);
			if (gamepad->isConnected) { PrintLine_D("Gamepad %u connected at startup", gIndex); }
		}
		
		Win32_UpdateAppInputTimeInfo(platform.currentInput);
	}
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.15f);
	glfwSwapBuffers(platform.window);
	
	// +==============================+
	// |      Call AppInitialize      |
	// +==============================+
	PrintLine_I("+==============================+");
	PrintLine_I("|    Calling AppInitialize     |");
	PrintLine_I("+==============================+");
	#if 1
	struct timeb appInitStartTime; ftime(&appInitStartTime);
	app.AppInitialize(&platform.info, &platform.appMemory, platform.currentInput);
	struct timeb appInitEndTime; ftime(&appInitEndTime);
	u32 appInitElapsedTime = (u32)(1000.0 * (appInitEndTime.time - appInitStartTime.time) + (appInitEndTime.millitm - appInitStartTime.millitm));
	PrintLine_D("AppInitialize took %s", FormattedMilliseconds(appInitElapsedTime));
	#else
	_Win32_CreateMutex(&platform.openglContextMutex);
	platform.appLoadingFinished = false;
	//TODO: Time this section
	Win32Thread_t* loadingThread = _Win32_CreateThread(Win32_CallAppInitialize, nullptr);
	while (!platform.appLoadingFinished && !glfwWindowShouldClose(platform.window))
	{
		glfwPollEvents();
		r32 loadingPercent = platform.appLoadingPercent;
		Win32_RenderLoadingScreen(0.15f + (0.85f * loadingPercent));
		glfwSwapBuffers(platform.window);
	}
	if (glfwWindowShouldClose(platform.window) && _Win32_IsThreadAlive(loadingThread)) { _Win32_DestroyThread(loadingThread); }
	glfwMakeContextCurrent(platform.window);
	#endif
	WriteLine_R("AppInitialize finished!");
	
	glfwPollEvents();
	Win32_RenderLoadingScreen(0.95f);
	glfwSwapBuffers(platform.window);
	
	struct timeb initEndTime;
	ftime(&initEndTime);
	u32 initElapsedTime = (u32)(1000.0 * (initEndTime.time - initStartTime.time) + (initEndTime.millitm - initStartTime.millitm));
	PrintLine_D("Startup took %s", FormattedMilliseconds(initElapsedTime));
	PrintLine_I("+==============================+");
	PrintLine_I("|   Entering Main Game Loop    |");
	PrintLine_I("+==============================+");
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0);
	
	// +--------------------------------------------------------------+
	// |                       Main Update Loop                       |
	// +--------------------------------------------------------------+
	while (glfwWindowShouldClose(platform.window) == false)
	{
		TempPushMark();
		
		#if DEBUG
		bool appReloaded = Win32_CheckAppDllForChanges();
		#endif
		
		//TODO: Check on monitor changes
		platform.info.numMonitors         = platform.glfwMonitors.length;
		platform.info.primaryMonitorIndex = platform.primaryMonitorIndex;
		platform.info.monitors            = (const MonitorInfo_t*)platform.glfwMonitors.items;
		
		// +==============================+
		// |     Call glfwPollEvents      |
		// +==============================+
		glfwPollEvents();
		#if STEAM_BUILD
		SteamAPI_RunCallbacks();
		#endif
		_Win32_UpdateFileWatching();
		
		// +==============================+
		// |        Swap AppInputs        |
		// +==============================+
		//Swap the pointers for current and last input
		AppInput_t* currentInput = platform.currentInput; //this one will be passed to the game on this update loop
		AppInput_t* newInput = &platform.appInputs[0];
		if (newInput == currentInput) { newInput++; }
		
		Plat_UpdateAppInputGamepadInfo(currentInput);
		Win32_UpdateAppInputTimeInfo(currentInput);
		glfwGetFramebufferSize(platform.window, &currentInput->screenSize.width, &currentInput->screenSize.height);
		
		//update the pointer in the platform so that any windows callbacks during the update will start filling the next appInput
		Plat_SwapAppInputs(currentInput, newInput);
		platform.currentInput = newInput;
		
		
		// +==============================+
		// |        Call AppUpdate        |
		// +==============================+
		ClearStruct(platform.appOutput);
		glViewport(0, 0, currentInput->screenSize.width, currentInput->screenSize.height);
		r64 appUpdateStart = glfwGetTime() * 1000.0;
		app.AppUpdate(&platform.info, &platform.appMemory, currentInput, &platform.appOutput);
		r64 appUpdateEnd = glfwGetTime() * 1000.0;
		newInput->lastUpdateElapsedMs = appUpdateEnd - appUpdateStart;
		glfwSwapBuffers(platform.window);
		
		// +==============================+
		// |       Handle AppOutput       |
		// +==============================+
		{
			if (platform.appOutput.cursorType != platform.currentCursor)
			{
				glfwSetCursor(platform.window, platform.glfwCursors[platform.appOutput.cursorType]);
				platform.currentCursor = platform.appOutput.cursorType;
			}
			if (platform.appOutput.closeWindow)
			{
				glfwSetWindowShouldClose(platform.window, true);
			}
			
			if (platform.appOutput.windowTitle[0] != '\0')
			{
				Assert(BufferIsNullTerminated(&platform.appOutput.windowTitle[0], ArrayCount(platform.appOutput.windowTitle)));
				Win32_UpdateWindowTitle(&platform.appOutput.windowTitle[0]);
			}
		}
		
		TempPopMark();
		Assert(ArenaNumMarks(TempArena) == 0);
	}
	WriteLine_W("We have exited the main game loop");
	
	WriteLine_R("Calling AppClosing...");
	app.AppClosing(&platform.info, &platform.appMemory);
	WriteLine_R("AppClosing done!");
	
	#if STEAM_BUILD
	WriteLine_I("Shutting down Steamworks...");
	SteamworksDestroyHandle();
	SteamAPI_Shutdown();
	#endif
	
	WriteLine_I("Terminating GLFW...");
	glfwTerminate();
	
	WriteLine_I("Terminating threads...");
	_Win32_TerminateAllThreads();
	
	WriteLine_E("Program finished. Goodbye :D");
	free(platform.heapSpace);
	free(platform.tempSpace);
	//TODO: Should we free any other allocated memory? Not sure if it matters.
	return 0;
}

#if (ASSERTIONS_ACTIVE && USE_ASSERT_FAILURE_FUNCTION)
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	u32 fileNameStart = 0;
	for (u32 cIndex = 0; filename[cIndex] != '\0'; cIndex++)
	{
		if (filename[cIndex] == '\\' || filename[cIndex] == '/')
		{
			fileNameStart = cIndex+1;
		}
	}
	
	#if CRASH_DUMP_ASSERTS
	if (numCrashDumps < MAX_CRASH_DUMPS)
	{
		AppInput_t dummyInput = {};
		Win32_UpdateAppInputTimeInfo(&dummyInput);
		
		u32 crashDumpSize = 0;
		char* crashDump = Win32_CrashDump(&crashDumpSize, function, &filename[fileNameStart], lineNumber, expressionStr, &dummyInput.systemTime, dummyInput.programTime);
		
		if (crashDump != nullptr)
		{
			char* crashFilename = MallocPrint("crash_%u-%u-%u_%u-%u-%u.txt", dummyInput.localTime.year, dummyInput.localTime.month+1, dummyInput.localTime.day+1, dummyInput.localTime.hour, dummyInput.localTime.minute, dummyInput.localTime.second);
			if (crashFilename == nullptr) { crashFilename = (char*)"crash.txt"; }
			Win32_WriteEntireFile(crashFilename, crashDump, crashDumpSize);
			numCrashDumps++;
			PrintLine_E("Saved crash dump to \"%s\"", crashFilename);
			free(crashDump);
			free(crashFilename);
		}
		else
		{
			WriteLine_E("No crash dump generated");
		}
	}
	#else
	PrintLine_E("Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, &filename[fileNameStart], lineNumber, expressionStr);
	#endif
}
#endif
