/*
File:   win32_threads.h
Author: Taylor Robbins
Date:   07\12\2019
*/

#ifndef _WIN_32_THREADS_H
#define _WIN_32_THREADS_H

#define WIN32_THREAD_FUNCTION(functionName, userPntr) DWORD WINAPI functionName(LPVOID userPntr)
typedef WIN32_THREAD_FUNCTION(Win32ThreadFunction_t, userPntr);

struct Win32Thread_t
{
	HANDLE handle;
	DWORD id;
	Win32ThreadFunction_t* function;
	void* userPntr;
};

struct Win32Mutex_t
{
	HANDLE handle;
};

#endif //  _WIN_32_THREADS_H
