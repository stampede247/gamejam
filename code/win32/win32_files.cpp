/*
File:   win32_files.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** Contains a bunch of functions that help the platform layer and the
	** application interact with the operating system's file system.
*/

// +==============================+
// |    Win32_DoesFolderExist     |
// +==============================+
// bool DoesFolderExist(const char* folderPath);
DoesFolderExist_DEFINITION(Win32_DoesFolderExist)
{
	Assert(folderPath != nullptr);
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, (u32)strlen(folderPath));
	DWORD fileType = GetFileAttributesA(absoluteFolderPath);
	TempPopMark();
	if (fileType == INVALID_FILE_ATTRIBUTES)
	{
		return false; //doesn't exist
	}
	else if (fileType & FILE_ATTRIBUTE_DIRECTORY)
	{
		return true; //does exist
	}
	else
	{
		return false; //it's a file path
	}
}

// +==============================+
// |      Win32_CreateFolder      |
// +==============================+
// bool CreateFolder(const char* folderPath);
CreateFolder_DEFINITION(Win32_CreateFolder)
{
	Assert(folderPath != nullptr);
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, (u32)strlen(folderPath));
	if (CreateDirectoryA(absoluteFolderPath, NULL))
	{
		TempPopMark();
		return true;
	}
	else
	{
		PrintLine_E("Failed to create directory at \"%s\"", absoluteFolderPath);
		TempPopMark();
		return false;
	}
}

// +==============================+
// |     Win32_DoesFileExist      |
// +==============================+
// bool DoesFileExist(const char* filePath);
DoesFileExist_DEFINITION(Win32_DoesFileExist)
{
	Assert(filePath != nullptr);
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	DWORD fileType = GetFileAttributesA(absoluteFilePath);
	TempPopMark();
	if (fileType == INVALID_FILE_ATTRIBUTES)
	{
		return false;
	}
	else
	{
		return true;
	}
}

// +==============================+
// |     Win32_FreeFileMemory     |
// +==============================+
// void FreeFileMemory(FileInfo_t* fileInfo)
FreeFileMemory_DEFINITION(Win32_FreeFileMemory)
{
	Assert(fileInfo != nullptr);
	if (fileInfo->content != nullptr)
	{
		VirtualFree(fileInfo->content, 0, MEM_RELEASE);
		fileInfo->content = nullptr;
	}
}

// +==============================+
// |     Win32_ReadEntireFile     |
// +==============================+
// FileInfo_t ReadEntireFile(const char* filePath)
ReadEntireFile_DEFINITION(Win32_ReadEntireFile)
{
	Assert(filePath != nullptr);
	FileInfo_t result = {};
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	HANDLE fileHandle = CreateFileA(absoluteFilePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		LARGE_INTEGER fileSize;
		if (GetFileSizeEx(fileHandle, &fileSize))
		{
			//TODO: Define and use SafeTruncateUInt64 
			u32 fileSize32 = (u32)(fileSize.QuadPart);
			result.content = VirtualAlloc(0, fileSize32+1, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
			if (result.content)
			{
				DWORD bytesRead;
				if (ReadFile(fileHandle, result.content, fileSize32, &bytesRead, 0) &&
				   (fileSize32 == bytesRead))
				{
					// NOTE: File read successfully
					result.size = fileSize32;
					((u8*)result.content)[fileSize32] = '\0';
				}
				else
				{
					Win32_FreeFileMemory(&result);
					result.content = 0;
				}
			}
			else
			{
				PrintLine_E("Failed allocate space for %u byte file at \"%s\"", fileSize32, absoluteFilePath);
			}
		}
		else
		{
			PrintLine_E("Failed to get file size of file at \"%s\"", absoluteFilePath);
		}
		CloseHandle(fileHandle);
	}
	else
	{
		PrintLine_E("Failed to access file for reading at \"%s\"", absoluteFilePath);
	}
	TempPopMark();
	
	return result;
}

// +==============================+
// |    Win32_WriteEntireFile     |
// +==============================+
// bool WriteEntireFile(const char* filePath, void* memory, u32 memorySize)
WriteEntireFile_DEFINITION(Win32_WriteEntireFile)
{
	Assert(filePath != nullptr);
	Assert(memory != nullptr);
	Assert(memorySize > 0);
	bool result = false;
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	HANDLE fileHandle = CreateFileA(
		absoluteFilePath,      //Name of the file
		GENERIC_WRITE,         //Open for writing
		0,                     //Do not share
		NULL,                  //Default security
		CREATE_ALWAYS,         //Always overwrite
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		0                      //No Template File
	);
	if (fileHandle != INVALID_HANDLE_VALUE)
	{
		DWORD bytesWritten;
		if (WriteFile(fileHandle, memory, memorySize, &bytesWritten, 0))
		{
			if (bytesWritten == memorySize)
			{
				result = true;
			}
			else
			{
				PrintLine_W("Only wrote %u/%u bytes to file at \"%s\"", bytesWritten, memorySize, absoluteFilePath);
			}
		}
		else
		{
			PrintLine_E("Failed to write %u bytes to file at \"%s\"", memorySize, absoluteFilePath);
		}
		CloseHandle(fileHandle);
	}
	else
	{
		PrintLine_E("Failed to open file for writing at \"%s\"", absoluteFilePath);
	}
	TempPopMark();
	
	return result;
}

// +==============================+
// |       Win32_DeleteFile       |
// +==============================+
// bool DeleteFile(const char* filePath);
DeleteFile_DEFINITION(Win32_DeleteFile)
{
	Assert(filePath != nullptr);
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	if (DeleteFile(absoluteFilePath))
	{
		TempPopMark();
		return true;
	}
	else
	{
		PrintLine_E("Failed to delete \"%s\"", absoluteFilePath);
		TempPopMark();
		return false;
	}
}

// +==============================+
// |        Win32_OpenFile        |
// +==============================+
// bool OpenFile(const char* filePath, OpenFile_t* openFileOut)
OpenFile_DEFINITION(Win32_OpenFile)
{
	Assert(openFileOut != nullptr);
	Assert(filePath != nullptr);
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	HANDLE fileHandle = CreateFileA(
		absoluteFilePath,      //Name of the file
		GENERIC_WRITE,         //Open for reading and writing
		FILE_SHARE_READ,       //Do not share
		NULL,                  //Default security
		OPEN_ALWAYS,           //Open existing or create new
		FILE_ATTRIBUTE_NORMAL, //Default file attributes
		NULL                   //No Template File
	);
	
	if (fileHandle == INVALID_HANDLE_VALUE)
	{
		PrintLine_E("Couldn't open/create file: \"%s\"", absoluteFilePath);
		TempPopMark();
		return false;
	}
	
	SetFilePointer(fileHandle, 0, NULL, FILE_END);
	
	openFileOut->isOpen = true;
	openFileOut->handle = fileHandle;
	TempPopMark();
	return true;
}

// +==============================+
// |       Win32_AppendFile       |
// +==============================+
// bool AppendFile(OpenFile_t* filePntr, const void* newData, u32 newDataSize)
AppendFile_DEFINITION(Win32_AppendFile)
{
	Assert(filePntr != nullptr);
	Assert(filePntr->isOpen);
	Assert(filePntr->handle != INVALID_HANDLE_VALUE);
	
	if (newDataSize == 0) return true;
	Assert(newData != nullptr);
	
	DWORD bytesWritten = 0;
	if (WriteFile(filePntr->handle, newData, newDataSize, &bytesWritten, 0))
	{
		// NOTE: File read successfully
		if (bytesWritten == newDataSize)
		{
			return true;
		}
		else
		{
			PrintLine_E("Not all bytes appended to file. %u/%u written", bytesWritten, newDataSize);
			return false;
		}
	}
	else
	{
		PrintLine_E("WriteFile failed. %u/%u written", bytesWritten, newDataSize);
		return false;
	}
}

// +==============================+
// |       Win32_CloseFile        |
// +==============================+
// void CloseFile(OpenFile_t* filePntr)
CloseFile_DEFINITION(Win32_CloseFile)
{
	if (filePntr == nullptr) return;
	if (filePntr->handle == INVALID_HANDLE_VALUE) return;
	if (filePntr->isOpen == false) return;
	
	CloseHandle(filePntr->handle);
	filePntr->handle = INVALID_HANDLE_VALUE;
	filePntr->isOpen = false;
}

// +==============================+
// |       Win32_LaunchFile       |
// +==============================+
// bool LaunchFile(const char* filePath)
LaunchFile_DEFINITION(Win32_LaunchFile)
{
	Assert(filePath != nullptr);
	bool result = false;
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	
	u64 executeResult = (u64)ShellExecute(
		NULL,   //No parent window
		"open", //The action verb
		absoluteFilePath, //The target file
		NULL, //No parameters
		NULL, //Use default working directory
		SW_SHOWNORMAL //Show command is normal
	);
	
	if (executeResult > 32)
	{
		result = true;
	}
	else
	{
		switch (executeResult)
		{
			case ERROR_FILE_NOT_FOUND: { PrintLine_E("ShellExecute returned ERROR_FILE_NOT_FOUND for file at \"%s\"", absoluteFilePath); } break;
			default: { PrintLine_E("ShellExecute failed with result 0x%02X for file at \"%s\"", executeResult, absoluteFilePath); } break;
		};
	}
	
	TempPopMark();
	return result;
}

// +==============================+
// | Win32_GetNumFilesInDirectory |
// +==============================+
// u32 GetNumFilesInDirectory(const char* folderPath)
GetNumFilesInDirectory_DEFINITION(Win32_GetNumFilesInDirectory)
{
	Assert(folderPath != nullptr);
	
	u32 result = 0;
	
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, (u32)strlen(folderPath));
	u32 absoluteFolderPathLength = (u32)strlen(absoluteFolderPath);
	Assert(absoluteFolderPath[absoluteFolderPathLength-1] == '\\');
	
	// PrintLine_D("Counting files in \"%s\"", absoluteFolderPath);
	
	WIN32_FIND_DATA findData = {};
	HANDLE findHandle = FindFirstFile(absoluteFolderPath, &findData);
	
	while (true)
	{
		if (findHandle == INVALID_HANDLE_VALUE) { break; }
		
		if (strcmp(findData.cFileName, ".") != 0 && strcmp(findData.cFileName, "..") != 0)
		{
			// PrintLine_D("Found file \"%s\"", findData.cFileName);
			result++;
		}
		
		if (FindNextFile(findHandle, &findData) == 0)
		{
			break;
		}
	}
	
	TempPopMark();
	return result;
}

// +==============================+
// |   Win32_GetFileInDirectory   |
// +==============================+
// char* GetFileInDirectory(MemoryArena_t* arenaPntr, const char* folderPath, u32 index)
GetFileInDirectory_DEFINITION(Win32_GetFileInDirectory)
{
	Assert(arenaPntr != nullptr);
	Assert(folderPath != nullptr);
	
	TempPushMark();
	char* absoluteFolderPath = Win32_CompleteRelativePath(TempArena, folderPath, (u32)strlen(folderPath));
	u32 absoluteFolderPathLength = (u32)strlen(absoluteFolderPath);
	Assert(absoluteFolderPath[absoluteFolderPathLength-1] == '\\');
	
	// PrintLine_D("Finding file %u in \"%s\"", index, absoluteFolderPath);
	
	WIN32_FIND_DATA findData = {};
	HANDLE findHandle = FindFirstFile(absoluteFolderPath, &findData);
	
	char* result = nullptr;
	u32 fIndex = 0;
	while (true)
	{
		if (findHandle == INVALID_HANDLE_VALUE) { break; }
		
		if (strcmp(findData.cFileName, ".") != 0 && strcmp(findData.cFileName, "..") != 0)
		{
			// PrintLine_D("Found file \"%s\"", findData.cFileName);
			
			if (fIndex == index)
			{
				result = ArenaNtString(arenaPntr, findData.cFileName);
				break;
			}
			
			fIndex++;
		}
		
		if (FindNextFile(findHandle, &findData) == 0)
		{
			break;
		}
	}
	
	TempPopMark();
	return result;
}

// +==============================+
// |     Win32_ShowSourceFile     |
// +==============================+
// bool ShowSourceFile(const char* filePath, u32 lineNumber)
ShowSourceFile_DEFINITION(Win32_ShowSourceFile)
{
	Assert(filePath != nullptr);
	bool result = false;
	if ((u32)strlen(filePath) == 0) { return false; }
	
	TempPushMark();
	char* absoluteFilePath = Win32_CompleteRelativePath(TempArena, filePath, (u32)strlen(filePath));
	char* commandStr = TempPrint("%s:%u", absoluteFilePath, lineNumber);
	
	u64 executeResult = (u64)ShellExecute(
		NULL,   //No parent window
		"open", //The action verb
		"\"C:\\Program Files\\Sublime Text 3\\subl.exe\"", //The target file
		commandStr, //No parameters
		NULL, //Use default working directory
		SW_SHOWNORMAL //Show command is normal
	);
	
	if (executeResult > 32)
	{
		result = true;
	}
	else
	{
		switch (executeResult)
		{
			case ERROR_FILE_NOT_FOUND:   { WriteLine_E("ShellExecute returned ERROR_FILE_NOT_FOUND"); } break;
			case ERROR_PATH_NOT_FOUND:   { WriteLine_E("ShellExecute returned ERROR_PATH_NOT_FOUND"); } break;
			case ERROR_BAD_FORMAT:       { WriteLine_E("ShellExecute returned ERROR_BAD_FORMAT"); } break;
			case SE_ERR_ACCESSDENIED:    { WriteLine_E("ShellExecute returned SE_ERR_ACCESSDENIED"); } break;
			case SE_ERR_ASSOCINCOMPLETE: { WriteLine_E("ShellExecute returned SE_ERR_ASSOCINCOMPLETE"); } break;
			case SE_ERR_DDEBUSY:         { WriteLine_E("ShellExecute returned SE_ERR_DDEBUSY"); } break;
			case SE_ERR_DDEFAIL:         { WriteLine_E("ShellExecute returned SE_ERR_DDEFAIL"); } break;
			case SE_ERR_DDETIMEOUT:      { WriteLine_E("ShellExecute returned SE_ERR_DDETIMEOUT"); } break;
			case SE_ERR_DLLNOTFOUND:     { WriteLine_E("ShellExecute returned SE_ERR_DLLNOTFOUND"); } break;
			// case SE_ERR_FNF:             { WriteLine_E("ShellExecute returned SE_ERR_FNF"); } break;
			case SE_ERR_NOASSOC:         { WriteLine_E("ShellExecute returned SE_ERR_NOASSOC"); } break;
			case SE_ERR_OOM:             { WriteLine_E("ShellExecute returned SE_ERR_OOM"); } break;
			// case SE_ERR_PNF:             { WriteLine_E("ShellExecute returned SE_ERR_PNF"); } break;
			case SE_ERR_SHARE:           { WriteLine_E("ShellExecute returned SE_ERR_SHARE"); } break;
			
			default:
			{
				PrintLine_E("ShellExecute failed with result 0x%02X for file at \"%s\"", executeResult, absoluteFilePath);
			} break;
		};
	}
	
	TempPopMark();
	return result;
}
