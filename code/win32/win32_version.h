/*
File:   win32_version.h
Author: Taylor Robbins
Date:   07\12\2019
Desription:
	** Defines the macros that contain the values for the current platform layer version number
	** for the win32 version of the platform layer.
	** The build number is incremented automatically by a python script that runs before each build of the platform layer
*/

#ifndef _WIN_32_VERSION_H
#define _WIN_32_VERSION_H

#define PLATFORM_VERSION_MAJOR 1
#define PLATFORM_VERSION_MINOR 0

//NOTE: Auto-incremented by a python script before each build
#define PLATFORM_VERSION_BUILD 129

#endif //  _WIN_32_VERSION_H
