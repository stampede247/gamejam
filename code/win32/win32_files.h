/*
File:   win32_files.h
Author: Taylor Robbins
Date:   07\12\2019
*/

#ifndef _WIN_32_FILES_H
#define _WIN_32_FILES_H

struct FileInfo_t
{
	u32 size;
	void* content;
};

struct OpenFile_t
{
	bool isOpen;
	HANDLE handle;
};

#endif //  _WIN_32_FILES_H
