/*
File:   win32_memory.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** Holds some function that help the platform layer allocate, free, and generally deal with memory.
	** Contains the Malloc and Free functions that are provided to the application
*/

void Win32_DumpApplicationAllocations()
{
	for (u32 aIndex = 0; aIndex < platform.appAllocations.length; aIndex++)
	{
		Win32Allocation_t* allocation = DynArrayGet(&platform.appAllocations, Win32Allocation_t, aIndex);
		Assert(allocation->base != nullptr);
		free(allocation->base);
	}
	if (platform.appAllocations.length > 0)
	{
		DynArrayRemoveRegion(&platform.appAllocations, 0, platform.appAllocations.length);
	}
}

// +==============================+
// |     Win32_AllocateMemory     |
// +==============================+
// void* AllocateMemory(u32 numBytes)
AllocateMemory_DEFINITION(Win32_AllocateMemory)
{
	if (numBytes == 0) { return nullptr; }
	
	Win32Allocation_t* newAllocation = DynArrayAdd(&platform.appAllocations, Win32Allocation_t);
	if (newAllocation == nullptr) { return nullptr; }
	ClearPointer(newAllocation);
	
	newAllocation->size = numBytes;
	newAllocation->base = malloc(numBytes);
	if (newAllocation->base == nullptr)
	{
		DynArrayRemove(&platform.appAllocations, platform.appAllocations.length-1);
		return nullptr;
	}
	
	return newAllocation->base;
}

// +==============================+
// |       Win32_FreeMemory       |
// +==============================+
// void FreeMemory(void* memPntr)
FreeMemory_DEFINITION(Win32_FreeMemory)
{
	Assert(memPntr != nullptr);
	
	Win32Allocation_t* matchPntr = nullptr;
	u32 matchIndex = 0;
	for (u32 aIndex = 0; aIndex < platform.appAllocations.length; aIndex++)
	{
		Win32Allocation_t* allocation = DynArrayGet(&platform.appAllocations, Win32Allocation_t, aIndex);
		if (allocation->base == memPntr)
		{
			matchPntr = allocation;
			matchIndex = aIndex;
			break;
		}
	}
	
	Assert(matchPntr != nullptr);
	
	free(matchPntr->base);
	DynArrayRemove(&platform.appAllocations, matchIndex);
}
