/*
File:   win32_debug.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** Holds functions that help the platform layer (and the application) output debug text to
	** the console, stdout, and anywhere else we would like to send the data
	** This file also holds the Win32_CrashDump function used by the AssertFailure function in win32_main.cpp
*/

char* Win32_CrashDump(u32* resultSizeOut, const char* crashFunction, const char* crashFilename, int crashLineNumber, const char* crashExpressionStr, const RealTime_t* realTime, u64 programTime)
{
	char* result = nullptr;
	u32 resultSize = 0;
	if (resultSizeOut != nullptr) { *resultSizeOut = 0; }
	
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numChars = 0;
		
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "| Plat Crash Dump v%u.%u(%5u)  |\n", PLATFORM_VERSION_MAJOR, PLATFORM_VERSION_MINOR, PLATFORM_VERSION_BUILD);
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "Time: %s %s %s %u at %u:%u:%u%s\n", GetDayOfWeekStr((DayOfWeek_t)realTime->dayOfWeek), GetMonthStr((Month_t)realTime->month), GetDayOfMonthString(realTime->day), realTime->year, Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		TwoPassPrint(&numChars, result, resultSize, "Failed Assertion in %s:%u function %s: (%s) is false\n\n", crashFilename, crashLineNumber, crashFunction, crashExpressionStr);
		
		//TODO: Can we retrieve any of the debug output
		
		if (pass == 0)
		{
			if (numChars == 0) { return nullptr; }
			resultSize = numChars;
			result = (char*)malloc(numChars+1);
			if (result == nullptr) { return nullptr; }
		}
		else
		{
			if (numChars != resultSize) { free(result); return nullptr; }
			result[resultSize] = '\0';
		}
	}
	if (resultSizeOut != nullptr) { *resultSizeOut = resultSize; }
	return result;
}

#if PLAT_DEBUG_OUTPUT_ENABLED

// +==============================+
// |      Win32_DebugOutput       |
// +==============================+
// void DebugOutput(const char* functionStr, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* message)
DebugOutput_DEFINITION(Win32_DebugOutput)
{
	//TODO: Loop over the contents and do prefixes after line-breaks
	OutputDebugStringA(message);
	if (newLine) { OutputDebugStringA("\n"); }
	printf(message);
	if (newLine) { printf("\n"); }
}

// +==============================+
// |       Win32_DebugPrint       |
// +==============================+
// void DebugPrint(const char* functionStr, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...)
DebugPrint_DEFINITION(Win32_DebugPrint)
{
	TempPushMark();
	TempPrintVa(printBuffer, length, formatString);
	if (length > 0 && printBuffer != nullptr)
	{
		Win32_DebugOutput(functionStr, fileName, lineNumber, dbgLevel, newLine, printBuffer);
	}
	else
	{
		Win32_DebugOutput(functionStr, fileName, lineNumber, dbgLevel, newLine, "[DEBUG_OUTPUT_OVERFLOW]");
	}
	TempPopMark();
}

#define WriteAt(dbgLevel, message)               Win32_DebugOutput(__func__, __FILE__, __LINE__, dbgLevel, false, message)
#define WriteLineAt(dbgLevel, message)           Win32_DebugOutput(__func__, __FILE__, __LINE__, dbgLevel, true,  message)
#define PrintAt(dbgLevel, formatString, ...)     Win32_DebugPrint (__func__, __FILE__, __LINE__, dbgLevel, false, formatString, ##__VA_ARGS__)
#define PrintLineAt(dbgLevel, formatString, ...) Win32_DebugPrint (__func__, __FILE__, __LINE__, dbgLevel, true,  formatString, ##__VA_ARGS__)

#define Write_D(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Debug, false, message)
#define WriteLine_D(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Debug, true,  message)
#define Print_D(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Debug, false, formatString, ##__VA_ARGS__)
#define PrintLine_D(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Debug, true,  formatString, ##__VA_ARGS__)

#define Write_R(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Regular, false, message)
#define WriteLine_R(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Regular, true,  message)
#define Print_R(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Regular, false, formatString, ##__VA_ARGS__)
#define PrintLine_R(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Regular, true,  formatString, ##__VA_ARGS__)

#define Write_I(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Info, false, message)
#define WriteLine_I(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Info, true,  message)
#define Print_I(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Info, false, formatString, ##__VA_ARGS__)
#define PrintLine_I(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Info, true,  formatString, ##__VA_ARGS__)

#define Write_N(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Notify, false, message)
#define WriteLine_N(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Notify, true,  message)
#define Print_N(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Notify, false, formatString, ##__VA_ARGS__)
#define PrintLine_N(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Notify, true,  formatString, ##__VA_ARGS__)

#define Write_O(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Other, false, message)
#define WriteLine_O(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Other, true,  message)
#define Print_O(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Other, false, formatString, ##__VA_ARGS__)
#define PrintLine_O(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Other, true,  formatString, ##__VA_ARGS__)

#define Write_W(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Warning, false, message)
#define WriteLine_W(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Warning, true,  message)
#define Print_W(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Warning, false, formatString, ##__VA_ARGS__)
#define PrintLine_W(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Warning, true,  formatString, ##__VA_ARGS__)

#define Write_E(message)                         Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Error, false, message)
#define WriteLine_E(message)                     Win32_DebugOutput(__func__, __FILE__, __LINE__, DbgLevel_Error, true,  message)
#define Print_E(formatString, ...)               Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Error, false, formatString, ##__VA_ARGS__)
#define PrintLine_E(formatString, ...)           Win32_DebugPrint (__func__, __FILE__, __LINE__, DbgLevel_Error, true,  formatString, ##__VA_ARGS__)

#else //!PLAT_DEBUG_OUTPUT_ENABLED

void Win32_DebugOutput(const char* functionName, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* message) {}
void Win32_DebugPrint(const char* functionName, const char* fileName, u32 lineNumber, DbgLevel_t dbgLevel, bool newLine, const char* formatString, ...) {}

#define WriteAt(dbgLevel, message)
#define WriteLineAt(dbgLevel, message)
#define PrintAt(dbgLevel, formatString, ...)
#define PrintLineAt(dbgLevel, formatString, ...)

#define Write_D(message)
#define WriteLine_D(message)
#define Print_D(formatString, ...)
#define PrintLine_D(formatString, ...)

#define Write_R(message)
#define WriteLine_R(message)
#define Print_R(formatString, ...)
#define PrintLine_R(formatString, ...)

#define Write_I(message)
#define WriteLine_I(message)
#define Print_I(formatString, ...)
#define PrintLine_I(formatString, ...)

#define Write_N(message)
#define WriteLine_N(message)
#define Print_N(formatString, ...)
#define PrintLine_N(formatString, ...)

#define Write_O(message)
#define WriteLine_O(message)
#define Print_O(formatString, ...)
#define PrintLine_O(formatString, ...)

#define Write_W(message)
#define WriteLine_W(message)
#define Print_W(formatString, ...)
#define PrintLine_W(formatString, ...)

#define Write_E(message)
#define WriteLine_E(message)
#define Print_E(formatString, ...)
#define PrintLine_E(formatString, ...)

#endif
