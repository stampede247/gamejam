/*
File:   win32_defines.h
Author: Taylor Robbins
Date:   07\12\2019
Description:
	** Holds definitions that are used to change various aspects of the win32 platform layer at compile time
*/

#ifndef _WIN_32_DEFINES_H
#define _WIN_32_DEFINES_H

// +--------------------------------------------------------------+
// |                           Defines                            |
// +--------------------------------------------------------------+
#define WINDOW_CLASS_NAME           "GameJam"
#define WINDOW_TITLE                "Game Jam"
#define APPLICATION_DLL_NAME        "GameJam.dll"
#define APPLICATION_DLL_TEMP_NAME   "GameJam_TEMP.dll"
#define OPEN_CONSOLE_WINDOW         DEBUG
#define PLAT_DEBUG_OUTPUT_ENABLED   true
#define WINDOW_SWAP_INTERVAL        1

#define OPENGL_REQUEST_VERSION_MAJOR  3
#define OPENGL_REQUEST_VERSION_MINOR  3
#define OPENGL_FORCE_FORWARD_COMPAT   false //TODO: We should see if we can make this work when set to true
#define OPENGL_DEBUG_CONTEXT          DEBUG

#define NUM_FRAMERATE_AVGS          5 //frames
#define PLATFORM_HEAP_SIZE          Kilobytes(128)
#define PLATFORM_TEMP_SIZE          Kilobytes(16)
#define PLATFORM_TEMP_MAX_NUM_MARKS 16 //marks
#define MAX_NUM_THREADS             4 //threads
#define MAX_CRASH_DUMPS             4 //dumps
#define STICK_DEAD_ZONE             0.8f

//NOTE: This must match resource.h in build directory!
#define WIN32_EXE_ICON_ID           101

#endif //  _WIN_32_DEFINES_H
