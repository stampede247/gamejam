/*
File:   win32_threads.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** Holds a bunch of functions that make creating, monitoring, and destroying various threads easier for the platform layer
*/

void _Win32_CreateMutex(Win32Mutex_t* mutex)
{
	Assert(mutex != nullptr);
	ClearPointer(mutex);
	mutex->handle = CreateMutex(nullptr, false, nullptr); //default security, no ownership, no name
	Assert(mutex->handle != NULL);
}

bool _Win32_LockMutex(Win32Mutex_t* mutex, u32 timeoutMs = 0xFFFFFFFFUL)
{
	DWORD realTimeout = (DWORD)timeoutMs;
	if (timeoutMs == 0xFFFFFFFFUL) { realTimeout = INFINITE; }
	DWORD mutexResult = WaitForSingleObject(mutex->handle, realTimeout);
	if (realTimeout == INFINITE && mutexResult != WAIT_OBJECT_0) { Assert(false); } //Failed to lock mutex!
	return (mutexResult == WAIT_OBJECT_0);
}

void _Win32_UnlockMutex(Win32Mutex_t* mutex)
{
	BOOL mutexResult = ReleaseMutex(mutex->handle);
	Assert(mutexResult != 0);
}

Win32Thread_t* _Win32_CreateThread(Win32ThreadFunction_t* function, void* userPntr)
{
	Assert(function != nullptr);
	if (!_Win32_LockMutex(&platform.threadsMutex, 10)) { return nullptr; }
	
	Win32Thread_t* openThread = nullptr;
	for (u32 tIndex = 0; tIndex < MAX_NUM_THREADS; tIndex++)
	{
		Win32Thread_t* threadPntr = &platform.threads[tIndex];
		if (threadPntr->handle == NULL)
		{
			openThread = threadPntr;
			ClearPointer(openThread);
			openThread->function = function;
			openThread->userPntr = userPntr;
			break;
		}
	}
	if (openThread == nullptr)
	{
		_Win32_UnlockMutex(&platform.threadsMutex);
		return nullptr; //Cannot create any more threads. All the thread slots are being used
	}
	
	ClearPointer(openThread);
	openThread->handle = CreateThread(
		NULL, //default security attributes
		0, //default stack size
		function,
		userPntr,
		0, //default creation flags
		&openThread->id
	);
	if (openThread->handle == NULL) //Failed to create thread for unknown reason
	{
		_Win32_UnlockMutex(&platform.threadsMutex);
		return nullptr;
	}
	
	_Win32_UnlockMutex(&platform.threadsMutex);
	return openThread;
}

bool _Win32_DestroyThread(Win32Thread_t* thread)
{
	Assert(thread != nullptr);
	
	if (!_Win32_LockMutex(&platform.threadsMutex, 10)) { return false; }
	
	if (thread->id == NULL) //Already stopped
	{
		_Win32_UnlockMutex(&platform.threadsMutex);
		return true;
	}
	
	bool result = false;
	if (TerminateThread(thread->handle, 0x00) != 0)
	{
		ClearPointer(thread);
		result = true;
	}
	else
	{
		Assert(false); //Failed to terminate thread, not sure why
	}
	
	_Win32_UnlockMutex(&platform.threadsMutex);
	return result;
}

void _Win32_TerminateAllThreads()
{
	for (u32 tIndex = 0; tIndex < ArrayCount(platform.threads); tIndex++)
	{
		Win32Thread_t* threadPntr = &platform.threads[tIndex];
		if (threadPntr->handle != NULL)
		{
			_Win32_DestroyThread(threadPntr);
		}
	}
}

bool _Win32_IsThreadAlive(Win32Thread_t* thread)
{
	Assert(thread != nullptr);
	
	if (!_Win32_LockMutex(&platform.threadsMutex, 10)) { Assert(false); return true; }
	
	if (thread->id == NULL)
	{
		_Win32_UnlockMutex(&platform.threadsMutex);
		return false;
	}
	
	DWORD waitResult = WaitForSingleObject(thread->handle, 0);
	bool result = (waitResult != WAIT_OBJECT_0);
	
	_Win32_UnlockMutex(&platform.threadsMutex);
	return result;
}

