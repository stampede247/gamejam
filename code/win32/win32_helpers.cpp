/*
File:   win32_helpers.cpp
Author: Taylor Robbins
Date:   07\12\2019
Description: 
	** This file contains many of the various functions that the win32 platform layer uses
	** to perform tasks that don't really have a home in any other file.
*/

char* Win32_GetWorkingDirectory(MemoryArena_t* memArena)
{
	Assert(memArena != nullptr);
	
	DWORD pathLength = GetCurrentDirectory(0, nullptr);
	if (pathLength <= 0) { return nullptr; }
	
	char* result = PushArray(memArena, char, pathLength+1);
	DWORD resultLength = GetCurrentDirectory(pathLength+1, result);
	Assert(resultLength <= pathLength);
	result[resultLength] = '\0';
	return result;
}

char* Win32_GetExecutablePath(MemoryArena_t* memArena)
{
	Assert(memArena != nullptr);
	
	DWORD pathLength = 0;
	{
		TempPushMark();
		u32 tempBufferSize = 512;
		char* tempBuffer = TempArray(char, tempBufferSize);
		pathLength = GetModuleFileNameA(0, tempBuffer, tempBufferSize);
		TempPopMark();
	}
	if (pathLength <= 0) { return nullptr; }
	
	char* result = PushArray(memArena, char, pathLength+1);
	DWORD resultLength = GetModuleFileNameA(0, result, pathLength+1);
	Assert(resultLength == pathLength);
	result[resultLength] = '\0';
	return result;
}

inline FILETIME Win32_GetFileWriteTime(const char* filePath)
{
	FILETIME lastWriteTime = {};
	WIN32_FILE_ATTRIBUTE_DATA attData;
	if (GetFileAttributesExA(filePath, GetFileExInfoStandard, &attData))
	{
		lastWriteTime = attData.ftLastWriteTime;
	}
	return lastWriteTime;
}

void Win32_UpdateWindowTitle(const char* baseName)
{
	#if DEBUG
	const char* newTitle = TempPrint("%s (Platform %u.%u:%03u App %u.%u:%03u)", baseName,
		platform.version.major, platform.version.minor, platform.version.build,
		app.version.major, app.version.minor, app.version.build
	);
	#else
	const char* newTitle = baseName;
	#endif
	
	if (strcmp(newTitle, platform.windowTitle) != 0)
	{
		PrintLine_D("Window title changed: \"%s\" -> \"%s\"", platform.windowTitle, newTitle);
		ArenaPop(&platform.mainHeap, platform.windowTitle);
		platform.windowTitle = ArenaNtString(&platform.mainHeap, newTitle);
		glfwSetWindowTitle(platform.window, platform.windowTitle);
	}
}

void Win32_UpdateAppInputTimeInfo(AppInput_t* appInput)
{
	r64 lastTimeF = appInput->programTimeF;
	u64 lastTime = appInput->programTime;
	appInput->programTimeF = glfwGetTime() * 1000.0;
	appInput->programTime = (u64)(appInput->programTimeF);
	appInput->elapsedMs = appInput->programTimeF - lastTimeF;
	appInput->framerate = 1000.0 / appInput->elapsedMs;
	appInput->timeDelta = appInput->elapsedMs / (1000.0 / appInput->targetFramerate);
	appInput->avgElapsedMs = ((appInput->avgElapsedMs * (NUM_FRAMERATE_AVGS-1)) + appInput->elapsedMs) / NUM_FRAMERATE_AVGS;
	appInput->avgFramerate = ((appInput->avgFramerate * (NUM_FRAMERATE_AVGS-1)) + appInput->framerate) / NUM_FRAMERATE_AVGS;
	
	// SYSTEMTIME systemTime = {};
	// GetSystemTime(&systemTime);
	time_t timeStruct = time(NULL);
	u64 timestamp = (u64)timeStruct; //TODO: Will this work on other platforms?
	
	appInput->systemTime = {};
	GetRealTime(timestamp, false, &appInput->systemTime);
	
	// appInput->localTime = {};
	// GetRealTime(localTimestamp, false, &appInput->localTime);
	
	SYSTEMTIME localTime = {};
	GetLocalTime(&localTime);
	appInput->localTime = {};
	//TODO: Fill the timestamp
	appInput->localTime.year         = localTime.wYear;
	appInput->localTime.month        = (u8)(localTime.wMonth-1);
	appInput->localTime.day          = (u8)(localTime.wDay-1);
	appInput->localTime.hour         = (u8)localTime.wHour;
	appInput->localTime.minute       = (u8)localTime.wMinute;
	appInput->localTime.second       = (u8)localTime.wSecond;
	// appInput->localTime.millisecond  = localTime.wMilliseconds;
}

void Win32_ExitOnError(const char* outputString)
{
		WriteLine_E(outputString);
		if (platform.glfwLastErrorStr != nullptr)
		{
			outputString = MallocPrint("%s\nLast GLFW Error %d: \"%s\"", outputString, platform.glfwLastErrorCode, platform.glfwLastErrorStr);
		}
		MessageBoxA(NULL, outputString, "Initialization Error Encountered!", MB_OK);
		if (platform.glfwInitialized)
		{
			glfwTerminate();
		}
		exit(1);
}

bool Win32_AreGlfwVideoModesEqual(const GLFWvidmode* mode1, const GLFWvidmode* mode2)
{
	if (mode1->width != mode2->width)             { return false; }
	if (mode1->height != mode2->height)           { return false; }
	if (mode1->redBits != mode2->redBits)         { return false; }
	if (mode1->greenBits != mode2->greenBits)     { return false; }
	if (mode1->blueBits != mode2->blueBits)       { return false; }
	if (mode1->refreshRate != mode2->refreshRate) { return false; }
	return true;
}

char* Win32_CompleteRelativePath(MemoryArena_t* memArena, const char* path, u32 pathLength)
{
	Assert(memArena != nullptr);
	Assert(path != nullptr);
	
	bool isRelative = true;
	if (pathLength > 0 && (path[0] == '/' || path[0] == '\\')) { isRelative = false; }
	if (pathLength >= 2 && IsCharClassAlphabet(path[0]) && path[1] == ':' && (path[2] == '/' || path[2] == '\\')) { isRelative = false; }
	if (pathLength > 0 && (path[pathLength-1] == '/' || path[pathLength-1] == '\\')) { pathLength -= 1; } //cut trailing /
	
	char* result = nullptr;
	if (isRelative)
	{
		result = MyStrCat(memArena, platform.exeDirectory, (u32)strlen(platform.exeDirectory), path, pathLength);
	}
	else
	{
		result = ArenaString(memArena, path, pathLength);
	}
	Assert(result != nullptr);
	StrReplaceInPlace(result, (u32)strlen(result), "/", "\\", 1);
	
	return result;
}

// +==============================+
// |   Win32_CallAppInitialize    |
// +==============================+
WIN32_THREAD_FUNCTION(Win32_CallAppInitialize, userPntr)
{
	app.AppInitialize(&platform.info, &platform.appMemory, platform.currentInput);
	platform.appLoadingFinished = true;
	return 0;
}
