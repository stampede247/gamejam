/*
File:   app_sound.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Handles playing, handling, and stopping sounds using OpenAL
*/

#if AUDIO_ENABLED
SoundInstance_t* StartSndInstance(const Sound_t* srcPntr, bool looping = false, r32 volume = 1.0f, r32 pitch = 1.0f)
{
	SoundInstance_t* result = nullptr;
	
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		if (app->soundInstances[sIndex].playing == false)
		{
			result = &app->soundInstances[sIndex];
			break;
		}
	}
	
	if (result == nullptr)
	{
		WriteLine_E("Sound instance buffer is full!");
		return nullptr;
	}
	
	ClearPointer(result);
	
	alGenSources(1, &result->id);
	alSourcef (result->id, AL_PITCH, pitch);
	alSourcef (result->id, AL_GAIN, volume);
	alSource3f(result->id, AL_POSITION, 0, 0, 0);
	alSource3f(result->id, AL_VELOCITY, 0, 0, 0);
	alSourcei (result->id, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);
	alSourcei (result->id, AL_BUFFER, srcPntr->id);
	
	alSourcePlay(result->id);
	
	result->playing = true;
	result->soundId = srcPntr->id;
	result->startTime = ProgramTime;
	return result;
}

void StopSndInstance(SoundInstance_t* instance)
{
	Assert(instance != nullptr);
	Assert(instance >= &app->soundInstances[0]);
	Assert(instance <= &app->soundInstances[MAX_SOUND_INSTANCES-1]);
	
	if (alIsSource(instance->id))
	{
		alSourceStop(instance->id);
		alDeleteSources(1, &instance->id);
	}
	
	ClearPointer(instance);
}

void SetSndInstanceVolume(SoundInstance_t* instance, r32 volume)
{
	Assert(instance != nullptr);
	
	if (alIsSource(instance->id))
	{
		alSourcef(instance->id, AL_GAIN, volume);
	}
}

void StopSoundsBySrc(const Sound_t* srcPntr)
{
	Assert(srcPntr != nullptr);
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->soundId == srcPntr->id)
		{
			StopSndInstance(instance);
		}
	}
}

SoundInstance_t* GetSndInstanceBySrc(const Sound_t* srcPntr, u32 index = 0)
{
	Assert(srcPntr != nullptr);
	u32 foundIndex = 0;
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		if (instance->playing && instance->soundId == srcPntr->id)
		{
			if (foundIndex >= index) { return instance; }
			foundIndex++;
		}
	}
	return nullptr;
}

// +--------------------------------------------------------------+
// |                    Sound Effect Functions                    |
// +--------------------------------------------------------------+
SoundInstance_t* SoundEffect_(const Sound_t* srcPntr, bool singleton, r32 pitch = 1.0f)
{
	if (app->masterSoundEnabled && app->soundEffectsEnabled)
	{
		Assert(srcPntr != nullptr);
		if (singleton)
		{
			u32 sIndex = 0;
			while (SoundInstance_t* currentSnd = GetSndInstanceBySrc(srcPntr, sIndex++))
			{
				if (TimeSince(currentSnd->startTime) <= SND_EFFECT_COMBINE_TIME)
				{
					//The sound is already playing, no point in playing it again
					return currentSnd;
				}
			}
		}
		return StartSndInstance(srcPntr, false, app->masterVolume * app->soundEffectVolume, pitch);
	}
	else { return nullptr; }
}

#define SoundEffect(sndName) SoundEffect_(&resources->sndName, false)
#define SoundEffectPitch(sndName, pitch) SoundEffect_(&resources->sndName, false, pitch)

#define SoundEffectSingle(sndName) SoundEffect_(&resources->sndName, true)
#define SoundEffectSinglePitch(sndName, pitch) SoundEffect_(&resources->sndName, true, pitch)

// +--------------------------------------------------------------+
// |                       Music Functions                        |
// +--------------------------------------------------------------+
r32 GetMusicTransitionAmount()
{
	if (app->musicTransStart == 0) { return 1.0f; }
	if (ProgramTime < app->musicTransStart) { return 0.0f; }
	if (ProgramTime - app->musicTransStart >= app->musicTransTime) { return 1.0f; }
	
	u64 transTime = ProgramTime - app->musicTransStart;
	r32 result = (r32)transTime / (r32)app->musicTransTime;
	return result;
}

void GetMusicTransitionVolumes(r32* volume1, r32* volume2)
{
	*volume1 = 1.0f;
	*volume2 = 1.0f;
	
	r32 amount = GetMusicTransitionAmount();
	if (amount >= 1.0f)
	{
		*volume1 = 0.0f; *volume2 = 1.0f;
		return;
	}
	
	switch (app->transitionType)
	{
		case SoundTransition_Immediate:
		{
			*volume1 = 1.0f; *volume2 = 0.0f;
		} break;
		
		case SoundTransition_FadeOut:
		{
			*volume1 = (1.0f - amount); *volume2 = 0.0f;
		} break;
		
		case SoundTransition_FadeIn:
		{
			*volume1 = 0.0f; *volume2 = amount;
		} break;
		
		case SoundTransition_FadeOutFadeIn:
		{
			if (amount < 0.5f) { *volume1 = (1.0f - amount*2); *volume2 = 0.0f; }
			else               { *volume1 = 0.0f; *volume2 = (amount - 0.5f) * 2; }
		} break;
		
		case SoundTransition_CrossFade:
		{
			*volume1 = (1.0f - amount); *volume2 = amount;
		} break;
	};
}

void QueueMusicChangeEx(const Sound_t* newSong, SoundTransition_t transitionType, u64 transitionTime)
{
	r32 currentTransitionTime = GetMusicTransitionAmount();
	if (currentTransitionTime < 1.0f)
	{
		WriteLine_W("Last music transition was cut short");
		if (app->lastMusic != nullptr) { StopSndInstance(app->lastMusic); }
		if (app->currentMusic != nullptr) { SetSndInstanceVolume(app->currentMusic, app->masterVolume * app->musicVolume * ((app->masterSoundEnabled && app->musicEnabled) ? 1.0f : 0.0f)); }
	}
	
	app->transitionType = transitionType;
	app->musicTransTime = transitionTime;
	app->musicTransStart = ProgramTime;
	app->lastMusic = app->currentMusic;
	app->currentMusic = nullptr;
	
	if (newSong != nullptr)
	{
		app->currentMusic = StartSndInstance(newSong, true, 0.0f);
	}
	
	r32 volume1, volume2;
	GetMusicTransitionVolumes(&volume1, &volume2);
	if (app->lastMusic    != nullptr) { SetSndInstanceVolume(app->lastMusic,    volume1 * app->masterVolume * app->musicVolume * ((app->masterSoundEnabled && app->musicEnabled) ? 1.0f : 0.0f)); }
	if (app->currentMusic != nullptr) { SetSndInstanceVolume(app->currentMusic, volume2 * app->masterVolume * app->musicVolume * ((app->masterSoundEnabled && app->musicEnabled) ? 1.0f : 0.0f)); }
}
void QueueMusicChange(const Sound_t* newSong)
{
	QueueMusicChangeEx(newSong, SoundTransition_CrossFade, DEF_MUSIC_TRANS_TIME);
}

void SoftQueueMusicChangeEx(const Sound_t* newSong, SoundTransition_t transitionType, u64 transitionTime)
{
	if (app->currentMusic != nullptr && app->currentMusic->soundId == newSong->id)
	{
		return;
	}
	QueueMusicChangeEx(newSong, transitionType, transitionTime);
}
void SoftQueueMusicChange(const Sound_t* newSong)
{
	SoftQueueMusicChangeEx(newSong, SoundTransition_CrossFade, DEF_MUSIC_TRANS_TIME);
}

// +--------------------------------------------------------------+
// |                       Update Function                        |
// +--------------------------------------------------------------+
void UpdateSndInstances()
{
	for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
	{
		SoundInstance_t* instance = &app->soundInstances[sIndex];
		
		if (instance->playing)
		{
			ALenum audioState = 0;
			alGetSourcei(instance->id, AL_SOURCE_STATE, &audioState);
			if (audioState != AL_PLAYING)
			{
				StopSndInstance(instance);
			}
		}
	}
	
	r32 transitionAmount = GetMusicTransitionAmount();
	if (transitionAmount >= 1.0f && app->lastMusic != nullptr)
	{
		WriteLine_D("Finished transition");
		StopSndInstance(app->lastMusic);
		app->lastMusic = nullptr;
	}
	r32 volume1, volume2;
	GetMusicTransitionVolumes(&volume1, &volume2);
	if (app->lastMusic    != nullptr) { SetSndInstanceVolume(app->lastMusic,    volume1 * app->masterVolume * app->musicVolume * ((app->masterSoundEnabled && app->musicEnabled) ? 1.0f : 0.0f)); }
	if (app->currentMusic != nullptr) { SetSndInstanceVolume(app->currentMusic, volume2 * app->masterVolume * app->musicVolume * ((app->masterSoundEnabled && app->musicEnabled) ? 1.0f : 0.0f)); }
}

#else 

#define SoundEffect(sndName)                   //nothing
#define SoundEffectPitch(sndName, pitch)       //nothing
#define SoundEffectSingle(sndName)             //nothing
#define SoundEffectSinglePitch(sndName, pitch) //nothing

#define QueueMusicChangeEx(newSong, transitionType, transitionTime)     //nothing
#define QueueMusicChange(newSong)                                       //nothing
#define SoftQueueMusicChangeEx(newSong, transitionType, transitionTime) //nothing
#define SoftQueueMusicChange(newSong)                                   //nothing

#endif
