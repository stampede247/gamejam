/*
File:   app_resources.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds functions that handle loading and reloading various kinds of resource files
*/

bool LoadComplexFontResource(u32 fontIndex, Font_t* font, MemoryArena_t* memArena, const char* filePath)
{
	switch (fontIndex)
	{
		// case 0: //debugFont
		// case 1: //pixelFont
		case 2: //mainFont
		{
			bool success;
			AppCreateFont(font, mainHeap);
			success = FontLoadFile(font, filePath, FontStyle_Default); Assert(success);
			success = FontLoadFile(font, FONTS_FOLDER "consolab.ttf", FontStyle_Bold); Assert(success);
			success = FontBake(font, MAIN_FONT_SIZE, FontStyle_Default); Assert(success);
			success = FontBake(font, MAIN_FONT_SIZE, FontStyle_Bold); Assert(success);
			FontDropFiles(font);
			return true;
		}
		default: return false;
	}
}

ResourceType_t GetResourceTypeByPntr(const void* resourcePntr, u32* subIndexOut = nullptr)
{
	const u8* bytePntr     = (const u8*)resourcePntr;
	const u8* sheetsPntr   = (const u8*)&resources->spriteSheets[0];
	const u8* texturesPntr = (const u8*)&resources->textures[0];
	const u8* soundsPntr   = (const u8*)&resources->sounds[0];
	const u8* musicsPntr   = (const u8*)&resources->musics[0];
	const u8* fontsPntr    = (const u8*)&resources->fonts[0];
	const u8* shadersPntr  = (const u8*)&resources->shaders[0];
	if (bytePntr >= sheetsPntr && bytePntr < sheetsPntr + sizeof(resources->spriteSheets))
	{
		Assert(((bytePntr - sheetsPntr) % sizeof(SpriteSheet_t)) == 0);
		if (subIndexOut != nullptr)
		{
			*subIndexOut = (u32)((bytePntr - sheetsPntr) / sizeof(SpriteSheet_t));
		}
		return ResourceType_SpriteSheet;
	}
	if (bytePntr >= texturesPntr && bytePntr < texturesPntr + sizeof(resources->textures))
	{
		Assert(((bytePntr - texturesPntr) % sizeof(Texture_t)) == 0);
		if (subIndexOut != nullptr)
		{
			*subIndexOut = (u32)((bytePntr - texturesPntr) / sizeof(Texture_t));
		}
		return ResourceType_Texture;
	}
	if (bytePntr >= soundsPntr && bytePntr < soundsPntr + sizeof(resources->sounds))
	{
		Assert(((bytePntr - soundsPntr) % sizeof(Sound_t)) == 0);
		if (subIndexOut != nullptr)
		{
			*subIndexOut = (u32)((bytePntr - soundsPntr) / sizeof(Sound_t));
		}
		return ResourceType_Sound;
	}
	if (bytePntr >= musicsPntr && bytePntr < musicsPntr + sizeof(resources->musics))
	{
		Assert(((bytePntr - musicsPntr) % sizeof(Sound_t)) == 0);
		if (subIndexOut != nullptr)
		{
			*subIndexOut = (u32)((bytePntr - musicsPntr) / sizeof(Sound_t));
		}
		return ResourceType_Music;
	}
	if (bytePntr >= fontsPntr && bytePntr < fontsPntr + sizeof(resources->fonts))
	{
		Assert(((bytePntr - fontsPntr) % sizeof(Font_t)) == 0);
		if (subIndexOut != nullptr)
		{
			*subIndexOut = (u32)((bytePntr - fontsPntr) / sizeof(Font_t));
		}
		return ResourceType_Font;
	}
	if (bytePntr >= shadersPntr && bytePntr < shadersPntr + sizeof(resources->shaders))
	{
		Assert(((bytePntr - shadersPntr) % sizeof(Shader_t)) == 0);
		if (subIndexOut != nullptr)
		{
			*subIndexOut = (u32)((bytePntr - shadersPntr) / sizeof(Shader_t));
		}
		return ResourceType_Shader;
	}
	Assert(false);
	return ResourceType_Unknown;
}

bool IsFileResource(const char* filePath, u32* resourceIndexOut = nullptr)
{
	Assert(filePath != nullptr);
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		ResourceType_t type = ResourceType_Unknown;
		const char* resourcePath = GetResourceFilePath(rIndex, &type);
		if (strcmp(filePath, resourcePath) == 0)
		{
			if (resourceIndexOut != nullptr) { *resourceIndexOut = rIndex; }
			return true;
		}
		else if (type == ResourceType_Shader)
		{
			TempPushMark();
			char* fragmentPath = StrReplace(TempArena, NtStr(resourcePath), NtStr("vert"), NtStr("frag"));
			if (strcmp(filePath, fragmentPath) == 0)
			{
				if (resourceIndexOut != nullptr) { *resourceIndexOut = rIndex; }
				TempPopMark();
				return true;
			}
			TempPopMark();
		}
	}
	return false;
}

void LoadResource(bool firstLoad, u32 resourceIndex)
{
	u32 subIndex = 0;
	ResourceType_t type = GetResourceType(resourceIndex, &subIndex);
	const char* resourcePath = GetResourceFilePath(resourceIndex);
	switch (type)
	{
		case ResourceType_SpriteSheet:
		{
			if (!firstLoad) { DestroySpriteSheet(&resources->spriteSheets[subIndex]); }
			resources->spriteSheets[subIndex] = LoadSpriteSheet(resourcePath, 1, GetSpriteSheetSize(subIndex), true);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Texture:
		{
			if (!firstLoad) { DestroyTexture(&resources->textures[subIndex]); }
			resources->textures[subIndex] = LoadTexture(resourcePath, true, true);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Sound:
		{
			if (!firstLoad) { DestroySound(&resources->sounds[subIndex]); }
			resources->sounds[subIndex] = LoadSound(resourcePath);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Music:
		{
			if (!firstLoad) { DestroySound(&resources->musics[subIndex]); }
			resources->musics[subIndex] = LoadSound(resourcePath);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			#endif
		} break;
		
		case ResourceType_Font:
		{
			if (!firstLoad) { AppDestroyFont(&resources->fonts[subIndex]); }
			r32 fontSize = 0;
			v2i bakeSize = Vec2i_Zero;
			FontResourceType_t fontType = GetFontResourceType(subIndex, &fontSize, &bakeSize);
			if (fontType == FontResourceType_Bitmap)
			{
				r32 fontCharAdvanceX = 0;
				r32 fontCharBaseline = 0;
				u8 fontFirstChar = 0x00;
				v2i fontCharSize = GetBitmapFontResourceInfo(subIndex, &fontCharAdvanceX, &fontCharBaseline, &fontFirstChar);
				bool createSuccess = LoadBitmapFont(&resources->fonts[subIndex], mainHeap, resourcePath, fontCharSize, fontCharAdvanceX, fontCharBaseline, fontFirstChar);
				Assert(createSuccess);
				#if DEVELOPER
				platform->WatchFile(resourcePath);
				#endif
			}
			else if (fontType == FontResourceType_Simple)
			{
				bool createSuccess = LoadFontSimple(&resources->fonts[subIndex], mainHeap, resourcePath, fontSize, bakeSize.width, bakeSize.height);
				Assert(createSuccess);
				//NOTE: No need to follow ttf files actively
			}
			else if (fontType == FontResourceType_Complex)
			{
				bool createSuccess = LoadComplexFontResource(subIndex, &resources->fonts[subIndex], mainHeap, resourcePath);
				Assert(createSuccess);
				//NOTE: No need to follow ttf files actively
			}
			else
			{
				Assert(false);
			}
			//NOTE: No need to follow ttf files actively
		} break;
		
		case ResourceType_Shader:
		{
			if (!firstLoad) { DestroyShader(&resources->shaders[subIndex]); }
			// TempPushMark();
			// char* fragmentPath = StrReplace(TempArena, NtStr(resourcePath), NtStr("vert"), NtStr("frag"));
			resources->shaders[subIndex] = LoadShader(resourcePath);
			#if DEVELOPER
			platform->WatchFile(resourcePath);
			// platform->WatchFile(fragmentPath);
			#endif
			// TempPopMark();
		} break;
		
		default:
		{
			Assert(false);
		} break;
	}
}

void LoadAllResources(bool firstLoad)
{
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		// PrintLine_I("Reloading Resource %u %s", rIndex, GetResourceTypeStr(GetResourceType(rIndex)));
		LoadResource(firstLoad, rIndex);
	}
}

SpriteSheet_t* GetSpriteSheetByFilename(const char* fileName)
{
	if (fileName == nullptr) { return nullptr; }
	for (u32 rIndex = 0; rIndex < NUM_RESOURCE_SHEETS; rIndex++)
	{
		const char* resourcePath = GetSpriteSheetFilePath(rIndex);
		const char* resourceFileName = GetFileNamePart(resourcePath);
		
		if (resourceFileName != nullptr && strcmp(resourceFileName, fileName) == 0)
		{
			return &resources->spriteSheets[rIndex];
		}
	}
	return nullptr;
}

const char* GetFilePathOfSpriteSheet(const SpriteSheet_t* spriteSheet)
{
	if (spriteSheet == nullptr) { return nullptr; }
	if (spriteSheet < &resources->spriteSheets[0]) { return nullptr; }
	if (spriteSheet >= &resources->spriteSheets[NUM_RESOURCE_SHEETS]) { return nullptr; }
	u32 sheetIndex = (u32)(spriteSheet - &resources->spriteSheets[0]);
	Assert(sheetIndex < NUM_RESOURCE_SHEETS);
	return GetSpriteSheetFilePath(sheetIndex);
}
