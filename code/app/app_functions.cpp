/*
File:   app_functions.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds some functions that the app uses to do various things
	** These functions are only available in app.cpp (none of the app_ subsidiary files)
*/

void AppEntryPoint(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
{
	platform = PlatformInfo;
	appInput = AppInput;
	appOutput = AppOutput;
	app = (AppData_t*)AppMemory->permanantPntr;
	mainMenu = &app->mainMenuData;
	game = &app->gameData;
	mess = &app->messData;
	settMenu = &app->settingsMenuData;
	
	TempArena = &app->tempArena;
	mainHeap = &app->mainHeap;
	staticHeap = &app->staticHeap;
	platArena = &app->platArena;
	plt = &app->colorPalette;
	resources = &app->resources;
	rc = &app->renderContext;
	
	if (appInput != nullptr)
	{
		RenderArea = NewRec(0, 0, (r32)appInput->screenSize.x, (r32)appInput->screenSize.y);
		RenderMousePos = NewVec2(appInput->mousePos.x, appInput->mousePos.y);
		RenderMouseStartLeft = NewVec2(appInput->mouseStartPos[MouseButton_Left].x, appInput->mouseStartPos[MouseButton_Left].y);
		RenderMouseStartRight = NewVec2(appInput->mouseStartPos[MouseButton_Right].x, appInput->mouseStartPos[MouseButton_Right].y);
		ProgramTime = appInput->programTime;
		ElapsedMs = (r32)appInput->elapsedMs;
		SystemTime = appInput->systemTime;
		LocalTime = appInput->localTime;
	}
	
	if (app != nullptr && platform != nullptr && mainHeap->base != nullptr)
	{
		MemoryArenaUpdateAllocateFunction(mainHeap, platform->AllocateMemory);
	}
	if (app != nullptr && platform != nullptr && app->platArena.type == MemoryArenaType_External)
	{
		MemoryArenaUpdateAllocateFunction(&app->platArena, platform->AllocateMemory);
		MemoryArenaUpdateFreeFunction(&app->platArena, platform->FreeMemory);
	}
}

#if USE_BOX2D
void* my_b2Alloc(i32 size) //pre-declared in app.cpp
{
	void* result = ArenaPush(mainHeap, size);
	// PrintLine_D("Allocating %u bytes: %p", size, result);
	return result;
}

void my_b2Free(void* mem) //pre-declared in app.cpp
{
	// PrintLine_D("Freeing %p", mem);
	return ArenaPop(&app->mainHeap, mem);
}

void my_b2Log(const char* line) //pre-declared in app.cpp
{
	Write_Rx(DbgFlag_Box2D, line);
}
#endif //USE_BOX2D
