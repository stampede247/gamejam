/*
File:   app_button.cpp
Author: Taylor Robbins
Date:   07\26\2019
Description: 
	** Holds functions that handle input and drawing a Button_t structure
*/

void DestroyButton(Button_t* btn)
{
	Assert(btn != nullptr);
	
	if (btn->text != nullptr)
	{
		Assert(btn->allocArena != nullptr);
		ArenaPop(btn->allocArena, btn->text);
		btn->text = nullptr;
	}
	
	ClearPointer(btn);
}

void CreateButton(Button_t* btn, MemoryArena_t* memArena, const char* nullTermStr, rec bounds, Font_t* font, r32 fontSize, u8 options = ButtonOp_Default)
{
	Assert(btn != nullptr);
	Assert(memArena != nullptr);
	
	ClearPointer(btn);
	btn->allocArena = memArena;
	
	u32 strLength = ((nullTermStr == nullptr) ? 0 : (u32)strlen(nullTermStr));
	btn->text = PushArray(memArena, char, strLength+1);
	btn->textLengthAlloc = strLength+1;
	btn->textLength = strLength;
	if (nullTermStr != nullptr && strLength > 0) { memcpy(btn->text, nullTermStr, strLength); }
	btn->text[strLength] = '\0';
	
	btn->font = font;
	btn->fontSize = fontSize;
	btn->fontStyle = FontStyle_Default;
	btn->minSize = Vec2_Zero;
	btn->maxSize = Vec2_Zero;
	btn->options = options;
	btn->bounds = bounds;
	btn->padding = 10; //px
	btn->rounding = 5;
	
	btn->textColor = VisWhite;
	btn->backColor = VisLightBlueGray;
	btn->textDownColor = VisLightGray;
	btn->backDownColor = VisBlueGray;
}

void BTN_IncreaseAllocSpace(Button_t* btn, u32 requiredSize) //private
{
	Assert(btn != nullptr);
	Assert(btn->allocArena != nullptr);
	Assert(btn->textLength+1 <= btn->textLengthAlloc);
	if (btn->textLengthAlloc >= requiredSize+1) { return; }
	
	u32 newAllocSize = btn->textLengthAlloc;
	while (newAllocSize < requiredSize+1) { newAllocSize += 16; }
	char* newSpace = PushArray(btn->allocArena, char, newAllocSize);
	if (btn->textLength > 0)
	{
		memcpy(newSpace, btn->text, btn->textLength+1);
	}
	if (btn->text != nullptr)
	{
		ArenaPop(btn->allocArena, btn->text);
	}
	btn->text = newSpace;
	btn->textLengthAlloc = newAllocSize;
	Assert(btn->textLength < btn->textLengthAlloc);
	btn->text[btn->textLength] = '\0';
}

void ButtonSetText(Button_t* btn, const char* strPntr, u32 strLength)
{
	Assert(btn != nullptr);
	Assert(strPntr != nullptr);
	
	btn->textLength = 0;
	if (strLength+1 > btn->textLengthAlloc)
	{
		BTN_IncreaseAllocSpace(btn, strLength);
	}
	Assert(strLength+1 <= btn->textLengthAlloc);
	if (strLength > 0)
	{
		memcpy(btn->text, strPntr, strLength);
	}
	btn->text[strLength] = '\0';
	btn->textLength = strLength;
}
void ButtonSetTextNt(Button_t* btn, const char* nullTermStr)
{
	u32 strLength = (u32)strlen(nullTermStr);
	ButtonSetText(btn, nullTermStr, strLength);
}

void ButtonSetPicture(Button_t* btn, const Texture_t* picture, r32 pictureScale = 1.0f)
{
	Assert(btn != nullptr);
	FlagSet(btn->options, ButtonOp_Picture);
	btn->picture = picture;
	btn->pictureScale = pictureScale;
}

void BTN_UpdateBounds(Button_t* btn)
{
	Assert(btn != nullptr);
	r32 lineHeight = FontGetLineHeight(btn->font, btn->fontSize, btn->fontStyle);
	// r32 maxExtendUp = FontGetMaxExtendUp(btn->font, btn->fontSize, btn->fontStyle);
	// r32 maxExtendDown = FontGetMaxExtendDown(btn->font, btn->fontSize, btn->fontStyle);
	v2 pictureSize = Vec2_Zero;
	if (IsFlagSet(btn->options, ButtonOp_Picture) && btn->picture != nullptr)
	{
		pictureSize = NewVec2(btn->picture->size) * btn->pictureScale;
	}
	
	v2 contentSize = btn->flowInfo.totalSize;
	if (IsFlagSet(btn->options, ButtonOp_Picture) && btn->picture != nullptr)
	{
		if (btn->textLength > 0) { contentSize.width += 5; }
		contentSize.width += pictureSize.width;
		if (contentSize.height < pictureSize.height)
		{
			contentSize.height = pictureSize.height;
		}
	}
	
	if (btn->flowInfoFilled && IsFlagSet(btn->options, ButtonOp_AutoSizeHori))
	{
		r32 newWidth = contentSize.width + btn->padding*2;
		if (newWidth < 30) { newWidth = 30; } //NOTE: Arbitrary minimum
		if (btn->bounds.width != newWidth)
		{
			btn->bounds.width = newWidth;
		}
	}
	if (btn->flowInfoFilled && IsFlagSet(btn->options, ButtonOp_AutoSizeVert))
	{
		r32 newHeight = contentSize.height + btn->padding*2;
		if (newHeight < lineHeight + btn->padding*2) { newHeight = lineHeight + btn->padding*2; }
		if (btn->bounds.height != newHeight)
		{
			btn->bounds.height = newHeight;
		}
	}
	if (btn->maxSize.width != 0 && btn->bounds.width > btn->maxSize.width)
	{
		btn->bounds.width = btn->maxSize.width;
	}
	if (btn->maxSize.height != 0 && btn->bounds.height > btn->maxSize.height)
	{
		btn->bounds.height = btn->maxSize.height;
	}
	if (btn->minSize.width != 0 && btn->bounds.width < btn->minSize.width)
	{
		btn->bounds.width = btn->minSize.width;
	}
	if (btn->minSize.height != 0 && btn->bounds.height < btn->minSize.height)
	{
		btn->bounds.height = btn->minSize.height;
	}
}

const char* BTN_GetUniqueName(Button_t* btn)
{
	Assert(btn != nullptr);
	return TempPrint("Button %p", btn);
}
void ButtonClaimMouse(Button_t* btn, v2 mousePos)
{
	Assert(btn != nullptr);
	BTN_UpdateBounds(btn);
	if (!IsMouseOverOther() && IsInsideRec(btn->bounds, mousePos))
	{
		MouseOverNamed(BTN_GetUniqueName(btn));
	}
}

bool ButtonUpdate(Button_t* btn, v2 mousePos, bool* buttonFocusedOut = nullptr)
{
	Assert(btn != nullptr);
	
	btn->isDown = false;
	bool buttonPressedDown = false;
	bool buttonReleasedUp = false;
	bool result = false;
	if (buttonFocusedOut != nullptr) { *buttonFocusedOut = false; }
	
	ButtonClaimMouse(btn, mousePos); //calls BTN_UpdateBounds for us
	bool mouseInside = IsMouseOver(BTN_GetUniqueName(btn));
	
	// +==============================+
	// |        Update Cursor         |
	// +==============================+
	if (mouseInside)
	{
		appOutput->cursorType = Cursor_Pointer;
	}
	
	// +==============================+
	// |   Handle Left Mouse Clicks   |
	// +==============================+
	{
		if (ButtonPressedSeq(MouseButton_Left, 0xFF))
		{
			if (mouseInside)
			{
				HandleButtonPressSeq(BTN_GetUniqueName(btn), MouseButton_Left, 0xFF);
				buttonPressedDown = true;
				btn->isDown = true;
			}
			btn->mouseStartedInside = mouseInside;
		}
		if (ButtonDownNoHandling(MouseButton_Left))
		{
			if (btn->mouseStartedInside && !app->buttonHandled[MouseButton_Left])
			{
				HandleButton(MouseButton_Left);
				if (mouseInside)
				{
					btn->isDown = true;
				}
			}
		}
		if (btn->mouseStartedInside && !ButtonDownNoHandling(MouseButton_Left) && !ButtonReleasedSeq(MouseButton_Left, 0xFF))
		{
			btn->mouseStartedInside = false;
		}
		if (ButtonReleasedSeq(MouseButton_Left, 0xFF))
		{
			if (btn->mouseStartedInside && mouseInside)
			{
				const InputEvent_t* inputEvent = HandleButtonReleaseSeq(BTN_GetUniqueName(btn), MouseButton_Left, 0xFF);
				
				btn->isDown = true;
				buttonReleasedUp = true;
				
				if (IsFlagSet(btn->options, ButtonOp_Focusable) && !IsUiFocused(btn))
				{
					// WriteLine_D("Button Focused");
					SetUiFocus(btn);
					if (buttonFocusedOut != nullptr) { *buttonFocusedOut = true; }
				}
			}
		}
	}
	
	// +==============================+
	// |     Handle Enter Button      |
	// +==============================+
	if (IsFlagSet(btn->options, ButtonOp_EnterPressable) && IsUiFocused(btn))
	{
		if (ButtonPressedSeq(Button_Enter, 0xFF, IsFlagSet(btn->options, ButtonOp_EnterRepeatable)))
		{
			HandleButtonPressSeq(BTN_GetUniqueName(btn), Button_Enter, 0xFF, IsFlagSet(btn->options, ButtonOp_EnterRepeatable));
			WriteLine_D("Enter pressed!!");
			btn->isDown = true;
			buttonPressedDown = true;
		}
		
		if (ButtonReleasedSeq(Button_Enter, 0xFF))
		{
			HandleButtonReleaseSeq(BTN_GetUniqueName(btn), Button_Enter, 0xFF);
			WriteLine_D("Enter pressed!!!");
			btn->isDown = true;
			buttonReleasedUp = true;
		}
	}
	if (IsFlagSet(btn->options, ButtonOp_EnterPressable) && ButtonDown(Button_Enter) && IsUiFocused(btn))
	{
		HandleButton(Button_Enter);
		WriteLine_D("Enter down");
		btn->isDown = true;
	}
	
	// +==============================+
	// |     Handle Button Press      |
	// +==============================+
	if (IsFlagSet(btn->options, ButtonOp_OnDown) && buttonPressedDown)
	{
		result = true;
	}
	if (!IsFlagSet(btn->options, ButtonOp_OnDown) && buttonReleasedUp)
	{
		result = true;
	}
	
	return result;
}

void ButtonDraw(Button_t* btn)
{
	r32 lineHeight = FontGetLineHeight(btn->font, btn->fontSize, btn->fontStyle);
	r32 maxExtendUp = FontGetMaxExtendUp(btn->font, btn->fontSize, btn->fontStyle);
	r32 maxExtendDown = FontGetMaxExtendDown(btn->font, btn->fontSize, btn->fontStyle);
	
	bool mouseInside = IsMouseOver(BTN_GetUniqueName(btn));
	v2 textPos = btn->bounds.topLeft + NewVec2(btn->bounds.width/2, btn->bounds.height/2 + lineHeight/2 - maxExtendDown);
	Alignment_t textAlignment = Alignment_Center;
	v2 picturePos = Vec2_Zero;
	v2 pictureSize = Vec2_Zero;
	if (IsFlagSet(btn->options, ButtonOp_Picture) && btn->picture != nullptr)
	{
		pictureSize = NewVec2(btn->picture->size) * btn->pictureScale;
		FontFlowInfo_t preFlowInfo = {};
		FontPerformTextFlow(false, btn->text, btn->textLength, Vec2_Zero, White, btn->font, Alignment_Left, btn->fontSize, btn->fontStyle, 0, &preFlowInfo);
		v2 textSize = NewVec2(preFlowInfo.extentRight, preFlowInfo.totalSize.height);
		v2 contentSize = NewVec2(pictureSize.width, MaxR32(pictureSize.height, textSize.height));
		if (btn->textLength > 0) { contentSize.width += 5 + textSize.width; }
		if (contentSize.width <= btn->bounds.width - btn->padding*2)
		{
			picturePos.x = btn->bounds.x + btn->bounds.width/2 - contentSize.width/2;
		}
		else
		{
			picturePos.x = btn->bounds.x + btn->padding;
		}
		picturePos.y = btn->bounds.y + btn->bounds.height/2 - pictureSize.height/2;
		textPos.x = picturePos.x + pictureSize.width + 5;
		textAlignment = Alignment_Left;
	}
	textPos = Vec2Round(textPos);
	
	rec oldViewport = rc->viewport;
	Font_t* oldFont = rc->boundFont;
	u16 oldStyle = rc->fontStyle;
	r32 oldSize = rc->fontSize;
	Alignment_t oldAlignment = rc->fontAlignment;
	
	Color_t textColor = btn->textColor;
	Color_t backColor = btn->backColor;
	if (btn->isDown)
	{
		textColor = btn->textDownColor;
		backColor = btn->backDownColor;
	}
	
	if (IsUiFocused(btn))
	{
		//TODO: Should we draw something different to indicate that the button is selected?
	}
	
	RcSetViewport(RecOverlap(btn->bounds, oldViewport));
	if (btn->rounding > 0)
	{
		RcDrawCircle(btn->bounds.topLeft + NewVec2(btn->rounding), btn->rounding, backColor);
		RcDrawCircle(btn->bounds.topLeft + NewVec2(btn->bounds.width - btn->rounding, btn->rounding), btn->rounding, backColor);
		RcDrawCircle(btn->bounds.topLeft + NewVec2(btn->rounding, btn->bounds.height - btn->rounding), btn->rounding, backColor);
		RcDrawCircle(btn->bounds.topLeft + NewVec2(btn->bounds.width - btn->rounding, btn->bounds.height - btn->rounding), btn->rounding, backColor);
		RcDrawRectangle(NewRec(btn->bounds.x + btn->rounding, btn->bounds.y, btn->bounds.width - btn->rounding*2, btn->bounds.height), backColor);
		RcDrawRectangle(NewRec(btn->bounds.x, btn->bounds.y + btn->rounding, btn->bounds.width, btn->bounds.height - btn->rounding*2), backColor);
	}
	else
	{
		RcDrawRectangle(btn->bounds, backColor);
	}
	
	RcSetViewport(RecOverlap(RecDeflate(btn->bounds, btn->padding), oldViewport));
	RcBindFont(btn->font);
	RcSetFontSize(btn->fontSize);
	RcSetFontStyle(btn->fontStyle);
	RcSetFontAlignment(textAlignment);
	
	if (IsFlagSet(btn->options, ButtonOp_Picture) && btn->picture != nullptr)
	{
		RcBindTexture(btn->picture);
		RcDrawTexturedRec(NewRec(picturePos, pictureSize), textColor);
	}
	RcDrawString(btn->text, btn->textLength, textPos, textColor, 0, &btn->flowInfo);
	btn->flowInfoFilled = true;
	
	RcSetViewport(oldViewport);
	RcBindFont(oldFont);
	RcSetFontSize(oldSize);
	RcSetFontStyle(oldStyle);
	RcSetFontAlignment(oldAlignment);
}
