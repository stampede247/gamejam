/*
File:   app_console_commands.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds the console commands and the functions that help us get meta-data about them
*/

typedef enum
{
	CslArgType_None = 0,
	CslArgType_String,
	CslArgType_Hex8,
	CslArgType_Hex32,
	CslArgType_Uint8,
	CslArgType_Uint32,
	CslArgType_Int32,
	CslArgType_NumTypes,
} CslArgType_t;

#define CslCmd_DEFINITION(functionName) void functionName(const char* baseCmd, u32 numArguments, StrSplitPiece_t* arguments)
typedef CslCmd_DEFINITION(CslCmd_f);

CslCmd_DEFINITION(CslCmd_Help); //pre-declaration

// +==============================+
// |             test             |
// +==============================+
CslCmd_DEFINITION(CslCmd_Test)
{
	PrintLine_I("Nothing to test right now");
}

// +==============================+
// |            memory            |
// +==============================+
CslCmd_DEFINITION(CslCmd_Memory)
{
	PrintLine_I("Static Heap: %s / %s (%%%.2f filled)", FormattedSizeStr(app->staticHeap.used), FormattedSizeStr(app->staticHeap.size), ((r32)app->staticHeap.used / (r32)app->staticHeap.size) * 100.0f);
	PrintLine_I("Main Heap: %s / %s (%%%.2f filled)", FormattedSizeStr(app->mainHeap.used), FormattedSizeStr(app->mainHeap.size), ((r32)app->mainHeap.used / (r32)app->mainHeap.size) * 100.0f);
	PrintLine_I("TempArena: %s / %s (%%%.2f filled) %u marks", FormattedSizeStr(TempArena->used), FormattedSizeStr(TempArena->size), ((r32)TempArena->used / (r32)TempArena->size) * 100.0f, ArenaNumMarks(TempArena));
	u32 fifoSize = app->dbgConsole.fifoSize;
	u32 fifoUsage = app->dbgConsole.fifoUsage;
	r32 fifoUsagePercentage = ((r32)fifoUsage / (r32)fifoSize) * 100.0f;
	u32 numLines = app->dbgConsole.numLines;
	PrintLine_I("Console Fifo: %s / %s (%%%.2f filled) %u lines", FormattedSizeStr(fifoUsage), FormattedSizeStr(fifoSize), fifoUsagePercentage, numLines);
	WriteLine_I("");
	PrintLine_I("AppData_t          %s", FormattedSizeStr(sizeof(AppData_t)));
	PrintLine_I("MainMenuData_t     %s", FormattedSizeStr(sizeof(MainMenuData_t)));
	PrintLine_I("MessData_t         %s", FormattedSizeStr(sizeof(MessData_t)));
}

// +==============================+
// |         memory_meta          |
// +==============================+
CslCmd_DEFINITION(CslCmd_MemoryMeta)
{
	#if MEMORY_ARENA_META_INFO_ENABLED
	
	u32 totalMetaSize = 0;
	u32 numMetaEntries = 0;
	ArenaAllocMetaInfo_t* currentInfo = app->mainHeap.firstMetaInfo;
	while (currentInfo != nullptr)
	{
		totalMetaSize += currentInfo->allocSize;
		numMetaEntries++;
		currentInfo = currentInfo->next;
	}
	
	PrintLine_I("Found meta info for %s / %s", FormattedSizeStr(totalMetaSize), FormattedSizeStr(app->mainHeap.used));
	PrintLine_I("%u Meta Entries:", numMetaEntries);
	
	u32 startIndex = 0;
	if (numArguments >= 1) { TryParseU32(arguments[0].pntr, arguments[0].length, &startIndex); }
	
	u32 numToDisplay = 50;
	if (numArguments >= 2) { TryParseU32(arguments[1].pntr, arguments[1].length, &numToDisplay); }
	
	u32 mIndex = 0;
	currentInfo = app->mainHeap.firstMetaInfo;
	while (currentInfo != nullptr)
	{
		if (mIndex >= startIndex)
		{
			char* fileName = (char*)(currentInfo+1);
			char* funcName = fileName + (currentInfo->fileNameLength+1);
			PrintLine_I("  [%u] %u Bytes (%p): %s:%u %s", mIndex, currentInfo->allocSize, currentInfo->allocPntr, fileName, currentInfo->lineNumber, funcName);
		}
		mIndex++;
		if (mIndex >= startIndex + numToDisplay) { break; }
		currentInfo = currentInfo->next;
	}
	
	#else
	WriteLine_E("Arena meta info not enabled");
	#endif
}

// +==============================+
// |            states            |
// +==============================+
CslCmd_DEFINITION(CslCmd_States)
{
	PrintLine_R("%u Active AppStates:", app->numActiveStates);
	for (u32 sIndex = 0; sIndex < app->numActiveStates; sIndex++)
	{
		AppState_t appState = app->activeStates[sIndex];
		const char* appStateName = GetAppStateName(appState);
		PrintLine_I("  [%u]: %s", sIndex, appStateName);
	}
	WriteLine_R("Initialized AppStates:");
	for (u32 sIndex = 0; sIndex < AppState_NumStates; sIndex++)
	{
		AppState_t appState = (AppState_t)(sIndex+1);
		const char* appStateName = GetAppStateName(appState);
		bool isInitialized = app->appStateInitialized[appState];
		if (isInitialized) { PrintLine_I("  %s: Initialized", appStateName); }
		else { PrintLine_W("  %s: Deinitialized", appStateName); }
	}
	
	PrintLine_R("%u Active AppMenus:", app->numActiveMenus);
	for (u32 sIndex = 0; sIndex < app->numActiveMenus; sIndex++)
	{
		AppMenu_t appMenu = app->activeMenus[sIndex];
		const char* appMenuName = GetAppMenuName(appMenu);
		PrintLine_I("  [%u]: %s", sIndex, appMenuName);
	}
	WriteLine_R("Initialized AppMenus:");
	for (u32 sIndex = 0; sIndex < AppMenu_NumMenus; sIndex++)
	{
		AppMenu_t appMenu = (AppMenu_t)(sIndex+1);
		const char* appMenuName = GetAppMenuName(appMenu);
		bool isInitialized = app->appMenuInitialized[appMenu];
		if (isInitialized) { PrintLine_I("  %s: Initialized", appMenuName); }
		else { PrintLine_W("  %s: Deinitialized", appMenuName); }
	}
}

// +==============================+
// |          resources           |
// +==============================+
CslCmd_DEFINITION(CslCmd_Resources)
{
	ResourceType_t lastResourceType = ResourceType_NumTypes;
	for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
	{
		u32 subIndex = 0;
		ResourceType_t type = GetResourceType(rIndex, &subIndex);
		if (type != lastResourceType)
		{
			PrintLine_R("%u %s Resources:", GetNumResourcesOfType(type), GetResourceTypeStr(type));
			lastResourceType = type;
		}
		PrintLine_I("  %s %u: \"%s\"", GetResourceTypeStr(type), subIndex, GetResourceFilePath(rIndex));
	}
}

// +==============================+
// |             show             |
// +==============================+
CslCmd_DEFINITION(CslCmd_Show)
{
	Assert(numArguments >= 1);
	PrintLine_I("Showing \"%s\"", arguments[0].pntr);
	//TODO: Implement me better
}

// +==============================+
// |             hide             |
// +==============================+
CslCmd_DEFINITION(CslCmd_Hide)
{
	Assert(numArguments >= 1);
	PrintLine_I("Hiding \"%s\"", arguments[0].pntr);
	//TODO: Implement me better
}

CslCmd_f* GetConsoleCommandInfo(u32 index, const char** baseCmd, u32* numArgs, const char** argName, CslArgType_t* argType, bool* argOptional)
{
	u32 cIndex = 0;
	
	// +==============================+
	// |          help info           |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "help"; *numArgs = 1;
		argName[0] = "command"; argType[0] = CslArgType_String; argOptional[0] = true;
		return CslCmd_Help;
	} cIndex++;
	
	// +==============================+
	// |          test info           |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "test"; *numArgs = 1;
		argName[0] = "test"; argType[0] = CslArgType_Uint32; argOptional[0] = true;
		return CslCmd_Test;
	} cIndex++;
	
	// +==============================+
	// |         memory info          |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "memory"; *numArgs = 0;
		return CslCmd_Memory;
	} cIndex++;
	
	// +==============================+
	// |       memory_meta info       |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "memory_meta"; *numArgs = 2;
		argName[0] = "startIndex"; argType[0] = CslArgType_Uint32; argOptional[0] = true;
		argName[1] = "numToDisplay"; argType[1] = CslArgType_Uint32; argOptional[1] = true;
		return CslCmd_MemoryMeta;
	} cIndex++;
	
	// +==============================+
	// |         states info          |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "states"; *numArgs = 0;
		return CslCmd_States;
	} cIndex++;
	
	// +==============================+
	// |        resources info        |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "resources"; *numArgs = 0;
		return CslCmd_Resources;
	} cIndex++;
	
	// +==============================+
	// |          show info           |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "show"; *numArgs = 1;
		argName[0] = "identifier"; argType[0] = CslArgType_String;
		return CslCmd_Show;
	} cIndex++;
	
	// +==============================+
	// |          hide info           |
	// +==============================+
	if (index == cIndex)
	{
		*baseCmd = "hide"; *numArgs = 1;
		argName[0] = "identifier"; argType[0] = CslArgType_String;
		return CslCmd_Hide;
	} cIndex++;
	
	return nullptr;
}

// +==============================+
// |             help             |
// +==============================+
CslCmd_DEFINITION(CslCmd_Help)
{
	const char* cmdName = nullptr;
	u32 cmdArgCount = 0;
	const char* cmdArgNames[CSL_MAX_CMD_ARGUMENTS];
	CslArgType_t cmdArgTypes[CSL_MAX_CMD_ARGUMENTS];
	bool cmdArgOptionals[CSL_MAX_CMD_ARGUMENTS];
	if (numArguments == 0)
	{
		for (u32 cIndex = 0; true; cIndex++)
		{
			cmdName = nullptr;
			cmdArgCount = 0;
			ClearArray(cmdArgNames);
			ClearArray(cmdArgTypes);
			ClearArray(cmdArgOptionals);
			CslCmd_f* cmdFunc = GetConsoleCommandInfo(cIndex, &cmdName, &cmdArgCount, &cmdArgNames[0], &cmdArgTypes[0], &cmdArgOptionals[0]);
			if (cmdFunc == nullptr) { break; }
			Assert(cmdName != nullptr);
			Assert(cmdArgCount <= CSL_MAX_CMD_ARGUMENTS);
			u32 cmdNameLength = (u32)strlen(cmdName);
			
			Print_I("[%u] %s", cIndex, cmdName);
			for (u32 aIndex = 0; aIndex < cmdArgCount; aIndex++)
			{
				const char* argName = cmdArgNames[aIndex];
				bool optional = cmdArgOptionals[aIndex];
				if (optional) { Print_I(" {%s}", argName); }
				else { Print_I(" [%s]", argName); }
			}
			WriteLine_I("");
		}
	}
	else
	{
		bool foundCommand = false;
		for (u32 cIndex = 0; true; cIndex++)
		{
			cmdName = nullptr;
			cmdArgCount = 0;
			ClearArray(cmdArgNames);
			ClearArray(cmdArgTypes);
			ClearArray(cmdArgOptionals);
			CslCmd_f* cmdFunc = GetConsoleCommandInfo(cIndex, &cmdName, &cmdArgCount, &cmdArgNames[0], &cmdArgTypes[0], &cmdArgOptionals[0]);
			if (cmdFunc == nullptr) { break; }
			Assert(cmdName != nullptr);
			Assert(cmdArgCount <= CSL_MAX_CMD_ARGUMENTS);
			u32 cmdNameLength = (u32)strlen(cmdName);
			
			if (cmdNameLength == arguments[0].length && StrCompareIgnoreCase(cmdName, arguments[0].pntr, cmdNameLength))
			{
				Print_I("[%u] %s", cIndex, cmdName);
				for (u32 aIndex = 0; aIndex < cmdArgCount; aIndex++)
				{
					const char* argName = cmdArgNames[aIndex];
					bool optional = cmdArgOptionals[aIndex];
					if (optional) { Print_I(" {%s}", argName); }
					else { Print_I(" [%s]", argName); }
				}
				WriteLine_I("");
				foundCommand = true;
				break;
			}
		}
		if (!foundCommand)
		{
			PrintLine_E("Unknown command: \"%.*s\"", arguments[0].length, arguments[0].pntr);
		}
	}
}
