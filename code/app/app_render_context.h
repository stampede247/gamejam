/*
File:   app_render_context.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_RENDER_CONTEXT_H
#define _APP_RENDER_CONTEXT_H

struct RenderContextState_t
{
	rec viewport;
	r32 depth;
	const Shader_t* boundShader;
	const FrameBuffer_t* boundFrameBuffer;
	const VertexBuffer_t* boundBuffer;
	
	Font_t* boundFont;
	r32 fontSize;
	u16 fontStyle;
	Alignment_t fontAlignment;
	
	mat4 worldMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	
	const Texture_t* boundTexture;
	rec sourceRectangle;
	rec maskRectangle;
	
	const Texture_t* boundAltTexture;
	bool useAltTexture;
	rec altSourceRectangle;
	bool gradientEnabled;
	
	Color_t primaryColor;
	Color_t secondaryColor;
	Color_t replaceColor1;
	Color_t replaceColor2;
	Color_t replaceColor3;
	Color_t replaceColor4;
	
	r32 circleRadius;
	r32 circleInnerRadius;
	r32 vigRadius;
	r32 vigSmoothness;
	
	r32 time;
	r32 saturation;
	r32 brightness;
	
	r32 value0;
	r32 value1;
	r32 value2;
	r32 value3;
	r32 value4;
};

struct RenderContext_t
{
	VertexBuffer_t squareBuffer;
	VertexBuffer_t rightTriangleBuffer;
	VertexBuffer_t equilTriangleBuffer;
	Texture_t dotTexture;
	Texture_t gradientTexture;
	
	u32 numBatchingFonts;
	const FontBake_t* batchingFontBakes[MAX_BATCHING_FONTS];
	DynArray_t batchingVerts[MAX_BATCHING_FONTS];
	VertexBuffer_t batchVertBuffers[MAX_BATCHING_FONTS];
	u32 numBatchedGlyphs;
	u32 numDrawnGlyphs;
	
	u32 numPushedStates;
	RenderContextState_t pushedStates[MAX_RENDER_CONTEXT_PUSHES];
	
	VertexBuffer_t dynamicBuffer;
	
	rec viewport;
	r32 depth;
	const Shader_t* boundShader;
	const FrameBuffer_t* boundFrameBuffer;
	const VertexBuffer_t* boundBuffer;
	
	Font_t* boundFont;
	r32 fontSize;
	u16 fontStyle;
	Alignment_t fontAlignment;
	
	mat4 worldMatrix;
	mat4 viewMatrix;
	mat4 projectionMatrix;
	
	const Texture_t* boundTexture;
	rec sourceRectangle;
	rec maskRectangle;
	
	const Texture_t* boundAltTexture;
	bool useAltTexture;
	rec altSourceRectangle;
	bool gradientEnabled;
	
	Color_t primaryColor;
	Color_t secondaryColor;
	Color_t replaceColor1;
	Color_t replaceColor2;
	Color_t replaceColor3;
	Color_t replaceColor4;
	
	r32 circleRadius;
	r32 circleInnerRadius;
	r32 vigRadius;
	r32 vigSmoothness;
	
	r32 time;
	r32 saturation;
	r32 brightness;
	
	r32 value0;
	r32 value1;
	r32 value2;
	r32 value3;
	r32 value4;
};

#endif //  _APP_RENDER_CONTEXT_H
