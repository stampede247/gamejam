/*
File:   app_font.h
Author: Taylor Robbins
Date:   07\26\2019
*/

#ifndef _APP_FONT_H
#define _APP_FONT_H

typedef enum
{
	Alignment_Left,
	Alignment_Center,
	Alignment_Right,
} Alignment_t;

typedef enum
{
	FontStyle_Default = 0x0000,
	
	FontStyle_Bold         = 0x0001,
	FontStyle_Italic       = 0x0002,
	FontStyle_Underline    = 0x0004,
	FontStyle_Wave         = 0x0008, //0x1
	FontStyle_Bubble       = 0x0010, //0x2
	FontStyle_Bounce       = 0x0020, //0x3
	FontStyle_Jitter       = 0x0040, //0x4
	FontStyle_Wobble       = 0x0080, //0x5
	FontStyle_Faded        = 0x0100, //0x6
	FontStyle_Rainbow      = 0x0200, //0x7
	FontStyle_Emboss       = 0x0400, //0x8
	
	FontStyle_RawData      = 0x1000,
	FontStyle_CreateGlyphs = 0x2000,
	FontStyle_StrictStyle  = 0x4000,
	FontStyle_StrictSize   = 0x8000,
	
	FontStyle_BoldItalic = FontStyle_Bold|FontStyle_Italic,
	FontStyle_BoldUnderline = FontStyle_Bold|FontStyle_Underline,
	FontStyle_ItalicUnderline = FontStyle_Italic|FontStyle_Underline,
	FontStyle_BoldItalicUnderline = FontStyle_Bold|FontStyle_Italic|FontStyle_Underline,
	
	FontStyle_BakeMask = FontStyle_Bold|FontStyle_Italic,
	FontStyle_StrictSizeStyle = FontStyle_StrictSize|FontStyle_StrictStyle,
} FontStyle_t;

struct FontCharInfo_t
{
	union
	{
		reci bakeRec;
		struct
		{
			v2i bakePos;
			v2i bakeSize;
		};
	};
	v2 size;
	v2 origin;
	r32 advanceX;
};

struct FontFile_t
{
	u16 styleFlags;
	u32 fileLength;
	u8* fileData;
	stbtt_fontinfo stbInfo;
};

struct FontBake_t
{
	bool monospaced;
	bool allowScaling;
	u8 firstChar;
	u8 numChars;
	u8 unknownCharIndex;
	r32 size; //in pixels
	u16 styleFlags;
	FontCharInfo_t* charInfos;
	r32 maxExtendUp;
	r32 maxExtendDown;
	r32 lineHeight;
	Texture_t texture;
};

struct FontGlyph_t
{
	u8 c;
	r32 size; //in pixels
	u16 styleFlags;
	FontCharInfo_t charInfo;
	Texture_t texture;
};

struct Font_t
{
	MemoryArena_t* allocArena;
	
	u32 numFiles;
	FontFile_t* files;
	
	u32 numBakes;
	FontBake_t* bakes;
	
	u32 numGlyphs;
	u32 maxGlyphs;
	FontGlyph_t* glyphs;
};

struct FontBatchVertex_t
{
	v3 position; //includes depth
	v4 color;
	rec sourceRec;
	r32 scale;
};

typedef void FontFlowSelectionEnd_f(rec backRec, const char* textPntr, u32 textLength, void* userPntr);

struct FontFlowInfo_t
{
	//Passed arguments
	const char* strPntr;
	u32 strLength;
	v2 position;
	Font_t* fontPntr;
	Alignment_t alignment;
	r32 fontSize;
	u16 styleFlags;
	
	//Results
	rec extents;
	v2 endPos;
	u32 numLines;
	u32 numRenderables;
	r32 extentRight;
	r32 extentDown;
	v2 totalSize;
};

struct FontFlowExtra_t
{
	//Hover Calculation
	bool calculateMouseIndex;
	v2 mousePos;
	i32 mouseIndex; //output
	v2 mouseIndexPos; //output
	
	bool calculateIndexPos;
	u32 index;
	v2 indexPos; //output
	
	//Background drawing
	bool doSelection;
	i32 selStart;
	i32 selEnd;
	bool selChangesColor;
	Color_t selColor;
	bool selChangesStyle;
	u16 selStyle;
	void* selEndFunctionUserPntr;
	FontFlowSelectionEnd_f* selEndFunction; //called as output
};

struct FontChar_t
{
	bool baked;
	FontBake_t* bake;
	Texture_t* texture;
	FontCharInfo_t* info;
	r32 scale;
};

#define C_RESET_ALL "\v\xFF"

#define C_BOLD "\b"
#define C_ITALIC "\r"
#define C_UNDERLINE "\a"

#define C_COLOR_DEF "\x02"
// #define C_COLOR_SET(R, G, B) "\x01\xR\xG\xB"

#define C_SIZE_DEF "\x04"
// #define C_SIZE_SET(S) "\x03\xS"

#define C_WAVE_ON "\v\x11"
#define C_WAVE_OFF "\v\x10"

#define C_BUBBLE_ON "\v\x21"
#define C_BUBBLE_OFF "\v\x20"

#define C_BOUNCE_ON "\v\x31"
#define C_BOUNCE_OFF "\v\x30"

#define C_JITTER_ON "\v\x41"
#define C_JITTER_OFF "\v\x40"

#define C_WOBBLE_ON "\v\x51"
#define C_WOBBLE_OFF "\v\x50"

#define C_FADED_ON "\v\x61"
#define C_FADED_OFF "\v\x60"

#define C_RAINBOW_ON "\v\x71"
#define C_RAINBOW_OFF "\v\x70"

#define C_EMBOSS_ON "\v\x81"
#define C_EMBOSS_OFF "\v\x80"

#endif //  _APP_FONT_H
