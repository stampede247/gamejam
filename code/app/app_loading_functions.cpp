/*
File:   app_loading_functions.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Includes a collection of functions that manage loading
	** various sorts of resources
*/

Texture_t CreateTexture(const u8* bitmapData, i32 width, i32 height, bool pixelated, bool repeat) //Pre-declared in app_func_defs.h
{
	Texture_t result = {};
	
	result.width = width;
	result.height = height;
	
	// platform->LockOpenglContext();
	glGenTextures(1, &result.id);
	glBindTexture(GL_TEXTURE_2D, result.id);
	
	glTexImage2D(
		GL_TEXTURE_2D, 		//bound texture type
		0,					//image level
		GL_RGBA,			//internal format
		width,		        //image width
		height,		        //image height
		0,					//border
		GL_RGBA,			//format
		GL_UNSIGNED_BYTE,	//type
		bitmapData  		//data
	);
	
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, pixelated ? GL_NEAREST_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, pixelated ? GL_NEAREST : GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, repeat ? GL_REPEAT : GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glGenerateMipmap(GL_TEXTURE_2D);
	
	// platform->UnlockOpenglContext();
	return result;
}
Texture_t LoadTexture(const char* fileName, bool pixelated = false, bool repeat = true)
{
	Texture_t result = {};
	bool isValid = true;
	
	FileInfo_t textureFile = platform->ReadEntireFile(fileName);
	
	if (textureFile.content == nullptr)
	{
		PrintLine_E("Texture is missing: \"%s\"", fileName);
		textureFile = platform->ReadEntireFile(MISSING_TEXTURE_PATH);
		Assert(textureFile.content != nullptr);
		isValid = false;
	}
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&width, &height, &numChannels, 4);
	
	Assert(imageData != nullptr);
	Assert(width > 0 && height > 0);
	
	result = CreateTexture(imageData, width, height, pixelated, repeat);
	
	stbi_image_free(imageData);
	platform->FreeFileMemory(&textureFile);
	
	result.isValid = isValid;
	return result;
}
void DestroyTexture(Texture_t* texturePntr) //Pre-declared in app_func_defs.h
{
	Assert(texturePntr != nullptr);
	// platform->LockOpenglContext();
	glDeleteTextures(1, &texturePntr->id);
	// platform->UnlockOpenglContext();
	ClearPointer(texturePntr);
}

void DestroyVertexBuffer(VertexBuffer_t* bufferPntr)
{
	Assert(bufferPntr != nullptr);
	// platform->LockOpenglContext();
	glDeleteBuffers(1, &bufferPntr->id);
	// platform->UnlockOpenglContext();
	ClearPointer(bufferPntr);
}
VertexBuffer_t CreateVertexBuffer(const Vertex_t* vertices, u32 numVertices)
{
	VertexBuffer_t result = {};
	result.numVertices = numVertices;
	
	// platform->LockOpenglContext();
	glGenBuffers(1, &result.id);
	glBindBuffer(GL_ARRAY_BUFFER, result.id);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_t) * numVertices, vertices, GL_STATIC_DRAW);
	// platform->UnlockOpenglContext();
	
	return result;
}
void DynamicVertexBufferExpand(VertexBuffer_t* vertBuffer, u32 numVertices, u32 chunkSize = DYNAMIC_VERT_BUFFER_CHUNK_SIZE)
{
	if (numVertices == 0) { return; }
	// platform->LockOpenglContext();
	if (vertBuffer->numVertices == 0)
	{
		vertBuffer->isDynamic = true;
		vertBuffer->numVertices = 0;
		glGenBuffers(1, &vertBuffer->id);
	}
	if (vertBuffer->numVertices < numVertices)
	{
		Assert(vertBuffer->isDynamic);
		
		u32 newSize = vertBuffer->numVertices;
		while (newSize < numVertices) { newSize += chunkSize; }
		glBindBuffer(GL_ARRAY_BUFFER, vertBuffer->id);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex_t) * newSize, nullptr, GL_DYNAMIC_DRAW); //uninitialized
		vertBuffer->numVertices = newSize;
	}
	// platform->UnlockOpenglContext();
}
void DynamicVertexBufferFill(VertexBuffer_t* vertBuffer, const Vertex_t* vertices, u32 numVertices)
{
	Assert(vertBuffer != nullptr);
	Assert(vertices != nullptr || numVertices == 0);
	Assert(numVertices <= vertBuffer->numVertices);
	Assert(vertBuffer->isDynamic);
	if (numVertices == 0) { return; }
	
	// platform->LockOpenglContext();
	glBindBuffer(GL_ARRAY_BUFFER, vertBuffer->id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex_t) * numVertices, vertices);
	// platform->UnlockOpenglContext();
}

u32 FindFragmentShaderSplit(const char* fileContents, u32 fileLength)
{
	Assert(fileContents != nullptr);
	if (fileLength == 0) { return 0; }
	u32 result = 0;
	
	//Find the titleStr
	const char* titleStr = "FRAGMENT_SHADER";
	u32 titleStrLength = (u32)strlen(titleStr);
	bool foundTitleStr = false;
	u32 titleStrIndex = 0;
	for (u32 cIndex = 0; cIndex+titleStrLength < fileLength; cIndex++)
	{
		if (strncmp(&fileContents[cIndex], titleStr, titleStrLength) == 0)
		{
			foundTitleStr = true;
			titleStrIndex = cIndex;
			break;
		}
	}
	if (!foundTitleStr) { return 0; }
	
	//Go back to previous line break
	bool foundLineBreak = false;
	for (u32 cIndex = titleStrIndex; cIndex > 0; cIndex--)
	{
		if (fileContents[cIndex] == '\n' || fileContents[cIndex] == '\r')
		{
			titleStrIndex = cIndex+1;
			foundLineBreak = true;
			break;
		}
	}
	if (!foundLineBreak) { return 0; }
	
	return titleStrIndex;
}
Shader_t LoadShader(const char* filePath)
{
	Shader_t result = {};
	result.isValid = false;
	GLint compiled;
	int logLength;
	char* logBuffer;
	
	FileInfo_t shaderFile = platform->ReadEntireFile(filePath);
	if (shaderFile.content == nullptr)
	{
		PrintLine_W("Couldn't open shader file \"%s\"", filePath);
		return result;
	}
	
	u32 splitIndex = FindFragmentShaderSplit((const char*)shaderFile.content, shaderFile.size);
	if (splitIndex == 0 || splitIndex >= shaderFile.size)
	{
		PrintLine_W("Couldn't find the split between vertex and fragment shader in \"%s\"", filePath);
		return result;
	}
	
	TempPushMark();
	// platform->LockOpenglContext();
	u32 vertShaderLength = splitIndex;
	char* vertShader = PushArray(TempArena, char, vertShaderLength+1);
	memcpy(vertShader, (const char*)shaderFile.content, vertShaderLength);
	vertShader[vertShaderLength] = '\0';
	
	u32 fragShaderLength = shaderFile.size - splitIndex;
	char* fragShader = PushArray(TempArena, char, fragShaderLength+1);
	memcpy(fragShader, (const char*)shaderFile.content + splitIndex, fragShaderLength);
	fragShader[fragShaderLength] = '\0';
	
	result.vertId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(result.vertId, 1, (const GLchar* const*)&vertShader, NULL);
	glCompileShader(result.vertId);
	
	glGetShaderiv(result.vertId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.vertId, GL_INFO_LOG_LENGTH, &logLength);
	if (compiled) { PrintLine_D("%s: Compiled Successfully: %d byte log", GetFileNamePart(filePath), logLength); }
	else { PrintLine_E("%s: Compilation Failed: %d byte log", GetFileNamePart(filePath), logLength); }
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetShaderInfoLog(result.vertId, logLength, NULL, logBuffer);
		PrintLine_W("Log: \"%s\"", logBuffer);
	}
	
	result.fragId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(result.fragId, 1, (const GLchar* const*)&fragShader, NULL);
	glCompileShader(result.fragId);
	
	glGetShaderiv(result.fragId, GL_COMPILE_STATUS, &compiled);
	glGetShaderiv(result.fragId, GL_INFO_LOG_LENGTH, &logLength);
	if (compiled) { PrintLine_D("%s: Compiled Successfully: %d byte log", GetFileNamePart(filePath), logLength); }
	else { PrintLine_E("%s: Compilation Failed: %d byte log", GetFileNamePart(filePath), logLength); }
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetShaderInfoLog(result.fragId, logLength, NULL, logBuffer);
		PrintLine_W("Log: \"%s\"", logBuffer);
	}
	
	platform->FreeFileMemory(&shaderFile);
	TempPopMark();
	
	result.programId = glCreateProgram();
	glAttachShader(result.programId, result.fragId);
	glAttachShader(result.programId, result.vertId);
	glLinkProgram(result.programId);
	
	glGetProgramiv(result.programId, GL_LINK_STATUS, &compiled);
	glGetProgramiv(result.programId, GL_INFO_LOG_LENGTH, &logLength);
	if (compiled) { PrintLine_D("Shader: Linked Successfully: %d byte log", logLength); }
	else { PrintLine_E("Shader: Linking Failed: %d byte log", logLength); }
	if (logLength > 0)
	{
		logBuffer = TempString(logLength+1);
		logBuffer[logLength] = '\0';
		glGetProgramInfoLog(result.programId, logLength, NULL, logBuffer);
		PrintLine_W("Log: \"%s\"", logBuffer);
	}
	
	result.locations.positionAttrib      = glGetAttribLocation(result.programId, "inPosition");
	result.locations.colorAttrib         = glGetAttribLocation(result.programId, "inColor");
	result.locations.texCoordAttrib      = glGetAttribLocation(result.programId, "inTexCoord");
	
	result.locations.worldMatrix        = glGetUniformLocation(result.programId, "WorldMatrix");
	result.locations.viewMatrix         = glGetUniformLocation(result.programId, "ViewMatrix");
	result.locations.projectionMatrix   = glGetUniformLocation(result.programId, "ProjectionMatrix");
	
	result.locations.texture            = glGetUniformLocation(result.programId, "Texture");
	result.locations.textureSize        = glGetUniformLocation(result.programId, "TextureSize");
	result.locations.sourceRectangle    = glGetUniformLocation(result.programId, "SourceRectangle");
	result.locations.maskRectangle      = glGetUniformLocation(result.programId, "MaskRectangle");
	
	result.locations.altTexture         = glGetUniformLocation(result.programId, "AltTexture");
	result.locations.altTextureSize     = glGetUniformLocation(result.programId, "AltTextureSize");
	result.locations.useAltTexture      = glGetUniformLocation(result.programId, "UseAltTexture");
	result.locations.altSourceRectangle = glGetUniformLocation(result.programId, "AltSourceRectangle");
	result.locations.gradientEnabled    = glGetUniformLocation(result.programId, "GradientEnabled");
	
	result.locations.primaryColor       = glGetUniformLocation(result.programId, "PrimaryColor");
	result.locations.secondaryColor     = glGetUniformLocation(result.programId, "SecondaryColor");
	result.locations.replaceColor1      = glGetUniformLocation(result.programId, "ReplaceColor1");
	result.locations.replaceColor2      = glGetUniformLocation(result.programId, "ReplaceColor2");
	result.locations.replaceColor3      = glGetUniformLocation(result.programId, "ReplaceColor3");
	result.locations.replaceColor4      = glGetUniformLocation(result.programId, "ReplaceColor4");
	
	result.locations.circleRadius       = glGetUniformLocation(result.programId, "CircleRadius");
	result.locations.circleInnerRadius  = glGetUniformLocation(result.programId, "CircleInnerRadius");
	result.locations.vignette           = glGetUniformLocation(result.programId, "Vignette");
	
	result.locations.time               = glGetUniformLocation(result.programId, "Time");
	result.locations.saturation         = glGetUniformLocation(result.programId, "Saturation");
	result.locations.brightness         = glGetUniformLocation(result.programId, "Brightness");
	
	result.locations.value0             = glGetUniformLocation(result.programId, "Value0");
	result.locations.value1             = glGetUniformLocation(result.programId, "Value1");
	result.locations.value2             = glGetUniformLocation(result.programId, "Value2");
	result.locations.value3             = glGetUniformLocation(result.programId, "Value3");
	result.locations.value4             = glGetUniformLocation(result.programId, "Value4");
	
	// PrintLine_D("positionAttrib: %d", result.locations.positionAttrib);
	// PrintLine_D("colorAttrib:    %d", result.locations.colorAttrib);
	// PrintLine_D("texCoordAttrib: %d", result.locations.texCoordAttrib);
	// PrintLine_D("worldMatrix:      %d", result.locations.worldMatrix);
	// PrintLine_D("viewMatrix:       %d", result.locations.viewMatrix);
	// PrintLine_D("projectionMatrix: %d", result.locations.projectionMatrix);
	// PrintLine_D("texture:          %d", result.locations.texture);
	// PrintLine_D("textureSize:      %d", result.locations.textureSize);
	// PrintLine_D("sourceRectangle:  %d", result.locations.sourceRectangle);
	// PrintLine_D("primaryColor:     %d", result.locations.primaryColor);
	
	glGenVertexArrays(1, &result.vertexArray);
	glBindVertexArray(result.vertexArray);
	glEnableVertexAttribArray(result.locations.positionAttrib);
	glEnableVertexAttribArray(result.locations.colorAttrib);
	glEnableVertexAttribArray(result.locations.texCoordAttrib);
	
	result.isValid = true;
	// platform->UnlockOpenglContext();
	return result;
}

void DestroyShader(Shader_t* shaderPntr)
{
	Assert(shaderPntr != nullptr);
	// platform->LockOpenglContext();
	glDeleteVertexArrays(1, &shaderPntr->vertexArray);
	glDeleteProgram(shaderPntr->programId);
	glDeleteShader(shaderPntr->vertId);
	glDeleteShader(shaderPntr->fragId);
	// platform->UnlockOpenglContext();
	ClearPointer(shaderPntr);
}

SpriteSheet_t LoadSpriteSheet(const char* fileName, u32 padding, v2i numFrames, bool pixelated = true)
{
	SpriteSheet_t result = {};
	
	FileInfo_t textureFile = platform->ReadEntireFile(fileName);
	if (textureFile.content == nullptr)
	{
		PrintLine_E("Failed to load spritesheet \"%s\"!", fileName);
		//TODO: Can we handle this better somehow? Maybe let the calling party know?
		return result;
	}
	
	i32 numChannels;
	v2i bitmapSize;
	u32* bitmapData = (u32*)stbi_load_from_memory(
		(u8*)textureFile.content, textureFile.size,
		&bitmapSize.width, &bitmapSize.height, &numChannels, 4);
	
	v2i paddingVec = NewVec2i(padding);
	v2i frameSize = NewVec2i(bitmapSize.width / numFrames.x, bitmapSize.height / numFrames.y);
	v2i newFrameSize = frameSize + paddingVec*2;
	v2i newSize = Vec2iMultiply(newFrameSize, numFrames);
	
	TempPushMark();
	u32* newData = PushArray(TempArena, u32, newSize.width*newSize.height);
	memset(newData, 0x00, sizeof(u32)*newSize.width*newSize.height);
	for (i32 frameY = 0; frameY < numFrames.y; frameY++)
	{
		for (i32 frameX = 0; frameX < numFrames.x; frameX++)
		{
			v2i frame = NewVec2i(frameX, frameY);
			v2i framePos = Vec2iMultiply(frameSize, frame);
			v2i newFramePos = Vec2iMultiply(newFrameSize, frame) + paddingVec;
			
			for (i32 yPos = 0; yPos < frameSize.y; yPos++)
			{
				for (i32 xPos = 0; xPos < frameSize.x; xPos++)
				{
					v2i sourcePos = framePos + NewVec2i(xPos, yPos);
					u32* source = &bitmapData[(sourcePos.y*bitmapSize.width) + sourcePos.x];
					
					v2i newPos = newFramePos + NewVec2i(xPos, yPos);
					newData[(newPos.y * newSize.width) + newPos.x] = *source;
					
					if (xPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(-1, 0);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(0, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1)
					{
						v2i adjPos = newPos + NewVec2i(1, 0);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(0, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					
					if (xPos == 0 && yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(-1, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1 && yPos == 0)
					{
						v2i adjPos = newPos + NewVec2i(1, -1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == 0 && yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(-1, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
					if (xPos == frameSize.width-1 && yPos == frameSize.height-1)
					{
						v2i adjPos = newPos + NewVec2i(1, 1);
						newData[(adjPos.y * newSize.width) + adjPos.x] = *source;
					}
				}
			}
		}
	}
	
	result.texture = CreateTexture((u8*)newData, newSize.width, newSize.height, pixelated, false);
	result.padding = paddingVec;
	result.frameSize = newFrameSize;
	result.innerFrameSize = newFrameSize - paddingVec*2;
	result.numFrames = numFrames;
	
	stbi_image_free((u8*)bitmapData);
	platform->FreeFileMemory(&textureFile);
	TempPopMark();
	
	return result;
}
void DestroySpriteSheet(SpriteSheet_t* sheetPntr)
{
	Assert(sheetPntr != nullptr);
	DestroyTexture(&sheetPntr->texture);
	ClearPointer(sheetPntr);
}

bool LoadFontSimple(Font_t* fontOut, MemoryArena_t* memArena, const char* fontFilePath, r32 fontSize, u32 bitmapWidth = 512, u32 bitmapHeight = 512)
{
	Assert(fontOut != nullptr);
	Assert(memArena != nullptr);
	Assert(fontFilePath != nullptr);
	Assert(fontSize > 0);
	
	FileInfo_t fontFile = platform->ReadEntireFile(fontFilePath);
	if (fontFile.content == nullptr)
	{
		PrintLine_W("Failed to open font file at \"%s\"", fontFilePath);
		return false;
	}
	
	AppCreateFont(fontOut, memArena);
	if (!FontAddFile(fontOut, fontFile.content, fontFile.size))
	{
		PrintLine_E("Failed to parse font from \"%s\"", fontFilePath);
		platform->FreeFileMemory(&fontFile);
		AppDestroyFont(fontOut);
		return false;
	}
	if (!FontBake(fontOut, fontSize, FontStyle_Default, 0x20, 96, bitmapWidth, bitmapHeight))
	{
		PrintLine_E("Failed to bake font from \"%s\" at %f scale", fontFilePath, fontSize);
		platform->FreeFileMemory(&fontFile);
		AppDestroyFont(fontOut);
		return false;
	}
	
	FontDropFiles(fontOut);
	platform->FreeFileMemory(&fontFile);
	
	return true;
}

bool LoadBitmapFont(Font_t* fontOut, MemoryArena_t* memArena, const char* bitmapPath, v2i charSize, r32 charAdvanceX, r32 baseLine, u8 firstChar = 0x00)
{
	Assert(fontOut != nullptr);
	Assert(memArena != nullptr);
	Assert(bitmapPath != nullptr);
	Assert(charSize.width > 0 && charSize.height > 0);
	
	Texture_t bitmap = LoadTexture(bitmapPath, true, false);
	if (!bitmap.isValid)
	{
		PrintLine_W("Failed to load texture for bitmap font from \"%s\"", bitmapPath);
		return false;
	}
	
	v2i cellGridSize = NewVec2i(bitmap.width / charSize.width, bitmap.height / charSize.height);
	if (cellGridSize.width * cellGridSize.height <= 0)
	{
		PrintLine_W("Bitmap was too small to fit any characters: bitmap %dx%d cell %dx%d", bitmap.width, bitmap.height, charSize.width, charSize.height);
		DestroyTexture(&bitmap);
		return false;
	}
	u32 numCharacters = (u32)(cellGridSize.width * cellGridSize.height);
	if (numCharacters > 255u - firstChar)
	{
		// PrintLine_W("Bitmap font has too many characters in it. %dx%d = %u characters", cellGridSize.width, cellGridSize.height, numCharacters);
		numCharacters = 255u - firstChar;
	}
	
	AppCreateFont(fontOut, memArena);
	
	FontBake_t* fontBake = FontAllocateBake(fontOut);
	Assert(fontBake != nullptr);
	ClearPointer(fontBake);
	fontBake->texture = bitmap;
	fontBake->firstChar = firstChar;
	if (firstChar <= 0x7F && firstChar + numCharacters > 0x7F) { fontBake->unknownCharIndex = 0x7F; }
	else if (firstChar <= '?' && firstChar + numCharacters > '?') { fontBake->unknownCharIndex = '?'; }
	else { fontBake->unknownCharIndex = firstChar; }
	fontBake->numChars = (u8)numCharacters;
	fontBake->size = (r32)charSize.height; //TODO: Should we just make this value 1 so font scaling stuff later is simple?
	fontBake->styleFlags = FontStyle_Default;
	fontBake->allowScaling = true;
	
	fontBake->charInfos = PushArray(fontOut->allocArena, FontCharInfo_t, fontBake->numChars);
	for (u8 cIndex = 0; cIndex < fontBake->numChars; cIndex++)
	{
		FontCharInfo_t* charPntr = &fontBake->charInfos[cIndex];
		v2i cell = NewVec2i(cIndex % cellGridSize.width, cIndex / cellGridSize.width);
		charPntr->bakePos  = NewVec2i(charSize.width * cell.x, charSize.height * cell.y);
		charPntr->bakeSize = NewVec2i(charSize.width, charSize.height);
		charPntr->size = NewVec2(charSize);
		charPntr->origin = NewVec2(0, baseLine);
		charPntr->advanceX = charAdvanceX;
	}
	
	fontBake->maxExtendUp = 0;
	fontBake->maxExtendDown = 0;
	fontBake->monospaced = true;
	for (u8 cIndex = 0; cIndex < fontBake->numChars; cIndex++)
	{
		FontCharInfo_t* charInfoPntr = &fontBake->charInfos[cIndex];
		if (fontBake->maxExtendUp < charInfoPntr->origin.y) { fontBake->maxExtendUp = charInfoPntr->origin.y; }
		if (fontBake->maxExtendDown < charInfoPntr->size.height - charInfoPntr->origin.y)
		{
			fontBake->maxExtendDown = charInfoPntr->size.height - charInfoPntr->origin.y;
		}
		if (cIndex > 0 && fontBake->charInfos[cIndex-1].advanceX != fontBake->charInfos[cIndex].advanceX)
		{
			fontBake->monospaced = false;
		}
	}
	fontBake->lineHeight = fontBake->maxExtendDown + fontBake->maxExtendUp;
	
	return true;
}

#if AUDIO_ENABLED
Sound_t LoadSound(const char* fileName)
{
	Sound_t result = {};
	
	FileInfo_t sndFile = platform->ReadEntireFile(fileName);
	if (sndFile.content == nullptr)
	{
		PrintLine_E("Can't open sound file \"%s\"", fileName);
		Assert(false);
		return result;
	}
	
	//TODO: Should we check the file extension rather than the contents?
	WAV_Header_t* wavHeader = (WAV_Header_t*)sndFile.content;
	if (wavHeader->RIFFID == WAV_ChunkID_RIFF &&
		wavHeader->WAVEID == WAV_ChunkID_WAVE)
	{
		bool parseSuccess = ParseWavFile(&sndFile, &result);
		if (parseSuccess)
		{
			result.isValid = true;
		}
		else
		{
			PrintLine_E("Failed to parse \"%s\" WAV file", fileName);
			result.isValid = false;
		}
	}
	else //TODO: Should we check to see if it's a ogg file somehow before trying to parse it as ogg?
	{
		#if 0
		DestroyOggSampleBuffer(&app->audioSampleBuffer);
		bool parseSuccess = ParseOggFile(&sndFile, &result, &app->audioSampleBuffer);
		#else
		bool parseSuccess = ParseOggFile(&sndFile, &result);
		#endif
		if (parseSuccess)
		{
			result.isValid = true;
		}
		else
		{
			PrintLine_E("Failed to parse \"%s\" OGG file", fileName);
			result.isValid = false;
		}
	}
	
	platform->FreeFileMemory(&sndFile);
	return result;
}
void DestroySound(Sound_t* soundPntr)
{
	Assert(soundPntr != nullptr);
	alDeleteBuffers(1, &soundPntr->id);
	ClearPointer(soundPntr);
}
#else
Sound_t LoadSound(const char* fileName)
{
	Sound_t result = {};
	return result;
}
void DestroySound(Sound_t* soundPntr)
{
	Assert(soundPntr != nullptr);
}
#endif

FrameBuffer_t CreateFrameBuffer(v2i size, bool includeTransparency = true, bool includeStencil = false)
{
	Assert(size.x > 0 && size.y > 0);
	FrameBuffer_t result = {};
	result.hasTransparency = includeTransparency;
	result.hasStencil = includeStencil;
	
	// platform->LockOpenglContext();
	glGenFramebuffers(1, &result.id);
	glBindFramebuffer(GL_FRAMEBUFFER, result.id);
	
	result.texture.size = size;
	glGenTextures(1, &result.texture.id);
	glBindTexture(GL_TEXTURE_2D, result.texture.id);
	if (includeTransparency)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, size.x, size.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size.x, size.y, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	}
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	
	glGenRenderbuffers(1, &result.depthBuffId);
	glBindRenderbuffer(GL_RENDERBUFFER, result.depthBuffId);
	if (includeStencil)
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, size.width, size.height);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, result.depthBuffId);
	}
	else
	{
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, size.width, size.height);
	}
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, result.depthBuffId);
	
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, result.texture.id, 0);
	GLenum drawBuffers[1] = {GL_COLOR_ATTACHMENT0};
	glDrawBuffers(1, drawBuffers);
	
	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		Print_E("Failed to create framebuffer. Status = ");
		switch (status)
		{
			case GL_FRAMEBUFFER_UNDEFINED:                     WriteLine_E("GL_FRAMEBUFFER_UNDEFINED"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:         WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:        WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:        WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER"); break;
			case GL_FRAMEBUFFER_UNSUPPORTED:                   WriteLine_E("GL_FRAMEBUFFER_UNSUPPORTED"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:        WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE"); break;
			case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:      WriteLine_E("GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS"); break;
			default: PrintLine_E("%d", status);
		}
		Assert(false);
	}
	
	// platform->UnlockOpenglContext();
	return result;
}
void DestroyFrameBuffer(FrameBuffer_t* frameBufferPntr)
{
	Assert(frameBufferPntr != nullptr);
	// platform->LockOpenglContext();
	glDeleteTextures(1, &frameBufferPntr->texture.id);
	glDeleteRenderbuffers(1, &frameBufferPntr->id);
	// platform->UnlockOpenglContext();
	ClearPointer(frameBufferPntr);
}

void CreateDirectories(const char* directoryPath)
{
	TempPushMark();
	
	u32 strLength = (u32)strlen(directoryPath);
	char* tempString = PushArray(TempArena, char, strLength+1);
	memcpy(tempString, directoryPath, strLength+1);
	
	for (u32 cIndex = 0; tempString[cIndex] != '\0'; cIndex++)
	{
		if (cIndex > 0 && tempString[cIndex] == '\\' || tempString[cIndex] == '/')
		{
			char oldChar = tempString[cIndex];
			tempString[cIndex] = '\0';
			if (!platform->DoesFolderExist(tempString))
			{
				PrintLine_D("Created directory \"%s\"", tempString);
				platform->CreateFolder(tempString);
			}
			else
			{
				// PrintLine_D("\"%s\" already exists", tempString);
			}
			tempString[cIndex] = oldChar;
		}
	}
	
	TempPopMark();
}
