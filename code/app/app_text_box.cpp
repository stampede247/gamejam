/*
File:   app_text_box.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Handles interaction and rendering with Text Boxes
*/

void DestroyTextBox(TextBox_t* tb)
{
	Assert(tb != nullptr);
	
	if (tb->text != nullptr)
	{
		Assert(tb->allocArena != nullptr);
		ArenaPop(tb->allocArena, tb->text);
		tb->text = nullptr;
	}
	if (tb->hint != nullptr)
	{
		Assert(tb->allocArena != nullptr);
		ArenaPop(tb->allocArena, tb->hint);
		tb->hint = nullptr;
	}
	
	ClearPointer(tb);
}

void CreateTextBox(TextBox_t* tb, MemoryArena_t* memArena, rec bounds, Font_t* font, r32 fontSize, u8 options = TextBoxOp_Default, TextBoxType_t type = TextBoxType_Default)
{
	Assert(tb != nullptr);
	Assert(memArena != nullptr);
	
	ClearPointer(tb);
	tb->allocArena = memArena;
	
	tb->text = PushArray(memArena, char, 16);
	tb->textLengthAlloc = 16;
	tb->textLength = 0;
	tb->text[0] = '\0';
	
	tb->font = font;
	tb->fontSize = fontSize;
	tb->fontStyle = FontStyle_Default;
	tb->maxLength = 0;
	tb->minSize = Vec2_Zero;
	tb->maxSize = Vec2_Zero;
	tb->options = options;
	tb->bounds = bounds;
	tb->padding = 5; //px
	tb->type = type;
	
	tb->selStart = 0;
	tb->selEnd = 0;
	tb->mouseHoverIndex = -1;
	tb->lastClickIndex = -1;
	
	tb->textColor = VisBlueGray;
	tb->textHighlightColor = VisWhite;
	tb->backColor = VisWhite;
	tb->borderColor = VisBlueGray;
	tb->borderFocusColor = VisBlue;
	tb->highlightColor = VisLightBlue;
}

void TB_IncreaseAllocSpace(TextBox_t* tb, u32 requiredSize)
{
	Assert(tb != nullptr);
	Assert(tb->allocArena != nullptr);
	Assert(tb->textLength+1 <= tb->textLengthAlloc);
	if (tb->textLengthAlloc >= requiredSize+1) { return; }
	
	u32 newAllocSize = tb->textLengthAlloc;
	while (newAllocSize < requiredSize+1) { newAllocSize += 16; }
	char* newSpace = PushArray(tb->allocArena, char, newAllocSize);
	if (tb->textLength > 0)
	{
		memcpy(newSpace, tb->text, tb->textLength+1);
	}
	if (tb->text != nullptr)
	{
		ArenaPop(tb->allocArena, tb->text);
	}
	tb->text = newSpace;
	tb->textLengthAlloc = newAllocSize;
	Assert(tb->textLength < tb->textLengthAlloc);
	tb->text[tb->textLength] = '\0';
}
void TextBoxSetText(TextBox_t* tb, const char* strPntr, u32 strLength)
{
	Assert(tb != nullptr);
	Assert(strPntr != nullptr);
	
	tb->textLength = 0;
	if (strLength+1 > tb->textLengthAlloc)
	{
		TB_IncreaseAllocSpace(tb, strLength);
	}
	Assert(strLength+1 <= tb->textLengthAlloc);
	if (strLength > 0)
	{
		memcpy(tb->text, strPntr, strLength);
	}
	tb->text[strLength] = '\0';
	tb->textLength = strLength;
	
	tb->selStart = tb->textLength;
	tb->selEnd = tb->textLength;
}
void TextBoxSetTextNt(TextBox_t* tb, const char* nullTermStr)
{
	u32 strLength = (u32)strlen(nullTermStr);
	TextBoxSetText(tb, nullTermStr, strLength);
}

void TextBoxSetHint(TextBox_t* tb, const char* strPntr, u32 strLength)
{
	Assert(tb != nullptr);
	if (tb->hint != nullptr)
	{
		ArenaPop(tb->allocArena, tb->hint);
		tb->hint = nullptr;
	}
	tb->hintLength = 0;
	
	if (strLength > 0)
	{
		Assert(strPntr != nullptr);
		tb->hint = ArenaString(tb->allocArena, strPntr, strLength);
		tb->hintLength = strLength;
	}
}
void TextBoxSetHintNt(TextBox_t* tb, const char* nullTermStr)
{
	Assert(tb != nullptr);
	Assert(nullTermStr != nullptr);
	u32 strLength = (u32)strlen(nullTermStr);
	TextBoxSetHint(tb, nullTermStr, strLength);
}

void TB_InsertStringAt(TextBox_t* tb, u32 cIndex, const char* strPntr, u32 strLength)
{
	Assert(cIndex <= tb->textLength);
	if (strLength == 0) { return; }
	
	if (tb->textLength + strLength >= tb->textLengthAlloc)
	{
		TB_IncreaseAllocSpace(tb, tb->textLength + strLength);
	}
	
	if (cIndex < tb->textLength)
	{
		memmove(&tb->text[cIndex + strLength], &tb->text[cIndex], tb->textLength - cIndex);
	}
	memcpy(&tb->text[cIndex], strPntr, strLength);
	
	if (tb->selStart > (i32)cIndex)
	{
		tb->selStart += (i32)strLength;
	}
	if (tb->selEnd > (i32)cIndex)
	{
		tb->selEnd += (i32)strLength;
	}
	
	tb->textLength += strLength;
	Assert(tb->textLength < tb->textLengthAlloc);
	tb->text[tb->textLength] = '\0';
	
	Assert(tb->selStart < 0 || (u32)tb->selStart <= tb->textLength);
	Assert(tb->selEnd < 0 || (u32)tb->selEnd <= tb->textLength);
}
void TB_RemovePart(TextBox_t* tb, u32 cIndex, u32 numChars)
{
	Assert(cIndex <= tb->textLength);
	Assert(cIndex + numChars <= tb->textLength);
	if (numChars == 0) { return; }
	
	if (cIndex + numChars < tb->textLength)
	{
		memmove(&tb->text[cIndex], &tb->text[cIndex + numChars], tb->textLength - (cIndex+numChars));
	}
	if (tb->selStart >= (i32)cIndex && tb->selStart < (i32)(cIndex+numChars))
	{
		tb->selStart = (i32)cIndex;
	}
	else if (tb->selStart >= (i32)(cIndex+numChars))
	{
		tb->selStart -= (i32)numChars;
	}
	if (tb->selEnd >= (i32)cIndex && tb->selEnd < (i32)(cIndex+numChars))
	{
		tb->selEnd = (i32)cIndex;
	}
	else if (tb->selEnd >= (i32)(cIndex+numChars))
	{
		tb->selEnd -= (i32)numChars;
	}
	
	tb->textLength -= numChars;
	Assert(tb->textLength < tb->textLengthAlloc);
	tb->text[tb->textLength] = '\0';
	
	Assert(tb->selStart < 0 || (u32)tb->selStart <= tb->textLength);
	Assert(tb->selEnd < 0 || (u32)tb->selEnd <= tb->textLength);
}
void TB_ReplacePart(TextBox_t* tb, u32 cIndex, u32 numChars, const char* strPntr, u32 strLength)
{
	Assert(cIndex <= tb->textLength);
	Assert(cIndex + numChars <= tb->textLength);
	if (numChars == 0) { TB_InsertStringAt(tb, cIndex, strPntr, strLength); return; }
	if (strLength == 0) { TB_RemovePart(tb, cIndex, numChars); return; }
	
	if (strLength == numChars)
	{
		memcpy(&tb->text[cIndex], strPntr, strLength);
		Assert(tb->text[tb->textLength] == '\0');
	}
	else if (strLength < numChars)
	{
		u32 shiftBy = numChars - strLength;
		if (cIndex + numChars < tb->textLength)
		{
			memmove(&tb->text[cIndex+numChars - shiftBy], &tb->text[cIndex+numChars], tb->textLength - (cIndex+numChars));
		}
		memcpy(&tb->text[cIndex], strPntr, strLength);
		
		if (tb->selStart > (i32)cIndex && tb->selStart < (i32)(cIndex+numChars))
		{
			tb->selStart = (i32)(cIndex+strLength);
		}
		else if (tb->selStart >= (i32)(cIndex+numChars))
		{
			tb->selStart -= (i32)shiftBy;
		}
		if (tb->selEnd > (i32)cIndex && tb->selEnd < (i32)(cIndex+numChars))
		{
			tb->selEnd = (i32)(cIndex+strLength);
		}
		else if (tb->selEnd >= (i32)(cIndex+numChars))
		{
			tb->selEnd -= (i32)shiftBy;
		}
		
		tb->textLength -= shiftBy;
		Assert(tb->textLength < tb->textLengthAlloc);
		tb->text[tb->textLength] = '\0';
	}
	else //strLength > numChars
	{
		u32 shiftBy = strLength - numChars;
		if (tb->textLength + shiftBy >= tb->textLengthAlloc)
		{
			TB_IncreaseAllocSpace(tb, tb->textLength + shiftBy);
		}
		
		if (cIndex + numChars < tb->textLength)
		{
			memmove(&tb->text[cIndex+numChars + shiftBy], &tb->text[cIndex+numChars], tb->textLength - (cIndex+numChars));
		}
		memcpy(&tb->text[cIndex], strPntr, strLength);
		
		if (tb->selStart > (i32)cIndex && tb->selStart < (i32)(cIndex+numChars))
		{
			tb->selStart = (i32)(cIndex+strLength);
		}
		else if (tb->selStart >= (i32)(cIndex+numChars))
		{
			tb->selStart += (i32)shiftBy;
		}
		if (tb->selEnd > (i32)cIndex && tb->selEnd < (i32)(cIndex+numChars))
		{
			tb->selEnd = (i32)(cIndex+strLength);
		}
		else if (tb->selEnd >= (i32)(cIndex+numChars))
		{
			tb->selEnd += (i32)shiftBy;
		}
		
		tb->textLength += shiftBy;
		Assert(tb->textLength < tb->textLengthAlloc);
		tb->text[tb->textLength] = '\0';
	}
	
	Assert(tb->selStart < 0 || (u32)tb->selStart <= tb->textLength);
	Assert(tb->selEnd < 0 || (u32)tb->selEnd <= tb->textLength);
}

void TB_DeleteButton(TextBox_t* tb, bool ctrl, bool wasDelete)
{
	if (tb->selStart < 0 || tb->selEnd < 0)
	{
		tb->selEnd = tb->textLength; 
		tb->selStart = tb->selEnd;
	}
	i32 selMin = MinI32(tb->selStart, tb->selEnd);
	i32 selMax = MaxI32(tb->selStart, tb->selEnd);
	
	if (tb->selStart != tb->selEnd) //selection isn't empty
	{
		if ((u32)MaxI32(tb->selStart, tb->selEnd) == tb->textLength)
		{
			tb->textLength = MinI32(tb->selStart, tb->selEnd);
			tb->text[tb->textLength] = '\0';
			tb->selEnd = tb->textLength;
			tb->selStart = tb->selEnd;
		}
		else
		{
			TB_RemovePart(tb, (u32)selMin, (u32)(selMax - selMin));
		}
	}
	else if (wasDelete) //Delete Button was pressed
	{
		if ((u32)tb->selEnd < tb->textLength) //there's chars after the cursor
		{
			if (ctrl) //Delete whole word following cursor
			{
				u32 wordEnd = FindWordBound(tb->text, tb->textLength, (u32)tb->selEnd, true);
				Assert(wordEnd > (u32)tb->selEnd);
				if (wordEnd == tb->textLength) //Word is at the end
				{
					tb->textLength = (u32)tb->selEnd;
					tb->text[tb->textLength] = '\0';
				}
				else //Word is in the middle
				{
					TB_RemovePart(tb, (u32)tb->selEnd, wordEnd - (u32)tb->selEnd);
				}
			}
			else //Delete character following cursor
			{
				if ((u32)tb->selEnd == tb->textLength-1) //Cursor is 1 from the end
				{
					tb->textLength--;
					tb->text[tb->textLength] = '\0';
				}
				else //Cursor is in the middle of the text
				{
					TB_RemovePart(tb, (u32)tb->selEnd, 1);
				}
			}
		}
		//else nothing to delete
	}
	else //Backspace button was pressed
	{
		if (tb->selEnd > 0) //There's chars before the cursor
		{
			if (ctrl)
			{
				u32 wordBegin = FindWordBound(tb->text, tb->textLength, (u32)tb->selEnd, false);
				Assert(wordBegin < (u32)tb->selEnd);
				if ((u32)tb->selEnd == tb->textLength) //word is at the end
				{
					tb->textLength = wordBegin;
					tb->text[tb->textLength] = '\0';
				}
				else //word is in the middle
				{
					TB_RemovePart(tb, wordBegin, (u32)tb->selEnd - wordBegin);
				}
				tb->selEnd = (i32)wordBegin;
				tb->selStart = tb->selEnd;
			}
			else //Delete character before cursor
			{
				if ((u32)tb->selEnd == tb->textLength) //Cursor is at the end
				{
					tb->textLength--;
					tb->text[tb->textLength] = '\0';
					tb->selEnd--;
					tb->selStart = tb->selEnd;
				}
				else //Cursor is in the middle
				{
					TB_RemovePart(tb, (u32)(tb->selEnd-1), 1);
				}
			}
		}
		//else nothing to delete
	}
}
void TB_CharTyped(TextBox_t* tb, char c)
{
	if (tb->selStart < 0 || tb->selEnd < 0)
	{
		tb->selEnd = tb->textLength; 
		tb->selStart = tb->selEnd;
	}
	i32 selMin = MinI32(tb->selStart, tb->selEnd);
	i32 selMax = MaxI32(tb->selStart, tb->selEnd);
	
	if (tb->selStart != tb->selEnd && tb->selStart >= 0 && tb->selEnd >= 0) //selection is not empty
	{
		//NOTE: No need to check maxLength since we should never increase in size when replacing non-empty selection with a single character
		if ((u32)MaxI32(tb->selStart, tb->selEnd) == tb->textLength) //Selection is at end
		{
			tb->textLength = MinI32(tb->selStart, tb->selEnd);
			tb->text[tb->textLength] = c;
			tb->textLength++;
			Assert(tb->textLength < tb->textLengthAlloc);
			tb->text[tb->textLength] = '\0';
			tb->selEnd = tb->textLength;
			tb->selStart = tb->selEnd;
		}
		else //Selection is in middle
		{
			tb->selEnd = selMax;
			tb->selStart = tb->selEnd;
			TB_ReplacePart(tb, (u32)selMin, (u32)(selMax - selMin), &c, 1);
		}
	}
	else //selection is empty
	{
		if (tb->maxLength == 0 || tb->textLength < tb->maxLength) //We haven't hit maxLength
		{
			if ((u32)tb->selEnd == tb->textLength) //Cursor is at end
			{
				if (tb->textLength+1 >= tb->textLengthAlloc)
				{
					TB_IncreaseAllocSpace(tb, tb->textLength+1);
				}
				tb->text[tb->textLength] = c;
				tb->textLength++;
				Assert(tb->textLength < tb->textLengthAlloc);
				tb->text[tb->textLength] = '\0';
				tb->selEnd++;
				tb->selStart = tb->selEnd;
			}
			else //Cursor is in middle
			{
				TB_InsertStringAt(tb, (u32)tb->selEnd, &c, 1);
				tb->selEnd++;
				tb->selStart = tb->selEnd;
			}
		}
		else //no space for a new character
		{
			PrintLine_W("Textbox is full at %u characters", tb->textLength);
		}
	}
}

r32 TextBoxLabelAlignY(TextBox_t* tb)
{
	Assert(tb != nullptr);
	
	r32 lineHeight = FontGetLineHeight(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendUp = FontGetMaxExtendUp(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendDown = FontGetMaxExtendDown(tb->font, tb->fontSize, tb->fontStyle);
	
	v2 textPos = tb->bounds.topLeft + NewVec2(tb->padding, tb->bounds.height/2 + lineHeight/2 - maxExtendDown) - tb->scrollOffset;
	if (IsFlagSet(tb->options, TextBoxOp_MultiLine))
	{
		textPos.y = tb->bounds.y + tb->padding + maxExtendUp - tb->scrollOffset.y;
	}
	textPos = Vec2Round(textPos);
	
	return textPos.y;
}
bool TextBoxIsValidNumber(TextBox_t* tb)
{
	Assert(tb != nullptr);
	
	if (tb->type == TextBoxType_UnsignedNumber)
	{
		return TryParseU32(tb->text, tb->textLength, nullptr);
	}
	else if (tb->type == TextBoxType_HexNumber)
	{
		for (u32 cIndex = 0; cIndex < tb->textLength; )
		{
			if (tb->text[cIndex] == ' ') { cIndex++; continue; }
			else if (cIndex+1 >= tb->textLength) { return false; }
			else if (!IsCharClassHexChar(tb->text[cIndex])) { return false; }
			else if (!IsCharClassHexChar(tb->text[cIndex+1])) { return false; }
			else { cIndex += 2; }
		}
		return true;
	}
	else
	{
		return TryParseI32(tb->text, tb->textLength, nullptr);
	}
}

void TextBoxSetSelection(TextBox_t* tb, i32 selStart, i32 selEnd, bool gotoCursor = true)
{
	Assert(tb != nullptr);
	if (gotoCursor) { tb->gotoCursor = true; }
	if (tb->selStart == selStart && tb->selEnd == selEnd) { return; }
	if (selStart < 0 || selEnd < 0)
	{
		Assert(selStart < 0 && selEnd < 0);
		tb->selStart = -1;
		tb->selEnd = -1;
	}
	else
	{
		Assert((u32)selStart <= tb->textLength);
		Assert((u32)selEnd <= tb->textLength);
		tb->selStart = selStart;
		tb->selEnd = selEnd;
	}
}
void TextBoxDeselectAll(TextBox_t* tb, bool gotoCursor = true)
{
	TextBoxSetSelection(tb, -1, -1, gotoCursor);
}
void TextBoxSelectAll(TextBox_t* tb, bool gotoCursor = true)
{
	TextBoxSetSelection(tb, 0, tb->textLength, gotoCursor);
}
void TextBoxSelectEnd(TextBox_t* tb, bool gotoCursor = true)
{
	TextBoxSetSelection(tb, tb->textLength, tb->textLength, gotoCursor);
}

void TB_UpdateBounds(TextBox_t* tb)
{
	Assert(tb != nullptr);
	r32 lineHeight = FontGetLineHeight(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendUp = FontGetMaxExtendUp(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendDown = FontGetMaxExtendDown(tb->font, tb->fontSize, tb->fontStyle);
	
	v2 oldSize = tb->bounds.size;
	if (tb->flowInfoFilled && IsFlagSet(tb->options, TextBoxOp_AutoSizeHori))
	{
		r32 newWidth = tb->flowInfo.extentRight + tb->padding*2;
		if (newWidth < 30) { newWidth = 30; } //NOTE: Arbitrary minimum
		if (tb->bounds.width != newWidth)
		{
			tb->bounds.width = newWidth;
		}
	}
	if (tb->flowInfoFilled && IsFlagSet(tb->options, TextBoxOp_AutoSizeVert))
	{
		r32 newHeight = tb->flowInfo.totalSize.height + tb->padding*2;
		if (newHeight < lineHeight + tb->padding*2) { newHeight = lineHeight + tb->padding*2; }
		if (tb->bounds.height != newHeight)
		{
			tb->bounds.height = newHeight;
		}
	}
	if (tb->maxSize.width != 0 && tb->bounds.width > tb->maxSize.width)
	{
		tb->bounds.width = tb->maxSize.width;
	}
	if (tb->maxSize.height != 0 && tb->bounds.height > tb->maxSize.height)
	{
		tb->bounds.height = tb->maxSize.height;
	}
	if (tb->minSize.width != 0 && tb->bounds.width < tb->minSize.width)
	{
		tb->bounds.width = tb->minSize.width;
	}
	if (tb->minSize.height != 0 && tb->bounds.height < tb->minSize.height)
	{
		tb->bounds.height = tb->minSize.height;
	}
	if (tb->bounds.size != oldSize)
	{
		// WriteLine_D("Textbox size changed");
		tb->gotoCursor = true;
	}
}

const char* TB_GetUniqueName(TextBox_t* tb)
{
	Assert(tb != nullptr);
	return TempPrint("TextBox %p", tb);
}
void TextBoxClaimMouse(TextBox_t* tb, v2 mousePos)
{
	Assert(tb != nullptr);
	TB_UpdateBounds(tb);
	if (!IsMouseOverOther() && IsInsideRec(tb->bounds, mousePos))
	{
		MouseOverNamed(TB_GetUniqueName(tb));
	}
}

void TextBoxUpdate(TextBox_t* tb, v2 mousePos, bool* textChangedOut = nullptr, bool* textBoxFocusedOut = nullptr)
{
	Assert(tb != nullptr);
	r32 lineHeight = FontGetLineHeight(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendUp = FontGetMaxExtendUp(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendDown = FontGetMaxExtendDown(tb->font, tb->fontSize, tb->fontStyle);
	bool textChanged = false;
	if (textBoxFocusedOut != nullptr) { *textBoxFocusedOut = false; }
	
	TextBoxClaimMouse(tb, mousePos); //calls TB_UpdateBounds for us
	bool mouseInside = IsMouseOver(TB_GetUniqueName(tb));
	
	// +==============================+
	// |        Update Cursor         |
	// +==============================+
	if (mouseInside)
	{
		appOutput->cursorType = IsUiFocused(tb) ? Cursor_Text : Cursor_Pointer;
	}
	
	// +==============================+
	// |       Update Scrolling       |
	// +==============================+
	//NOTE: We use the filled flowInfo in tb that was calculated in TextBoxDraw
	if (tb->flowInfoFilled)
	{
		//calculate maxScroll
		tb->maxScroll = Vec2_Zero;
		tb->actualMaxScroll = Vec2_Zero;
		if (IsFlagSet(tb->options, TextBoxOp_ScrollHori))
		{
			tb->maxScroll.x = tb->flowInfo.totalSize.width + (tb->bounds.width / 4) - (tb->bounds.width - tb->padding*2);
			if (tb->maxScroll.x < 0) { tb->maxScroll.x = 0; }
			
			tb->actualMaxScroll.x = tb->flowInfo.totalSize.width - (tb->bounds.width - tb->padding*2);
			if (tb->actualMaxScroll.x <= 0)
			{
				tb->actualMaxScroll.x = 0;
				tb->maxScroll.x = 0; //If we don't actually overflow the text box then don't add padding whitespace
			}
		}
		if (IsFlagSet(tb->options, TextBoxOp_ScrollVert))
		{
			tb->maxScroll.y = tb->flowInfo.totalSize.height + lineHeight - (tb->bounds.height - tb->padding*2);
			if (tb->maxScroll.y < 0) { tb->maxScroll.y = 0; }
			
			tb->actualMaxScroll.y = tb->flowInfo.totalSize.height - (tb->bounds.height - tb->padding*2);
			if (tb->actualMaxScroll.y <= 0)
			{
				tb->actualMaxScroll.y = 0;
				tb->maxScroll.y = 0; //If we don't actually overflow the text box then don't add padding whitespace
			}
		}
		
		//Clamp values to acceptable region
		if (tb->scrollOffset.x > tb->maxScroll.x) { tb->scrollOffset.x = tb->maxScroll.x; }
		if (tb->scrollOffset.y > tb->maxScroll.y) { tb->scrollOffset.y = tb->maxScroll.y; }
		if (tb->scrollOffset.x < 0) { tb->scrollOffset.x = 0; }
		if (tb->scrollOffset.y < 0) { tb->scrollOffset.y = 0; }
		if (tb->scrollTarget.x > tb->maxScroll.x) { tb->scrollTarget.x = tb->maxScroll.x; }
		if (tb->scrollTarget.y > tb->maxScroll.y) { tb->scrollTarget.y = tb->maxScroll.y; }
		if (tb->scrollTarget.x < 0) { tb->scrollTarget.x = 0; }
		if (tb->scrollTarget.y < 0) { tb->scrollTarget.y = 0; }
		
		//Tween scrollOffset towards scrollTarget
		v2 distVec = tb->scrollTarget - tb->scrollOffset;
		r32 dist = Vec2Length(distVec);
		if (dist > TEXT_BOX_SCROLL_TWEEN_SNAP_DIST)
		{
			tb->scrollOffset += distVec / (r32)TEXT_BOX_SCROLL_LATENCY;
		}
		else
		{
			tb->scrollOffset = tb->scrollTarget;
		}
	}
	
	// +====================================+
	// | Calculate textPos and maxTextWidth |
	// +====================================+
	//NOTE: This code should match below in TextBoxDraw
	v2 textPos = tb->bounds.topLeft + NewVec2(tb->padding, tb->bounds.height/2 + lineHeight/2 - maxExtendDown) - tb->scrollOffset;
	if (IsFlagSet(tb->options, TextBoxOp_MultiLine))
	{
		textPos.y = tb->bounds.y + tb->padding + maxExtendUp - tb->scrollOffset.y;
	}
	textPos = Vec2Round(textPos);
	r32 maxTextWidth = 0;
	if (IsFlagSet(tb->options, TextBoxOp_MultiLine) && !IsFlagSet(tb->options, TextBoxOp_AutoSizeHori) && !IsFlagSet(tb->options, TextBoxOp_ScrollHori))
	{
		maxTextWidth = tb->bounds.width - tb->padding*2;
	}
	
	// +==============================+
	// |   Calculate mouseHoverPos    |
	// +==============================+
	tb->mouseHoverIndex = -1;
	tb->mouseHoverIndexPos = tb->bounds.topLeft;
	if (IsUiFocused(tb) && (mouseInside || tb->mouseStartedInside))
	{
		FontFlowExtra_t flowExtra = {};
		flowExtra.calculateMouseIndex = true;
		flowExtra.mousePos = mousePos;
		
		//In single line text boxes we don't want it to consider mousePos.y
		if (!IsFlagSet(tb->options, TextBoxOp_MultiLine))
		{
			flowExtra.mousePos.y = textPos.y;
		}
		
		v2 flowEnd = FontPerformTextFlow(false, tb->text, tb->textLength, textPos, NewColor(Color_Black), tb->font, Alignment_Left, tb->fontSize, tb->fontStyle, maxTextWidth, nullptr, &flowExtra);
		
		if (flowExtra.mouseIndex >= 0)
		{
			tb->mouseHoverIndex = flowExtra.mouseIndex;
			tb->mouseHoverIndexPos = flowExtra.mouseIndexPos;
		}
		else if (IsInsideRec(tb->bounds, mousePos) && IsFlagSet(tb->options, TextBoxOp_MultiLine))
		{
			if (mousePos.y >= textPos.y)
			{
				tb->mouseHoverIndex = tb->textLength;
				tb->mouseHoverIndexPos = flowEnd;
			}
			else
			{
				tb->mouseHoverIndex = 0;
				tb->mouseHoverIndexPos = textPos;
			}
		}
	}
	
	// +==============================+
	// |   Handle Left Mouse Clicks   |
	// +==============================+
	{
		if (ButtonPressedSeq(MouseButton_Left, 0xFF))
		{
			// PrintLine_D("Mouse down %s", mouseInside ? "Inside" : "Outside");
			if (mouseInside)
			{
				const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), MouseButton_Left, 0xFF);
				bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
				if (IsUiFocused(tb) && tb->mouseHoverIndex >= 0)
				{
					if (!shift)
					{
						tb->selStart = tb->mouseHoverIndex;
					}
					tb->selEnd = tb->mouseHoverIndex;
					tb->gotoCursor = true;
				}
			}
			tb->mouseStartedInside = mouseInside;
		}
		if (ButtonDownNoHandling(MouseButton_Left))
		{
			if (tb->mouseStartedInside && !app->buttonHandled[MouseButton_Left])
			{
				HandleButton(MouseButton_Left);
				if (IsUiFocused(tb) && tb->mouseHoverIndex >= 0)
				{
					tb->selEnd = tb->mouseHoverIndex;
					tb->gotoCursor = true;
				}
			}
		}
		if (tb->mouseStartedInside && !ButtonDownNoHandling(MouseButton_Left) && !ButtonReleasedSeq(MouseButton_Left, 0xFF))
		{
			tb->mouseStartedInside = false;
		}
		if (ButtonReleasedSeq(MouseButton_Left, 0xFF))
		{
			if (tb->mouseStartedInside)
			{
				const InputEvent_t* inputEvent = HandleButtonReleaseSeq(TB_GetUniqueName(tb), MouseButton_Left, 0xFF);
				bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
				if (IsUiFocused(tb))
				{
					if (tb->mouseHoverIndex >= 0)
					{
						tb->selEnd = tb->mouseHoverIndex;
						tb->gotoCursor = true;
						
						if (tb->selStart == tb->selEnd)
						{
							i32 selEndStore = tb->selEnd; //store selEnd since it might get changed on double/triple click
							if (TimeSince(tb->lastClickTime) < TEXT_BOX_DOUBLE_CLICK_TIME && tb->selEnd == tb->lastClickIndex)
							{
								tb->numClicks++;
								// +==============================+
								// |         Double Click         |
								// +==============================+
								if (tb->numClicks == 2)
								{
									u32 wordEnd = FindWordBound(tb->text, tb->textLength, (u32)tb->selEnd, true);
									u32 wordBegin = FindWordBound(tb->text, tb->textLength, (u32)tb->selEnd, false);
									tb->selStart = wordBegin;
									tb->selEnd = wordEnd;
								}
								// +==============================+
								// |         Triple Click         |
								// +==============================+
								else if (tb->numClicks == 3)
								{
									u32 lineEnd = FindLineEnd(tb->text, tb->textLength, (u32)tb->selEnd);
									u32 lineBegin = FindLineStart(tb->text, tb->textLength, (u32)tb->selEnd);
									tb->selStart = lineBegin;
									tb->selEnd = lineEnd;
								}
							}
							else
							{
								tb->numClicks = 1;
							}
							tb->lastClickTime = ProgramTime;
							tb->lastClickIndex = selEndStore;
						}
					}
					tb->mouseStartedInside = false;
				}
				else if (mouseInside)
				{
					SetUiFocus(tb);
					if (textBoxFocusedOut != nullptr) { *textBoxFocusedOut = true; }
					// tb->gotoCursor = true;
				}
			}
		}
	}
	
	// +==============================+
	// | Selection Arrow Key Movement |
	// +==============================+
	if (IsUiFocused(tb))
	{
		if (ButtonPressedSeq(Button_Right, 0xFF, true))
		{
			const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), Button_Right, 0xFF, true);
			bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
			bool ctrl = IsFlagSet(inputEvent->flags, BtnTransFlag_Ctrl);
			tb->gotoCursor = true;
			if (tb->selEnd < 0 || tb->selStart < 0)
			{
				tb->selEnd = tb->textLength;
				tb->selStart = tb->selEnd;
			}
			else if ((u32)tb->selEnd < tb->textLength)
			{
				if (ctrl)
				{
					u32 wordBound = FindWordBound(tb->text, tb->textLength, (u32)tb->selEnd, true);
					if (shift)
					{
						tb->selEnd = wordBound;
					}
					else
					{
						tb->selEnd = wordBound;
						tb->selStart = tb->selEnd;
					}
				}
				else
				{
					if (shift)
					{
						tb->selEnd++;
					}
					else
					{
						if (tb->selStart == tb->selEnd)
						{
							tb->selEnd++;
							tb->selStart = tb->selEnd;
						}
						else
						{
							tb->selEnd = MaxI32(tb->selStart, tb->selEnd);
							tb->selStart = tb->selEnd;
						}
					}
				}
			}
			else if (tb->selStart != tb->selEnd && !shift)
			{
				tb->selEnd = MaxI32(tb->selStart, tb->selEnd);
				tb->selStart = tb->selEnd;
			}
			else
			{
				// WriteLine_D("No selection change");
			}
		}
		
		if (ButtonPressedSeq(Button_Left, 0xFF, true))
		{
			const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), Button_Left, 0xFF, true);
			bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
			bool ctrl = IsFlagSet(inputEvent->flags, BtnTransFlag_Ctrl);
			tb->gotoCursor = true;
			if (tb->selEnd < 0 || tb->selStart < 0)
			{
				tb->selEnd = tb->textLength;
				tb->selStart = tb->selEnd;
			}
			else if (tb->selEnd > 0)
			{
				if (ctrl)
				{
					u32 wordBound = FindWordBound(tb->text, tb->textLength, (u32)tb->selEnd, false);
					if (shift)
					{
						tb->selEnd = wordBound;
					}
					else
					{
						tb->selEnd = wordBound;
						tb->selStart = tb->selEnd;
					}
				}
				else
				{
					if (shift)
					{
						tb->selEnd--;
					}
					else
					{
						if (tb->selStart == tb->selEnd)
						{
							tb->selEnd--;
							tb->selStart = tb->selEnd;
						}
						else
						{
							tb->selEnd = MinI32(tb->selStart, tb->selEnd);
							tb->selStart = tb->selEnd;
						}
					}
				}
			}
			else if (tb->selStart != tb->selEnd && !shift)
			{
				tb->selEnd = MinI32(tb->selStart, tb->selEnd);
				tb->selStart = tb->selEnd;
			}
			else
			{
				WriteLine_D("No selection change");
			}
		}
		
		if (IsFlagSet(tb->options, TextBoxOp_HandleUpDown))
		{
			if (ButtonPressedSeq(Button_Up, 0xFF, true))
			{
				const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), Button_Up, 0xFF, true);
				bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
				tb->gotoCursor = true;
				if (tb->selEnd < 0 || tb->selStart < 0)
				{
					tb->selEnd = tb->textLength;
					tb->selStart = tb->selEnd;
				}
				else if (tb->selEnd != tb->selStart && !shift)
				{
					tb->selEnd = MinI32(tb->selStart, tb->selEnd);
					tb->selStart = tb->selEnd;
				}
				else if (tb->selEnd > 0)
				{
					//Find the position of the selection end (cursor pos)
					FontFlowExtra_t flowExtra = {};
					flowExtra.calculateIndexPos = true;
					flowExtra.index = (u32)tb->selEnd;
					FontPerformTextFlow(false, tb->text, tb->textLength, textPos, NewColor(Color_Black), tb->font, Alignment_Left, tb->fontSize, tb->fontStyle, maxTextWidth, nullptr, &flowExtra);
					
					//Find the index of 1 line above the current cursor
					flowExtra.calculateIndexPos = false;
					flowExtra.calculateMouseIndex = true;
					flowExtra.mousePos = flowExtra.indexPos + NewVec2(0, -lineHeight);
					FontPerformTextFlow(false, tb->text, tb->textLength, textPos, NewColor(Color_Black), tb->font, Alignment_Left, tb->fontSize, tb->fontStyle, maxTextWidth, nullptr, &flowExtra);
					
					u32 upIndex = 0;
					if (flowExtra.mouseIndex >= 0)
					{
						upIndex = flowExtra.mouseIndex;
					}
					
					tb->selEnd = upIndex;
					if (!shift)
					{
						tb->selStart = tb->selEnd;
					}
				}
				else
				{
					WriteLine_D("No selection change");
				}
			}
			
			if (ButtonPressedSeq(Button_Down, 0xFF, true))
			{
				const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), Button_Down, 0xFF, true);
				bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
				tb->gotoCursor = true;
				if (tb->selEnd < 0 || tb->selStart < 0)
				{
					tb->selEnd = 0;
					tb->selStart = tb->selEnd;
				}
				else if (tb->selEnd != tb->selStart && !shift)
				{
					tb->selEnd = MaxI32(tb->selStart, tb->selEnd);
					tb->selStart = tb->selEnd;
				}
				else if ((u32)tb->selEnd < tb->textLength)
				{
					//Find the position of the selection end (cursor pos)
					FontFlowExtra_t flowExtra = {};
					flowExtra.calculateIndexPos = true;
					flowExtra.index = (u32)tb->selEnd;
					FontPerformTextFlow(false, tb->text, tb->textLength, textPos, NewColor(Color_Black), tb->font, Alignment_Left, tb->fontSize, tb->fontStyle, maxTextWidth, nullptr, &flowExtra);
					
					//Find the index of 1 line below the current cursor
					flowExtra.calculateIndexPos = false;
					flowExtra.calculateMouseIndex = true;
					flowExtra.mousePos = flowExtra.indexPos + NewVec2(0, lineHeight);
					FontPerformTextFlow(false, tb->text, tb->textLength, textPos, NewColor(Color_Black), tb->font, Alignment_Left, tb->fontSize, tb->fontStyle, maxTextWidth, nullptr, &flowExtra);
					
					u32 downIndex = tb->textLength;
					if (flowExtra.mouseIndex >= 0)
					{
						downIndex = flowExtra.mouseIndex;
					}
					
					tb->selEnd = downIndex;
					if (!shift)
					{
						tb->selStart = tb->selEnd;
					}
				}
				else
				{
					WriteLine_D("No selection change");
				}
			}
		}
	}
	
	// +==============================+
	// |    End And Home Movement     |
	// +==============================+
	if (IsUiFocused(tb))
	{
		if (ButtonPressedSeq(Button_End, 0xFF))
		{
			const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), Button_End, 0xFF);
			bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
			bool ctrl = IsFlagSet(inputEvent->flags, BtnTransFlag_Ctrl);
			tb->gotoCursor = true;
			if (IsFlagSet(tb->options, TextBoxOp_MultiLine) && !ctrl)
			{
				u32 lineEnd = FindLineEnd(tb->text, tb->textLength, tb->selEnd);
				tb->selEnd = lineEnd;
			}
			else
			{
				tb->selEnd = tb->textLength;
			}
			if (!shift) { tb->selStart = tb->selEnd; }
		}
		
		if (ButtonPressedSeq(Button_Home, 0xFF))
		{
			const InputEvent_t* inputEvent = HandleButtonPressSeq(TB_GetUniqueName(tb), Button_Home, 0xFF);
			bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
			bool ctrl = IsFlagSet(inputEvent->flags, BtnTransFlag_Ctrl);
			tb->gotoCursor = true;
			if (IsFlagSet(tb->options, TextBoxOp_MultiLine) && !ctrl)
			{
				u32 lineStart = FindLineStart(tb->text, tb->textLength, tb->selEnd);
				tb->selEnd = lineStart;
			}
			else
			{
				tb->selEnd = 0;
			}
			if (!shift) { tb->selStart = tb->selEnd; }
		}
	}
	
	// +==============================+
	// |        Handle ctrl+a         |
	// +==============================+
	if (IsUiFocused(tb))
	{
		if (ButtonPressedSeq(Button_A, BtnTransFlag_Ctrl))
		{
			HandleButtonPressSeq(TB_GetUniqueName(tb), Button_A, BtnTransFlag_Ctrl);
			tb->selStart = 0;
			tb->selEnd = tb->textLength;
			tb->gotoCursor = true; //TODO: Do actually want to move the view here?
		}
	}
	
	// +==============================+
	// |      Handle Copy Hotkey      |
	// +==============================+
	if (IsUiFocused(tb))
	{
		if (ButtonPressedSeq(Button_C, BtnTransFlag_Ctrl))
		{
			HandleButtonPressSeq(TB_GetUniqueName(tb), Button_C, BtnTransFlag_Ctrl);
			if (tb->selStart >= 0 && tb->selEnd >= 0 && tb->selEnd != tb->selStart)
			{
				i32 selMin = MinI32(tb->selStart, tb->selEnd);
				i32 selMax = MaxI32(tb->selStart, tb->selEnd);
				platform->CopyToClipboard(&tb->text[selMin], selMax - selMin + 1);
				PrintLine_D("Copied %u bytes to the clipboard", selMax - selMin);
			}
			else
			{
				WriteLine_W("No selection to copy");
			}
		}
	}
	
	// +====================================+
	// | Process Sequential Keyboard Input  |
	// +====================================+
	if (IsUiFocused(tb))
	{
		for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
		{
			const InputEvent_t* inputEvent = &appInput->inputEvents[eIndex];
			if (!app->inputEventHandled[eIndex] && inputEvent->type == InputEventType_ButtonTransition)
			{
				bool shift = IsFlagSet(inputEvent->flags, BtnTransFlag_Shift);
				bool ctrl  = IsFlagSet(inputEvent->flags, BtnTransFlag_Ctrl);
				bool alt   = IsFlagSet(inputEvent->flags, BtnTransFlag_Alt);
				bool super = IsFlagSet(inputEvent->flags, BtnTransFlag_Super);
				if (IsFlagSet(inputEvent->flags, BtnTransFlag_Pressed) && IsFlagSet(inputEvent->flags, BtnTransFlag_Char) && inputEvent->character != '\0')
				{
					char c = inputEvent->character;
					if (c == '\n')
					{
						if (IsFlagSet(tb->options, TextBoxOp_MultiLine)) 
						{
							app->inputEventHandled[eIndex] = true;
							app->buttonHandled[inputEvent->button] = true;
							#if DEBUG
							app->inputEventHandledFunc[eIndex] = TB_GetUniqueName(tb);
							#endif
							TB_CharTyped(tb, c);
							tb->gotoCursor = true;
							textChanged = true;
						}
					}
					else if (c == '\b')
					{
						app->inputEventHandled[eIndex] = true;
						app->buttonHandled[inputEvent->button] = true;
						#if DEBUG
						app->inputEventHandledFunc[eIndex] = TB_GetUniqueName(tb);
						#endif
						TB_DeleteButton(tb, ctrl, false);
						tb->gotoCursor = true;
						textChanged = true;
					}
					else
					{
						app->inputEventHandled[eIndex] = true;
						app->buttonHandled[inputEvent->button] = true;
						#if DEBUG
						app->inputEventHandledFunc[eIndex] = TB_GetUniqueName(tb);
						#endif
						TB_CharTyped(tb, c);
						tb->gotoCursor = true;
						textChanged = true;
					}
				}
				else if (IsFlagSet(inputEvent->flags, BtnTransFlag_Pressed) && inputEvent->button == Button_Delete)
				{
					app->inputEventHandled[eIndex] = true;
					app->buttonHandled[inputEvent->button] = true;
					#if DEBUG
					app->inputEventHandledFunc[eIndex] = TB_GetUniqueName(tb);
					#endif
					TB_DeleteButton(tb, ctrl, true);
					tb->gotoCursor = true;
					textChanged = true;
				}
			}
		}
	}
	
	// +==============================+
	// |        Process Paste         |
	// +==============================+
	if (IsUiFocused(tb))
	{
		if (ButtonPressedSeq(Button_V, BtnTransFlag_Ctrl))
		{
			HandleButtonPressSeq(TB_GetUniqueName(tb), Button_V, BtnTransFlag_Ctrl);
			TempPushMark();
			u32 clipboardLength = 0;
			char* clipboardStr = (char*)platform->CopyFromClipboard(TempArena, &clipboardLength);
			//TODO: Crop the size to fit within our maxLength
			if (clipboardStr != nullptr && clipboardLength > 0 && clipboardLength <= TEXT_BOX_MAX_CLIPBOARD_LENGTH)
			{
				u32 sanatizedLength = clipboardLength;
				char* sanatizedStr = SanatizeString(TempArena, clipboardStr, clipboardLength, true, !IsFlagSet(tb->options, TextBoxOp_MultiLine), &sanatizedLength);
				
				if (sanatizedStr != nullptr && sanatizedLength > 0)
				{
					if (tb->selEnd < 0 || tb->selStart < 0)
					{
						tb->selEnd = tb->textLength;
						tb->selStart = tb->selEnd;
					}
					
					if (tb->selEnd == tb->selStart)
					{
						if (tb->maxLength != 0 && tb->textLength + sanatizedLength > tb->maxLength)
						{
							u32 croppedLength = tb->maxLength - tb->textLength;
							if (croppedLength > 0)
							{
								PrintLine_W("Only pasting %u/%u bytes", croppedLength, sanatizedLength);
							}
							else
							{
								WriteLine_W("Can't fit anything from the clipboard paste");
							}
							sanatizedLength = croppedLength;
						}
						
						if (sanatizedLength > 0)
						{
							TB_InsertStringAt(tb, tb->selEnd, sanatizedStr, sanatizedLength);
							tb->selEnd += sanatizedLength;
							tb->selStart = tb->selEnd;
							tb->gotoCursor = true;
							textChanged = true;
						}
					}
					else
					{
						i32 selMin = MinI32(tb->selStart, tb->selEnd);
						i32 selMax = MaxI32(tb->selStart, tb->selEnd);
						if (tb->maxLength != 0 && sanatizedLength > (u32)(selMax - selMin))
						{
							if (tb->textLength + (sanatizedLength - (selMax - selMin)) > tb->maxLength)
							{
								u32 croppedLength = tb->maxLength - tb->textLength + (selMax - selMin);
								PrintLine_W("Only pasting %u/%u bytes", croppedLength, sanatizedLength);
								Assert(croppedLength > 0);
								sanatizedLength = croppedLength;
							}
						}
						
						tb->selEnd = selMax;
						tb->selStart = tb->selEnd;
						TB_ReplacePart(tb, selMin, selMax-selMin, sanatizedStr, sanatizedLength);
						tb->gotoCursor = true;
						textChanged = true;
					}
				}
				else
				{
					PrintLine_W("Sanatized entire %u byte clipboard contents. Nothing can be pasted", clipboardLength);
				}
			}
			else if (clipboardLength > TEXT_BOX_MAX_CLIPBOARD_LENGTH)
			{
				PrintLine_E("WARNING: Can't paste %u byte clipboard contents. Too large!", clipboardLength);
			}
			TempPopMark();
		}
	}
	
	// +==============================+
	// |     Update scrollTarget      |
	// +==============================+
	{
		FontFlowExtra_t flowExtra = {};
		flowExtra.calculateIndexPos = true;
		flowExtra.index = (u32)tb->selEnd;
		
		FontPerformTextFlow(false, tb->text, tb->textLength, textPos, tb->textColor, tb->font, Alignment_Left, tb->fontSize, tb->fontStyle, maxTextWidth, nullptr, &flowExtra);
		
		tb->selEndPos = flowExtra.indexPos;
	}
	if (tb->gotoCursor && tb->selEnd >= 0)
	{
		tb->gotoCursor = false;
		if (IsFlagSet(tb->options, TextBoxOp_ScrollHori))
		{
			r32 extraAmount = tb->bounds.width / 4;
			if (tb->scrollTarget.x + (tb->bounds.width - tb->padding*2) < tb->selEndPos.x - (tb->bounds.x + tb->padding) + tb->scrollOffset.x)
			{
				tb->scrollTarget.x = tb->selEndPos.x - (tb->bounds.x + tb->padding) + tb->scrollOffset.x - (tb->bounds.width - tb->padding*2) + extraAmount;
			}
			if (tb->scrollTarget.x > tb->selEndPos.x - (tb->bounds.x + tb->padding) + tb->scrollOffset.x)
			{
				tb->scrollTarget.x = tb->selEndPos.x - (tb->bounds.x + tb->padding) + tb->scrollOffset.x - extraAmount;
			}
		}
		if (IsFlagSet(tb->options, TextBoxOp_ScrollVert))
		{
			r32 extraAmount = lineHeight/2;
			if (tb->scrollTarget.y + (tb->bounds.height - tb->padding*2) < tb->selEndPos.y - (tb->bounds.y + tb->padding) + tb->scrollOffset.y)
			{
				tb->scrollTarget.y = tb->selEndPos.y - (tb->bounds.y + tb->padding) + tb->scrollOffset.y - (tb->bounds.height - tb->padding*2) + extraAmount;
			}
			if (tb->scrollTarget.y > tb->selEndPos.y - maxExtendUp - (tb->bounds.y + tb->padding) + tb->scrollOffset.y)
			{
				tb->scrollTarget.y = tb->selEndPos.y - maxExtendUp - (tb->bounds.y + tb->padding) + tb->scrollOffset.y - extraAmount;
			}
		}
	}
	
	// +==============================+
	// |     Handle scroll-wheel      |
	// +==============================+
	if (MouseScrolled() && mouseInside)
	{
		HandleMouseScroll();
		if (appInput->scrollDelta.y != 0)
		{
			if (IsFlagSet(tb->options, TextBoxOp_ScrollVert) && !ButtonDown(Button_Shift))
			{
				tb->scrollTarget.y += -appInput->scrollDelta.y * (r32)TEXT_BOX_SCROLL_SPEED;
			}
			else if (IsFlagSet(tb->options, TextBoxOp_ScrollHori))
			{
				tb->scrollTarget.x += -appInput->scrollDelta.y * (r32)TEXT_BOX_SCROLL_SPEED;
			}
		}
		
		if (appInput->scrollDelta.x != 0)
		{
			if (IsFlagSet(tb->options, TextBoxOp_ScrollHori))
			{
				tb->scrollTarget.x += -appInput->scrollDelta.x * (r32)TEXT_BOX_SCROLL_SPEED;
			}
		}
		
		//scrollTarget will get clamped in the beginning of the next update
	}
	
	// +==============================+
	// |          Callbacks           |
	// +==============================+
	if (textChanged && textChangedOut != nullptr)
	{
		*textChangedOut = true;
	}
}

void TextBoxDrawSelectionRec(rec backRec, const char* textPntr, u32 textLength, void* userPntr)
{
	Assert(userPntr != nullptr);
	TextBox_t* tb = (TextBox_t*)userPntr;
	RcDrawRectangle(backRec, IsUiFocused(tb) ? tb->highlightColor : VisGray);
}

void TextBoxDraw(TextBox_t* tb)
{
	r32 lineHeight = FontGetLineHeight(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendUp = FontGetMaxExtendUp(tb->font, tb->fontSize, tb->fontStyle);
	r32 maxExtendDown = FontGetMaxExtendDown(tb->font, tb->fontSize, tb->fontStyle);
	
	// +====================================+
	// | Calculate textPos and maxTextWidth |
	// +====================================+
	//NOTE: This code should match above in TextBoxUpdate
	v2 textPos = tb->bounds.topLeft + NewVec2(tb->padding, tb->bounds.height/2 + lineHeight/2 - maxExtendDown) - tb->scrollOffset;
	if (IsFlagSet(tb->options, TextBoxOp_MultiLine))
	{
		textPos.y = tb->bounds.y + tb->padding + maxExtendUp - tb->scrollOffset.y;
	}
	textPos = Vec2Round(textPos);
	r32 maxTextWidth = 0;
	if (IsFlagSet(tb->options, TextBoxOp_MultiLine) && !IsFlagSet(tb->options, TextBoxOp_AutoSizeHori) && !IsFlagSet(tb->options, TextBoxOp_ScrollHori))
	{
		maxTextWidth = tb->bounds.width - tb->padding*2;
	}
	
	i32 selectionMin = MinI32(tb->selStart, tb->selEnd);
	i32 selectionMax = MaxI32(tb->selStart, tb->selEnd);
	if (selectionMin < 0 || selectionMax < 0)
	{
		selectionMin = 0;
		selectionMax = 0;
	}
	bool showCursor = (IsUiFocused(tb) && tb->selEnd >= 0 && (u32)tb->selEnd <= tb->textLength);
	
	rec oldViewport = rc->viewport;
	Font_t* oldFont = rc->boundFont;
	u16 oldStyle = rc->fontStyle;
	r32 oldSize = rc->fontSize;
	Alignment_t oldAlignment = rc->fontAlignment;
	
	Color_t backColor = tb->backColor;
	Color_t borderColor = tb->borderColor;
	Color_t shadowColor = NewColor(Color_Black);
	if (IsUiFocused(tb))
	{
		borderColor = tb->borderFocusColor;
		// shadowColor = tb->borderFocusColor;
	}
	else
	{
		backColor = ColorDarken(backColor, 25);
	}
	if (tb->type == TextBoxType_SignedNumber || tb->type == TextBoxType_UnsignedNumber || tb->type == TextBoxType_HexNumber)
	{
		if (!TextBoxIsValidNumber(tb))
		{
			borderColor = VisRed;
		}
	}
	if (tb->type == TextBoxType_Url)
	{
		if (!TryParseUrl(tb->text, tb->textLength, nullptr))
		{
			borderColor = VisRed;
		}
	}
	if (tb->type == TextBoxType_UrlOrAddress)
	{
		if (!TryParseIPv4(tb->text, tb->textLength, nullptr) && !TryParseUrl(tb->text, tb->textLength, nullptr))//TODO: Check if it's parsable as IPv6
		{
			borderColor = VisRed;
		}
	}
	
	RcSetViewport(RecOverlap(tb->bounds, oldViewport));
	RcDrawRectangle(tb->bounds, backColor);
	
	RcSetViewport(RecOverlap(RecDeflate(tb->bounds, tb->padding), oldViewport));
	RcBindFont(tb->font);
	RcSetFontSize(tb->fontSize);
	RcSetFontStyle(tb->fontStyle);
	RcSetFontAlignment(Alignment_Left);
	
	FontFlowExtra_t flowExtra = {};
	if (showCursor)
	{
		flowExtra.calculateIndexPos = true;
		flowExtra.index = (u32)tb->selEnd;
	}
	flowExtra.doSelection = true;
	flowExtra.selStart = MinI32(tb->selStart, tb->selEnd);
	flowExtra.selEnd = MaxI32(tb->selStart, tb->selEnd);
	flowExtra.selChangesColor = true;
	flowExtra.selColor = tb->textHighlightColor;
	flowExtra.selEndFunction = TextBoxDrawSelectionRec;
	flowExtra.selEndFunctionUserPntr = (void*)tb;
	
	RcDrawString(tb->text, tb->textLength, textPos, tb->textColor, maxTextWidth, &tb->flowInfo, &flowExtra);
	tb->flowInfoFilled = true;
	if (tb->textLength == 0 && tb->hint != nullptr)
	{
		RcDrawString(tb->hint, tb->hintLength, textPos, ColorTransparent(tb->textColor, 0.5f), maxTextWidth, nullptr, nullptr);
	}
	
	if (showCursor)
	{
		v2 cursorPos = flowExtra.indexPos;
		rec cursorRec = NewRec(cursorPos.x, cursorPos.y - RcMaxExtendUp(), 2, RcLineHeight());
		RcDrawRectangle(cursorRec, ColorLerp(Transparent, tb->textColor, OscillatePhase(0, 1, 1000)));
	}
	
	RcSetViewport(RecOverlap(tb->bounds, oldViewport));
	// if (!IsUiFocused(tb))
	// {
	// 	RcDrawRightTriangle(NewRec(tb->bounds.topLeft, NewVec2(tb->padding*2)), tb->borderColor, 0);
	// 	RcDrawRightTriangle(NewRec(tb->bounds.topLeft + NewVec2(tb->bounds.width - tb->padding*2, 0), NewVec2(tb->padding*2)), tb->borderColor, Pi32/2);
	// 	RcDrawRightTriangle(NewRec(tb->bounds.topLeft + NewVec2(0, tb->bounds.height - tb->padding*2), NewVec2(tb->padding*2)), tb->borderColor, Pi32*3/2);
	// 	RcDrawRightTriangle(NewRec(tb->bounds.topLeft + tb->bounds.size - NewVec2(tb->padding*2), NewVec2(tb->padding*2)), tb->borderColor, Pi32);
	// }
	// +==============================+
	// |         Draw Shadows         |
	// +==============================+
	if (IsFlagSet(tb->options, TextBoxOp_ScrollHori) && tb->scrollTarget.x > 0) //left
	{
		rec shadowRec = NewRec(tb->bounds.x, tb->bounds.y, tb->padding*2, tb->bounds.height);
		RcDrawGradient(shadowRec, ColorTransparent(shadowColor, 0.0f), ColorTransparent(shadowColor, 1.0f), Dir2_Left);
	}
	if (IsFlagSet(tb->options, TextBoxOp_ScrollHori) && tb->scrollTarget.x < tb->actualMaxScroll.x) //right
	{
		rec shadowRec = NewRec(tb->bounds.x + tb->bounds.width - tb->padding*2, tb->bounds.y, tb->padding*2, tb->bounds.height);
		RcDrawGradient(shadowRec, ColorTransparent(shadowColor, 0.0f), ColorTransparent(shadowColor, 1.0f), Dir2_Right);
	}
	if (IsFlagSet(tb->options, TextBoxOp_ScrollVert) && tb->scrollTarget.y > 0) //top
	{
		rec shadowRec = NewRec(tb->bounds.x, tb->bounds.y, tb->bounds.width, tb->padding*2);
		RcDrawGradient(shadowRec, ColorTransparent(shadowColor, 0.0f), ColorTransparent(shadowColor, 1.0f), Dir2_Up);
	}
	if (IsFlagSet(tb->options, TextBoxOp_ScrollVert) && tb->scrollTarget.y < tb->actualMaxScroll.y) //bottom
	{
		rec shadowRec = NewRec(tb->bounds.x, tb->bounds.y + tb->bounds.height - tb->padding*2, tb->bounds.width, tb->padding*2);
		RcDrawGradient(shadowRec, ColorTransparent(shadowColor, 0.0f), ColorTransparent(shadowColor, 1.0f), Dir2_Down);
	}
	
	RcDrawBorderedRec(tb->bounds, Transparent, borderColor, 2);
	
	RcSetViewport(oldViewport);
	RcBindFont(oldFont);
	RcSetFontSize(oldSize);
	RcSetFontStyle(oldStyle);
	RcSetFontAlignment(oldAlignment);
}
