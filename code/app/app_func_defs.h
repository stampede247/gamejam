/*
File:   app_func_defs.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_FUNC_DEFS_H
#define _APP_FUNC_DEFS_H

//app_loading_functions.cpp
void DestroyTexture(Texture_t* texturePntr);
Texture_t CreateTexture(const u8* bitmapData, i32 width, i32 height, bool pixelated = false, bool repeat = true);

//app_font_flow.cpp
v2 RcDrawString(const char* strPntr, u32 strLength, v2 position, Color_t color, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr);
v2 RcDrawNtString(const char* nullTermString, v2 position, Color_t color, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr);
v2 RcPrintStringFull(v2 position, Color_t color, r32 maxWidth, FontFlowInfo_t* flowInfo, FontFlowExtra_t* flowExtra, const char* formatString, ...);
#define RcPrintString(position, color, formatString, ...) RcPrintStringFull(position, color, 0, nullptr, nullptr, formatString, ##__VA_ARGS__)
v2 RcMeasureString(const char* strPntr, u32 strLength, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr);
v2 RcMeasureNtString(const char* nullTermString, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr);
r32 RcMeasureLineWidth(const char* strPntr, u32 strLength, r32 maxWidth = 0);
r32 RcMeasureNtLineWidth(const char* nullTermString, r32 maxWidth = 0);
r32 RcLineHeight();
r32 RcMaxExtendUp();
r32 RcMaxExtendDown();
r32 RcLineHeightAt(r32 fontSize);
r32 RcMaxExtendUpAt(r32 fontSize);
r32 RcMaxExtendDownAt(r32 fontSize);

//app_console_command_parser.cpp
void HandleConsoleCommand(const char* command);

#endif //  _APP_FUNC_DEFS_H
