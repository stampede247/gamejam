/*
File:   app_text_box.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_TEXT_BOX_H
#define _APP_TEXT_BOX_H

enum //TextBoxOp_
{
	TextBoxOp_AllowCopy    = 0x01,
	TextBoxOp_AllowPaste   = 0x02,
	TextBoxOp_AutoSizeHori = 0x04,
	TextBoxOp_AutoSizeVert = 0x08,
	TextBoxOp_MultiLine    = 0x10,
	TextBoxOp_ScrollHori   = 0x20,
	TextBoxOp_ScrollVert   = 0x40,
	TextBoxOp_HandleUpDown = 0x80,
};

#define TextBoxOp_Default    (TextBoxOp_AllowPaste|TextBoxOp_AllowCopy|TextBoxOp_ScrollHori|TextBoxOp_HandleUpDown)
#define TextBoxOp_SingleLine (TextBoxOp_AllowPaste|TextBoxOp_AllowCopy|TextBoxOp_ScrollHori|TextBoxOp_AutoSizeVert)

typedef enum
{
	TextBoxType_Default,
	TextBoxType_UnsignedNumber,
	TextBoxType_SignedNumber,
	TextBoxType_HexNumber,
	TextBoxType_Url,
	TextBoxType_UrlOrAddress,
} TextBoxType_t;

struct TextBox_t
{
	MemoryArena_t* allocArena;
	u32 textLength;
	u32 textLengthAlloc; //includes '\0'
	char* text;
	u32 hintLength;
	char* hint;
	
	Font_t* font;
	u16 fontStyle;
	r32 fontSize;
	u32 maxLength;
	v2 minSize;
	v2 maxSize;
	u8 options;
	r32 padding;
	TextBoxType_t type;
	
	Color_t textColor;
	Color_t textHighlightColor;
	Color_t backColor;
	Color_t borderColor;
	Color_t borderFocusColor;
	Color_t highlightColor;
	
	rec bounds;
	v2 scrollOffset;
	v2 scrollTarget;
	v2 maxScroll;
	v2 actualMaxScroll; //doesn't include padding whitespace on bottom/right
	i32 selStart;
	i32 selEnd;
	v2 selEndPos;
	bool gotoCursor;
	bool mouseStartedInside;
	i32 mouseHoverIndex;
	v2 mouseHoverIndexPos;
	bool flowInfoFilled;
	u64 lastClickTime;
	u32 numClicks;
	i32 lastClickIndex;
	FontFlowInfo_t flowInfo;
};

#endif //  _APP_TEXT_BOX_H
