/*
File:   app_console_command_parser.cpp
Author: Taylor Robbins
Date:   07\28\2019
Description: 
	** This file handles parsing the console commands and making sure that their arguments are present and correctly formatted
*/

void HandleConsoleCommand(const char* command) //Pre-declared in app_func_defs.h
{
	u32 commandLength = (u32)strlen(command);
	u32 numPieces = 0;
	StrSplitPiece_t* pieces = SplitString(TempArena, command, commandLength, " ", 1, &numPieces);
	if (numPieces == 0 || pieces[0].length == 0) { return; }
	char* baseCmd = ArenaString(TempArena, pieces[0].pntr, pieces[0].length);
	u32 baseCmdLength = pieces[0].length;
	
	if (baseCmdLength == 0) { WriteLine_E("No command given!"); return; }
	
	const char* cmdName = nullptr;
	u32 cmdArgCount = 0;
	const char* cmdArgNames[CSL_MAX_CMD_ARGUMENTS];
	CslArgType_t cmdArgTypes[CSL_MAX_CMD_ARGUMENTS];
	bool cmdArgOptionals[CSL_MAX_CMD_ARGUMENTS];
	for (u32 cIndex = 0; true; cIndex++)
	{
		cmdName = nullptr;
		cmdArgCount = 0;
		ClearArray(cmdArgNames);
		ClearArray(cmdArgTypes);
		ClearArray(cmdArgOptionals);
		CslCmd_f* cmdFunc = GetConsoleCommandInfo(cIndex, &cmdName, &cmdArgCount, &cmdArgNames[0], &cmdArgTypes[0], &cmdArgOptionals[0]);
		if (cmdFunc == nullptr) { break; }
		Assert(cmdName != nullptr);
		Assert(cmdArgCount <= CSL_MAX_CMD_ARGUMENTS);
		u32 cmdNameLength = (u32)strlen(cmdName);
		if (cmdNameLength == baseCmdLength && StrCompareIgnoreCase(cmdName, baseCmd, baseCmdLength))
		{
			bool argsValid = true;
			char* invalidStr = nullptr;
			for (u32 aIndex = 0; aIndex < cmdArgCount; aIndex++)
			{
				Assert(cmdArgNames[aIndex] != nullptr);
				Assert(cmdArgTypes[aIndex] != CslArgType_None && cmdArgTypes[aIndex] < CslArgType_NumTypes);
				const char* argName = cmdArgNames[aIndex];
				CslArgType_t argType = cmdArgTypes[aIndex];
				bool optional = cmdArgOptionals[aIndex];
				
				if (1+aIndex >= numPieces && !optional) { argsValid = false; invalidStr = TempPrint("Missing argument %u(%s)", aIndex+1, argName); break; }
				if (1+aIndex >= numPieces) { break; }
				const char* argStr = pieces[1+aIndex].pntr;
				u32 argLength = pieces[1+aIndex].length;
				
				if (argLength == 0 && (argType == CslArgType_Hex8 || argType == CslArgType_Hex32 || argType == CslArgType_Uint8 || argType == CslArgType_Uint32 || argType == CslArgType_Int32))
				{ argsValid = false; invalidStr = TempPrint("Arguement %u(%s) cannot be empty", aIndex+1, argName); break; }
				
				if (argType == CslArgType_Hex8)
				{
					u8 value = 0x00;
					if (argLength != 2) { argsValid = false; invalidStr = TempPrint("Expected 2 character HEX for argument %u(%s): \"%.*s\"", aIndex+1, argName, argLength, argStr); break; }
					if (!TryParseHexU8(argStr, &value)) { argsValid = false; invalidStr = TempPrint("Couldn't parse argument %u(%s) as HEX uint8: \"%.*s\"", aIndex+1, argName, argLength, argStr); break; }
				}
				if (argType == CslArgType_Hex32)
				{
					u32 value = 0x00000000;
					if (!TryParseHexU32(argStr, argLength, &value)) { argsValid = false; invalidStr = TempPrint("Couldn't parse argument %u(%s) as HEX uint32: \"%.*s\"", aIndex+1, argName, argLength, argStr); break; }
				}
				if (argType == CslArgType_Uint8)
				{
					u8 value = 0;
					if (!TryParseU8(argStr, argLength, &value)) { argsValid = false; invalidStr = TempPrint("Couldn't parse argument %u(%s) as uint8: \"%.*s\"", aIndex+1, argName, argLength, argStr); break; }
				}
				if (argType == CslArgType_Uint32)
				{
					u32 value = 0;
					if (!TryParseU32(argStr, argLength, &value)) { argsValid = false; invalidStr = TempPrint("Couldn't parse argument %u(%s) as uint32: \"%.*s\"", aIndex+1, argName, argLength, argStr); break; }
				}
				if (argType == CslArgType_Int32)
				{
					i32 value = 0;
					if (!TryParseI32(argStr, argLength, &value)) { argsValid = false; invalidStr = TempPrint("Couldn't parse argument %u(%s) as int32: \"%.*s\"", aIndex+1, argName, argLength, argStr); break; }
				}
			}
			
			if (argsValid)
			{
				cmdFunc(baseCmd, numPieces-1, &pieces[1]);
				return;
			}
			else
			{
				Print_E("Usage: %s", cmdName);
				for (u32 aIndex = 0; aIndex < cmdArgCount; aIndex++)
				{
					const char* argName = cmdArgNames[aIndex];
					bool optional = cmdArgOptionals[aIndex];
					if (optional) { Print_E(" {%s}", argName); }
					else { Print_E(" [%s]", argName); }
				}
				PrintLine_E(" Error: %s", invalidStr);
				return;
			}
		}
	}
	
	PrintLine_E("Unknown command \"%s\"", baseCmd);
}