/*
File:   app_states.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_STATES_H
#define _APP_STATES_H

typedef enum
{
	AppState_None = 0x00,
	
	AppState_MainMenu,
	AppState_Game,
	AppState_Mess,
	
	AppState_NumStates = 7,
} AppState_t;

const char* GetAppStateName(AppState_t appState)
{
	switch (appState)
	{
		case AppState_None:         return "None";
		case AppState_MainMenu:     return "Main Menu";
		case AppState_Game:         return "Game";
		case AppState_Mess:         return "Mess";
		default: return "Unknown";
	};
}

typedef enum
{
	AppMenu_None = 0x00,
	
	AppMenu_Settings,
	
	AppMenu_NumMenus = 1,
} AppMenu_t;

const char* GetAppMenuName(AppMenu_t appMenu)
{
	switch (appMenu)
	{
		case AppMenu_None:     return "None";
		case AppMenu_Settings: return "Settings";
		default: return "Unknown";
	};
}

typedef enum //TransInfoType_
{
	TransInfoType_Nothing = 0x00,
	TransInfoType_TargetLevelInfo,
	TransInfotype_Multiplayer,
	TransInfotype_SaveGame,
	TransInfoType_PackComplete,
} TransInfoType_t;

enum
{
	AppStateChangeType_Push,
	AppStateChangeType_Pop,
	AppStateChangeType_Change,
};

struct AppStateChange_t
{
	u8 type;
	AppState_t newAppState;
	bool stayInitialized;
	bool forceResetNewState;
};

struct AppMenuChange_t
{
	u8 type;
	AppMenu_t newAppMenu;
	bool stayInitialized;
	bool forceResetNewMenu;
};

// +--------------------------------------------------------------+
// |                       Public Functions                       |
// +--------------------------------------------------------------+
void PutTransitionInfo(TransInfoType_t type, void* infoPntr);

bool IsAppStateInitialized(AppState_t appState);
bool IsAppStateActive(AppState_t appState, u32* activeIndexOut = nullptr);
AppState_t CurrentAppState();
AppState_t AppStateBelowMe(u32 numBelow = 1);
AppState_t AppStateAboveMe(u32 numAbove = 1);
bool KillAppState(AppState_t appState, bool stayInitialized = false);
void PushAppState(AppState_t newAppState, bool forceResetNewState = false);
void PushAppStateExt(AppState_t newAppState, TransInfoType_t type, void* infoPntr, bool forceResetNewState = false);
void PopAppState(bool stayInitialized = false, bool forceResetNewState = false);
void PopAppStateExt(TransInfoType_t type, void* infoPntr, bool stayInitialized = false, bool forceResetNewState = false);
void ChangeAppState(AppState_t newAppState, bool stayInitialized = false, bool forceResetNewState = false);
void ChangeAppStateExt(AppState_t newAppState, TransInfoType_t type, void* infoPntr, bool stayInitialized = false, bool forceResetNewState = false);

bool IsAppMenuInitialized(AppMenu_t appMenu);
bool IsAppMenuActive(AppMenu_t appMenu, u32* activeIndexOut = nullptr);
AppMenu_t CurrentAppMenu();
AppMenu_t AppMenuBelowMe(u32 numBelow = 1);
AppMenu_t AppMenuAboveMe(u32 numAbove = 1);
bool KillAppMenu(AppMenu_t appMenu, bool stayInitialized = false);
void PushAppMenu(AppMenu_t newAppMenu, bool forceResetNewMenu = false);
void PushAppMenuExt(AppMenu_t newAppMenu, TransInfoType_t type, void* infoPntr, bool forceResetNewMenu = false);
void PopAppMenu(bool stayInitialized = false, bool forceResetNewMenu = false);
void PopAppMenuExt(TransInfoType_t type, void* infoPntr, bool stayInitialized = false, bool forceResetNewMenu = false);
void ChangeAppMenu(AppMenu_t newAppMenu, bool stayInitialized = false, bool forceResetNewMenu = false);
void ChangeAppMenuExt(AppMenu_t newAppMenu, TransInfoType_t type, void* infoPntr, bool stayInitialized = false, bool forceResetNewMenu = false);

#endif //  _APP_STATES_H
