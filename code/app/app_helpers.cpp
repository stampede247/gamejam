/*
File:   app_helpers.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds functions that the various AppStates might use for random tasks.
	** It's mostly just a dumping ground for functions that have no proper home
*/

char* FormattedTimeStr(RealTime_t realTime)
{
	char* result;
	
	result = TempPrint("%s %u:%02u:%02u%s (%s %s, %u)",
		GetDayOfWeekStr((DayOfWeek_t)realTime.dayOfWeek),
		Convert24HourTo12Hour(realTime.hour), realTime.minute, realTime.second,
		IsPostMeridian(realTime.hour) ? "pm" : "am",
		GetMonthStr((Month_t)realTime.month), GetDayOfMonthString(realTime.day), realTime.year
	);
	
	return result;
}

char* FormattedTimeStr(u64 timestamp)
{
	RealTime_t realTime;
	GetRealTime(timestamp, true, &realTime);
	return FormattedTimeStr(realTime);
}

char* SanatizeStringAdvanced(const char* strPntr, u32 numChars, MemoryArena_t* arenaPntr,
	bool keepNewLines, bool keepBackspaces, bool convertInvalidToHex)
{
	char* result = nullptr;
	u32 resultLength = 0;
	
	for (u8 round = 0; round < 2; round++)
	{
		Assert(round == 0 || result != nullptr);
		
		//Run this code twice, once to measure and once to actually copy into buffer
		u32 fromIndex = 0;
		u32 toIndex = 0;
		while (fromIndex < numChars)
		{
			char c = strPntr[fromIndex];
			char lastChar = '\0';
			if (fromIndex > 0) { lastChar = strPntr[fromIndex-1]; }
			char nextChar = strPntr[fromIndex+1];
			
			bool charHandled = false;
			if (c == '\n')
			{
				if (keepNewLines)
				{
					if (result != nullptr) { result[toIndex] = '\n'; }
					toIndex++;
					charHandled = true;
				}
			}
			else if (c == '\r')
			{
				if (nextChar == '\n' || lastChar == '\n')
				{
					//Drop the \r character
					charHandled = true;
				}
				else if (keepNewLines)
				{
					if (result != nullptr) { result[toIndex] = '\n'; }
					toIndex++;
					charHandled = true;
				}
			}
			else if (c == '\b')
			{
				if (keepBackspaces)
				{
					if (result != nullptr) { result[toIndex] = '\b'; }
					toIndex++;
					charHandled = true;
				}
			}
			else if (IsCharClassPrintable(c) || c == '\t')
			{
				if (result != nullptr) { result[toIndex] = c; }
				toIndex++;
				charHandled = true;
			}
			
			if (charHandled == false && convertInvalidToHex)
			{
				if (result != nullptr)
				{
					result[toIndex+0] = '0';
					result[toIndex+1] = 'x';
					result[toIndex+2] = UpperHexChar(c);
					result[toIndex+3] = LowerHexChar(c);
				}
				toIndex += 4;
			}
			
			fromIndex++;
		}
		
		resultLength = toIndex;
		if (resultLength == 0) { break; }
		if (round == 0)
		{
			result = (char*)ArenaPush(arenaPntr, resultLength+1);
		}
		result[resultLength] = '\0';
	}
	
	return result;
}

char* GetElapsedString(u64 timespan)
{
	char* result = nullptr;
	
	u32 numDays = (u32)(timespan/(60*60*24));
	u32 numHours = (u32)(timespan/(60*60)) - (numDays*24);
	u32 numMinutes = (u32)(timespan/60) - (numDays*60*24) - (numHours*60);
	u32 numSeconds = (u32)(timespan) - (numDays*60*60*24) - (numHours*60*60) - (numMinutes*60);
	if (numDays > 0)
	{
		result = TempPrint("%ud %uh %um %us", numDays, numHours, numMinutes, numSeconds);
	}
	else if (numHours > 0)
	{
		result = TempPrint("%uh %um %us", numHours, numMinutes, numSeconds);
	}
	else if (numMinutes > 0)
	{
		result = TempPrint("%um %us", numMinutes, numSeconds);
	}
	else
	{
		result = TempPrint("%us", numSeconds);
	}
	
	return result;
}

r32 Oscillate(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (SinR32(((ProgramTime % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	return min + (max - min) * lerpValue;
}
r32 OscillatePhase(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (SawR32(((ProgramTime % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	lerpValue = Ease(EasingStyle_CubicOut, lerpValue);
	return min + (max - min) * lerpValue;
}
r32 OscillateSaw(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (SawR32(((ProgramTime % periodMs) / (r32)periodMs) * 2*Pi32) + 1.0f) / 2.0f;
	return min + (max - min) * lerpValue;
}
r32 Animate(r32 min, r32 max, u32 periodMs)
{
	r32 lerpValue = (ProgramTime % periodMs) / (r32)periodMs;
	return min + (max - min) * lerpValue;
}

u64 TimeSince(u64 programTimeSnapshot)
{
	u64 programTime = ProgramTime;
	if (programTimeSnapshot <= programTime)
	{
		return programTime - programTimeSnapshot;
	}
	else 
	{
		return 0;
	}
}
