/*
File:   app_version.h
Author: Taylor Robbins
Date:   07\14\2019
Description:
	** Defines the macros that contain the values for the current application version numbers
	** The build number is incremented automatically by a python script that runs before each build of the application
*/

#ifndef _APP_VERSION_H
#define _APP_VERSION_H

#define APP_VERSION_MAJOR 1
#define APP_VERSION_MINOR 0

//NOTE: Auto-incremented by a python script before each build
#define APP_VERSION_BUILD 739

#endif //  _APP_VERSION_H
