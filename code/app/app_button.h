/*
File:   app_button.h
Author: Taylor Robbins
Date:   07\26\2019
*/

#ifndef _APP_BUTTON_H
#define _APP_BUTTON_H

enum //ButtonOp_
{
	ButtonOp_AutoSizeHori    = 0x01,
	ButtonOp_AutoSizeVert    = 0x02,
	ButtonOp_Focusable       = 0x04,
	ButtonOp_EnterPressable  = 0x08,
	ButtonOp_EnterRepeatable = 0x10,
	ButtonOp_OnDown          = 0x20,
	ButtonOp_Picture         = 0x40,
};

#define ButtonOp_Default (ButtonOp_AutoSizeHori|ButtonOp_AutoSizeVert)

struct Button_t
{
	MemoryArena_t* allocArena;
	u32 textLength;
	u32 textLengthAlloc;
	char* text;
	
	Font_t* font;
	u16 fontStyle;
	r32 fontSize;
	v2 minSize;
	v2 maxSize;
	u8 options;
	r32 padding;
	r32 rounding;
	const Texture_t* picture;
	r32 pictureScale;
	
	Color_t textColor;
	Color_t textDownColor;
	Color_t backColor;
	Color_t backDownColor;
	
	rec bounds;
	bool isDown;
	bool mouseStartedInside;
	bool flowInfoFilled;
	FontFlowInfo_t flowInfo;
};

#endif //  _APP_BUTTON_H
