/*
File:   app_defines.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_DEFINES_H
#define _APP_DEFINES_H

#define USE_BOX2D true

#define ALL_DEBUG_OUTPUT_ENABLED true
#define DEBUG_OUTPUT_ENABLED     (true && ALL_DEBUG_OUTPUT_ENABLED)
#define REGULAR_OUTPUT_ENABLED   (true && ALL_DEBUG_OUTPUT_ENABLED)
#define INFO_OUTPUT_ENABLED      (true && ALL_DEBUG_OUTPUT_ENABLED)
#define NOTIFY_OUTPUT_ENABLED    (true && ALL_DEBUG_OUTPUT_ENABLED)
#define OTHER_OUTPUT_ENABLED     (true && ALL_DEBUG_OUTPUT_ENABLED)
#define WARNING_OUTPUT_ENABLED   (true && ALL_DEBUG_OUTPUT_ENABLED)
#define ERROR_OUTPUT_ENABLED     (true && ALL_DEBUG_OUTPUT_ENABLED)

#define TAB_WIDTH                      4 //spaces
#define TRANSIENT_MAX_NUMBER_MARKS     32
#define TILE_SIZE                      16 //px
#define GROWING_HEAP_PAGE_SIZE         Megabytes(1)
#define GROWING_HEAP_MAX_NUM_PAGES     128
#define MAX_BATCHING_FONTS             4
#define FONT_BATCH_CHUNK_SIZE          1024 //glyphs
#define FONT_BATCH_BASE_CHUNK_SIZE     2048 //glyphs
#define MAX_RENDER_CONTEXT_PUSHES      4
#define DYNAMIC_VERT_BUFFER_CHUNK_SIZE 32 //vertices
#define RC_MAX_DYNAMIC_VERTICES        32 //vertices         

#define DEBUG_FONT_SIZE 14 //pnt
#define MAIN_FONT_SIZE  16 //pnt

#define INITIAL_APP_STATE  AppState_Mess //AppState_Game

//MM stands for Main Menu, PW_ stands for Pack Window
#define MM_SHOW_VERSION_NUMS true

//SETT stands for Settings Menu
#define SETT_MENU_OPEN_TIME      200
#define SETT_MENU_CLOSE_TIME     200
#define SETT_KEYBORD_KEY_SIZE    (16*1) //px
#define SETT_KEYBORD_KEY_PADDING (2*1) //px

//CSL stands for Debug Console
#define CSL_FIFO_SPACE_SIZE             Kilobytes(64)
#define CSL_OPEN_TIME                   100 //ms
#define CSL_CLOSE_TIME                  100 //ms
#define CSL_DROP_DOWN_OPEN_TIME         100 //ms
#define CSL_DROP_DOWN_CLOSE_TIME        100 //ms
#define CSL_FONT                        (&resources->debugFont)
#define CSL_FONT_SIZE                   14
#define CSL_GUTTER_NUM_FONT             (&resources->debugFont)
#define CSL_GUTTER_NUM_FONT_SIZE        14
#define CSL_FILE_NAME_FONT              (&resources->pixelFont)
#define CSL_FILE_NAME_FONT_SIZE         8
#define CSL_LINE_NUM_FONT               (&resources->pixelFont)
#define CSL_LINE_NUM_FONT_SIZE          8
#define CSL_FUNC_NAME_FONT              (&resources->pixelFont)
#define CSL_FUNC_NAME_FONT_SIZE         8
#define CSL_LINE_SPACING                1 //px
#define CSL_SCROLL_BAR_WIDTH            5 //px
#define CSL_MIN_SCROLLBAR_HEIGHT        20 //px
#define CSL_INPUT_BOX_MAX_CHARS         256 //chars
#define CSL_SELECT_SCROLL_OVER_AMOUNT   200 //px
#define CSL_SCROLL_SPEED                (16*2) //px/notch
#define CSL_CHECKBOX_SIZE               16 //px
#define CSL_DEFAULT_LINE_WRAP           true
#define CSL_DEFAULT_SHOW_GUTTER_NUMS    true
#define CSL_DEFAULT_SHOW_FILE_NAMES     true
#define CSL_DEFAULT_SHOW_LINE_NUMBERS   true
#define CSL_DEFAULT_SHOW_FUNCTION_NAMES true
#define CSL_MAX_PREV_COMMADS_RECALL     16
#define CSL_MAX_CMD_ARGUMENTS           8

//app_textbox.cpp
#define TEXT_BOX_MAX_CLIPBOARD_LENGTH   2048 //chars
#define TEXT_BOX_SCROLL_TWEEN_SNAP_DIST 2 //px
#define TEXT_BOX_SCROLL_LATENCY         5 //divisor
#define TEXT_BOX_SCROLL_SPEED           50 //px
#define TEXT_BOX_DOUBLE_CLICK_TIME      300 //ms

#define MOVEMENT_STICK_DEADZONE 0.2f

#define MAX_SOUND_INSTANCES                  16
#define DOUBLE_CLICK_TIME                    500//ms
#define DEF_MUSIC_TRANS_TIME                 2000 //ms
#define SND_EFFECT_COMBINE_TIME              33 //ms (2 frames)
#define FRAMERATE_GRAPH_WIDTH                120 //ticks
#define FRAMERATE_VERT_TICK_VAL              4 //ms

#define GAME_BTN_ANALOG_DEADZONE   0.8f

#define MISSING_TEXTURE_PATH "Resources/Textures/missing.jpg"
#define PALETTE_FILE_PATH    "Resources/Palette/palette1.png"

#define FONTS_FOLDER    "Resources/Fonts/"
#define MUSIC_FOLDER    "Resources/Music/"
#define SHADERS_FOLDER  "Resources/Shaders/"
#define SOUNDS_FOLDER   "Resources/Sounds/"
#define SPRITES_FOLDER  "Resources/Sprites/"
#define TEXTURES_FOLDER "Resources/Textures/"
#define SHEETS_FOLDER   "Resources/Sheets/"

#define CHAR_DIAMOND "\x03"
#define CHAR_CHECK   "\x04"

#define White            NewColor(0xFFFFFFFF)
#define Transparent      NewColor(0x00000000)

#define VisLightSeaGreen NewColor(0xFF1ABC9C)
#define VisLightGreen    NewColor(0xFF2ECC71)
#define VisLightBlue     NewColor(0xFF3498DB)
#define VisLightPurple   NewColor(0xFF9B59B6)
#define VisLightBlueGray NewColor(0xFF34495E)
#define VisSeaGreen      NewColor(0xFF16A085)
#define VisGreen         NewColor(0xFF27AE60)
#define VisBlue          NewColor(0xFF2980B9)
#define VisPurple        NewColor(0xFF8E44AD)
#define VisBlueGray      NewColor(0xFF2C3E50)
#define VisYellow        NewColor(0xFFF1C40F)
#define VisLightOrange   NewColor(0xFFE67E22)
#define VisRed           NewColor(0xFFE74C3C)
#define VisWhite         NewColor(0xFFECF0F1)
#define VisLightGray     NewColor(0xFF95A5A6)
#define VisDarkYellow    NewColor(0xFFF39C12)
#define VisOrange        NewColor(0xFFD35400)
#define VisDarkRed       NewColor(0xFFC0392B)
#define VisSilver        NewColor(0xFFBDC3C7)
#define VisGray          NewColor(0xFF7F8C8D)

#define MonokaiBack        NewColor(0xFF3B3A32)
#define MonokaiYellow      NewColor(0xFFE6DB74)
#define MonokaiLightYellow NewColor(0xFFFFE792)
#define MonokaiFadedYellow NewColor(0xFFFFEFB7)
#define MonokaiPurple      NewColor(0xFFAE81FF)
#define MonokaiLightPurple NewColor(0xFFE777FF)
#define MonokaiGreen       NewColor(0xFFA6E22E)
#define MonokaiDarkGreen   NewColor(0xFF829520)
#define MonokaiOrange      NewColor(0xFFFD971F)
#define MonokaiBrown       NewColor(0xFF9D550F)
#define MonokaiMagenta     NewColor(0xFFF92672)
#define MonokaiRed         NewColor(0xFFF83333)
#define MonokaiLightRed    NewColor(0xFFFF5959)
#define MonokaiBlue        NewColor(0xFF66D9EF)
#define MonokaiLightBlue   NewColor(0xFFA9FFFF)
#define MonokaiWhite       NewColor(0xFFF8F8F2)
#define MonokaiLightGray   NewColor(0xFFBBBBBB)
#define MonokaiGray1       NewColor(0xFFAFAFA2)
#define MonokaiGray2       NewColor(0xFF75715E)
#define MonokaiDarkGray    NewColor(0xFF212121)
#define MonokaiBlack       NewColor(0xFF000000)

#endif //  _APP_DEFINES_H
