/*
File:   app_input_display.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Handles laying out and displaying the keyboard, DS4 controller, and XBOX controller displays for various purposes
*/

const char* GetKeyboardDisplayStr(Buttons_t button, bool isAlt = false)
{
	const char* result = GetButtonName(button);
	char btnChar = GetButtonChar(button, false, true);
	if (button == Button_Backspace)     { result = "\x1B-"; }
	else if (button == Button_Enter)    { result = "\x1B\x1C"; }
	else if (button == Button_Shift)    { result = "Shift"; }
	else if (button == Button_Control)  { result = "Ctrl"; }
	else if (button == Button_Escape)   { result = "Esc"; }
	else if (button == Button_CapsLock) { result = "Caps"; }
	else if (button == Button_Insert)   { result = "Ins"; }
	else if (button == Button_Delete)   { result = "Dlt"; }
	else if (button == Button_PageUp)   { result = "PgUp"; }
	else if (button == Button_PageDown) { result = "PgDn"; }
	else if (button == Button_Up)       { result = "\x18"; }
	else if (button == Button_Left)     { result = "\x1B"; }
	else if (button == Button_Right)    { result = "\x1A"; }
	else if (button == Button_Down)     { result = "\x19"; }
	else if (button == Button_NumLock)  { result = "#Lk"; }
	else if (IsCharClassPrintable(btnChar)) { result = TempPrint("%c", btnChar); }
	return result;
}

bool IsKeyboardBtnUnbindable(Buttons_t button)
{
	// if (button == Button_Shift)   { return true; }
	if (button == Button_Control) { return true; }
	if (button == Button_Alt)     { return true; }
	#if DEVELOPER
	if (button == Button_Tilde)   { return true; }
	if (button == Button_Pipe)    { return true; }
	if (button == Button_F4)      { return true; }
	if (button == Button_F5)      { return true; }
	if (button == Button_F6)      { return true; }
	if (button == Button_F7)      { return true; }
	#endif
	if (button == Button_F11)     { return true; }
	if (button == Button_F12)     { return true; }
	return false;
}

void CreateKeyboardBtnRecs()
{
	if (app->keyboardBtns != nullptr)
	{
		ArenaPop(mainHeap, app->keyboardBtns);
		app->keyboardBtns = nullptr;
	}
	app->numKeyboardBtns = 0;
	
	TempPushMark();
	DynArray_t buttonsArray;
	CreateDynamicArray(&buttonsArray, TempArena, sizeof(DisplayBtn_t));
	
	// +==============================+
	// |   Escape and Function Keys   |
	// +==============================+
	for (u32 cIndex = 0; cIndex < 1+12+3; cIndex++)
	{
		rec buttonRec = NewRec((SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)cIndex, 0, SETT_KEYBORD_KEY_SIZE, SETT_KEYBORD_KEY_SIZE);
		if (cIndex > 0) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*0.75f; }
		if (cIndex > 4) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*0.75f; }
		if (cIndex > 8) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*0.75f; }
		buttonRec.x += 0;
		buttonRec.y += 0;
		
		bool isAlt = false;
		DisplayBtnShape_t shape = DisplayBtnShape_Rectangle;
		r32 wideness = 1.0f;
		r32 paddingWideness = ((r32)SETT_KEYBORD_KEY_PADDING / (r32)SETT_KEYBORD_KEY_SIZE);
		Buttons_t btn = Buttons_NumButtons;
		switch (cIndex)
		{
			case 0:  btn = Button_Escape; break;
			case 1:  btn = Button_F1; break;
			case 2:  btn = Button_F2; break;
			case 3:  btn = Button_F3; break;
			case 4:  btn = Button_F4; break;
			case 5:  btn = Button_F5; break;
			case 6:  btn = Button_F6; break;
			case 7:  btn = Button_F7; break;
			case 8:  btn = Button_F8; break;
			case 9:  btn = Button_F9; break;
			case 10: btn = Button_F10; break;
			case 11: btn = Button_F11; break;
			case 12: btn = Button_F12; break;
			// case 13: btn = Button_PrintScreen; break; //Should we implement these buttons?
			// case 14: btn = Button_ScrollLock; break; //Should we implement these buttons?
			// case 15: btn = Button_PauseBreak; break; //Should we implement these buttons?
		}
		
		if (btn < Buttons_NumButtons)
		{
			DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
			if (newBtn != nullptr)
			{
				newBtn->isGamepad = false;
				newBtn->btn = btn;
				newBtn->drawRec = buttonRec;
				newBtn->drawRec.width *= wideness;
				newBtn->isAlt = isAlt;
				newBtn->shape = shape;
			}
		}
	}
	
	// +==============================+
	// |         Main Key Set         |
	// +==============================+
	r32 mainKeySetWidth = 0.0f;
	r32 mainKeySetHeight = 0.0f;
	for (u32 cIndex = 0; cIndex < 15; cIndex++)
	{
		for (u32 rIndex = 0; rIndex < 5; rIndex++)
		{
			rec buttonRec = NewRec((SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)cIndex, (SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)rIndex, SETT_KEYBORD_KEY_SIZE, SETT_KEYBORD_KEY_SIZE);
			if (rIndex == 1 && cIndex > 0) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*0.5f; }
			if (rIndex == 2 && cIndex > 0) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*0.75f; }
			if (rIndex == 3 && cIndex > 0) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*1.25f; }
			if (rIndex == 4) { buttonRec.x = (SETT_KEYBORD_KEY_SIZE*1.5f + SETT_KEYBORD_KEY_PADDING) * (r32)cIndex; }
			if (rIndex == 4 && cIndex > 3) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*4.5f; }
			if (rIndex == 4 && cIndex > 5) { buttonRec.x += SETT_KEYBORD_KEY_SIZE*0.75f + SETT_KEYBORD_KEY_PADDING; }
			buttonRec.x += 0;
			buttonRec.y += SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING*2;
			
			bool isAlt = false;
			DisplayBtnShape_t shape = DisplayBtnShape_Rectangle;
			r32 wideness = 1.0f;
			r32 paddingWideness = ((r32)SETT_KEYBORD_KEY_PADDING / (r32)SETT_KEYBORD_KEY_SIZE);
			Buttons_t btn = Buttons_NumButtons;
			switch (rIndex)
			{
				case 0: switch (cIndex) //numbers and backspace
				{
					case 0:  btn = Button_Tilde; break;
					case 1:  btn = Button_1; break;
					case 2:  btn = Button_2; break;
					case 3:  btn = Button_3; break;
					case 4:  btn = Button_4; break;
					case 5:  btn = Button_5; break;
					case 6:  btn = Button_6; break;
					case 7:  btn = Button_7; break;
					case 8:  btn = Button_8; break;
					case 9:  btn = Button_9; break;
					case 10: btn = Button_0; break;
					case 11: btn = Button_Minus; break;
					case 12: btn = Button_Plus; break;
					case 13: btn = Button_Backspace; wideness = 2.0f; break;
				} break;
				case 1: switch (cIndex) //qwerty
				{
					case 0:  btn = Button_Tab; wideness = 1.5f; break;
					case 1:  btn = Button_Q; break;
					case 2:  btn = Button_W; break;
					case 3:  btn = Button_E; break;
					case 4:  btn = Button_R; break;
					case 5:  btn = Button_T; break;
					case 6:  btn = Button_Y; break;
					case 7:  btn = Button_U; break;
					case 8:  btn = Button_I; break;
					case 9:  btn = Button_O; break;
					case 10: btn = Button_P; break;
					case 11: btn = Button_OpenBracket; break;
					case 12: btn = Button_CloseBracket; break;
					case 13: btn = Button_Pipe; wideness = 1.5f; break;
				} break;
				case 2: switch (cIndex) //asdf
				{
					case 0:  btn = Button_CapsLock; wideness = 1.75f; break;
					case 1:  btn = Button_A; break;
					case 2:  btn = Button_S; break;
					case 3:  btn = Button_D; break;
					case 4:  btn = Button_F; break;
					case 5:  btn = Button_G; break;
					case 6:  btn = Button_H; break;
					case 7:  btn = Button_J; break;
					case 8:  btn = Button_K; break;
					case 9:  btn = Button_L; break;
					case 10: btn = Button_Colon; break;
					case 11: btn = Button_Quote; break;
					case 12: btn = Button_Enter; wideness = 2.25f + paddingWideness; break;
				} break;
				case 3: switch (cIndex) //zxcv
				{
					case 0:  btn = Button_Shift; wideness = 2.25f; break;
					case 1:  btn = Button_Z; break;
					case 2:  btn = Button_X; break;
					case 3:  btn = Button_C; break;
					case 4:  btn = Button_V; break;
					case 5:  btn = Button_B; break;
					case 6:  btn = Button_N; break;
					case 7:  btn = Button_M; break;
					case 8:  btn = Button_Comma; break;
					case 9:  btn = Button_Period; break;
					case 10: btn = Button_QuestionMark; break;
					case 11: btn = Button_Shift; isAlt = true; wideness = 3.0f; break;
				} break;
				case 4: wideness = 1.5f; switch (cIndex) //ctrl, alt, space
				{
					case 0: btn = Button_Control; break;
					// case 1: btn = Button_Windows; break; //Windows Button could be implemented somehow
					case 2: btn = Button_Alt; break;
					case 3: btn = Button_Space; wideness = 6.0f; break;
					case 4: btn = Button_Alt; isAlt = true; break;
					// case 5: btn = Button_Windows; break; //Windows Button could be implemented somehow
					case 6: btn = Button_Control; isAlt = true; break;
				} break;
			}
			
			if (btn < Buttons_NumButtons)
			{
				DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
				if (newBtn != nullptr)
				{
					newBtn->isGamepad = false;
					newBtn->btn = btn;
					newBtn->drawRec = buttonRec;
					newBtn->drawRec.width *= wideness;
					newBtn->isAlt = isAlt;
					newBtn->shape = shape;
					
					if (mainKeySetWidth < newBtn->drawRec.x + newBtn->drawRec.width) { mainKeySetWidth = newBtn->drawRec.x + newBtn->drawRec.width; }
					if (mainKeySetHeight < newBtn->drawRec.y + newBtn->drawRec.height) { mainKeySetHeight = newBtn->drawRec.y + newBtn->drawRec.height; }
				}
			}
		}
	}
	
	// +==============================+
	// |        Home, End, Etc        |
	// +==============================+
	for (u32 cIndex = 0; cIndex < 3; cIndex++)
	{
		for (u32 rIndex = 0; rIndex < 2; rIndex++)
		{
			rec buttonRec = NewRec((SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)cIndex, (SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)rIndex, SETT_KEYBORD_KEY_SIZE, SETT_KEYBORD_KEY_SIZE);
			buttonRec.x += mainKeySetWidth + SETT_KEYBORD_KEY_PADDING*2;
			buttonRec.y += SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING*2;
			
			bool isAlt = false;
			DisplayBtnShape_t shape = DisplayBtnShape_Rectangle;
			r32 wideness = 1.0f;
			r32 paddingWideness = ((r32)SETT_KEYBORD_KEY_PADDING / (r32)SETT_KEYBORD_KEY_SIZE);
			Buttons_t btn = Buttons_NumButtons;
			switch (rIndex)
			{
				case 0: switch (cIndex)
				{
					case 0: btn = Button_Insert; break;
					case 1: btn = Button_Home; break;
					case 2: btn = Button_PageUp; break;
				} break;
				case 1: switch (cIndex)
				{
					case 0: btn = Button_Delete; break;
					case 1: btn = Button_End; break;
					case 2: btn = Button_PageDown; break;
				} break;
			}
			
			if (btn < Buttons_NumButtons)
			{
				DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
				if (newBtn != nullptr)
				{
					newBtn->isGamepad = false;
					newBtn->btn = btn;
					newBtn->drawRec = buttonRec;
					newBtn->drawRec.width *= wideness;
					newBtn->isAlt = isAlt;
					newBtn->shape = shape;
				}
			}
		}
	}
	
	// +==============================+
	// |          Arrow Keys          |
	// +==============================+
	for (u32 cIndex = 0; cIndex < 3; cIndex++)
	{
		for (u32 rIndex = 0; rIndex < 2; rIndex++)
		{
			rec buttonRec = NewRec((SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)cIndex, (SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)rIndex, SETT_KEYBORD_KEY_SIZE, SETT_KEYBORD_KEY_SIZE);
			buttonRec.x += mainKeySetWidth + SETT_KEYBORD_KEY_PADDING*2;
			buttonRec.y += mainKeySetHeight - SETT_KEYBORD_KEY_PADDING*1 - SETT_KEYBORD_KEY_SIZE*2;
			
			bool isAlt = false;
			DisplayBtnShape_t shape = DisplayBtnShape_Rectangle;
			r32 wideness = 1.0f;
			r32 paddingWideness = ((r32)SETT_KEYBORD_KEY_PADDING / (r32)SETT_KEYBORD_KEY_SIZE);
			Buttons_t btn = Buttons_NumButtons;
			switch (rIndex)
			{
				case 0: switch (cIndex)
				{
					case 1: btn = Button_Up; break;
				} break;
				case 1: switch (cIndex)
				{
					case 0: btn = Button_Left; break;
					case 1: btn = Button_Down; break;
					case 2: btn = Button_Right; break;
				} break;
			}
			
			if (btn < Buttons_NumButtons)
			{
				DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
				if (newBtn != nullptr)
				{
					newBtn->isGamepad = false;
					newBtn->btn = btn;
					newBtn->drawRec = buttonRec;
					newBtn->drawRec.width *= wideness;
					newBtn->isAlt = isAlt;
					newBtn->shape = shape;
				}
			}
		}
	}
	
	// +==============================+
	// |       Number Pad Keys        |
	// +==============================+
	for (u32 cIndex = 0; cIndex < 4; cIndex++)
	{
		for (u32 rIndex = 0; rIndex < 5; rIndex++)
		{
			rec buttonRec = NewRec((SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)cIndex, (SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING) * (r32)rIndex, SETT_KEYBORD_KEY_SIZE, SETT_KEYBORD_KEY_SIZE);
			buttonRec.x += mainKeySetWidth + SETT_KEYBORD_KEY_PADDING*6 + SETT_KEYBORD_KEY_SIZE*3;
			buttonRec.y += SETT_KEYBORD_KEY_SIZE + SETT_KEYBORD_KEY_PADDING*2;
			
			bool isAlt = false;
			DisplayBtnShape_t shape = DisplayBtnShape_Rectangle;
			r32 wideness = 1.0f;
			r32 tallness = 1.0f;
			r32 paddingWideness = ((r32)SETT_KEYBORD_KEY_PADDING / (r32)SETT_KEYBORD_KEY_SIZE);
			r32 paddingTallness = ((r32)SETT_KEYBORD_KEY_PADDING / (r32)SETT_KEYBORD_KEY_SIZE);
			Buttons_t btn = Buttons_NumButtons;
			switch (rIndex)
			{
				case 0: switch (cIndex)
				{
					case 0: btn = Button_NumLock; break;
					case 1: btn = Button_NumDivide; break;
					case 2: btn = Button_NumMultiply; break;
					case 3: btn = Button_NumSubtract; break;
				} break;
				case 1: switch (cIndex)
				{
					case 0: btn = Button_Num7; break;
					case 1: btn = Button_Num8; break;
					case 2: btn = Button_Num9; break;
					case 3: btn = Button_NumAdd; tallness = 2.0f + paddingTallness; break;
				} break;
				case 2: switch (cIndex)
				{
					case 0: btn = Button_Num4; break;
					case 1: btn = Button_Num5; break;
					case 2: btn = Button_Num6; break;
				} break;
				case 3: switch (cIndex)
				{
					case 0: btn = Button_Num1; break;
					case 1: btn = Button_Num2; break;
					case 2: btn = Button_Num3; break;
					case 3: btn = Button_Enter; isAlt = true; tallness = 2.0f + paddingTallness; break;
				} break;
				case 4: switch (cIndex)
				{
					case 0: btn = Button_Num0; wideness = 2.0f + paddingWideness; break;
					case 2: btn = Button_NumPeriod; break;
				} break;
			}
			
			if (btn < Buttons_NumButtons)
			{
				DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
				if (newBtn != nullptr)
				{
					newBtn->isGamepad = false;
					newBtn->btn = btn;
					newBtn->drawRec = buttonRec;
					newBtn->drawRec.width *= wideness;
					newBtn->drawRec.height *= tallness;
					newBtn->isAlt = isAlt;
					newBtn->shape = shape;
				}
			}
		}
	}
	
	app->numKeyboardBtns = buttonsArray.length;
	app->keyboardBtns = PushArray(mainHeap, DisplayBtn_t, app->numKeyboardBtns);
	for (u32 bIndex = 0; bIndex < app->numKeyboardBtns; bIndex++)
	{
		DisplayBtn_t* tempBtn = DynArrayGet(&buttonsArray, DisplayBtn_t, bIndex);
		DisplayBtn_t* realBtn = &app->keyboardBtns[bIndex];
		Assert(tempBtn != nullptr && realBtn != nullptr);
		memcpy(realBtn, tempBtn, sizeof(DisplayBtn_t));
	}
	TempPopMark();
	
	app->keyboardSize = Vec2_Zero;
	for (u32 bIndex = 0; bIndex < app->numKeyboardBtns; bIndex++)
	{
		DisplayBtn_t* button = &app->keyboardBtns[bIndex];
		if (app->keyboardSize.width < button->drawRec.x + button->drawRec.width) { app->keyboardSize.width = button->drawRec.x + button->drawRec.width; }
		if (app->keyboardSize.height < button->drawRec.y + button->drawRec.height) { app->keyboardSize.height = button->drawRec.y + button->drawRec.height; }
	}
}

void CreateGamepadBtnRecs()
{
	if (app->gamepadBtns != nullptr)
	{
		ArenaPop(mainHeap, app->gamepadBtns);
		app->gamepadBtns = nullptr;
	}
	app->numGamepadBtns = 0;
	
	TempPushMark();
	DynArray_t buttonsArray;
	CreateDynamicArray(&buttonsArray, TempArena, sizeof(DisplayBtn_t));
	
	// +==============================+
	// |           A Button           |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_A;
		newBtn->drawRec = NewRec(164, 71, 16, 18);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |           B Button           |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_B;
		newBtn->drawRec = NewRec(180, 55, 16, 18);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |           X Button           |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_X;
		newBtn->drawRec = NewRec(148, 55, 16, 18);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |           Y Button           |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Y;
		newBtn->drawRec = NewRec(164, 39, 16, 18);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	
	// +==============================+
	// |       DPad Left Button       |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Left;
		newBtn->drawRec = NewRec(17, 57, 20, 16);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |      DPad Right Button       |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Right;
		newBtn->drawRec = NewRec(45, 57, 20, 16);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |        DPad Up Button        |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Up;
		newBtn->drawRec = NewRec(33, 41, 16, 20);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |       DPad Down Button       |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Down;
		newBtn->drawRec = NewRec(33, 69, 16, 20);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	
	// +==============================+
	// |        Select Button         |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Back;
		newBtn->drawRec = NewRec(64, 32, 6, 10);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |         Start Button         |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Start;
		newBtn->drawRec = NewRec(144, 32, 6, 10);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	
	// +==============================+
	// |         Home Button          |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_Home;
		newBtn->drawRec = NewRec(102, 82, 10, 10);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	
	// +==============================+
	// |         Left Bumper          |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_LeftBumper;
		newBtn->drawRec = NewRec(25, 16, 26, 12);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |         Left Trigger         |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_LeftTrigger;
		newBtn->drawRec = NewRec(25, 1, 26, 14);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	
	// +==============================+
	// |         Right Bumper         |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_RightBumper;
		newBtn->drawRec = NewRec(163, 16, 26, 12);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	// +==============================+
	// |        Right Trigger         |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_RightTrigger;
		newBtn->drawRec = NewRec(163, 1, 26, 14);
		newBtn->shape = DisplayBtnShape_Texture;
	}
	
	// +==============================+
	// |      Left Analog Stick       |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_LeftStick;
		newBtn->drawRec = NewRec(73, 85, 19, 19);
		newBtn->shape = DisplayBtnShape_AnalogStick;
	}
	// +==============================+
	// |      Right Analog Stick      |
	// +==============================+
	{
		DisplayBtn_t* newBtn = DynArrayAdd(&buttonsArray, DisplayBtn_t);
		ClearPointer(newBtn);
		newBtn->isGamepad = true;
		newBtn->gamepadBtn = Gamepad_RightStick;
		newBtn->drawRec = NewRec(122, 85, 19, 19);
		newBtn->shape = DisplayBtnShape_AnalogStick;
	}
	
	app->numGamepadBtns = buttonsArray.length;
	app->gamepadBtns = PushArray(mainHeap, DisplayBtn_t, app->numGamepadBtns);
	for (u32 bIndex = 0; bIndex < app->numGamepadBtns; bIndex++)
	{
		DisplayBtn_t* tempBtn = DynArrayGet(&buttonsArray, DisplayBtn_t, bIndex);
		DisplayBtn_t* realBtn = &app->gamepadBtns[bIndex];
		Assert(tempBtn != nullptr && realBtn != nullptr);
		memcpy(realBtn, tempBtn, sizeof(DisplayBtn_t));
	}
	TempPopMark();
	
	app->gamepadSize = NewVec2(resources->ds4Controller.size);
}

void RenderCurrentKeyboardInputDisplay(rec drawRec, Color_t outlineColor, Color_t activeColor, Color_t primaryColor, Color_t secondaryColor, bool showButtonsList = false)
{
	RcBindFont(&resources->pixelFont);
	RcSetViewport(drawRec);
	RcBindShader(&resources->pixelShader);
	
	// +==============================+
	// |      Draw Keyboard Back      |
	// +==============================+
	rec keyboardRec = NewRec(Vec2_Zero, app->keyboardSize + NewVec2(SETT_KEYBORD_KEY_PADDING*2));
	r32 keyboardScale = 1.0f;
	while (keyboardRec.width * (keyboardScale+1) < drawRec.width && keyboardRec.height * (keyboardScale+1) < drawRec.height/2) { keyboardScale += 1.0f; }
	keyboardRec.size = keyboardRec.size * keyboardScale;
	keyboardRec.x = (r32)RoundR32(drawRec.x + drawRec.width/2 - keyboardRec.width/2);
	keyboardRec.y = (r32)RoundR32(drawRec.y + drawRec.height - keyboardRec.height);
	// RcDrawRectangle(keyboardRec, VisBlueGray);
	
	// +==============================+
	// |    Decide Key Text Scale     |
	// +==============================+
	r32 keyboardTextScale = 8.0f;
	GameBtn_t hoveringGameBtn = GameBtn_NumButtons;
	for (u32 bIndex = 0; bIndex < app->numKeyboardBtns; bIndex++)
	{
		DisplayBtn_t* button = &app->keyboardBtns[bIndex];
		rec btnRec = button->drawRec;
		btnRec.topLeft = keyboardRec.topLeft + NewVec2(SETT_KEYBORD_KEY_PADDING) * keyboardScale + (btnRec.topLeft * keyboardScale);
		btnRec.size = btnRec.size * keyboardScale;
		if (btnRec.width > 0 && btnRec.height > 0)
		{
			const char* btnStr = GetKeyboardDisplayStr(button->btn, button->isAlt);
			r32 borderWidth = 1.0f;
			v2 btnStrSize = FontMeasureNtString(btnStr, &resources->pixelFont);
			r32 btnStrScale = 1.0f;
			while (btnStrSize.width * (btnStrScale+1) < (btnRec.width - borderWidth*(btnStrScale+1)*2) && btnStrSize.height * (btnStrScale+1) < (btnRec.height - borderWidth*(btnStrScale+1)*2)) { btnStrScale += 1.0f; }
			
			if (btnStrScale < keyboardTextScale) { keyboardTextScale = btnStrScale; }
			
			if (appInput->mouseInsideWindow && IsInsideRec(btnRec, RenderMousePos))
			{
				GameBtn_t gameBtn = GetGameBtnForButton(button->btn);
				if (gameBtn < GameBtn_NumButtons) { hoveringGameBtn = gameBtn; }
			}
		}
	}
	
	// +==============================+
	// |    Draw Keyboard Buttons     |
	// +==============================+
	RcBindFont(&resources->pixelFont);
	for (u32 bIndex = 0; bIndex < app->numKeyboardBtns; bIndex++)
	{
		DisplayBtn_t* button = &app->keyboardBtns[bIndex];
		rec btnRec = button->drawRec;
		btnRec.topLeft = keyboardRec.topLeft + NewVec2(SETT_KEYBORD_KEY_PADDING) * keyboardScale + (btnRec.topLeft * keyboardScale);
		btnRec.size = btnRec.size * keyboardScale;
		if (btnRec.width > 0 && btnRec.height > 0)
		{
			const char* btnStr = GetKeyboardDisplayStr(button->btn, button->isAlt);
			r32 borderWidth = 1.0f * keyboardTextScale;
			v2 btnStrSize = FontMeasureNtString(btnStr, &resources->pixelFont) * keyboardTextScale;
			v2 textPos = NewVec2(btnRec.x + btnRec.width/2 - btnStrSize.width/2, btnRec.y + btnRec.height/2 - btnStrSize.height/2 + FontGetMaxExtendUp(&resources->pixelFont)*keyboardTextScale);
			Color_t backColor = Transparent;
			Color_t textColor = outlineColor;
			Color_t borderColor = outlineColor;
			GameBtn_t gameBtn = GetGameBtnForButton(button->btn);
			bool isModifier = (button->btn == Button_Control || button->btn == Button_Shift || button->btn == Button_Alt);
			if (ButtonDown(button->btn) || (gameBtn < GameBtn_NumButtons && GameBtnDown(gameBtn, false, 0)) || (hoveringGameBtn < GameBtn_NumButtons && hoveringGameBtn == gameBtn))
			{
				backColor = activeColor;
				textColor = ColorOpposite(textColor);
				
			}
			else if (GetGameBtnForButton(button->btn, true, false) < GameBtn_NumButtons)
			{
				backColor = primaryColor;
			}
			else if (GetGameBtnForButton(button->btn, false, true) < GameBtn_NumButtons)
			{
				backColor = secondaryColor;
			}
			else if (IsKeyboardBtnUnbindable(button->btn))
			{
				backColor = NewColor(30, 40, 50, primaryColor.a);
			}
			
			RcDrawBorderedRec(btnRec, backColor, borderColor, borderWidth);
			RcSetViewport(RecDeflate(btnRec, borderWidth));
			RcDrawNtString(btnStr, textPos, textColor, keyboardTextScale);
			RcSetViewport(drawRec);
		}
	}
	
	// +==============================+
	// |      Draw GameBtn List       |
	// +==============================+
	if (showButtonsList)
	{
		v2 gameBtnTextPos = NewVec2(keyboardRec.x + keyboardRec.width/2, keyboardRec.y - 5 - FontGetMaxExtendDown(&resources->pixelFont) * keyboardScale);
		r32 gameBtnTextHeight = FontGetLineHeight(&resources->pixelFont) * keyboardScale;
		for (u32 gIndex = 0; gIndex < GameBtn_NumButtons; gIndex++)
		{
			GameBtn_t gameBtn = (GameBtn_t)gIndex;
			if (GameBtnDown(gameBtn, false, 0))
			{
				RcSetFontAlignment(Alignment_Center);
				RcDrawNtString(GetGameBtnStr(gameBtn), gameBtnTextPos + NewVec2(0, 1*keyboardScale), NewColor(30, 40, 50), keyboardScale);
				RcDrawNtString(GetGameBtnStr(gameBtn), gameBtnTextPos, VisWhite, keyboardScale);
				RcSetFontAlignment(Alignment_Left);
				gameBtnTextPos.y -= gameBtnTextHeight;
			}
		}
	}
	
	#if 0
	// +==============================+
	// |  Draw Unrepresented Buttons  |
	// +==============================+
	v2 textPos = NewVec2(keyboardRec.x, keyboardRec.y - 5 - resources->pixelFont.maxExtendDown);
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
	{
		Buttons_t btn = (Buttons_t)bIndex;
		bool foundButton = false;
		for (u32 bIndex2 = 0; bIndex2 < app->numKeyboardBtns; bIndex2++)
		{
			DisplayBtn_t* button = &app->keyboardBtns[bIndex2];
			if (button->btn == btn) { foundButton = true; break; }
		}
		
		if (!foundButton)
		{
			RcDrawString(GetButtonName(btn), textPos, VisWhite, 1.0f);
			textPos.y -= resources->pixelFont.lineHeight;
		}
	}
	#endif
}
void RenderCurrentKeyboardInputDisplay()
{
	RenderCurrentKeyboardInputDisplay(RenderArea, VisWhite, VisWhite, VisPurple, VisBlue);
}

void RenderCurrentGamepadInputDisplay(rec drawRec, Color_t outlineColor, Color_t activeColor, Color_t primaryColor, Color_t secondaryColor, bool showButtonsList = false)
{
	RcBindFont(&resources->pixelFont);
	RcSetViewport(drawRec);
	RcBindShader(&resources->pixelShader);
	
	// +==============================+
	// |     Draw Gamepad Texture     |
	// +==============================+
	rec gamepadRec = NewRec(Vec2_Zero, app->gamepadSize);
	r32 gamepadScale = 1.0f;
	while (gamepadRec.width * (gamepadScale+1) < drawRec.width && gamepadRec.height * (gamepadScale+1) < drawRec.height/2) { gamepadScale += 1.0f; }
	gamepadRec.size = gamepadRec.size * gamepadScale;
	gamepadRec.x = (r32)RoundR32(drawRec.x + drawRec.width/2 - gamepadRec.width/2);
	gamepadRec.y = (r32)RoundR32(drawRec.y + drawRec.height - gamepadRec.height);
	RcSetReplaceColor1(Transparent);
	RcSetReplaceColor3(outlineColor);
	RcSetReplaceColor4(ColorDarken(outlineColor, 50));
	RcBindTexture(&resources->ds4Controller); //TODO: Change the texture based on whether or now we are showing XBOX or PS4 Controller
	RcDrawTexturedRec(gamepadRec, outlineColor);
	
	// +==============================+
	// |     Find Hovering Button     |
	// +==============================+
	GameBtn_t hoveringGameBtn = GameBtn_NumButtons;
	for (u32 bIndex = 0; bIndex < app->numGamepadBtns; bIndex++)
	{
		DisplayBtn_t* button = &app->gamepadBtns[bIndex];
		rec btnRec = button->drawRec;
		btnRec.topLeft = gamepadRec.topLeft + (btnRec.topLeft * gamepadScale);
		btnRec.size = btnRec.size * gamepadScale;
		
		if (btnRec.width > 0 && btnRec.height > 0)
		{
			if (appInput->mouseInsideWindow && IsInsideRec(btnRec, RenderMousePos))
			{
				GameBtn_t gameBtn = GetGameBtnForGamepadBtn(button->gamepadBtn);
				if (gameBtn < GameBtn_NumButtons) { hoveringGameBtn = gameBtn; }
			}
		}
	}
	
	// +==============================+
	// |     Draw Gamepad Buttons     |
	// +==============================+
	for (u32 bIndex = 0; bIndex < app->numGamepadBtns; bIndex++)
	{
		DisplayBtn_t* button = &app->gamepadBtns[bIndex];
		rec btnRec = button->drawRec;
		btnRec.topLeft = gamepadRec.topLeft + (btnRec.topLeft * gamepadScale);
		btnRec.size = btnRec.size * gamepadScale;
		r32 borderWidth = 1.0f * gamepadScale;
		
		Color_t backColor = Transparent;
		Color_t textColor = outlineColor;
		Color_t borderColor = outlineColor;
		GameBtn_t gameBtn = GetGameBtnForGamepadBtn(button->gamepadBtn);
		if ((appInput->gamepads[0].isConnected && GamepadDown(0, button->gamepadBtn)) || (gameBtn < GameBtn_NumButtons && GameBtnDown(gameBtn, false, 0)) || (hoveringGameBtn < GameBtn_NumButtons && hoveringGameBtn == gameBtn))
		{
			backColor = activeColor;
			textColor = ColorOpposite(textColor);
		}
		else if (GetGameBtnForGamepadBtn(button->gamepadBtn, true, false) < GameBtn_NumButtons)
		{
			backColor = primaryColor;
		}
		else if (GetGameBtnForGamepadBtn(button->gamepadBtn, false, true) < GameBtn_NumButtons)
		{
			backColor = secondaryColor;
		}
		
		if (button->shape == DisplayBtnShape_Rectangle)
		{
			RcDrawBorderedRec(btnRec, backColor, borderColor, borderWidth);
		}
		else if (button->shape == DisplayBtnShape_Texture)
		{
			RcSetReplaceColor1(backColor);
			RcSetReplaceColor3(borderColor);
			RcSetReplaceColor4(textColor);
			RcBindTexture(&resources->ds4Controller); //TODO: Change this texture if we are drawing the XBOX controller
			RcDrawTexturedRec(btnRec, White, button->drawRec);
		}
		else if (button->shape == DisplayBtnShape_AnalogStick)
		{
			v2 analogValue = GetGameBtnVec((button->gamepadBtn == Gamepad_RightStick), true);
			
			btnRec.topLeft += Vec2Multiply(analogValue, (btnRec.size / 4));
			
			RcSetReplaceColor1(ColorTransparent(0.0f));
			RcSetReplaceColor3(borderColor);
			RcSetReplaceColor4(ColorTransparent(0.0f));
			RcDrawSheetFrame(&resources->analogStickSheet, NewVec2i(0, 0), btnRec, White);
			RcSetReplaceColor1(backColor);
			RcSetReplaceColor4(textColor);
			RcDrawSheetFrame(&resources->analogStickSheet, NewVec2i(1, 0), btnRec, White);
			
			for (u8 dIndex = 0; dIndex < 4; dIndex++)
			{
				Dir2_t direction = Dir2ByIndexClockwise(dIndex);
				GamepadButtons_t gamepadBtn = Gamepad_NumButtons;
				switch (direction)
				{
					case Dir2_Left:  gamepadBtn = ((button->gamepadBtn == Gamepad_RightStick) ? Gamepad_rsLeft  : Gamepad_lsLeft);  break;
					case Dir2_Right: gamepadBtn = ((button->gamepadBtn == Gamepad_RightStick) ? Gamepad_rsRight : Gamepad_lsRight); break;
					case Dir2_Up:    gamepadBtn = ((button->gamepadBtn == Gamepad_RightStick) ? Gamepad_rsUp    : Gamepad_lsUp);    break;
					case Dir2_Down:  gamepadBtn = ((button->gamepadBtn == Gamepad_RightStick) ? Gamepad_rsDown  : Gamepad_lsDown);  break;
					default: Assert(false); break;
				}
				
				GameBtn_t primaryBtn = GetGameBtnForGamepadBtn(gamepadBtn, true, false);
				GameBtn_t secondaryBtn = GetGameBtnForGamepadBtn(gamepadBtn, false, true);
				
				backColor = Transparent;
				textColor = outlineColor;
				borderColor = outlineColor;
				
				if ((appInput->gamepads[0].isConnected && GamepadDown(0, gamepadBtn)) ||
					(primaryBtn < GameBtn_NumButtons && GameBtnDown(primaryBtn, false, 0xFF)) ||
					(secondaryBtn < GameBtn_NumButtons && GameBtnDown(secondaryBtn, false, 0xFF)) ||
					(hoveringGameBtn < GameBtn_NumButtons && (hoveringGameBtn == primaryBtn || hoveringGameBtn == secondaryBtn)))
				{
					backColor = activeColor;
					textColor = ColorOpposite(textColor);
				}
				else if (primaryBtn < GameBtn_NumButtons)
				{
					backColor = primaryColor;
				}
				else if (secondaryBtn < GameBtn_NumButtons)
				{
					backColor = secondaryColor;
				}
				
				RcSetReplaceColor1(backColor);
				RcSetReplaceColor3(borderColor);
				RcSetReplaceColor4(textColor);
				RcDrawSheetFrame(&resources->analogStickSheet, NewVec2i(3, 0), btnRec, White, direction);
			}
		}
	}
	
	// +==============================+
	// |      Draw GameBtn List       |
	// +==============================+
	if (showButtonsList)
	{
		v2 gameBtnTextPos = NewVec2(gamepadRec.x + gamepadRec.width/2, gamepadRec.y - 5 - FontGetMaxExtendDown(&resources->pixelFont) * gamepadScale);
		r32 gameBtnTextHeight = FontGetLineHeight(&resources->pixelFont) * gamepadScale;
		for (u32 gIndex = 0; gIndex < GameBtn_NumButtons; gIndex++)
		{
			GameBtn_t gameBtn = (GameBtn_t)gIndex;
			if (GameBtnDown(gameBtn, false, 0))
			{
				RcSetFontAlignment(Alignment_Center);
				RcDrawNtString(GetGameBtnStr(gameBtn), gameBtnTextPos + NewVec2(0, 1*gamepadScale), NewColor(30, 40, 50), gamepadScale);
				RcDrawNtString(GetGameBtnStr(gameBtn), gameBtnTextPos, VisWhite, gamepadScale);
				RcSetFontAlignment(Alignment_Left);
				gameBtnTextPos.y -= gameBtnTextHeight;
			}
		}
	}
}
void RenderCurrentGamepadInputDisplay()
{
	RenderCurrentGamepadInputDisplay(RenderArea, VisWhite, VisWhite, VisPurple, VisBlue);
}
