/*
File:   app.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_H
#define _APP_H

struct AppData_t
{
	bool initialized;
	
	// +==============================+
	// |        Memory Arenas         |
	// +==============================+
	MemoryArena_t mainHeap;
	MemoryArena_t staticHeap;
	MemoryArena_t tempArena;
	MemoryArena_t platArena;
	u32 appInitTempHighWaterMark;
	
	// +==============================+
	// |          Singletons          |
	// +==============================+
	RenderContext_t renderContext;
	ColorPalette_t colorPalette;
	AppResources_t resources;
	
	// +==============================+
	// |    AppState Data sections    |
	// +==============================+
	MainMenuData_t mainMenuData;
	GameData_t gameData;
	MessData_t messData;
	SettingsMenuData_t settingsMenuData;
	
	// +==============================+
	// |       App States/Menus       |
	// +==============================+
	bool allowAppStateChanges;
	AppState_t calledAppState;
	AppMenu_t calledAppMenu;
	bool appStateChanged;
	AppStateChange_t appStateChange;
	bool appMenuChanged;
	AppMenuChange_t appMenuChange;
	bool appStateInitialized[AppState_NumStates+1];
	u32 numActiveStates;
	AppState_t activeStates[AppState_NumStates];
	bool appMenuInitialized[AppMenu_NumMenus+1];
	u32 numActiveMenus;
	AppMenu_t activeMenus[AppMenu_NumMenus];
	
	TransInfoType_t transInfoType;
	void* transInfo;
	
	FrameBuffer_t mainFrameBuffer;
	FrameBuffer_t secondaryBuffer;
	
	// +==============================+
	// |        Input Handling        |
	// +==============================+
	bool inputEventHandled[MAX_NUM_INPUT_EVENTS];
	bool buttonHandled[Buttons_NumButtons];
	bool gamepadHandled[MAX_NUM_GAMEPADS][Gamepad_NumButtons];
	bool mouseScrollHandled;
	bool mouseMoveHandled;
	bool mouseOverOther;
	
	//In Debug mode we keep track of which functions handled which inputs so we can debug problems with handling order
	#if DEBUG
	const char* inputEventHandledFunc[MAX_NUM_INPUT_EVENTS];
	const char* buttonHandledFunc[Buttons_NumButtons];
	const char* gamepadHandledFunc[MAX_NUM_GAMEPADS][Buttons_NumButtons];
	const char* mouseScrollHandledFunc;
	const char* mouseMoveHandledFunc;
	#endif
	const char* mouseOverOtherName;
	
	const void* focusPntr;
	
	// +==============================+
	// |            Audio             |
	// +==============================+
	SoundInstance_t soundInstances[MAX_SOUND_INSTANCES];
	SoundTransition_t transitionType;
	u64 musicTransStart;
	u64 musicTransTime;
	SoundInstance_t* currentMusic;
	SoundInstance_t* lastMusic;
	bool masterSoundEnabled;
	bool musicEnabled;
	bool soundEffectsEnabled;
	r32 masterVolume;
	r32 musicVolume;
	r32 soundEffectVolume;
	
	// +==============================+
	// |        Debug Overlay         |
	// +==============================+
	bool showFramerateGraph;
	bool showMemoryUsage;
	bool showDebugOverlay;
	r32 elapsedTimesGraphValues[FRAMERATE_GRAPH_WIDTH];
	r32 updateElapsedTimesGraphValues[FRAMERATE_GRAPH_WIDTH];
	
	// +==============================+
	// |        Debug Console         |
	// +==============================+
	DbgConsoleLine_t dbgWorkingLine;
	DynArray_t dbgWorkingLineData;
	DbgConsole_t dbgConsole;
	u32 numCrashDumps;
	
	// +==============================+
	// |       Keyboard Display       |
	// +==============================+
	u32 numKeyboardBtns;
	DisplayBtn_t* keyboardBtns;
	v2 keyboardSize;
	
	// +==============================+
	// |       Gamepad Display        |
	// +==============================+
	bool gamepadIsXbox;
	u32 numGamepadBtns;
	DisplayBtn_t* gamepadBtns;
	v2 gamepadSize;
};

#endif //  _APP_H
