/*
File:   app_palette.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds functions that load and parse a color palette from a png file 
*/

bool LoadColorPalette(ColorPalette_t* paletteOut, const char* filePath)
{
	FileInfo_t paletteFile = platform->ReadEntireFile(filePath);
	
	if (paletteFile.content == nullptr)
	{
		PrintLine_E("WARNING: Couldn't find palette at \"%s\"", filePath);
		return false;
	}
	
	i32 numChannels;
	i32 width, height;
	u8* imageData = stbi_load_from_memory(
		(u8*)paletteFile.content, paletteFile.size,
		&width, &height, &numChannels, 4);
	
	Assert(imageData != nullptr);
	Assert(width > 0 && height > 0);
	
	if (width < 4 || height < NUM_PALETTE_COLORS)
	{
		PrintLine_E("WARNING: Palette is not large enough! Found %dx%d palette when we need 4x%d", width, height, NUM_PALETTE_COLORS);
		return false;
	}
	
	for (u32 pIndex = 0; pIndex < NUM_PALETTE_COLORS; pIndex++)
	{
		for (u8 cIndex = 0; cIndex < 4; cIndex++)
		{
			v2i pixelPos = NewVec2i(cIndex, pIndex);
			u8* bytePntr = &imageData[4 * ((pixelPos.y * width) + pixelPos.x)];
			paletteOut->colors[pIndex].colors[cIndex].r = bytePntr[0];
			paletteOut->colors[pIndex].colors[cIndex].g = bytePntr[1];
			paletteOut->colors[pIndex].colors[cIndex].b = bytePntr[2];
			paletteOut->colors[pIndex].colors[cIndex].a = bytePntr[3];
		}
	}
	
	stbi_image_free(imageData);
	platform->FreeFileMemory(&paletteFile);
	
	return true;
}
