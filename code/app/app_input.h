/*
File:   app_input.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_INPUT_H
#define _APP_INPUT_H

typedef enum
{
	GameBtn_Accept = 0, //Z, Enter, Gamepad_A
	GameBtn_Cancel,     //X, Backspace, Gamepad_B
	GameBtn_Options,    //V, G, Gamepad_X
	GameBtn_Sample,     //C, F, Gamepad_Y
	
	GameBtn_RightTrig,  //E, Tab, Gamepad_RightTrigger
	GameBtn_LeftTrig,   //Q, Gamepad_LeftTrigger
	GameBtn_RightBmp,   //R, Gamepad_RightBumper
	GameBtn_LeftBmp,    //T, Gamepad_LeftBumper
	
	GameBtn_Start,      //Escape, Gamepad_Start
	GameBtn_Select,     //Space, Gamepad_Select
	
	GameBtn_Undo,       //U, <, Gamepad_LeftStick
	GameBtn_Redo,       //I, >, Gamepad_RightStick
	
	GameBtn_Down,       //Down, Gamepad_Down, Gamepad_lsDown
	GameBtn_Up,         //Up, Gamepad_Up, Gamepad_lsUp
	GameBtn_Left,       //Left, Gamepad_Left, Gamepad_lsLeft
	GameBtn_Right,      //Right, Gamepad_Right, Gamepad_lsRight
	
	GameBtn_ViewDown,   //S, Gamepad_rsDown
	GameBtn_ViewUp,     //W, Gamepad_rsUp
	GameBtn_ViewLeft,   //A, Gamepad_rsLeft
	GameBtn_ViewRight,  //D, Gamepad_rsRight
	
	GameBtn_NumButtons,
} GameBtn_t;

typedef enum
{
	InputMethod_KeyboardMouse = 0,
	InputMethod_XbController,
	InputMethod_PsController,
	InputMethod_NumInputMethods,
} InputMethod_t;

const char* GetGameBtnStr(GameBtn_t gameBtn)
{
	switch (gameBtn)
	{
		case GameBtn_Accept:    return "Accept";
		case GameBtn_Cancel:    return "Cancel";
		case GameBtn_Options:   return "Options";
		case GameBtn_Sample:    return "Sample";
		case GameBtn_RightTrig: return "RightTrig";
		case GameBtn_LeftTrig:  return "LeftTrig";
		case GameBtn_RightBmp:  return "RightBmp";
		case GameBtn_LeftBmp:   return "LeftBmp";
		case GameBtn_Start:     return "Start";
		case GameBtn_Select:    return "Select";
		case GameBtn_Undo:      return "Undo";
		case GameBtn_Redo:      return "Redo";
		case GameBtn_Down:      return "Down";
		case GameBtn_Up:        return "Up";
		case GameBtn_Left:      return "Left";
		case GameBtn_Right:     return "Right";
		case GameBtn_ViewDown:  return "ViewDown";
		case GameBtn_ViewUp:    return "ViewUp";
		case GameBtn_ViewLeft:  return "ViewLeft";
		case GameBtn_ViewRight: return "ViewRight";
		default: return "Unknown";
	}
}

#endif //  _APP_INPUT_H
