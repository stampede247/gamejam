/*
File:   app_ogg.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_OGG_H
#define _APP_OGG_H

#define OGG_READ_BUFFER_SIZE         (4*1024) //bytes
#define OGG_SAMPLE_BUFFER_ALLOC_SIZE (1*1024*1024) //samples

struct OggSampleBuffer_t
{
	u32 numSamples;
	u32 numSamplesAlloc;
	i16* samples;
};

#endif //  _APP_OGG_H
