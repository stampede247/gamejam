/*
File:   app_font_flow.cpp
Author: Taylor Robbins
Date:   07\26\2019
Description: 
	** This file mostly stands around the all important function FontPerformTextFlow.
	** Flow is what we call the system that lays out the characters in a string to display text.
	** The way text is flowed on the screen can affect many parts of the program so it's important to
	** make sure that the way things are formatted is consistent and not split this function into
	** multiple different functions (like one for measuring, one for drawing, etc.)
*/

/*
NOTE: There are defines in app_font.h that help you use these formatting options
NOTE: These characters will be ignored if the font style has the FontStyle_RawData flag set
Formatting options Key:
\b               - Toggle Bold
\r               - Toggle Italics
\a               - Toggle Underline
\x01\xRR\xGG\xBB - Set the Color to RRGGBB
\x02             - Default Color
\x03\xSS         - Set Size to SS
\x04             - Default Size
\v\xFF           - Reset Everything

**For generic actions 0 = Off, 1 = On, 2 = Toggle
\v\x1? - Turn on/off Wave
\v\x2? - Turn on/off Bubble
\v\x3? - Turn on/off Bounce
\v\x4? - Turn on/off Jitter
\v\x5? - Turn on/off Wobble
\v\x6? - Turn on/off Faded
\v\x7? - Turn on/off Rainbow
\v\x8? - Turn on/off Emboss
*/

//NOTE: If maxWidth is a valid pntr then it will be filled with the actual width of the line that fit into that space
u32 FontMeasureLineWidth(const char* strPntr, u32 strLength, r32* maxWidthPntr, Font_t* fontPntr, r32 fontSize = 0, u16 styleFlags = FontStyle_Default)
{
	Assert(strPntr != nullptr);
	Assert(strLength > 0);
	Assert(fontPntr != nullptr);
	
	u16 currentStyle = styleFlags;
	r32 currentSize = fontSize;
	r32 maxWidth = 0;
	if (maxWidthPntr != nullptr)
	{
		maxWidth = *maxWidthPntr;
		*maxWidthPntr = 0;
	}
	
	u32 breakPoint = 0;
	r32 xPos = 0;
	r32 lastRenderWidth = 0;
	bool wasRenderable = false;
	FontBake_t* currentBake = nullptr;
	for (u32 cIndex = 0; cIndex < strLength; cIndex++)
	{
		char nextChar = strPntr[cIndex];
		u32 numCharsLeft = strLength - (cIndex+1);
		
		FontChar_t fontChar;
		r32 charWidth = 0;
		r32 charAdvance = 0;
		bool isRenderable = false;
		if (nextChar == '\t')
		{
			if (FontGetChar(fontPntr, &fontChar, ' ', currentSize, currentStyle, currentBake))
			{
				currentBake = fontChar.bake;
				charWidth = (fontChar.info->size.width - fontChar.info->origin.x) * fontChar.scale * TAB_WIDTH;
				charAdvance = fontChar.info->advanceX * fontChar.scale * TAB_WIDTH;
			}
		}
		else if (nextChar == ' ')
		{
			if (FontGetChar(fontPntr, &fontChar, ' ', currentSize, currentStyle, currentBake))
			{
				currentBake = fontChar.bake;
				charWidth = (fontChar.info->size.width - fontChar.info->origin.x) * fontChar.scale;
				charAdvance = fontChar.info->advanceX * fontChar.scale;
			}
		}
		else if (nextChar == '\n')
		{
			//Handled more below
		}
		else if (nextChar == '\b' && !IsFlagSet(currentStyle, FontStyle_RawData))
		{
			currentStyle ^= FontStyle_Bold;
			currentBake = nullptr;
		}
		else if (nextChar == '\r' && !IsFlagSet(currentStyle, FontStyle_RawData))
		{
			currentStyle ^= FontStyle_Italic;
			currentBake = nullptr;
		}
		else if (nextChar == '\x01' && numCharsLeft >= 3 && !IsFlagSet(currentStyle, FontStyle_RawData))
		{
			//NOTE: Color doesn't matter for size
			cIndex += 3;
		}
		else if (nextChar == '\x02' && !IsFlagSet(currentStyle, FontStyle_RawData))
		{
			//NOTE: Color doesn't matter for size
		}
		else if (nextChar == '\x03' && numCharsLeft >= 1 && !IsFlagSet(currentStyle, FontStyle_RawData))
		{
			currentSize = (r32)((u8)strPntr[cIndex+1]);
			cIndex += 1;
		}
		else if (nextChar == '\x04' && !IsFlagSet(currentStyle, FontStyle_RawData))
		{
			currentSize = fontSize;
		}
		else if (nextChar == '\v' && numCharsLeft >= 1 && !IsFlagSet(currentStyle, FontStyle_RawData)) //Generic action
		{
			cIndex += 1;
		}
		else
		{
			if (FontGetChar(fontPntr, &fontChar, nextChar, currentSize, currentStyle, currentBake) ||
				FontGetInvalidChar(fontPntr, &fontChar, nextChar, currentSize, currentStyle, currentBake))
			{
				currentBake = fontChar.bake;
				if (!IsCharClassAlphaNumeric(nextChar) && IsCharClassBeginningCharacter(nextChar))
				{
					breakPoint = cIndex;
					if (maxWidthPntr != nullptr) { *maxWidthPntr = lastRenderWidth; }
				}
				isRenderable = true;
				charWidth = (fontChar.info->size.width - fontChar.info->origin.x) * fontChar.scale;
				charAdvance = fontChar.info->advanceX * fontChar.scale;
			}
		}
		
		r32 renderWidth = xPos + charWidth;
		
		if (isRenderable)
		{
			if (maxWidthPntr != nullptr && maxWidth != 0 && renderWidth > maxWidth)
			{
				if (breakPoint == 0)
				{
					if (cIndex > 0)
					{
						breakPoint = cIndex;
						*maxWidthPntr = lastRenderWidth;
					}
					else
					{
						breakPoint = 1;
						*maxWidthPntr = renderWidth;
					}
				}
				break;
			}
			else if (!IsCharClassAlphaNumeric(nextChar) && !IsCharClassBeginningCharacter(nextChar))
			{
				breakPoint = cIndex+1;
				if (maxWidthPntr != nullptr) { *maxWidthPntr = renderWidth; }
			}
			else if (cIndex == strLength-1) //end of the string
			{
				breakPoint = cIndex+1; //All the characters fit
				if (maxWidthPntr != nullptr) { *maxWidthPntr = renderWidth; }
			}
		}
		else
		{
			breakPoint = cIndex+1;
			if (wasRenderable && maxWidthPntr != nullptr)
			{
				*maxWidthPntr = lastRenderWidth;
			}
			if (nextChar == '\n') { break; }
		}
		
		
		xPos += charAdvance;
		lastRenderWidth = renderWidth;
		wasRenderable = isRenderable;
	}
	
	// if (breakPoint == 0) { breakPoint = 1; }
	return breakPoint;
}

//Returns the end position of the text
v2 FontPerformTextFlow(bool drawToScreen, const char* strPntr, u32 strLength, v2 position, Color_t color, Font_t* fontPntr,
	Alignment_t alignment = Alignment_Left, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr)
{
	// StartTimeBlock("FontPerformTextFlow");
	r32 lineHeight = FontGetLineHeight(fontPntr, fontSize, styleFlags);
	r32 maxExtendUp = FontGetMaxExtendUp(fontPntr, fontSize, styleFlags);
	r32 maxExtendDown = FontGetMaxExtendDown(fontPntr, fontSize, styleFlags);
	if (flowInfo != nullptr)
	{
		ClearPointer(flowInfo);
		flowInfo->strPntr = strPntr;
		flowInfo->strLength = strLength;
		flowInfo->position = position;
		flowInfo->fontPntr = fontPntr;
		flowInfo->alignment = alignment;
		flowInfo->fontSize = fontSize;
		flowInfo->styleFlags = styleFlags;
		
		flowInfo->totalSize = NewVec2(0, lineHeight);
		flowInfo->extents = NewRec(position.x, position.y - maxExtendUp, 0, lineHeight);
		flowInfo->endPos = position;
		flowInfo->numLines = 0;
		flowInfo->numRenderables = 0;
	}
	
	bool doSelection = false;
	bool foundMouseIndex = false;
	r32 minMouseIndexDist = 0;
	if (flowExtra != nullptr)
	{
		doSelection = (flowExtra->doSelection && flowExtra->selStart >= 0 && flowExtra->selEnd >= 0 && flowExtra->selEnd > flowExtra->selStart);
		
		flowExtra->mouseIndex = 0;
		flowExtra->mouseIndexPos = Vec2_Zero;
		flowExtra->indexPos = position;
	}
	
	r32 underlineThickness = 1;
	v2 underlineOffset = NewVec2(0, 1);
	u16 currentStyle = styleFlags;
	Color_t currentColor = color;
	r32 currentSize = fontSize;
	FontBake_t* currentBake = nullptr;
	// v2 drawPos = position;
	r32 drawPosX = position.x;
	r32 drawPosY = position.y;
	FontChar_t fontChar;
	u32 chunkStart = 0;
	while (chunkStart < strLength)
	{
		const char* chunkPntr = &strPntr[chunkStart];
		r32 lineWidth = maxWidth;
		u32 chunkLength = FontMeasureLineWidth(chunkPntr, strLength - chunkStart, &lineWidth, fontPntr, currentSize, currentStyle);
		// PrintLine_D("Got chunk %u-%u/%u", chunkStart, chunkStart + chunkLength, strLength);
		if (alignment == Alignment_Right)
		{
			drawPosX -= lineWidth;
		}
		else if (alignment == Alignment_Center)
		{
			drawPosX -= lineWidth / 2;
		}
		
		v2 underlineStart = NewVec2(drawPosX, drawPosY);
		bool startedSelection = false;
		u32 selStartIndex = 0;
		v2 selRecTopLeft = Vec2_Zero;
		v2 selRecBottomLeft = Vec2_Zero;
		bool foundNewLine = false;
		for (u32 cIndex = 0; cIndex < chunkLength; cIndex++)
		{
			u32 numCharsLeft = chunkLength - (cIndex+1);
			char nextChar = chunkPntr[cIndex];
			r32 charLeft   = drawPosX;
			r32 charRight  = drawPosX;
			r32 charTop    = drawPosY;
			r32 charBottom = drawPosY;
			r32 charLineLeft   = drawPosX;
			r32 charLineRight  = drawPosX;
			r32 charLineTop    = drawPosY;
			r32 charLineBottom = drawPosY;
			
			if (flowExtra != nullptr && flowExtra->calculateIndexPos && chunkStart + cIndex == flowExtra->index)
			{
				flowExtra->indexPos = NewVec2(drawPosX, drawPosY);
			}
			
			if (flowExtra != nullptr && flowExtra->calculateMouseIndex)
			{
				r32 newDist = Vec2Length(flowExtra->mousePos - NewVec2(drawPosX, drawPosY));
				if (!foundMouseIndex || newDist < minMouseIndexDist)
				{
					foundMouseIndex = true;
					minMouseIndexDist = newDist;
					flowExtra->mouseIndex = chunkStart + cIndex;
					flowExtra->mouseIndexPos = NewVec2(drawPosX, drawPosY);
				}
			}
			
			if (nextChar == '\n')
			{
				Assert(cIndex == chunkLength-1);
				
				//We give the new-line character some volume so it shows up in the selection regions
				charLeft   = drawPosX;
				charRight  = drawPosX + 8;
				charTop    = drawPosY - maxExtendUp;
				charBottom = drawPosY + maxExtendDown;
				charLineLeft   = charLeft;
				charLineRight  = charRight;
				charLineTop    = charTop;
				charLineBottom = charBottom;
				
				drawPosX = position.x;
				drawPosY += lineHeight;
				foundNewLine = true;
			}
			else if (nextChar == '\t')
			{
				if (FontGetChar(fontPntr, &fontChar, ' ', currentSize, currentStyle, currentBake))
				{
					currentBake = fontChar.bake;
					charLeft = drawPosX - fontChar.info->origin.x * fontChar.scale;
					charTop  = drawPosY - fontChar.info->origin.y * fontChar.scale;
					charRight  = drawPosX + (fontChar.info->size.x - fontChar.info->origin.x) * fontChar.scale;
					charBottom = drawPosY + (fontChar.info->size.y - fontChar.info->origin.y) * fontChar.scale;
					charLineLeft = drawPosX;
					charLineTop = drawPosY - maxExtendUp; //TODO: Should this be a recall to FontGetMaxExtendUp?
					drawPosX += fontChar.info->advanceX * fontChar.scale * TAB_WIDTH;
					charLineRight = drawPosX;
					charLineBottom = drawPosY + maxExtendDown; //TODO: Should this be a recall to FontGetMaxExtendDown?
				}
				else
				{
					//TODO: Can't render a tab because no space character available!
				}
			}
			else if (nextChar == '\b' && !IsFlagSet(currentStyle, FontStyle_RawData)) //Bold
			{
				currentStyle ^= FontStyle_Bold;
				currentBake = nullptr;
			}
			else if (nextChar == '\r' && !IsFlagSet(currentStyle, FontStyle_RawData)) //Italic
			{
				currentStyle ^= FontStyle_Italic;
				currentBake = nullptr;
			}
			else if (nextChar == '\a' && !IsFlagSet(currentStyle, FontStyle_RawData)) //Underline
			{
				if (IsFlagSet(currentStyle, FontStyle_Underline) && (underlineStart.x != drawPosX || underlineStart.y != drawPosY))
				{
					if (drawToScreen)
					{
						RcDrawLine(underlineStart + underlineOffset, NewVec2(drawPosX, drawPosY) + underlineOffset, underlineThickness, currentColor);
					}
				}
				else
				{
					underlineStart = NewVec2(drawPosX, drawPosY);
				}
				currentStyle ^= FontStyle_Underline;
			}
			else if (nextChar == '\x01' && numCharsLeft >= 3 && !IsFlagSet(currentStyle, FontStyle_RawData)) //Set Color
			{
				if (IsFlagSet(currentStyle, FontStyle_Underline) && (underlineStart.x != drawPosX || underlineStart.y != drawPosY))
				{
					if (drawToScreen)
					{
						RcDrawLine(underlineStart + underlineOffset, NewVec2(drawPosX, drawPosY) + underlineOffset, underlineThickness, currentColor);
					}
					underlineStart = NewVec2(drawPosX, drawPosY);
				}
				currentColor.red = (u8)(chunkPntr[cIndex+1]);
				currentColor.green = (u8)(chunkPntr[cIndex+2]);
				currentColor.blue = (u8)(chunkPntr[cIndex+3]);
				cIndex += 3;
			}
			else if (nextChar == '\x02' && !IsFlagSet(currentStyle, FontStyle_RawData)) //Default Color
			{
				if (IsFlagSet(currentStyle, FontStyle_Underline) && (underlineStart.x != drawPosX || underlineStart.y != drawPosY))
				{
					if (drawToScreen)
					{
						RcDrawLine(underlineStart + underlineOffset, NewVec2(drawPosX, drawPosY) + underlineOffset, underlineThickness, currentColor);
					}
					underlineStart = NewVec2(drawPosX, drawPosY);
				}
				currentColor = color;
			}
			else if (nextChar == '\x03' && numCharsLeft >= 1 && !IsFlagSet(currentStyle, FontStyle_RawData)) //Set Size
			{
				currentSize = (r32)((u8)chunkPntr[cIndex+1]);
				//TODO: Should we recalculate the maxExtendUp, maxExtendDown, and lineHeight?
				cIndex += 1;
			}
			else if (nextChar == '\x04' && !IsFlagSet(currentStyle, FontStyle_RawData)) //Default Size
			{
				currentSize = fontSize;
				//TODO: Should we recalculate the maxExtendUp, maxExtendDown, and lineHeight?
			}
			else if (nextChar == '\v' && numCharsLeft >= 1 && !IsFlagSet(currentStyle, FontStyle_RawData)) //Generic action
			{
				u8 cmd = (u8)chunkPntr[cIndex+1];
				u8 stylePart = (cmd & 0xFC);
				u8 actionBits = (cmd & 0x03);
				
				if (stylePart == 0xFE) //Everything
				{
					if (actionBits == 0x2) //Reset
					{
						currentStyle = styleFlags;
						currentColor = color;
					}
				}
				else if (stylePart == 0x10) //Wave
				{
					PrintLine_D("Wave! %X", actionBits);
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Wave); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Wave); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Wave); }
				}
				else if (stylePart == 0x20) //Bubble
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Bubble); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Bubble); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Bubble); }
				}
				else if (stylePart == 0x30) //Bounce
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Bounce); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Bounce); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Bounce); }
				}
				else if (stylePart == 0x40) //Jitter
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Jitter); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Jitter); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Jitter); }
				}
				else if (stylePart == 0x50) //Wobble
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Wobble); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Wobble); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Wobble); }
				}
				else if (stylePart == 0x60) //Faded
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Faded); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Faded); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Faded); }
				}
				else if (stylePart == 0x70) //Rainbow
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Rainbow); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Rainbow); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Rainbow); }
				}
				else if (stylePart == 0x80) //Emboss
				{
					if (actionBits == 0x0) { FlagUnset(currentStyle,  FontStyle_Emboss); }
					if (actionBits == 0x1) { FlagSet(currentStyle,    FontStyle_Emboss); }
					if (actionBits == 0x2) { FlagToggle(currentStyle, FontStyle_Emboss); }
				}
				cIndex += 1;
			}
			else //regular characters
			{
				bool isSelection = (flowExtra != nullptr && flowExtra->doSelection && chunkStart + cIndex >= (u32)flowExtra->selStart && chunkStart + cIndex < (u32)flowExtra->selEnd);
				Color_t drawColor = currentColor;
				if (isSelection && flowExtra->selChangesColor)
				{
					drawColor = flowExtra->selColor;
				}
				u16 drawStyle = currentStyle;
				if (isSelection && flowExtra->selChangesStyle)
				{
					drawStyle = flowExtra->selStyle;
				}
				
				if (FontGetChar(fontPntr, &fontChar, nextChar, currentSize, drawStyle, currentBake) ||
					FontGetInvalidChar(fontPntr, &fontChar, nextChar, currentSize, drawStyle, currentBake))
				{
					currentBake = fontChar.bake;
					if (drawToScreen)
					{
						RcBindTexture(fontChar.texture);
						v2 glyphPos = NewVec2(
							drawPosX - fontChar.info->origin.x * fontChar.scale,
							drawPosY - fontChar.info->origin.y * fontChar.scale
						);
						r32 glyphScale = fontChar.scale;
						// +==============================+
						// |        FontStyle_Wave        |
						// +==============================+
						if (IsFlagSet(drawStyle, FontStyle_Wave))
						{
							r32 amount = maxExtendDown/2;
							if (amount < 1) { amount = 1; }
							glyphPos.y += amount * SinR32((r32)((ProgramTime + cIndex*30)%800) / 800 * 2*Pi32);
						}
						// +==============================+
						// |       FontStyle_Bounce       |
						// +==============================+
						if (IsFlagSet(drawStyle, FontStyle_Bounce))
						{
							r32 amount = maxExtendDown/2;
							if (amount < 1) { amount = 1; }
							glyphPos.y -= amount * AbsR32(SinR32((r32)((ProgramTime)%1600) / 1600 * 2*Pi32));
						}
						// +==============================+
						// |       FontStyle_Bubble       |
						// +==============================+
						if (IsFlagSet(drawStyle, FontStyle_Bubble))
						{
							r32 scale = 1.0f + 0.1f*SinR32((r32)((ProgramTime + cIndex*30)%800) / 800 * 2*Pi32);
							glyphScale *= scale;
						}
						// +==============================+
						// |      FontStyle_Rainbow       |
						// +==============================+
						if (IsFlagSet(drawStyle, FontStyle_Rainbow))
						{
							r32 hue = 180 + 180*SinR32((r32)((ProgramTime + cIndex*0)%4000) / 4000 * 2*Pi32);
							drawColor = ColorFromHSV((u16)RoundR32(hue), 0.7f, 1.0f);
						}
						// +==============================+
						// |       FontStyle_Jitter       |
						// +==============================+
						if (IsFlagSet(drawStyle, FontStyle_Jitter))
						{
							glyphPos.x += (r32)RandI32(-1, 1);
							glyphPos.y += (r32)RandI32(-1, 1);
						}
						// +==============================+
						// |       FontStyle_Faded        |
						// +==============================+
						if (IsFlagSet(drawStyle, FontStyle_Faded))
						{
							drawColor.a = (u8)(drawColor.a * 0.5f);
						}
						glyphPos.x = (r32)RoundR32(glyphPos.x);
						glyphPos.y = (r32)RoundR32(glyphPos.y);
						#if 0
						RcDrawRectangle(NewRec(glyphPos, fontChar.info->size * glyphScale), drawColor);
						#else
						//attempt to batch, otherwise draw the glyph directly
						bool batched = false;
						if (fontChar.bake != nullptr) { batched = RcBatchGlyph(fontChar.bake, glyphPos, drawColor, NewRec(fontChar.info->bakeRec), glyphScale); }
						if (!batched)
						{
							RcDrawTexturedRec(NewRec(glyphPos, fontChar.info->size * glyphScale), drawColor, NewRec(fontChar.info->bakeRec));
							rc->numDrawnGlyphs++;
						}
						#endif
					}
					
					charLeft = drawPosX - fontChar.info->origin.x * fontChar.scale;
					charTop = drawPosY - fontChar.info->origin.y * fontChar.scale;
					charRight = charLeft + fontChar.info->size.width * fontChar.scale;
					charBottom = charTop + fontChar.info->size.height * fontChar.scale;
					charLineLeft = drawPosX;
					charLineTop = drawPosY - maxExtendUp; //TODO: Should this be a recall to FontGetMaxExtendUp?
					drawPosX += fontChar.info->advanceX * fontChar.scale;
					charLineRight = drawPosX;
					charLineBottom = drawPosY + maxExtendDown; //TODO: Should this be a recall to FontGetMaxExtendDown?
					if (flowInfo != nullptr) { flowInfo->numRenderables++; }
				}
				else
				{
					//TODO: Should we somehow render an invalid character?
				}
			}
			
			if (doSelection)
			{
				if (chunkStart + cIndex >= (u32)flowExtra->selStart && chunkStart + cIndex < (u32)flowExtra->selEnd)
				{
					if (!startedSelection)
					{
						selStartIndex = cIndex;
						selRecTopLeft.x = charLeft;
						selRecTopLeft.y = charTop;
						selRecBottomLeft.x = charLeft;
						selRecBottomLeft.y = charTop;
						startedSelection = true;
					}
					if (charLineLeft < selRecTopLeft.x) { selRecTopLeft.x = charLineLeft; }
					if (charLineTop < selRecTopLeft.y) { selRecTopLeft.y = charLineTop; }
					if (charLineRight > selRecBottomLeft.x) { selRecBottomLeft.x = charLineRight; }
					if (charLineBottom > selRecBottomLeft.y) { selRecBottomLeft.y = charLineBottom; }
				}
				else if (startedSelection)
				{
					//NOTE: This should match the code below that runs at the end of each line
					Assert(cIndex >= selStartIndex);
					if (flowExtra->selEndFunction != nullptr)
					{
						flowExtra->selEndFunction(NewRec(selRecTopLeft, selRecBottomLeft - selRecTopLeft), &chunkPntr[selStartIndex], cIndex - selStartIndex, flowExtra->selEndFunctionUserPntr);
					}
					startedSelection = false;
				}
			}
			
			if (flowExtra != nullptr && flowExtra->calculateIndexPos && chunkStart + cIndex+1 == flowExtra->index)
			{
				flowExtra->indexPos = NewVec2(drawPosX, drawPosY);
			}
			
			if (flowExtra != nullptr && flowExtra->calculateMouseIndex)
			{
				r32 newDist = Vec2Length(flowExtra->mousePos - NewVec2(drawPosX, drawPosY));
				if (!foundMouseIndex || newDist < minMouseIndexDist)
				{
					foundMouseIndex = true;
					minMouseIndexDist = newDist;
					flowExtra->mouseIndex = chunkStart + cIndex+1;
					flowExtra->mouseIndexPos = NewVec2(drawPosX, drawPosY);
				}
			}
			
			if (flowInfo != nullptr)
			{
				if (nextChar == '\n')
				{
					if (drawPosY + maxExtendDown > flowInfo->extents.y + flowInfo->extents.height)
					{
						flowInfo->extents.height = drawPosY + maxExtendDown - flowInfo->extents.y;
					}
				}
				else
				{
					if (charLeft < flowInfo->extents.x)
					{
						flowInfo->extents.width += flowInfo->extents.x - charLeft;
						flowInfo->extents.x = charLeft;
					}
					if (charTop < flowInfo->extents.y)
					{
						flowInfo->extents.height += flowInfo->extents.y - charTop;
						flowInfo->extents.y = charTop;
					}
					if (charRight - flowInfo->extents.x > flowInfo->extents.width) { flowInfo->extents.width = charRight - flowInfo->extents.x; }
					if (charBottom - flowInfo->extents.y > flowInfo->extents.height) { flowInfo->extents.height = charBottom - flowInfo->extents.y; }
				}
			}
		}
		
		if (IsFlagSet(currentStyle, FontStyle_Underline) && (underlineStart.x != drawPosX || underlineStart.y != drawPosY))
		{
			if (drawToScreen)
			{
				RcDrawLine(underlineStart + underlineOffset, NewVec2(drawPosX, drawPosY) + underlineOffset, underlineThickness, currentColor);
			}
		}
		
		if (doSelection && startedSelection)
		{
			//NOTE: This should match the code above that runs when we leave backDraw area
			Assert(chunkLength >= selStartIndex);
			if (flowExtra->selEndFunction != nullptr)
			{
				flowExtra->selEndFunction(NewRec(selRecTopLeft, selRecBottomLeft - selRecTopLeft), &chunkPntr[selStartIndex], chunkLength - selStartIndex, flowExtra->selEndFunctionUserPntr);
			}
			startedSelection = false;
		}
		
		chunkStart += chunkLength;
		if (chunkStart < strLength && !foundNewLine)
		{
			//NOTE: We intentionally use lineHeight not currentSize and currentStyle so the lines
			//      accommadate the default font height at all times
			drawPosX = position.x;
			drawPosY += lineHeight;
		}
	}
	
	if (flowInfo != nullptr)
	{
		flowInfo->endPos = NewVec2(drawPosX, drawPosY);
		flowInfo->totalSize = flowInfo->extents.size;
		flowInfo->extentRight = flowInfo->extents.x + flowInfo->extents.width - position.x;
		flowInfo->extentDown = flowInfo->extents.y + flowInfo->extents.height - position.y;
	}
	// EndTimeBlock();
	return NewVec2(drawPosX, drawPosY);
}

v2 FontMeasureString(const char* strPntr, u32 strLength, Font_t* fontPntr, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr)
{
	if (flowInfo == nullptr) { flowInfo = PushStruct(TempArena, FontFlowInfo_t); }
	FontPerformTextFlow(false, strPntr, strLength, Vec2_Zero, White, fontPntr, Alignment_Left, fontSize, styleFlags, maxWidth, flowInfo, flowExtra);
	return NewVec2(flowInfo->extents.width, flowInfo->extents.height);
}
v2 FontMeasureNtString(const char* nullTermString, Font_t* fontPntr, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr)
{
	if (flowInfo == nullptr) { flowInfo = PushStruct(TempArena, FontFlowInfo_t); }
	FontPerformTextFlow(false, nullTermString, (u32)strlen(nullTermString), Vec2_Zero, White, fontPntr, Alignment_Left, fontSize, styleFlags, maxWidth, flowInfo, flowExtra);
	return NewVec2(flowInfo->extents.width, flowInfo->extents.height);
}

r32 FontMeasureLineWidth(const char* strPntr, u32 strLength, Font_t* fontPntr, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0)
{
	r32 result = maxWidth;
	FontMeasureLineWidth(strPntr, strLength, &result, fontPntr, fontSize, styleFlags);
	return result;
}
r32 FontMeasureNtLineWidth(const char* nullTermString, Font_t* fontPntr, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0)
{
	r32 result = maxWidth;
	FontMeasureLineWidth(nullTermString, (u32)strlen(nullTermString), &result, fontPntr, fontSize, styleFlags);
	return result;
}

i32 GetStringIndexForLocation(const char* strPntr, u32 strLength, v2 relativePos, Font_t* fontPntr, Alignment_t alignment = Alignment_Left, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0, v2* indexPosOut = nullptr, FontFlowInfo_t* flowInfo = nullptr)
{
	if (strLength == 0) { return -1; }
	Assert(strPntr != nullptr);
	
	FontFlowExtra_t flowExtra = {};
	flowExtra.calculateMouseIndex = true;
	flowExtra.mousePos = relativePos;
	
	v2 endPos = FontPerformTextFlow(false, strPntr, strLength, Vec2_Zero, White, fontPntr, alignment, fontSize, styleFlags, maxWidth, flowInfo, &flowExtra);
	
	if (indexPosOut != nullptr) { *indexPosOut = flowExtra.mouseIndexPos; }
	return flowExtra.mouseIndex;
}
i32 GetNtStringIndexForLocation(const char* nullTermString, v2 relativePos, Font_t* fontPntr, Alignment_t alignment = Alignment_Left, r32 fontSize = 0, u16 styleFlags = FontStyle_Default, r32 maxWidth = 0, v2* indexPosOut = nullptr, FontFlowInfo_t* flowInfo = nullptr)
{
	Assert(nullTermString != nullptr);
	u32 strLength = (u32)strlen(nullTermString);
	return GetStringIndexForLocation(nullTermString, strLength, relativePos, fontPntr, alignment, fontSize, styleFlags, maxWidth, indexPosOut, flowInfo);
}

// +--------------------------------------------------------------+
// |                Render Context Like Functions                 |
// +--------------------------------------------------------------+
v2 RcDrawString(const char* strPntr, u32 strLength, v2 position, Color_t color, r32 maxWidth, FontFlowInfo_t* flowInfo, FontFlowExtra_t* flowExtra) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	if (flowExtra != nullptr && flowExtra->doSelection && flowExtra->selEndFunction != nullptr)
	{
		//Draw selection rectangles with first flow pass
		FontPerformTextFlow(false, strPntr, strLength, position, color, rc->boundFont, rc->fontAlignment, rc->fontSize, rc->fontStyle, maxWidth, flowInfo, flowExtra);
		flowExtra->selEndFunction = nullptr;
	}
	return FontPerformTextFlow(true, strPntr, strLength, position, color, rc->boundFont, rc->fontAlignment, rc->fontSize, rc->fontStyle, maxWidth, flowInfo, flowExtra);
}
v2 RcDrawNtString(const char* nullTermString, v2 position, Color_t color, r32 maxWidth, FontFlowInfo_t* flowInfo, FontFlowExtra_t* flowExtra) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	if (flowExtra != nullptr && flowExtra->doSelection && flowExtra->selEndFunction != nullptr)
	{
		//Draw selection rectangles with first flow pass
		FontPerformTextFlow(false, nullTermString, (u32)strlen(nullTermString), position, color, rc->boundFont, rc->fontAlignment, rc->fontSize, rc->fontStyle, maxWidth, flowInfo, flowExtra);
		flowExtra->selEndFunction = nullptr;
	}
	return FontPerformTextFlow(true, nullTermString, (u32)strlen(nullTermString), position, color, rc->boundFont, rc->fontAlignment, rc->fontSize, rc->fontStyle, maxWidth, flowInfo, flowExtra);
}

v2 RcPrintStringFull(v2 position, Color_t color, r32 maxWidth, FontFlowInfo_t* flowInfo, FontFlowExtra_t* flowExtra, const char* formatString, ...) //Pre-declared in app_func_defs.h
{
	TempPrintVa(printResult, printLength, formatString);
	return RcDrawString(printResult, printLength, position, color, maxWidth, flowInfo, flowExtra);
}

v2 RcMeasureString(const char* strPntr, u32 strLength, r32 maxWidth, FontFlowInfo_t* flowInfo, FontFlowExtra_t* flowExtra) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	if (flowInfo == nullptr) { flowInfo = PushStruct(TempArena, FontFlowInfo_t); }
	FontPerformTextFlow(false, strPntr, strLength, Vec2_Zero, White, rc->boundFont, rc->fontAlignment, rc->fontSize, rc->fontStyle, maxWidth, flowInfo, flowExtra);
	return flowInfo->totalSize;
}
v2 RcMeasureNtString(const char* nullTermString, r32 maxWidth, FontFlowInfo_t* flowInfo, FontFlowExtra_t* flowExtra) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	if (flowInfo == nullptr) { flowInfo = PushStruct(TempArena, FontFlowInfo_t); }
	FontPerformTextFlow(false, nullTermString, (u32)strlen(nullTermString), Vec2_Zero, White, rc->boundFont, rc->fontAlignment, rc->fontSize, rc->fontStyle, maxWidth, flowInfo, flowExtra);
	return flowInfo->totalSize;
}

r32 RcMeasureLineWidth(const char* strPntr, u32 strLength, r32 maxWidth) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	r32 result = maxWidth;
	FontMeasureLineWidth(strPntr, strLength, &result, rc->boundFont, rc->fontSize, rc->fontStyle);
	return result;
}
r32 RcMeasureNtLineWidth(const char* nullTermString, r32 maxWidth) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	r32 result = maxWidth;
	FontMeasureLineWidth(nullTermString, (u32)strlen(nullTermString), &result, rc->boundFont, rc->fontSize, rc->fontStyle);
	return result;
}

r32 RcLineHeight() //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	return FontGetLineHeight(rc->boundFont, rc->fontSize, rc->fontStyle);
}
r32 RcMaxExtendUp() //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	return FontGetMaxExtendUp(rc->boundFont, rc->fontSize, rc->fontStyle);
}
r32 RcMaxExtendDown() //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	return FontGetMaxExtendDown(rc->boundFont, rc->fontSize, rc->fontStyle);
}

r32 RcLineHeightAt(r32 fontSize) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	return FontGetLineHeight(rc->boundFont, fontSize, rc->fontStyle);
}
r32 RcMaxExtendUpAt(r32 fontSize) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	return FontGetMaxExtendUp(rc->boundFont, fontSize, rc->fontStyle);
}
r32 RcMaxExtendDownAt(r32 fontSize) //Pre-declared in app_func_defs.h
{
	Assert(rc->boundFont != nullptr);
	return FontGetMaxExtendDown(rc->boundFont, fontSize, rc->fontStyle);
}
