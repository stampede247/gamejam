/*
File:   app_resources.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_RESOURCES_H
#define _APP_RESOURCES_H

typedef enum
{
	ResourceType_SpriteSheet = 0,
	ResourceType_Texture,
	ResourceType_Sound,
	ResourceType_Music,
	ResourceType_Font,
	ResourceType_Shader,
	ResourceType_NumTypes,
	
	ResourceType_Unknown = 0xFF,
} ResourceType_t;

struct AppResources_t
{
	// +==============================+
	// |        Sprite Sheets         |
	// +==============================+
	#define NUM_RESOURCE_SHEETS 2
	union
	{
		SpriteSheet_t spriteSheets[NUM_RESOURCE_SHEETS];
		struct
		{
			SpriteSheet_t testSheet;
			SpriteSheet_t analogStickSheet;
		};
	};
	
	// +==============================+
	// |           Textures           |
	// +==============================+
	#define NUM_RESOURCE_TEXTURES 6
	union
	{
		Texture_t textures[NUM_RESOURCE_TEXTURES];
		struct
		{
			Texture_t testTexture;
			Texture_t testSprite;
			Texture_t alphaTexture;
			Texture_t missing;
			Texture_t ds4Controller;
			Texture_t gearSprite;
		};
	};
	
	// +==============================+
	// |            Sounds            |
	// +==============================+
	#define NUM_RESOURCE_SOUNDS 1
	union
	{
		Sound_t sounds[NUM_RESOURCE_SOUNDS];
		struct
		{
			Sound_t sndTest;
		};
	};
	
	// +==============================+
	// |            Musics            |
	// +==============================+
	#define NUM_RESOURCE_MUSICS 1
	union
	{
		Sound_t musics[NUM_RESOURCE_MUSICS];
		struct
		{
			Sound_t testSong;
		};
	};
	
	// +==============================+
	// |            Fonts             |
	// +==============================+
	#define NUM_RESOURCE_FONTS 4
	union
	{
		Font_t fonts[NUM_RESOURCE_FONTS];
		struct
		{
			Font_t debugFont;
			Font_t pixelFont;
			Font_t mainFont;
			Font_t emojiFont;
		};
	};
	
	// +==============================+
	// |           Shaders            |
	// +==============================+
	#define NUM_RESOURCE_SHADERS 7
	union
	{
		Shader_t shaders[NUM_RESOURCE_SHADERS];
		struct
		{
			Shader_t mainShader;
			Shader_t pixelShader;
			Shader_t outlineShader;
			Shader_t gaussianVertShader;
			Shader_t gaussianHoriShader;
			Shader_t fontShader;
			Shader_t metaBallShader;
		};
	};
	
	#define NUM_RESOURCES (NUM_RESOURCE_SHEETS + NUM_RESOURCE_TEXTURES + NUM_RESOURCE_SOUNDS + NUM_RESOURCE_MUSICS + NUM_RESOURCE_FONTS + NUM_RESOURCE_SHADERS)
};

typedef enum
{
	FontResourceType_Simple = 0,
	FontResourceType_Complex,
	FontResourceType_Bitmap,
	FontResourceType_NumTypes,
} FontResourceType_t;

const char* GetSpriteSheetFilePath(u32 sheetIndex)
{
	switch (sheetIndex)
	{
		case 0:  return SHEETS_FOLDER "test.png";         //testSheet
		case 1:  return SHEETS_FOLDER "analog_stick.png"; //analogStickSheet
		default: Assert(false); return nullptr;
	};
}
v2i GetSpriteSheetSize(u32 sheetIndex)
{
	switch (sheetIndex)
	{
		case 0:  return NewVec2i(10, 1); //testSheet
		case 1:  return NewVec2i(4, 1);  //analogStickSheet
		default: Assert(false); return Vec2i_Zero;
	};
}
const char* GetTextureFilePath(u32 textureIndex)
{
	switch (textureIndex)
	{
		case 0:  return TEXTURES_FOLDER "test.png";           //testTexture
		case 1:  return SPRITES_FOLDER  "test.png";           //testSprite
		case 2:  return TEXTURES_FOLDER "alpha.png";          //alphaTexture
		case 3:  return TEXTURES_FOLDER "missing.jpg";        //missing
		case 4:  return SPRITES_FOLDER  "ds4_controller.png"; //ds4Controller
		case 5:  return SPRITES_FOLDER  "gear.png";           //gearSprite
		default: Assert(false); return nullptr;
	};
}
const char* GetSoundFilePath(u32 soundIndex)
{
	switch (soundIndex)
	{
		case 0:  return SOUNDS_FOLDER "test.wav"; //sndTest
		default: Assert(false); return nullptr;
	};
}
const char* GetMusicFilePath(u32 musicIndex)
{
	switch (musicIndex)
	{
		case 0:  return MUSIC_FOLDER "test.wav"; //testSong
		default: Assert(false); return nullptr;
	};
}
const char* GetFontFilePath(u32 fontIndex)
{
	switch (fontIndex)
	{
		case 0:  return FONTS_FOLDER "consola.ttf";   //debugFont
		case 1:  return FONTS_FOLDER "pixelFont.png"; //pixelFont
		case 2:  return FONTS_FOLDER "consola.ttf";   //mainFont
		case 3:  return FONTS_FOLDER "emojiFont.png"; //emojiFont
		default: Assert(false); return nullptr;
	};
}
FontResourceType_t GetFontResourceType(u32 fontIndex, r32* fontSize, v2i* bakeSize)
{
	Assert(fontSize != nullptr);
	Assert(bakeSize != nullptr);
	bakeSize->width = 512;//default
	bakeSize->height = 512;//default
	switch (fontIndex)
	{
		case 0:  *fontSize = DEBUG_FONT_SIZE; return FontResourceType_Simple;  //debugFont
		case 1:                               return FontResourceType_Bitmap;  //pixelFont
		case 2:                               return FontResourceType_Complex; //mainFont
		case 3:                               return FontResourceType_Bitmap;  //emojiFont
		default: Assert(false); return FontResourceType_NumTypes;
	}
}
v2i GetBitmapFontResourceInfo(u32 fontIndex, r32* advanceX, r32* baseline, u8* firstChar)
{
	Assert(advanceX != nullptr);
	Assert(baseline != nullptr);
	Assert(firstChar != nullptr);
	*advanceX = 5;
	*baseline = 0;
	*firstChar = 0x00;
	switch (fontIndex)
	{
		// case 0: //debugFont
		case 1:  *advanceX = 6; *baseline = 8; return NewVec2i(6, 8); //pixelFont
		// case 2: //mainFont
		case 3:  *advanceX = 32; *baseline = 32; return NewVec2i(32, 32); //emojiFont
		default: Assert(false); return NewVec2i(1,1);
	}
}

const char* GetShaderFilePath(u32 shaderIndex)
{
	switch (shaderIndex)
	{
		case 0:  return SHADERS_FOLDER "main.glsl";                //mainShader
		case 1:  return SHADERS_FOLDER "pixel.glsl";               //pixelShader
		case 2:  return SHADERS_FOLDER "outline.glsl";             //outlineShader
		case 3:  return SHADERS_FOLDER "gaussian-vertical.glsl";   //gaussianVertShader
		case 4:  return SHADERS_FOLDER "gaussian-horizontal.glsl"; //gaussianHoriShader
		case 5:  return SHADERS_FOLDER "font.glsl";                //fontShader
		case 6:  return SHADERS_FOLDER "meta-ball.glsl";           //metaBallShader
		default: Assert(false); return nullptr;
	};
}

const char* GetResourceFilePath(u32 resourceIndex, ResourceType_t* typeOut = nullptr)
{
	if (resourceIndex < NUM_RESOURCE_SHEETS)   { if (typeOut != nullptr) { *typeOut = ResourceType_SpriteSheet; } return GetSpriteSheetFilePath(resourceIndex); } resourceIndex -= NUM_RESOURCE_SHEETS;
	if (resourceIndex < NUM_RESOURCE_TEXTURES) { if (typeOut != nullptr) { *typeOut = ResourceType_Texture;     } return GetTextureFilePath(resourceIndex);     } resourceIndex -= NUM_RESOURCE_TEXTURES;
	if (resourceIndex < NUM_RESOURCE_SOUNDS)   { if (typeOut != nullptr) { *typeOut = ResourceType_Sound;       } return GetSoundFilePath(resourceIndex);       } resourceIndex -= NUM_RESOURCE_SOUNDS;
	if (resourceIndex < NUM_RESOURCE_MUSICS)   { if (typeOut != nullptr) { *typeOut = ResourceType_Music;       } return GetMusicFilePath(resourceIndex);       } resourceIndex -= NUM_RESOURCE_MUSICS;
	if (resourceIndex < NUM_RESOURCE_FONTS)    { if (typeOut != nullptr) { *typeOut = ResourceType_Font;        } return GetFontFilePath(resourceIndex);        } resourceIndex -= NUM_RESOURCE_FONTS;
	if (resourceIndex < NUM_RESOURCE_SHADERS)  { if (typeOut != nullptr) { *typeOut = ResourceType_Shader;      } return GetShaderFilePath(resourceIndex);      } resourceIndex -= NUM_RESOURCE_SHADERS;
	Assert(false);
	return nullptr;
}
const char* GetResourceFilePath(ResourceType_t type, u32 subIndex)
{
	switch (type)
	{
		case ResourceType_SpriteSheet: return GetSpriteSheetFilePath(subIndex);
		case ResourceType_Texture:     return GetTextureFilePath(subIndex);
		case ResourceType_Sound:       return GetSoundFilePath(subIndex);
		case ResourceType_Music:       return GetMusicFilePath(subIndex);
		case ResourceType_Font:        return GetFontFilePath(subIndex);
		case ResourceType_Shader:      return GetShaderFilePath(subIndex);
		default: Assert(false); return nullptr;
	};
}

ResourceType_t GetResourceType(u32 resourceIndex, u32* subIndexOut = nullptr)
{
	if (resourceIndex < NUM_RESOURCE_SHEETS)
	{
		if (subIndexOut != nullptr) { *subIndexOut = resourceIndex; }
		return ResourceType_SpriteSheet;
	}
	resourceIndex -= NUM_RESOURCE_SHEETS;
	if (resourceIndex < NUM_RESOURCE_TEXTURES)
	{
		if (subIndexOut != nullptr) { *subIndexOut = resourceIndex; }
		return ResourceType_Texture;
	}
	resourceIndex -= NUM_RESOURCE_TEXTURES;
	if (resourceIndex < NUM_RESOURCE_SOUNDS)
	{
		if (subIndexOut != nullptr) { *subIndexOut = resourceIndex; }
		return ResourceType_Sound;
	}
	resourceIndex -= NUM_RESOURCE_SOUNDS;
	if (resourceIndex < NUM_RESOURCE_MUSICS)
	{
		if (subIndexOut != nullptr) { *subIndexOut = resourceIndex; }
		return ResourceType_Music;
	}
	resourceIndex -= NUM_RESOURCE_MUSICS;
	if (resourceIndex < NUM_RESOURCE_FONTS)
	{
		if (subIndexOut != nullptr) { *subIndexOut = resourceIndex; }
		return ResourceType_Font;
	}
	resourceIndex -= NUM_RESOURCE_FONTS;
	if (resourceIndex < NUM_RESOURCE_SHADERS)
	{
		if (subIndexOut != nullptr) { *subIndexOut = resourceIndex; }
		return ResourceType_Shader;
	}
	resourceIndex -= NUM_RESOURCE_SHADERS;
	Assert(false);
	return ResourceType_Unknown;
}
u32 GetNumResourcesOfType(ResourceType_t type)
{
	switch (type)
	{
		case ResourceType_SpriteSheet: return NUM_RESOURCE_SHEETS;
		case ResourceType_Texture:     return NUM_RESOURCE_TEXTURES;
		case ResourceType_Sound:       return NUM_RESOURCE_SOUNDS;
		case ResourceType_Music:       return NUM_RESOURCE_MUSICS;
		case ResourceType_Font:        return NUM_RESOURCE_FONTS;
		case ResourceType_Shader:      return NUM_RESOURCE_SHADERS;
		default: return 0;
	}
}

u32 GetResourceIndex(ResourceType_t type, u32 subIndex)
{
	u32 baseIndex = 0;
	if (type == ResourceType_SpriteSheet) { Assert(subIndex < NUM_RESOURCE_SHEETS);   return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_SHEETS;
	if (type == ResourceType_Texture)     { Assert(subIndex < NUM_RESOURCE_TEXTURES); return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_TEXTURES;
	if (type == ResourceType_Sound)       { Assert(subIndex < NUM_RESOURCE_SOUNDS);   return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_SOUNDS;
	if (type == ResourceType_Music)       { Assert(subIndex < NUM_RESOURCE_MUSICS);   return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_MUSICS;
	if (type == ResourceType_Font)        { Assert(subIndex < NUM_RESOURCE_FONTS);    return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_FONTS;
	if (type == ResourceType_Shader)      { Assert(subIndex < NUM_RESOURCE_SHADERS);  return baseIndex + subIndex; } baseIndex += NUM_RESOURCE_SHADERS;
	Assert(false);
	return baseIndex;
}

const char* GetResourceTypeStr(ResourceType_t type)
{
	switch (type)
	{
		case ResourceType_SpriteSheet: return "SpriteSheet";
		case ResourceType_Texture:     return "Texture";
		case ResourceType_Sound:       return "Sound";
		case ResourceType_Music:       return "Music";
		case ResourceType_Font:        return "Font";
		case ResourceType_Shader:      return "Shader";
		default: return "Unknown";
	};
}

#endif //  _APP_RESOURCES_H
