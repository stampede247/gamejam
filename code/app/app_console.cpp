/*
File:   app_console.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds functions that handle capturing debug output and
	** rendering it in the console window overlay
*/

void DbgConsoleInit(DbgConsole_t* console, void* fifoSpace, u32 fifoSize)
{
	Assert(console != nullptr);
	Assert(fifoSpace != nullptr);
	Assert(fifoSize > CSL_INPUT_BOX_MAX_CHARS+1 + sizeof(DbgConsoleLine_t));
	
	ClearPointer(console);
	console->isOpen = false;
	console->openLarge = false;
	console->openAnimProgress = 0.0f;
	
	console->fifoUsage = 0;
	console->fifoSpace = (u8*)fifoSpace;
	// console->fifoSpace += CSL_INPUT_BOX_MAX_CHARS+1;
	console->fifoSize = fifoSize;
	// console->fifoSize -= CSL_INPUT_BOX_MAX_CHARS+1;
	
	console->numLines = 0;
	console->nextGutterNumber = 1;
	console->firstLine = nullptr;
	console->lastLine = nullptr;
	
	console->lineWrap          = CSL_DEFAULT_LINE_WRAP;
	console->showGutterNumbers = CSL_DEFAULT_SHOW_GUTTER_NUMS;
	console->showFileNames     = CSL_DEFAULT_SHOW_FILE_NAMES;
	console->showLineNumbers   = CSL_DEFAULT_SHOW_LINE_NUMBERS;
	console->showFuncNames     = CSL_DEFAULT_SHOW_FUNCTION_NAMES;
	console->scrollToEnd = true;
	console->scroll = Vec2_Zero;
	console->scrollGoto = console->scroll;
	console->contentSize = Vec2_Zero;
	console->remeasureContent = false;
	console->fullyOpaque = false;
	
	CreateTextBox(&console->inputBox, mainHeap, NewRec(0, 0, 0, 0), CSL_FONT, CSL_FONT_SIZE, TextBoxOp_SingleLine);
	CreateButton(&console->sendButton, mainHeap, "Send", NewRec(0, 0, 0, 0), CSL_FONT, CSL_FONT_SIZE);
	CreateButton(&console->dropDownButton, mainHeap, "", NewRec(0, 0, 0, 0), CSL_FONT, CSL_FONT_SIZE);
	CreateButton(&console->clearButton, mainHeap, "Clear", NewRec(0, 0, 0, 0), CSL_FONT, CSL_FONT_SIZE);
	
	console->selStartLine = -1;
	console->selEndLine = -1;
	CreateDynamicArray(&console->selRecs, mainHeap, sizeof(rec), 16, 16);
}

DbgConsoleLine_t* GetDbgConsoleLine(DbgConsole_t* console, u32 lineIndex)
{
	Assert(console != nullptr);
	if (lineIndex >= console->numLines) { return nullptr; }
	DbgConsoleLine_t* line = console->firstLine;
	u32 lIndex = 0;
	while (lIndex < lineIndex && line != nullptr)
	{
		line = line->next;
		lIndex++;
	}
	return line;
}

u32 GetDbgConsoleLineTotalSize(const DbgConsoleLine_t* line)
{
	Assert(line != nullptr);
	return sizeof(DbgConsoleLine_t) + line->filePathLength+1 + line->funcNameLength+1 + line->messageLength+1;
}

const char* GetDbgLineFilePath(const DbgConsoleLine_t* line)
{
	const u8* bytePntr = (const u8*)(line);
	bytePntr += sizeof(DbgConsoleLine_t);
	Assert(bytePntr[line->filePathLength] == 0x00);
	return (const char*)bytePntr;
}
const char* GetDbgLineFuncName(const DbgConsoleLine_t* line)
{
	const u8* bytePntr = (const u8*)(line);
	bytePntr += sizeof(DbgConsoleLine_t);
	bytePntr += line->filePathLength+1;
	Assert(bytePntr[line->funcNameLength] == 0x00);
	return (const char*)bytePntr;
}
const char* GetDbgLineMessage(const DbgConsoleLine_t* line)
{
	const u8* bytePntr = (const u8*)(line);
	bytePntr += sizeof(DbgConsoleLine_t);
	bytePntr += line->filePathLength+1;
	bytePntr += line->funcNameLength+1;
	Assert(bytePntr[line->messageLength] == 0x00);
	return (const char*)bytePntr;
}
const u8* GetPntrAfterDbgLine(const DbgConsoleLine_t* line)
{
	const u8* bytePntr = (const u8*)(line);
	bytePntr += sizeof(DbgConsoleLine_t);
	bytePntr += line->filePathLength+1;
	bytePntr += line->funcNameLength+1;
	bytePntr += line->messageLength+1;
	return bytePntr;
}

u32 DbgConsoleGetFifoIndex(const DbgConsole_t* console, const void* pntr)
{
	Assert(console != nullptr);
	Assert(console->fifoSpace != nullptr);
	Assert(pntr != nullptr);
	Assert(pntr >= console->fifoSpace && pntr <= console->fifoSpace + console->fifoSize);
	return (u32)(((u8*)pntr) - console->fifoSpace);
}
u32 DbgConsoleSpaceTillEndAfter(const DbgConsole_t* console, const DbgConsoleLine_t* line)
{
	Assert(console != nullptr);
	Assert(console->fifoSpace != nullptr);
	Assert(line != nullptr);
	Assert((u8*)line >= console->fifoSpace && (u8*)line < console->fifoSpace + console->fifoSize);
	const u8* lineEndPntr = GetPntrAfterDbgLine(line);
	u32 lineEndIndex = DbgConsoleGetFifoIndex(console, lineEndPntr);
	return console->fifoSize - lineEndIndex;
}
u32 DbgConsoleSpaceTillOtherLine(const DbgConsole_t* console, const DbgConsoleLine_t* firstLine, const DbgConsoleLine_t* secondLine)
{
	Assert(console != nullptr);
	Assert(console->fifoSpace != nullptr);
	Assert(firstLine != nullptr);
	Assert(secondLine != nullptr);
	Assert((u8*)firstLine >= console->fifoSpace && (u8*)firstLine < console->fifoSpace + console->fifoSize);
	Assert((u8*)secondLine >= console->fifoSpace && (u8*)secondLine < console->fifoSpace + console->fifoSize);
	Assert(firstLine < secondLine);
	const u8* firstLineEndPntr = GetPntrAfterDbgLine(firstLine);
	u32 firstLineEndIndex = DbgConsoleGetFifoIndex(console, firstLineEndPntr);
	const u8* secondLineStartPntr = (const u8*)secondLine;
	u32 secondLineStartIndex = DbgConsoleGetFifoIndex(console, secondLineStartPntr);
	Assert(secondLineStartIndex >= firstLineEndIndex);
	return secondLineStartIndex - firstLineEndIndex;
}

v2 MeasureDbgConsoleLine(DbgConsole_t* console, DbgConsoleLine_t* line, r32 maxWidth)
{
	Assert(line != nullptr);
	
	v2 result = Vec2_Zero;
	
	result.height = FontGetLineHeight(CSL_FONT, CSL_FONT_SIZE);
	
	bool addPadding = false;
	
	TempPushMark();
	char* gutterNumStr = TempPrint("%u", line->gutterNumber);
	line->gutterNumWidth = 0;
	if (console->showGutterNumbers && gutterNumStr != nullptr)
	{
		v2 textSize = FontMeasureNtString(gutterNumStr, CSL_GUTTER_NUM_FONT, CSL_GUTTER_NUM_FONT_SIZE);
		if (textSize.width < console->maxGutterNumWidth) { textSize.width = console->maxGutterNumWidth; }
		line->gutterNumWidth = textSize.width+5;
		result.width += line->gutterNumWidth;
		maxWidth -= line->gutterNumWidth;
	}
	
	const char* filePath = GetDbgLineFilePath(line);
	line->fileNameWidth = 0;
	if (console->showFileNames && filePath != nullptr)
	{
		const char* fileName = GetFileNamePart(filePath);
		v2 textSize = FontMeasureNtString(fileName, CSL_FILE_NAME_FONT, CSL_FILE_NAME_FONT_SIZE);
		if (textSize.width < console->maxFileNameWidth) { textSize.width = console->maxFileNameWidth; }
		line->fileNameWidth = textSize.width + (console->showLineNumbers ? 0 : 1);
		result.width += line->fileNameWidth;
		maxWidth -= line->fileNameWidth;
		addPadding = true;
	}
	
	char* lineNumStr = TempPrint(":%u", line->fileLineNumber);
	line->lineNumWidth = 0;
	if (console->showLineNumbers && lineNumStr != nullptr)
	{
		v2 textSize = FontMeasureNtString(lineNumStr, CSL_LINE_NUM_FONT, CSL_LINE_NUM_FONT_SIZE);
		if (textSize.width < console->maxLineNumWidth) { textSize.width = console->maxLineNumWidth; }
		line->lineNumWidth = textSize.width+1;
		result.width += line->lineNumWidth;
		maxWidth -= line->lineNumWidth;
		addPadding = true;
	}
	TempPopMark();
	
	const char* funcName = GetDbgLineFuncName(line);
	line->funcNameWidth = 0;
	if (console->showFuncNames && funcName != nullptr)
	{
		v2 textSize = FontMeasureNtString(funcName, CSL_FUNC_NAME_FONT, CSL_FUNC_NAME_FONT_SIZE);
		if (textSize.width < console->maxFuncNameWidth) { textSize.width = console->maxFuncNameWidth; }
		line->funcNameWidth = textSize.width+1;
		result.width += line->funcNameWidth;
		maxWidth -= line->funcNameWidth;
		addPadding = true;
	}
	
	if (addPadding) { result.width += 4; maxWidth -= 4; }
	line->messageOffsetX = result.width;
	
	const char* message = GetDbgLineMessage(line);
	if (message != nullptr)
	{
		u16 fontStyle = FontStyle_Default;
		if (IsFlagSet(line->flags, DbgFlag_RawText)) { fontStyle |= FontStyle_RawData; }
		v2 textSize = FontMeasureNtString(message, CSL_FONT, CSL_FONT_SIZE, fontStyle, console->lineWrap ? maxWidth : 0);
		result.width += textSize.width;
		if (result.height < textSize.height) { result.height = textSize.height; }
	}
	
	line->drawRec.size = result;
	return result;
}

u32 DbgConsoleLineGetIndexForRelativePos(DbgConsoleLine_t* line, v2 relativePos, r32 maxWidth, v2* relativePosOut = nullptr)
{
	Assert(line != nullptr);
	const char* message = GetDbgLineMessage(line);
	Assert(message != nullptr);
	
	maxWidth -= line->messageOffsetX;
	relativePos.x -= line->messageOffsetX;
	
	FontFlowExtra_t flowExtra = {};
	flowExtra.calculateMouseIndex = true;
	flowExtra.mousePos = relativePos;
	
	u16 fontStyle = FontStyle_Default;
	if (IsFlagSet(line->flags, DbgFlag_RawText)) { fontStyle |= FontStyle_RawData; }
	FontPerformTextFlow(false, message, line->messageLength, Vec2_Zero, White, CSL_FONT, Alignment_Left, CSL_FONT_SIZE, fontStyle, maxWidth, nullptr, &flowExtra);
	flowExtra.mouseIndexPos.x += line->messageOffsetX;
	flowExtra.mouseIndexPos.y += FontGetMaxExtendUp(CSL_FONT, CSL_FONT_SIZE);
	
	if (relativePosOut != nullptr) { *relativePosOut = flowExtra.mouseIndexPos; }
	return flowExtra.mouseIndex;
}

bool DbgConsoleRemoveFirstLine(DbgConsole_t* console)
{
	Assert(console != nullptr);
	Assert(console->fifoSpace != nullptr);
	
	if (console->firstLine != nullptr)
	{
		Assert(console->lastLine != nullptr);
		
		r32 lineHeight = console->firstLine->drawRec.height + CSL_LINE_SPACING;
		console->scroll.y      -= lineHeight;
		console->scrollGoto.y  -= lineHeight;
		console->contentSize.y -= lineHeight;
		if (console->selStartLine >= 0)
		{
			console->selStartLine--;
			if (console->selStartLine < 0) { console->selStartLine = 0; console->selStartIndex = 0; }
		}
		if (console->selEndLine >= 0)
		{
			console->selEndLine--;
			if (console->selEndLine < 0) { console->selEndLine = 0; console->selEndIndex = 0; }
		}
		console->selectionChanged = true;
		
		u32 firstLineSize = GetDbgConsoleLineTotalSize(console->firstLine);
		Assert(console->fifoUsage >= firstLineSize)
		console->fifoUsage -= firstLineSize;
		if (console->lastLine == console->firstLine)
		{
			console->firstLine = nullptr;
			console->lastLine = nullptr;
		}
		else
		{
			console->firstLine = console->firstLine->next;
			Assert(console->firstLine != nullptr);
			console->firstLine->prev = nullptr;
		}
		console->numLines--;
		return true;
	}
	else
	{
		Assert(console->lastLine == nullptr);
		Assert(console->fifoUsage == 0);
		Assert(console->numLines == 0);
		return false;
	}
}

DbgConsoleLine_t* DbgConsoleAllocLine(DbgConsole_t* console, u32 filePathLength, u32 funcNameLength, u32 messageLength)
{
	Assert(console != nullptr);
	Assert(console->fifoSpace != nullptr);
	Assert(console->fifoSize > sizeof(DbgConsoleLine_t) + filePathLength+1 + funcNameLength+1);
	
	u32 requiredSize = sizeof(DbgConsoleLine_t) + filePathLength+1 + funcNameLength+1 + messageLength+1;
	
	DbgConsoleLine_t* result = nullptr;
	while (result == nullptr)
	{
		if (console->firstLine == nullptr || console->lastLine == nullptr)
		{
			//Fifo empty, insert first line
			Assert(console->firstLine == nullptr && console->lastLine == nullptr);
			Assert(console->numLines == 0);
			Assert(console->fifoUsage == 0);
			
			if (console->fifoSize >= requiredSize)
			{
				result = (DbgConsoleLine_t*)console->fifoSpace;
				ClearPointer(result);
				result->next = nullptr;
				result->prev = nullptr;
				console->firstLine = result;
				console->lastLine = result;
				console->fifoUsage += requiredSize;
				console->numLines++;
			}
			else
			{
				//NOTE: To avoid recursion we use the platform layer DebugPrint instead of the normal AppDebugPrint macros
				platform->DebugPrint(__func__, __FILE__, __LINE__, DbgLevel_Error, true,
					"Debug console fifo cannot fit space for new %u char line (%u total bytes, %u available)",
					messageLength, requiredSize, console->fifoSize
				);
				return nullptr;
			}
		}
		else if (console->firstLine <= console->lastLine)
		{
			//Look to insert after lastLine and before end (if space)
			u32 spaceTillEnd = DbgConsoleSpaceTillEndAfter(console, console->lastLine);
			if (spaceTillEnd >= requiredSize)
			{
				result = (DbgConsoleLine_t*)GetPntrAfterDbgLine(console->lastLine);
				ClearPointer(result);
				result->next = nullptr;
				result->prev = console->lastLine;
				console->lastLine->next = result;
				console->lastLine = result;
				console->fifoUsage += requiredSize;
				console->numLines++;
			}
			else
			{
				//There's no space at the end, see if there is space at the beginning before the firstLine
				u32 firstLineIndex = DbgConsoleGetFifoIndex(console, console->firstLine);
				if (firstLineIndex >= requiredSize)
				{
					result = (DbgConsoleLine_t*)console->fifoSpace;
					ClearPointer(result);
					result->next = nullptr;
					result->prev = console->lastLine;
					console->lastLine->next = result;
					console->lastLine = result;
					console->fifoUsage += requiredSize;
					console->numLines++;
				}
				else
				{
					//Not enough space at the beginning, remove the firstLine
					bool removed = DbgConsoleRemoveFirstLine(console);
					Assert(removed);
				}
			}
		}
		else
		{
			u32 spaceBetween = DbgConsoleSpaceTillOtherLine(console, console->lastLine, console->firstLine);
			if (spaceBetween >= requiredSize)
			{
				result = (DbgConsoleLine_t*)GetPntrAfterDbgLine(console->lastLine);
				ClearPointer(result);
				result->next = nullptr;
				result->prev = console->lastLine;
				console->lastLine->next = result;
				console->lastLine = result;
				console->fifoUsage += requiredSize;
				console->numLines++;
			}
			else
			{
				//Not enough space between lastLine and firstLien, remove the firstLine
				bool removed = DbgConsoleRemoveFirstLine(console);
				Assert(removed);
			}
		}
	}
	
	Assert(result != nullptr);
	result->filePathLength = filePathLength;
	result->funcNameLength = funcNameLength;
	result->messageLength = messageLength;
	result->gutterNumber = console->nextGutterNumber;
	console->nextGutterNumber++;
	
	return result;
}

//NOTE: This function is pre-declared in app_console.h
DbgConsoleLine_t* DbgConsoleOutput(DbgConsole_t* console, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, const char* message)
{
	Assert(console != nullptr);
	Assert(console->fifoSpace != nullptr);
	
	if (funcName == nullptr) { funcName = ""; }
	if (filePath == nullptr) { filePath = ""; }
	if (message == nullptr) { message = ""; }
	// const char* fileName = GetFileNamePart(filePath);
	
	u32 filePathLength = (u32)strlen(filePath);
	u32 funcNameLength = (u32)strlen(funcName);
	u32 messageLength = (u32)strlen(message);
	DbgConsoleLine_t* line = DbgConsoleAllocLine(console, filePathLength, funcNameLength, messageLength);
	if (line == nullptr) { return line; }
	
	char* charPntr = (char*)line;
	charPntr += sizeof(DbgConsoleLine_t);
	memcpy(charPntr, filePath, filePathLength+1); charPntr += filePathLength+1;
	memcpy(charPntr, funcName, funcNameLength+1); charPntr += funcNameLength+1;
	memcpy(charPntr, message, messageLength+1); charPntr += messageLength+1;
	
	line->flags = flags;
	line->time = ProgramTime;
	line->dbgLevel = dbgLevel;
	line->fileLineNumber = lineNumber;
	console->remeasureContent = true;
	
	return line;
}

void DbgConsoleClear(DbgConsole_t* console)
{
	console->fifoUsage = 0;
	console->numLines = 0;
	console->nextGutterNumber = 1;
	console->firstLine = nullptr;
	console->lastLine = nullptr;
	console->selStartLine = -1;
	console->selStartIndex = 0;
	console->selEndLine = -1;
	console->selEndIndex = 0;
	console->remeasureContent = true;
	console->selectionChanged = true;
	if (console->recallIndex > 0)
	{
		console->recallIndex = 0;
		ArenaPop(mainHeap, console->tempRecallBuffer);
	}
	console->scrollToEnd = true;
	console->scroll = Vec2_Zero;
	console->scrollGoto = Vec2_Zero;
	console->mouseHoverLine = -1;
	console->mouseHoverIndex = 0;
	console->mouseHoverCursorPos = Vec2_Zero;
	console->mouseStartedOverText = false;
}

// +--------------------------------------------------------------+
// |                     Update Debug Console                     |
// +--------------------------------------------------------------+
char* GetDbgConsoleLinePrefixStr(DbgConsole_t* console, DbgConsoleLine_t* line, MemoryArena_t* memArena)
{
	Assert(line != nullptr);
	Assert(memArena != nullptr);
	const char* filePath = GetDbgLineFilePath(line);
	if (filePath == nullptr) { filePath = ""; }
	const char* fileName = GetFileNamePart(filePath);
	u32 fileNameLength = (u32)strlen(fileName);
	const char* funcName = GetDbgLineFuncName(line);
	if (funcName == nullptr) { funcName = ""; }
	
	u32 resultLength = 0;
	if (console->showGutterNumbers) { resultLength += NumDigitsDEC(line->gutterNumber) + 2; }
	if (console->showFileNames)     { resultLength += fileNameLength; }
	if (console->showLineNumbers)   { resultLength += 1 + NumDigitsDEC(line->fileLineNumber) + 1; }
	if (console->showFuncNames)     { resultLength += 1 + line->funcNameLength + 1; }
	
	char* result = PushArray(memArena, char, resultLength+1);
	u32 cIndex = 0;
	if (console->showGutterNumbers)
	{
		Assert(cIndex + NumDigitsDEC(line->gutterNumber) + 2 <= resultLength);
		TempPushMark();
		char* gutterNumStr = TempPrint("%u: ", line->gutterNumber);
		u32 gutterNumStrLength = (u32)strlen(gutterNumStr);
		Assert(gutterNumStrLength == (u32)NumDigitsDEC(line->gutterNumber) + 2);
		memcpy(&result[cIndex], gutterNumStr, gutterNumStrLength); cIndex += gutterNumStrLength;
		TempPopMark();
	}
	if (console->showFileNames)
	{
		Assert(cIndex + fileNameLength+1 <= resultLength);
		memcpy(&result[cIndex], fileName, fileNameLength); cIndex += fileNameLength;
	}
	if (console->showLineNumbers)
	{
		Assert(cIndex + 1 + NumDigitsDEC(line->fileLineNumber) + 1 <= resultLength);
		TempPushMark();
		char* lineNumStr = TempPrint(":%u ", line->fileLineNumber);
		u32 lineNumStrLength = (u32)strlen(lineNumStr);
		Assert(lineNumStrLength == 1 + (u32)NumDigitsDEC(line->fileLineNumber) + 1);
		memcpy(&result[cIndex], lineNumStr, lineNumStrLength); cIndex += lineNumStrLength;
		TempPopMark();
	}
	if (console->showFuncNames)
	{
		Assert(cIndex + 1 + line->funcNameLength + 1 <= resultLength);
		result[cIndex] = '{'; cIndex++;
		memcpy(&result[cIndex], funcName, line->funcNameLength); cIndex += line->funcNameLength;
		result[cIndex] = '}'; cIndex++;
	}
	
	Assert(cIndex == resultLength);
	result[resultLength] = '\0';
	return result;
}
char* GetDbgConsoleSelectionText(DbgConsole_t* console, MemoryArena_t* memArena, u32* lengthOut = nullptr)
{
	Assert(console != nullptr);
	Assert(memArena != nullptr || lengthOut != nullptr);
	if (lengthOut != nullptr) { *lengthOut = 0; }
	if (console->selStartLine < 0 || console->selEndLine < 0) { return nullptr; }
	if (console->selStartLine == console->selEndLine && console->selStartIndex == console->selEndIndex)
	{
		if (memArena != nullptr) { return ArenaString(memArena, "", 0); }
		else { return nullptr; }
	}
	
	u32 minLine = (u32)console->selStartLine;
	u32 minIndex = console->selStartIndex;
	u32 maxLine = (u32)console->selEndLine;
	u32 maxIndex = console->selEndIndex;
	if (minLine > maxLine || (minLine == maxLine && minIndex > maxIndex))
	{
		u32 temp = minLine; minLine = maxLine; maxLine = temp; //swap
		temp = minIndex; minIndex = maxIndex; maxIndex = temp; //swap
	}
	
	u32 calcLength = 0;
	u32 numPrefixes = 0;
	u32 maxPrefixLength = 0;
	
	u32 lIndex = 0;
	DbgConsoleLine_t* line = console->firstLine;
	while (line != nullptr)
	{
		if (lIndex >= minLine && lIndex <= maxLine)
		{
			u32 startIndex = 0;
			u32 endIndex = line->messageLength;
			if (lIndex == minLine) { startIndex = minIndex; }
			if (lIndex == maxLine) { endIndex = maxIndex; }
			#if CSL_COPY_LINE_PREFIXES
			if (startIndex == 0)
			{
				TempPushMark();
				char* prefix = GetDbgConsoleLinePrefixStr(console, line, TempArena);
				u32 prefixLength = (u32)strlen(prefix);
				if (maxPrefixLength < prefixLength) { maxPrefixLength = prefixLength; }
				numPrefixes++;
				TempPopMark();
			}
			#endif
			calcLength += endIndex - startIndex;
			if (lIndex < maxLine) { calcLength += 1; } //for new line
		}
		line = line->next;
		lIndex++;
	}
	if (numPrefixes > 0 && maxPrefixLength > 0) { maxPrefixLength += 2; } //+2 for " |" between prefix and line
	calcLength += maxPrefixLength * numPrefixes;
	
	if (lengthOut != nullptr) { *lengthOut = calcLength; }
	if (memArena == nullptr) { return nullptr; }
	
	char* result = PushArray(memArena, char, calcLength+1);
	if (result == nullptr) { return nullptr; }
	
	u32 cIndex = 0;
	lIndex = 0;
	line = console->firstLine;
	while (line != nullptr)
	{
		if (lIndex >= minLine && lIndex <= maxLine)
		{
			u32 startIndex = 0;
			u32 endIndex = line->messageLength;
			if (lIndex == minLine) { startIndex = minIndex; }
			if (lIndex == maxLine) { endIndex = maxIndex; }
			#if CSL_COPY_LINE_PREFIXES
			if (startIndex == 0)
			{
				Assert(cIndex + maxPrefixLength <= calcLength);
				TempPushMark();
				char* prefix = GetDbgConsoleLinePrefixStr(console, line, TempArena);
				u32 prefixLength = (u32)strlen(prefix);
				Assert(prefixLength <= maxPrefixLength);
				memcpy(&result[cIndex], prefix, prefixLength); cIndex += prefixLength;
				//pad with spaces
				for (u32 sIndex = prefixLength; sIndex < maxPrefixLength; sIndex++)
				{
					result[cIndex] = (sIndex == maxPrefixLength-1) ? '|' : ' ';
					cIndex++;
				}
				TempPopMark();
			}
			#endif
			const char* messageStr = GetDbgLineMessage(line);
			Assert(messageStr != nullptr);
			Assert(cIndex + (endIndex - startIndex) <= calcLength);
			if (endIndex - startIndex > 0)
			{
				memcpy(&result[cIndex], messageStr + startIndex, endIndex - startIndex);
				cIndex += (endIndex - startIndex);
			}
			if (lIndex < maxLine)
			{
				Assert(cIndex < calcLength);
				result[cIndex] = '\n';
				cIndex++;
			}
		}
		line = line->next;
		lIndex++;
	}
	
	Assert(cIndex == calcLength);
	result[calcLength] = '\0';
	return result;
}

void DbgConsoleRemeasureContent(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	console->contentSize = Vec2_Zero;
	console->maxGutterNumWidth = 0.0f;
	console->maxFileNameWidth = 0.0f;
	console->maxLineNumWidth = 0.0f;
	console->maxFuncNameWidth = 0.0f;
	
	DbgConsoleLine_t* line = console->firstLine;
	while (line != nullptr)
	{
		TempPushMark();
		char* gutterNumStr = TempPrint("%u", line->gutterNumber);
		const char* filePath = GetDbgLineFilePath(line);
		char* lineNumStr = TempPrint(":%u", line->fileLineNumber);
		const char* funcName = GetDbgLineFuncName(line);
		if (gutterNumStr != nullptr)
		{
			v2 gutterNumSize = FontMeasureNtString(gutterNumStr, CSL_GUTTER_NUM_FONT, CSL_GUTTER_NUM_FONT_SIZE);
			if (console->maxGutterNumWidth < gutterNumSize.width) { console->maxGutterNumWidth = gutterNumSize.width; }
		}
		if (filePath != nullptr)
		{
			const char* fileName = GetFileNamePart(filePath);
			v2 fileNameSize = FontMeasureNtString(fileName, CSL_FILE_NAME_FONT, CSL_FILE_NAME_FONT_SIZE);
			if (console->maxFileNameWidth < fileNameSize.width) { console->maxFileNameWidth = fileNameSize.width; }
		}
		if (lineNumStr != nullptr)
		{
			v2 lineNumSize = FontMeasureNtString(lineNumStr, CSL_LINE_NUM_FONT, CSL_LINE_NUM_FONT_SIZE);
			if (console->maxLineNumWidth < lineNumSize.width) { console->maxLineNumWidth = lineNumSize.width; }
		}
		if (funcName != nullptr)
		{
			v2 funcNameSize = FontMeasureNtString(funcName, CSL_FUNC_NAME_FONT, CSL_FUNC_NAME_FONT_SIZE);
			if (console->maxFuncNameWidth < funcNameSize.width) { console->maxFuncNameWidth = funcNameSize.width; }
		}
		line = line->next;
		TempPopMark();
	}
	
	v2 position = Vec2_Zero;
	line = console->firstLine;
	while (line != nullptr)
	{
		v2 newLineSize = MeasureDbgConsoleLine(console, line, console->viewRec.width - 2*2);
		if (console->contentSize.width < newLineSize.width) { console->contentSize.width = newLineSize.width; }
		line->drawRec.topLeft = position;
		position.y += newLineSize.height + CSL_LINE_SPACING;
		console->contentSize.height += newLineSize.height + CSL_LINE_SPACING;
		line = line->next;
	}
}

void DbgConsoleSelectionCallbackHelper(rec backRec, const char* textPntr, u32 textLength, void* userPntr)
{
	Assert(userPntr != nullptr);
	DbgConsole_t* console = (DbgConsole_t*)userPntr;
	rec* newRec = DynArrayAdd(&console->selRecs, rec);
	*newRec = backRec;
	newRec->height += CSL_LINE_SPACING;
	if (console->selRecAddToBeginning) { newRec->x -= 5; newRec->width += 5; }
	if (console->selRecAddToEnd)       { newRec->width += 5; }
}

void DbgConsoleUpdateSelectionRecs(DbgConsole_t* console)
{
	console->selRecs.length = 0;
	console->selCursorPos = Vec2_Zero;
	
	if (console->selStartLine >= 0 || console->selEndLine >= 0)
	{
		Assert(console->selStartLine >= 0 && (u32)console->selStartLine < console->numLines);
		Assert(console->selEndLine >= 0 && (u32)console->selEndLine < console->numLines);
		
		
		FontFlowExtra_t flowExtra = {};
		flowExtra.doSelection = true;
		flowExtra.selEndFunctionUserPntr = (void*)console;
		flowExtra.selEndFunction = DbgConsoleSelectionCallbackHelper;
		
		u32 startLine = (u32)console->selStartLine;
		u32 startIndex = console->selStartIndex;
		u32 endLine = (u32)console->selEndLine;
		u32 endIndex = console->selEndIndex;
		if (startLine > endLine || (startLine == endLine && startIndex > endIndex))
		{
			u32 temp = startLine; startLine = endLine; endLine = temp; //swap
			temp = startIndex; startIndex = endIndex; endIndex = temp; //swap
		}
		u32 numLines = endLine - startLine;
		
		u32 lIndex = 0;
		DbgConsoleLine_t* line = console->firstLine;
		while (line != nullptr)
		{
			if (lIndex >= startLine && lIndex <= endLine)
			{
				console->selRecAddToEnd = true;
				console->selRecAddToBeginning = true;
				flowExtra.selStart = 0;
				flowExtra.selEnd = line->messageLength;
				if (lIndex == startLine) { Assert(startIndex <= line->messageLength); flowExtra.selStart = startIndex; console->selRecAddToBeginning = false; }
				if (lIndex == endLine)   { Assert(endIndex   <= line->messageLength); flowExtra.selEnd   = endIndex;   console->selRecAddToEnd       = false; }
				if (lIndex == (u32)console->selEndLine)
				{
					flowExtra.calculateIndexPos = true;
					flowExtra.index = console->selEndIndex;
				}
				else
				{
					flowExtra.calculateIndexPos = false;
				}
				
				const char* messageStr = GetDbgLineMessage(line);
				if (messageStr != nullptr)
				{
					u16 fontStyle = FontStyle_Default;
					if (IsFlagSet(line->flags, DbgFlag_RawText)) { fontStyle |= FontStyle_RawData; }
					v2 messagePosRelative = line->drawRec.topLeft;
					messagePosRelative.x += line->messageOffsetX;
					messagePosRelative.y += FontGetMaxExtendUp(CSL_FONT, CSL_FONT_SIZE);
					r32 messageMaxWidth = 0;
					if (console->lineWrap)
					{
						r32 viewableWidth = console->viewRec.width - 2*2;
						messageMaxWidth = viewableWidth - line->messageOffsetX;
					}
					u32 numRecsBefore = console->selRecs.length;
					v2 flowEnd = FontPerformTextFlow(false, messageStr, line->messageLength, messagePosRelative, White, CSL_FONT, Alignment_Left, CSL_FONT_SIZE, fontStyle, messageMaxWidth, nullptr, &flowExtra);
					
					if (flowExtra.calculateIndexPos)
					{
						console->selCursorPos = flowExtra.indexPos;
					}
					if (numRecsBefore == console->selRecs.length && !(startLine == endLine && startIndex == endIndex))
					{
						rec* newRec = DynArrayAdd(&console->selRecs, rec);
						newRec->width = 5;
						newRec->height = line->drawRec.height + CSL_LINE_SPACING;
						if (flowEnd.x > messagePosRelative.x && (u32)flowExtra.selStart == line->messageLength)
						{
							newRec->x = flowEnd.x;
							newRec->y = flowEnd.y - FontGetMaxExtendUp(CSL_FONT, CSL_FONT_SIZE);
						}
						else
						{
							newRec->x = line->drawRec.x + line->messageOffsetX - 5;
							newRec->y = line->drawRec.y;
						}
					}
				}
			}
			line = line->next;
			lIndex++;
			if (lIndex > endLine) { break; }
		}
	}
	else
	{
		Assert(console->selStartLine < 0 && console->selEndLine < 0);
	}
}

void DbgConsoleClampScrollValues(DbgConsole_t* console)
{
	Assert(console != nullptr);
	r32 viewableWidth = console->viewRec.width - 2*2;
	r32 viewableHeight = console->viewRec.height - 2*2;
	if (console->scroll.x > console->contentSize.width - viewableWidth) { console->scroll.x = console->contentSize.width - viewableWidth; }
	if (console->scroll.x < 0) { console->scroll.x = 0; }
	if (console->scroll.y > console->contentSize.height - viewableHeight) { console->scroll.y = console->contentSize.height - viewableHeight; }
	if (console->scroll.y < 0) { console->scroll.y = 0; }
	if (console->scrollGoto.x > console->contentSize.width - viewableWidth) { console->scrollGoto.x = console->contentSize.width - viewableWidth; }
	if (console->scrollGoto.x < 0) { console->scrollGoto.x = 0; }
	if (console->scrollGoto.y > console->contentSize.height - viewableHeight) { console->scrollGoto.y = console->contentSize.height - viewableHeight; }
	if (console->scrollGoto.y < 0) { console->scrollGoto.y = 0; }
	if (console->lineWrap) { console->scroll.x = 0; console->scrollGoto.x = 0; }
}

void DbgConsoleUpdateRecs(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	console->drawRec = NewRec(0, 0, RenderArea.width, RenderArea.height/4);
	if (console->openLarge) { console->drawRec.height *= 3; }
	console->drawRec.y -= console->drawRec.height * (1.0f - console->openAnimProgress);
	
	FlagUnset(console->sendButton.options, ButtonOp_AutoSizeVert);
	console->sendButton.padding = 0;
	console->sendButton.fontStyle = FontStyle_Bold;
	console->sendButton.textColor = MonokaiWhite;
	console->sendButton.textDownColor = MonokaiLightGray;
	console->sendButton.backColor = MonokaiGreen;
	console->sendButton.backDownColor = MonokaiDarkGreen;
	console->sendButton.minSize.width = 100.0f;
	
	console->sendButton.bounds.x = console->drawRec.x + console->drawRec.width - 5 - console->sendButton.bounds.width;
	console->sendButton.bounds.y = console->drawRec.y + console->drawRec.height - 5 - console->sendButton.bounds.height;
	console->sendButton.bounds.height = console->inputBox.bounds.height;
	
	r32 textHeight = FontGetLineHeight(CSL_FONT, CSL_FONT_SIZE);
	console->inputBox.bounds.x = console->drawRec.x + 5;
	console->inputBox.bounds.y = console->drawRec.y + console->drawRec.height - 5 - console->inputBox.bounds.height;
	console->inputBox.bounds.width = console->sendButton.bounds.x - 5 - console->inputBox.bounds.x;
	
	r32 oldViewableWidth = console->viewRec.width - 2*2;
	console->viewRec = NewRec(console->drawRec.x + 5, console->drawRec.y + 5, console->drawRec.width - 5*2 - CSL_SCROLL_BAR_WIDTH, 0);
	console->viewRec.height = console->inputBox.bounds.y-5 - console->viewRec.y;
	
	console->scrollGutterRec = NewRec(console->viewRec.x + console->viewRec.width, console->viewRec.y, CSL_SCROLL_BAR_WIDTH, console->viewRec.height);
	
	DbgConsoleClampScrollValues(console);
	
	r32 viewableWidth = console->viewRec.width - 2*2;
	r32 viewableHeight = console->viewRec.height - 2*2;
	
	r32 viewablePercent = viewableHeight / console->contentSize.height;
	r32 scrollPercent = console->scroll.y / (console->contentSize.height - viewableHeight);
	
	if (viewablePercent < 1.0f)
	{
		console->scrollBarRec = NewRec(console->scrollGutterRec.x, console->scrollGutterRec.y, console->scrollGutterRec.width, console->scrollGutterRec.height * viewablePercent);
		if (console->scrollBarRec.height < CSL_MIN_SCROLLBAR_HEIGHT) { console->scrollBarRec.height = CSL_MIN_SCROLLBAR_HEIGHT; }
		r32 scrollableHeight = console->scrollGutterRec.height - console->scrollBarRec.height;
		console->scrollBarRec.y += scrollableHeight * scrollPercent;
	}
	else
	{
		console->viewRec.width += console->scrollGutterRec.width;
		viewableWidth += console->scrollGutterRec.width;
		console->scrollGutterRec.x += console->scrollGutterRec.width;
		console->scrollGutterRec.width = 0;
		console->scrollBarRec = console->scrollGutterRec;
	}
	
	if (oldViewableWidth != viewableWidth)
	{
		console->remeasureContent = true;
		console->selectionChanged = true;
	}
	if (console->remeasureContent && console->isOpen)
	{
		DbgConsoleRemeasureContent(console);
		console->remeasureContent = false;
	}
	if (console->selectionChanged && console->isOpen)// && !console->remeasureContent)
	{
		DbgConsoleUpdateSelectionRecs(console);
		console->selectionChanged = false;
	}
	
	FlagSet(console->dropDownButton.options, ButtonOp_Picture);
	console->dropDownButton.picture = &resources->gearSprite;
	console->dropDownButton.pictureScale = 1.0f;
	console->dropDownButton.padding = 2;
	console->dropDownButton.textColor = MonokaiWhite;
	console->dropDownButton.textDownColor = MonokaiDarkGray;
	console->dropDownButton.backColor = ColorTransparent(0.0f);
	console->dropDownButton.backDownColor = ColorTransparent(0.0f);
	console->dropDownButton.bounds.x = console->drawRec.x + console->drawRec.width - 5 - console->dropDownButton.bounds.width;
	console->dropDownButton.bounds.y = console->drawRec.y + 5;
	
	u32 numCheckboxes = 5;
	r32 checkboxOffsetY = MaxR32(FontGetLineHeight(CSL_FONT, CSL_FONT_SIZE), CSL_CHECKBOX_SIZE) + 5;
	console->dropDownRec = NewRec(console->drawRec.x + console->drawRec.width, console->drawRec.y, 0, 0);
	console->dropDownRec.width = 5 + CSL_CHECKBOX_SIZE + FontMeasureNtString("Function Names", CSL_FONT, CSL_FONT_SIZE).width + console->dropDownButton.bounds.width + 5;
	console->dropDownRec.height = 5 + console->clearButton.bounds.height + 5 + checkboxOffsetY * numCheckboxes;
	console->dropDownRec.x -= console->dropDownRec.width * console->dropDownAnimProgress;
	console->dropDownRec.width *= console->dropDownAnimProgress;
	
	FlagUnset(console->clearButton.options, ButtonOp_AutoSizeHori);
	console->clearButton.textColor = MonokaiWhite;
	console->clearButton.textDownColor = MonokaiWhite;
	console->clearButton.backColor = MonokaiOrange;
	console->clearButton.backDownColor = MonokaiBrown;
	console->clearButton.bounds.x = console->dropDownRec.x + 5;
	console->clearButton.bounds.y = console->dropDownRec.y + 5;
	console->clearButton.bounds.width = console->dropDownButton.bounds.x - 5 - console->clearButton.bounds.x;
	
	v2 checkboxPos = NewVec2(console->dropDownRec.x + 5, console->clearButton.bounds.y + console->clearButton.bounds.height + 5);
	console->lineWrapCheckboxRec  = NewRec(checkboxPos, NewVec2(CSL_CHECKBOX_SIZE)); checkboxPos.y += checkboxOffsetY;
	console->gutterCheckboxRec    = NewRec(checkboxPos, NewVec2(CSL_CHECKBOX_SIZE)); checkboxPos.y += checkboxOffsetY;
	console->fileNamesCheckboxRec = NewRec(checkboxPos, NewVec2(CSL_CHECKBOX_SIZE)); checkboxPos.y += checkboxOffsetY;
	console->lineNumsCheckboxRec  = NewRec(checkboxPos, NewVec2(CSL_CHECKBOX_SIZE)); checkboxPos.y += checkboxOffsetY;
	console->funcNamesCheckboxRec = NewRec(checkboxPos, NewVec2(CSL_CHECKBOX_SIZE)); checkboxPos.y += checkboxOffsetY;
}

void DbgConsoleClaimMouse(DbgConsole_t* console)
{
	Assert(console != nullptr);
	
	DbgConsoleUpdateRecs(console);
	
	if (!IsMouseOverOther() && console->isOpen && console->fullyOpaque && console->mouseStartedOverText && ButtonDownNoHandling(MouseButton_Left))
	{
		MouseOverNamed("DebugConsoleView");
	}
	
	ButtonClaimMouse(&console->dropDownButton, RenderMousePos);
	if (console->clearButton.bounds.width >= 10) { ButtonClaimMouse(&console->clearButton, RenderMousePos); }
	if (!IsMouseOverOther() && console->isOpen && console->fullyOpaque && console->dropDownOpen && IsInsideRec(console->dropDownRec, RenderMousePos))
	{
		if (IsInsideRec(console->lineWrapCheckboxRec, RenderMousePos))
		{
			appOutput->cursorType = Cursor_Pointer;
			MouseOverNamed("DebugConsoleLineWrapCheckbox");
		}
		else if (IsInsideRec(console->gutterCheckboxRec, RenderMousePos))
		{
			appOutput->cursorType = Cursor_Pointer;
			MouseOverNamed("DebugConsoleGutterCheckbox");
		}
		else if (IsInsideRec(console->fileNamesCheckboxRec, RenderMousePos))
		{
			appOutput->cursorType = Cursor_Pointer;
			MouseOverNamed("DebugConsoleFileNamesCheckbox");
		}
		else if (IsInsideRec(console->lineNumsCheckboxRec, RenderMousePos))
		{
			appOutput->cursorType = Cursor_Pointer;
			MouseOverNamed("DebugConsoleLineNumsCheckbox");
		}
		else if (IsInsideRec(console->funcNamesCheckboxRec, RenderMousePos))
		{
			appOutput->cursorType = Cursor_Pointer;
			MouseOverNamed("DebugConsoleFuncNamesCheckbox");
		}
		else
		{	
			MouseOverNamed("DebugConsoleDropDown");
		}
	}
	
	ButtonClaimMouse(&console->sendButton, RenderMousePos);
	TextBoxClaimMouse(&console->inputBox, RenderMousePos);
	
	if (!IsMouseOverOther() && console->isOpen && console->fullyOpaque && appInput->mouseInsideWindow && IsInsideRec(console->viewRec, RenderMousePos))
	{
		MouseOverNamed("DebugConsoleView");
	}
	
	if (!IsMouseOverOther() && console->isOpen && console->fullyOpaque && appInput->mouseInsideWindow)
	{
		MouseOverNamed("DebugConsole");
	}
}

void DbgConsoleUpdate(DbgConsole_t* console)
{
	Assert(console != nullptr);
	bool consumeInputBox = false;
	bool selecting = (ButtonDownNoHandling(MouseButton_Left) && console->mouseStartedOverText);
	
	// +==============================+
	// |   Update Console Open Anim   |
	// +==============================+
	if (console->isOpen && console->openAnimProgress < 1.0f)
	{
		console->openAnimProgress += ((r32)ElapsedMs / CSL_OPEN_TIME);
		if (console->openAnimProgress >= 1.0f) { console->openAnimProgress = 1.0f; }
	}
	if (!console->isOpen && console->openAnimProgress > 0.0f)
	{
		console->openAnimProgress -= ((r32)ElapsedMs / CSL_CLOSE_TIME);
		if (console->openAnimProgress <= 0.0f) { console->openAnimProgress = 0.0f; }
	}
	
	// +==============================+
	// |    Update Drop Down Anim     |
	// +==============================+
	if (console->dropDownOpen && console->dropDownAnimProgress < 1.0f)
	{
		console->dropDownAnimProgress += ((r32)ElapsedMs / CSL_DROP_DOWN_OPEN_TIME);
		if (console->dropDownAnimProgress >= 1.0f) { console->dropDownAnimProgress = 1.0f; }
	}
	if (!console->dropDownOpen && console->dropDownAnimProgress > 0.0f)
	{
		console->dropDownAnimProgress -= ((r32)ElapsedMs / CSL_DROP_DOWN_CLOSE_TIME);
		if (console->dropDownAnimProgress <= 0.0f) { console->dropDownAnimProgress = 0.0f; }
	}
	
	r32 viewableWidth = console->viewRec.width - 2*2;
	r32 viewableHeight = console->viewRec.height - 2*2;
	r32 maxScrollY = console->contentSize.height - viewableHeight;
	
	if (console->scrollGoto.y >= maxScrollY)
	{
		console->scrollToEnd = true;
	}
	if (console->scrollToEnd)
	{
		console->scrollGoto.y = maxScrollY;
	}
	
	// +==============================+
	// |     Handle Escape Button     |
	// +==============================+
	if (ButtonPressed(Button_Escape) && console->isOpen && console->fullyOpaque)
	{
		HandleButton(Button_Escape);
		if (IsUiFocused(&console->inputBox))
		{
			SetUiFocus(nullptr);
		}
		else
		{
			console->isOpen = false;
		}
	}
	
	// +==============================+
	// | Update DropDownButton Button |
	// +==============================+
	if (console->isOpen)
	{
		if (ButtonUpdate(&console->dropDownButton, RenderMousePos))
		{
			console->dropDownOpen = !console->dropDownOpen;
		}
	}
	
	// +==============================+
	// |     Update Clear Button      |
	// +==============================+
	if (console->isOpen && console->dropDownOpen && console->clearButton.bounds.width >= 10)
	{
		if (ButtonUpdate(&console->clearButton, RenderMousePos))
		{
			DbgConsoleClear(console);
			DbgConsoleUpdateRecs(console);
		}
	}
	
	// +==============================+
	// |     Handle Ctrl+Shift+C      |
	// +==============================+
	if (console->isOpen && console->fullyOpaque && ButtonPressedSeq(Button_C, BtnTransFlag_Ctrl|BtnTransFlag_Shift))
	{
		const InputEvent_t* inputEvent = HandleButtonPressSeq("DebugConsoleView", Button_C, BtnTransFlag_Ctrl|BtnTransFlag_Shift);
		DbgConsoleClear(console);
		DbgConsoleUpdateRecs(console);
	}
	
	// +==============================+
	// |      Update Send Button      |
	// +==============================+
	if (console->isOpen)
	{
		if (ButtonUpdate(&console->sendButton, RenderMousePos))
		{
			consumeInputBox = true;
			console->fullyOpaque = true;
		}
	}
	
	// +==============================+
	// |       Update InputBox        |
	// +==============================+
	if (console->isOpen)
	{
		bool textBoxFocused = false;
		TextBoxUpdate(&console->inputBox, RenderMousePos, nullptr, &textBoxFocused);
		if (textBoxFocused)
		{
			console->fullyOpaque = true;
			TextBoxSelectAll(&console->inputBox);
		}
	}
	
	// +==============================+
	// | Determine Mouse Hover Index  |
	// +==============================+
	console->mouseHoverLine = -1;
	console->mouseHoverIndex = 0;
	bool mouseStartedInsideHover = false;
	rec viewableRec = RecDeflate(console->viewRec, 2);
	v2 viewOrigin = NewVec2(viewableRec.x - console->scroll.x, viewableRec.y - console->scroll.y);
	if (appInput->mouseInsideWindow && console->isOpen && console->fullyOpaque && (IsInsideRec(viewableRec, RenderMousePos) || selecting) && IsMouseOver("DebugConsoleView"))
	{
		u32 lineIndex = 0;
		DbgConsoleLine_t* line = console->firstLine;
		while (line != nullptr)
		{
			rec drawRec = line->drawRec;
			drawRec.topLeft += viewOrigin;
			drawRec.height += CSL_LINE_SPACING;
			rec lineRec = drawRec;
			lineRec.width = viewableRec.x + viewableRec.width - lineRec.x;
			if (RecIntersects(lineRec, viewableRec) || selecting)
			{
				rec lineWindowRec = NewRec(0, drawRec.y, RenderArea.width, drawRec.height);
				rec backgroundRec = drawRec;
				backgroundRec.x += line->gutterNumWidth + line->fileNameWidth + line->lineNumWidth + line->funcNameWidth;
				backgroundRec.width = viewableRec.x + viewableRec.width - backgroundRec.x;
				
				rec gutterNumRec = lineRec;
				gutterNumRec.width = line->gutterNumWidth;
				rec fileNameRec = lineRec;
				fileNameRec.x += line->gutterNumWidth;
				fileNameRec.width = line->fileNameWidth;
				rec lineNumRec = lineRec;
				lineNumRec.x += line->gutterNumWidth;
				lineNumRec.x += line->fileNameWidth;
				lineNumRec.width = line->lineNumWidth;
				rec funcNameRec = lineRec;
				funcNameRec.x += line->gutterNumWidth;
				funcNameRec.x += line->fileNameWidth;
				funcNameRec.x += line->lineNumWidth;
				funcNameRec.width = line->funcNameWidth;
				
				bool insideLine = false;
				if (IsInsideRec(lineRec, RenderMousePos)) { insideLine = true; }
				else if (selecting)
				{
					if (IsInsideRec(lineWindowRec, RenderMousePos)) { insideLine = true; }
					else if (lineIndex >= console->numLines-1) { insideLine = true; } //last line claims anything below
					else if (lineIndex == 0 && RenderMousePos.y <= drawRec.y) { insideLine = true; } //first line claims anything above
				}
				
				if (insideLine)
				{
					console->mouseHoverLine = (i32)lineIndex;
					console->mouseHoverIndex = 0;
					if (console->showGutterNumbers && IsInsideRec(gutterNumRec, RenderMousePos) && !selecting)
					{
						console->mouseHoverIndex = -4;
						// appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(gutterNumRec, RenderMouseStartLeft);
					}
					else if (console->showFileNames && IsInsideRec(fileNameRec, RenderMousePos) && !selecting)
					{
						console->mouseHoverIndex = -3;
						// appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(fileNameRec, RenderMouseStartLeft);
					}
					else if (console->showLineNumbers && IsInsideRec(lineNumRec, RenderMousePos) && !selecting)
					{
						console->mouseHoverIndex = -2;
						appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(lineNumRec, RenderMouseStartLeft);
					}
					else if (console->showFuncNames && IsInsideRec(funcNameRec, RenderMousePos) && !selecting)
					{
						console->mouseHoverIndex = -1;
						// appOutput->cursorType = Cursor_Pointer;
						mouseStartedInsideHover = IsInsideRec(funcNameRec, RenderMouseStartLeft);
					}
					else
					{
						appOutput->cursorType = Cursor_Text;
						mouseStartedInsideHover = IsInsideRec(lineRec, RenderMouseStartLeft);
						if (selecting) { mouseStartedInsideHover = true; }
						
						v2 relativeMousePos = RenderMousePos - lineRec.topLeft;
						console->mouseHoverIndex = DbgConsoleLineGetIndexForRelativePos(line, relativeMousePos, viewableRec.width, &console->mouseHoverCursorPos);
						console->mouseHoverCursorPos += lineRec.topLeft - viewOrigin;
					}
					break;
				}
				
			}
			line = line->next;
			lineIndex++;
		}
	}
	
	DbgConsoleLine_t* hoverLine = (console->mouseHoverLine >= 0) ? GetDbgConsoleLine(console, (u32)console->mouseHoverLine) : nullptr;
	
	// +==============================+
	// |    Handle Mouse Selection    |
	// +==============================+
	if (ButtonPressed(MouseButton_Left) && console->isOpen && console->fullyOpaque && IsMouseOver("DebugConsoleView"))
	{
		HandleButton(MouseButton_Left);
		if (console->mouseHoverLine >= 0 && console->mouseHoverIndex >= 0)
		{
			console->mouseStartedOverText = true;
			console->selStartLine = console->mouseHoverLine;
			console->selStartIndex = console->mouseHoverIndex;
			console->selEndLine = console->mouseHoverLine;
			console->selEndIndex = console->mouseHoverIndex;
			console->selectionChanged = true;
			selecting = true;
			if (IsUiFocused(&console->inputBox)) { SetUiFocus(nullptr); }
		}
		else
		{
			console->mouseStartedOverText = false;
		}
	}
	if (ButtonDown(MouseButton_Left) && console->isOpen && console->fullyOpaque && console->mouseStartedOverText)
	{
		HandleButton(MouseButton_Left);
		if (console->mouseHoverLine >= 0 && console->mouseHoverIndex >= 0)
		{
			if (console->selEndLine != console->mouseHoverLine || console->selEndIndex != (u32)console->mouseHoverIndex)
			{
				console->selEndLine = console->mouseHoverLine;
				console->selEndIndex = console->mouseHoverIndex;
				console->selectionChanged = true;
			}
		}
	}
	if (console->mouseStartedOverText && ButtonReleasedNoHandling(MouseButton_Left))
	{
		if (!app->buttonHandled[MouseButton_Left]) { HandleButton(MouseButton_Left); }
		console->mouseStartedOverText = false;
	}
	
	// +==============================+
	// |        Handle Ctrl+A         |
	// +==============================+
	if (ButtonPressedSeq(Button_A, BtnTransFlag_Ctrl) && console->isOpen && console->fullyOpaque && !selecting && !IsUiFocused(&console->inputBox))
	{
		const InputEvent_t* inputEvent = HandleButtonPressSeq("DebugConsole", Button_A, BtnTransFlag_Ctrl);
		if (console->numLines > 0)
		{
			if (console->selStartLine != 0 || console->selStartIndex != 0 || console->selEndLine != (i32)console->numLines-1 || console->selEndIndex != console->lastLine->messageLength)
			{
				console->selStartLine = 0; console->selStartIndex = 0;
				console->selEndLine = console->numLines-1; console->selEndIndex = console->lastLine->messageLength;
			}
			else
			{
				console->selStartLine = -1; console->selStartIndex = 0;
				console->selEndLine = -1; console->selEndIndex = 0;
			}
			console->selectionChanged = true;
		}
	}
	
	// +==============================+
	// |       Show Source File       |
	// +==============================+
	if (ButtonReleased(MouseButton_Left) && hoverLine != nullptr && console->mouseHoverIndex == -2 && mouseStartedInsideHover && !console->mouseStartedOverText)
	{
		HandleButton(MouseButton_Left);
		const char* filePath = GetDbgLineFilePath(hoverLine);
		u32 fileLineNumber = hoverLine->fileLineNumber;
		
		if (platform->ShowSourceFile(filePath, fileLineNumber))
		{
			// PrintLine_I("Showed %s:%u in Sublime Text", filePath, fileLineNumber);
		}
		else
		{
			PrintLine_W("Couldn't show %s:%u in Sublime Text", filePath, fileLineNumber);
		}
	}
	
	if (hoverLine != nullptr && console->mouseHoverIndex >= 0)
	
	// +==============================+
	// |  View Movement Interactions  |
	// +==============================+
	{
		// +==============================+
		// |     Scroll Wheel Scroll      |
		// +==============================+
		if (MouseScrolled() && console->isOpen && console->fullyOpaque && IsMouseOver("DebugConsoleView"))
		{
			HandleMouseScroll();
			if (appInput->scrollDelta.y != 0)
			{
				if (ButtonDown(Button_Shift))
				{
					console->scrollGoto.x += -appInput->scrollDelta.y * CSL_SCROLL_SPEED;
				}
				else
				{
					console->scrollGoto.y += -appInput->scrollDelta.y * CSL_SCROLL_SPEED;
					if (console->scrollGoto.y < maxScrollY)
					{
						console->scrollToEnd = false;
					}
				}
			}
			if (appInput->scrollDelta.x != 0)
			{
				console->scrollGoto.x += -appInput->scrollDelta.x * CSL_SCROLL_SPEED;
			}
		}
		
		// +==============================+
		// |        Handle PageUp         |
		// +==============================+
		if (console->isOpen && console->fullyOpaque && ButtonPressed(Button_PageUp))
		{
			HandleButton(Button_PageUp);
			console->scrollGoto.y -= viewableHeight;
			console->scrollToEnd = false;
		}
		
		// +==============================+
		// |       Handle PageDown        |
		// +==============================+
		if (console->isOpen && console->fullyOpaque && ButtonPressed(Button_PageDown))
		{
			HandleButton(Button_PageDown);
			console->scrollGoto.y += viewableHeight;
			if (console->scrollGoto.y >= maxScrollY)
			{
				console->scrollToEnd = true;
			}
		}
		
		// +==============================+
		// |         Handle Home          |
		// +==============================+
		if (console->isOpen && console->fullyOpaque && !IsUiFocused(&console->inputBox) && ButtonPressed(Button_Home))
		{
			HandleButton(Button_Home);
			console->scrollGoto.y = 0;
			console->scrollToEnd = false;
		}
		
		// +==============================+
		// |          Handle End          |
		// +==============================+
		if (console->isOpen && console->fullyOpaque && !IsUiFocused(&console->inputBox) && ButtonPressed(Button_End))
		{
			HandleButton(Button_End);
			console->scrollGoto.y = maxScrollY;
			console->scrollToEnd = true;
		}
		
		// +==============================+
		// |     Follow Selection End     |
		// +==============================+
		if (selecting && console->isOpen && console->fullyOpaque && !console->selectionChanged)
		{
			r32 cursorTop = console->selCursorPos.y - FontGetMaxExtendUp(CSL_FONT, CSL_FONT_SIZE);
			if (cursorTop < console->scrollGoto.y)
			{
				console->scrollGoto.y = cursorTop - CSL_SELECT_SCROLL_OVER_AMOUNT;
				console->scrollToEnd = false;
			}
			r32 cursorBottom = console->selCursorPos.y + FontGetMaxExtendDown(CSL_FONT, CSL_FONT_SIZE) + CSL_LINE_SPACING;
			if (cursorBottom > console->scrollGoto.y + viewableRec.height)
			{
				console->scrollGoto.y = cursorBottom - viewableRec.height + CSL_SELECT_SCROLL_OVER_AMOUNT;
			}
		}
	}
	
	// +==============================+
	// | Handle Drop Down Checkboxes  |
	// +==============================+
	if (console->isOpen && console->fullyOpaque && console->dropDownOpen)
	{
		if (IsMouseOver("DebugConsoleLineWrapCheckbox") && ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			console->lineWrap = !console->lineWrap;
			console->remeasureContent = true;
			console->selectionChanged = true;
		}
		if (IsMouseOver("DebugConsoleGutterCheckbox") && ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			console->showGutterNumbers = !console->showGutterNumbers;
			console->remeasureContent = true;
			console->selectionChanged = true;
		}
		if (IsMouseOver("DebugConsoleFileNamesCheckbox") && ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			console->showFileNames = !console->showFileNames;
			console->remeasureContent = true;
			console->selectionChanged = true;
		}
		if (IsMouseOver("DebugConsoleLineNumsCheckbox") && ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			console->showLineNumbers = !console->showLineNumbers;
			console->remeasureContent = true;
			console->selectionChanged = true;
		}
		if (IsMouseOver("DebugConsoleFuncNamesCheckbox") && ButtonPressed(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			console->showFuncNames = !console->showFuncNames;
			console->remeasureContent = true;
			console->selectionChanged = true;
		}
	}
	
	// +========================================+
	// | Handle Mouse Interaction Over Console  |
	// +========================================+
	if ((console->isOpen && console->fullyOpaque) || (console->isOpen && (IsMouseOver("DebugConsole") || IsMouseOver("DebugConsoleView"))) || IsMouseOver(TB_GetUniqueName(&console->inputBox)))
	{
		if (ButtonPressed(MouseButton_Left) || ButtonDown(MouseButton_Left) || ButtonReleased(MouseButton_Left))
		{
			HandleButton(MouseButton_Left);
			if (!IsMouseOver(TB_GetUniqueName(&console->inputBox)) && IsUiFocused(&console->inputBox))
			{
				SetUiFocus(nullptr);
			}
			if (!IsInsideRec(console->drawRec, RenderMousePos))
			{
				console->fullyOpaque = false;
			}
		}
		if (MouseMoved())
		{
			HandleMouseMove();
		}
		if (MouseScrolled())
		{
			HandleMouseScroll();
		}
	}
	
	// +==============================+
	// |     Handle Enter Button      |
	// +==============================+
	if (console->isOpen)
	{
		if (IsUiFocused(&console->inputBox))
		{
			if (ButtonPressed(Button_Enter))
			{
				HandleButton(Button_Enter);
				consumeInputBox = true;
			}
			if (ButtonDown(Button_Enter))
			{
				HandleButton(Button_Enter);
				console->sendButton.isDown = true;
			}
			if (ButtonReleased(Button_Enter))
			{
				HandleButton(Button_Enter);
				console->sendButton.isDown = false;
			}
		}
		else if (console->fullyOpaque && ButtonPressed(Button_Enter))
		{
			HandleButton(Button_Enter);
			SetUiFocus(&console->inputBox);
			TextBoxSelectAll(&console->inputBox);
		}
	}
	
	if (console->isOpen && IsUiFocused(&console->inputBox))
	{
		// +==============================+
		// |       Handle Up Button       |
		// +==============================+
		if (ButtonPressed(Button_Up))
		{
			HandleButton(Button_Up);
			
			bool foundPrevious = false;
			u32 findIndex = 0;
			DbgConsoleLine_t* line = console->lastLine;
			while (line != nullptr)
			{
				if (IsFlagSet(line->flags, DbgFlag_UserInput))
				{
					if (findIndex == console->recallIndex)
					{
						foundPrevious = true;
						break;
					}
					findIndex++;
				}
				line = line->prev;
			}
			
			if (foundPrevious)
			{
				Assert(line != nullptr);
				Assert(IsFlagSet(line->flags, DbgFlag_UserInput));
				
				if (console->recallIndex == 0)
				{
					Assert(console->tempRecallBuffer == nullptr);
					if (console->inputBox.textLength > 0)
					{
						console->tempRecallBuffer = ArenaString(mainHeap, console->inputBox.text, console->inputBox.textLength);
					}
				}
				
				const char* lastCmd = GetDbgLineMessage(line);
				Assert(lastCmd != nullptr);
				if (line->messageLength >= 3 && strncmp(lastCmd, ">> ", 3) == 0) { lastCmd += 3; }
				TextBoxSetTextNt(&console->inputBox, lastCmd);
				TextBoxSelectEnd(&console->inputBox);
				console->recallIndex++;
			}
			else
			{
				// WriteLine_W("No more previous user inputs");
			}
		}
		
		// +==============================+
		// |      Handle Down Button      |
		// +==============================+
		if (ButtonPressed(Button_Down))
		{
			HandleButton(Button_Down);
			if (console->recallIndex == 1)
			{
				if (console->tempRecallBuffer != nullptr)
				{
					TextBoxSetTextNt(&console->inputBox, console->tempRecallBuffer);
					TextBoxSelectEnd(&console->inputBox);
					ArenaPop(mainHeap, console->tempRecallBuffer);
					console->tempRecallBuffer = nullptr;
				}
				else
				{
					TextBoxSetTextNt(&console->inputBox, "");
				}
				console->recallIndex = 0;
			}
			else if (console->recallIndex > 1)
			{
				bool foundPrevious = false;
				u32 findIndex = 0;
				DbgConsoleLine_t* line = console->lastLine;
				while (line != nullptr)
				{
					if (IsFlagSet(line->flags, DbgFlag_UserInput))
					{
						if (findIndex == console->recallIndex-2)
						{
							foundPrevious = true;
							break;
						}
						findIndex++;
					}
					line = line->prev;
				}
				
				if (foundPrevious)
				{
					Assert(line != nullptr);
					Assert(IsFlagSet(line->flags, DbgFlag_UserInput));
					
					const char* lastCmd = GetDbgLineMessage(line);
					Assert(lastCmd != nullptr);
					if (line->messageLength >= 3 && strncmp(lastCmd, ">> ", 3) == 0) { lastCmd += 3; }
					TextBoxSetTextNt(&console->inputBox, lastCmd);
					TextBoxSelectEnd(&console->inputBox);
					console->recallIndex--;
				}
				else
				{
					WriteLine_W("Couldn't find the next user input"); //This kind of shouldn't happen
				}
			}
			else
			{
				// WriteLine_W("Already at present");
			}
		}
	}
	
	// +==============================+
	// |        Handle Ctrl+C         |
	// +==============================+
	if (console->isOpen && console->fullyOpaque && !IsUiFocused(&console->inputBox) && console->selStartLine >= 0 && ButtonPressedSeq(Button_C, BtnTransFlag_Ctrl))
	{
		const InputEvent_t* inputEvent = HandleButtonPressSeq("DebugConsoleView", Button_C, BtnTransFlag_Ctrl);
		TempPushMark();
		u32 selectionTextLength = 0;
		char* selectionText = GetDbgConsoleSelectionText(console, TempArena, &selectionTextLength);
		if (selectionTextLength > 0)
		{
			if (selectionText != nullptr)
			{
				platform->CopyToClipboard(selectionText, selectionTextLength+1);
				PrintLine_I("Copied %u bytes to clipboard", selectionTextLength);
			}
			else
			{
				PrintLine_W("Failed to collect %u char selection", selectionTextLength);
			}
		}
		else 
		{
			WriteLine_W("No selection to copy");
		}
		TempPopMark();
	}
	
	// +==============================+
	// |      Consume Input Box       |
	// +==============================+
	if (consumeInputBox)
	{
		TempPushMark();
		char* commandStr = ArenaString(TempArena, console->inputBox.text, console->inputBox.textLength);
		PrintLine_Rx(DbgFlag_UserInput, ">> %s", commandStr);
		HandleConsoleCommand(commandStr);
		TempPopMark();
		TextBoxSetTextNt(&console->inputBox, "");
		
		if (console->recallIndex != 0)
		{
			if (console->tempRecallBuffer != nullptr)
			{
				ArenaPop(mainHeap, console->tempRecallBuffer);
				console->tempRecallBuffer = nullptr;
			}
			console->recallIndex = 0;
		}
	}
	
	// +==============================+
	// |    Update Display Scroll     |
	// +==============================+
	{
		DbgConsoleUpdateRecs(console);
		DbgConsoleClampScrollValues(console);
		
		if (AbsR32(console->scrollGoto.x - console->scroll.x) > 1.0f)
		{
			console->scroll.x += (console->scrollGoto.x - console->scroll.x) / 3; //TODO: Make this a define
		}
		else { console->scroll.x = console->scrollGoto.x; }
		
		if (AbsR32(console->scrollGoto.y - console->scroll.y) > 1.0f)
		{
			console->scroll.y += (console->scrollGoto.y - console->scroll.y) / 3; //TODO: Make this a define
		}
		else { console->scroll.y = console->scrollGoto.y; }
	}
}

Color_t GetDbgLineColor(const DbgConsoleLine_t* line)
{
	Color_t result = MonokaiWhite;
	if (line->dbgLevel == DbgLevel_Debug)   { result = MonokaiGray1;     }
	if (line->dbgLevel == DbgLevel_Regular) { result = MonokaiWhite;     }
	if (line->dbgLevel == DbgLevel_Info)    { result = MonokaiGreen;     }
	if (line->dbgLevel == DbgLevel_Notify)  { result = MonokaiPurple;    }
	if (line->dbgLevel == DbgLevel_Other)   { result = MonokaiLightBlue; }
	if (line->dbgLevel == DbgLevel_Warning) { result = MonokaiOrange;    }
	if (line->dbgLevel == DbgLevel_Error)   { result = MonokaiMagenta;   }
	return result;
}

Color_t GetDbgFunctionColor(const DbgConsoleLine_t* line)
{
	Color_t result = MonokaiWhite;
	const char* filePath = GetDbgLineFilePath(line);
	if (filePath != nullptr)
	{
		const char* fileName = GetFileNamePart(filePath);
		u32 fileNameLength = (u32)strlen(fileName);
		if (fileNameLength >= 3  && strncmp(fileName, "app", 3)       == 0) { result = MonokaiBlue;   }
		if (fileNameLength >= 4  && strncmp(fileName, "game", 4)      == 0) { result = MonokaiGreen;  }
		if (fileNameLength >= 9  && strncmp(fileName, "main_menu", 9) == 0) { result = MonokaiOrange; }
		if (fileNameLength >= 4  && strncmp(fileName, "mess", 4)      == 0) { result = MonokaiBrown;  }
	}
	return result;
}

void DbgConsoleRender(DbgConsole_t* console)
{
	Assert(console != nullptr);
	if (console->openAnimProgress <= 0.0f) { return; } //nothing to draw if the console is closed
	
	DbgConsoleUpdateRecs(console);
	
	r32 consoleAlpha = 0.25f;
	// if (IsInsideRec(console->drawRec, RenderMousePos) && input->mouseInsideWindow)
	if (console->fullyOpaque)
	{
		consoleAlpha = 1.0f;
	}
	
	RcPushState();
	
	RcDrawBorderedRec(console->drawRec, ColorTransparent(MonokaiGray2, consoleAlpha), MonokaiWhite, 1);
	
	// RcDrawRectangle(console->inputBoxRec, ColorTransparent(MonokaiWhite, consoleAlpha));
	TextBoxDraw(&console->inputBox);
	ButtonDraw(&console->sendButton);
	
	RcDrawRectangle(console->viewRec, ColorTransparent(MonokaiDarkGray, consoleAlpha));
	
	// +==============================+
	// |       Draw Gutter Rec        |
	// +==============================+
	if (console->showGutterNumbers)
	{
		Color_t gutterColor = ColorTransparent(MonokaiGray1, consoleAlpha);
		rec gutterRec = NewRec(console->viewRec.x, console->viewRec.y, 2+console->maxGutterNumWidth - console->scroll.x, console->viewRec.height);
		if (gutterRec.width > 0)
		{
			RcDrawRectangle(gutterRec, gutterColor);
		}
	}
	
	// +==============================+
	// |          Draw Lines          |
	// +==============================+
	RcBindFontEx(CSL_FONT, CSL_FONT_SIZE, FontStyle_Default, Alignment_Left);
	rec oldViewport = rc->viewport;
	rec viewableRec = RecDeflate(console->viewRec, 2);
	r32 maxScrollY = console->contentSize.height - viewableRec.height;
	RcSetViewport(viewableRec);
	r32 maxWidth = console->viewRec.width - 2*2;
	v2 viewOrigin = console->viewRec.topLeft + NewVec2(2, 2) - console->scroll;
	v2 textPos = viewOrigin;
	textPos.y += FontGetMaxExtendUp(CSL_FONT, CSL_FONT_SIZE);
	textPos = Vec2Round(textPos);
	// r32 endDrawY = console->viewRec.y + console->viewRec.height + CSL_FONT->maxExtendUp * CSL_FONT_SIZE;
	r32 sameFileNameBegin = viewOrigin.y;
	r32 sameLineNumBegin  = viewOrigin.y;
	r32 sameFuncNameBegin = viewOrigin.y;
	
	bool doSelectionColoring = false;
	u32 selMinLine = 0;
	u32 selMinIndex = 0;
	u32 selMaxLine = 0;
	u32 selMaxIndex = 0;
	if (console->selStartLine >= 0 || console->selEndLine >= 0)
	{
		Assert(console->selStartLine >= 0 && console->selEndLine >= 0);
		selMinLine = (u32)console->selStartLine;
		selMinIndex = console->selStartIndex;
		selMaxLine = (u32)console->selEndLine;
		selMaxIndex = console->selEndIndex;
		if (selMinLine > selMaxLine || (selMinLine == selMaxLine && selMinIndex > selMaxIndex))
		{
			u32 temp = selMinLine;
			selMinLine = selMaxLine;
			selMaxLine = temp;
			temp = selMinIndex;
			selMinIndex = selMaxIndex;
			selMaxIndex = temp;
		}
		doSelectionColoring = (selMinLine != selMaxLine || selMinIndex != selMaxIndex);
	}
	
	// +==============================+
	// |  Draw Selection Rectangles   |
	// +==============================+
	RcSetDepth(0.95f);
	for (u32 rIndex = 0; rIndex < console->selRecs.length; rIndex++)
	{
		rec selRec = *DynArrayGet(&console->selRecs, rec, rIndex);
		selRec.topLeft += viewOrigin;
		if (RecIntersects(selRec, console->viewRec))
		{
			RcDrawRectangle(selRec, MonokaiLightBlue);
		}
	}
	
	RcSetDepth(0.90f);
	RcStartFontBatch(FontGetBakeFor(CSL_FONT, CSL_FONT_SIZE), mainHeap);
	RcStartFontBatch(FontGetBakeFor(CSL_FILE_NAME_FONT, CSL_FILE_NAME_FONT_SIZE), mainHeap);
	
	DbgConsoleLine_t* hoverLine = nullptr;
	DbgConsoleLine_t* line = console->firstLine;
	u32 lIndex = 0;
	while (line != nullptr)
	{
		TempPushMark();
		
		rec drawRec = line->drawRec;
		drawRec.topLeft += viewOrigin;
		
		const char* messageStr = GetDbgLineMessage(line);
		
		char* gutterNumStr = TempPrint("%u", line->gutterNumber);
		
		const char* filePath = GetDbgLineFilePath(line);
		const char* nextFilePath = nullptr;
		if (line->next != nullptr) { nextFilePath = GetDbgLineFilePath(line->next); }
		bool nextFileIsSame = (filePath != nullptr && nextFilePath != nullptr && strcmp(filePath, nextFilePath) == 0);
		
		char* lineNumStr = TempPrint(":%u", line->fileLineNumber);
		bool nextLineNumIsSame = (line->next != nullptr && line->fileLineNumber == line->next->fileLineNumber);
		
		const char* funcName = GetDbgLineFuncName(line);
		const char* nextFuncName = nullptr;
		if (line->next != nullptr) { nextFuncName = GetDbgLineFuncName(line->next); }
		bool nextFuncIsSame = (funcName != nullptr && nextFuncName != nullptr && strcmp(funcName, nextFuncName) == 0);
		
		Color_t fileNameBackColor = ColorTransparent(GetDbgFunctionColor(line), consoleAlpha);
		Color_t fileNameColor = MonokaiDarkGray;
		Color_t textColor = GetDbgLineColor(line);
		v2 messagePos = textPos;
		r32 messageMaxWidth = maxWidth;
		bool addPadding = false;
		bool drawBackground = false;
		Color_t backgroundColor = MonokaiDarkGray;
		rec backgroundRec = drawRec;
		backgroundRec.x -= 5;
		backgroundRec.width += 5*2;
		backgroundRec.x += line->gutterNumWidth + line->fileNameWidth + line->lineNumWidth + line->funcNameWidth;
		backgroundRec.width -= line->gutterNumWidth + line->fileNameWidth + line->lineNumWidth + line->funcNameWidth;
		bool isHoverLine = (console->mouseHoverLine >= 0 && lIndex == (u32)console->mouseHoverLine);
		if (isHoverLine) { hoverLine = line; }
		if (IsFlagSet(line->flags, DbgFlag_Inverted))
		{
			drawBackground = true;
			backgroundColor = textColor;
			textColor = MonokaiDarkGray;
			if (isHoverLine && console->mouseHoverIndex >= 0) { backgroundColor = ColorDarken(backgroundColor, 16); }
		}
		
		// RcDrawRectangle(drawRec, (lIndex%2) ? VisRed : VisPurple);
		RcSetDepth(1.00f);
		// +==============================+
		// |     Draw Line Background     |
		// +==============================+
		if (isHoverLine)
		{
			rec hoverRec = backgroundRec;
			hoverRec.width = rc->viewport.x + rc->viewport.width - hoverRec.x;
			RcDrawRectangle(hoverRec, ColorTransparent(NewColor(Color_Black), consoleAlpha));
		}
		if (drawBackground)
		{
			RcDrawRectangle(backgroundRec, ColorTransparent(backgroundColor, consoleAlpha));
		}
		
		// +==============================+
		// |     Draw UserInput Back      |
		// +==============================+
		if (IsFlagSet(line->flags, DbgFlag_UserInput))
		{
			rec feedRec = backgroundRec;
			feedRec.width = drawRec.x + line->messageOffsetX + FontMeasureNtString(">>", CSL_FONT, CSL_FONT_SIZE).width + 2 - backgroundRec.x;
			feedRec.height = RcLineHeight();
			RcDrawRectangle(feedRec, ColorTransparent(ColorComplimentary(textColor), consoleAlpha));
			// RcDrawRectangle(NewRec(backgroundRec.x, backgroundRec.y, backgroundRec.width, 1), ColorTransparent(textColor, consoleAlpha));
			// RcDrawRectangle(NewRec(backgroundRec.x, backgroundRec.y + backgroundRec.height-1, backgroundRec.width, 1), ColorTransparent(textColor, consoleAlpha));
		}
		
		RcSetDepth(0.90f);
		// +==============================+
		// |     Draw Gutter Numbers      |
		// +==============================+
		RcBindFontEx(CSL_GUTTER_NUM_FONT, CSL_GUTTER_NUM_FONT_SIZE, FontStyle_Default, Alignment_Left);
		if (console->showGutterNumbers)
		{
			v2 gutterNumStrSize = RcMeasureNtString(gutterNumStr);
			
			rec gutterNumRec = NewRec(messagePos.x, drawRec.y, console->maxGutterNumWidth, drawRec.height);
			if (RecIntersects(gutterNumRec, console->viewRec))
			{
				rec viewport = rc->viewport;
				rec usableRec = RecOverlap(gutterNumRec, viewport);
				
				r32 fontCenteringY = -RcLineHeight()/2 + RcMaxExtendUp();
				v2 gutterNumStrPos = NewVec2(gutterNumRec.x + gutterNumRec.width/2 - gutterNumStrSize.width/2, gutterNumRec.y + gutterNumRec.height/2 + fontCenteringY);
				gutterNumStrPos = Vec2Round(gutterNumStrPos);
				
				Color_t numColor = MonokaiDarkGray;
				RcSetViewport(usableRec);
				
				RcDrawNtString(gutterNumStr, gutterNumStrPos, numColor);
				RcSetViewport(viewport);
			}
			messagePos.x += console->maxGutterNumWidth+5;
			messageMaxWidth -= console->maxGutterNumWidth+5;
		}
		
		// +==============================+
		// |       Draw File Names        |
		// +==============================+
		RcBindFontEx(CSL_FILE_NAME_FONT, CSL_FILE_NAME_FONT_SIZE, FontStyle_Default, Alignment_Left);
		if (filePath != nullptr && console->showFileNames)
		{
			const char* fileName = GetFileNamePart(filePath);
			v2 fileNameSize = RcMeasureNtString(fileName);
			if (!nextFileIsSame)
			{
				rec fileNameRec = NewRec(messagePos.x, sameFileNameBegin, console->maxFileNameWidth, drawRec.y + drawRec.height - sameFileNameBegin);
				if (RecIntersects(fileNameRec, console->viewRec))
				{
					rec viewport = rc->viewport;
					rec usableRec = RecOverlap(fileNameRec, viewport);
					
					r32 fontCenteringY = -RcLineHeight()/2 + RcMaxExtendUp();
					v2 fileNamePos = NewVec2(fileNameRec.x + fileNameRec.width/2 - fileNameSize.width/2, fileNameRec.y + fileNameRec.height/2 + fontCenteringY);
					if (fileNamePos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { fileNamePos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendDown(); }
					if (fileNamePos.y < usableRec.y + 2 + RcMaxExtendUp()) { fileNamePos.y = usableRec.y + 2 + RcMaxExtendUp(); }
					if (fileNamePos.y < fileNameRec.y + RcMaxExtendUp()) { fileNamePos.y = fileNameRec.y + RcMaxExtendUp(); }
					if (fileNamePos.y > fileNameRec.y + fileNameRec.height - RcMaxExtendDown()) { fileNamePos.y = fileNameRec.y + fileNameRec.height - RcMaxExtendDown(); }
					fileNamePos = Vec2Round(fileNamePos);
					
					Color_t backColor = fileNameBackColor;
					if (console->fullyOpaque && console->showLineNumbers)
					{
						rec lineNumsRec = usableRec;
						lineNumsRec.x = messagePos.x + line->fileNameWidth;
						lineNumsRec.width = line->lineNumWidth;
						if (IsInsideRec(lineNumsRec, RenderMousePos))
						{
							backColor = ColorDarken(backColor, 16);
						}
					}
					RcDrawRectangle(fileNameRec, backColor);
					RcSetViewport(usableRec);
					RcDrawNtString(fileName, fileNamePos, fileNameColor);
					RcSetViewport(viewport);
				}
				sameFileNameBegin = drawRec.y + drawRec.height + CSL_LINE_SPACING;
			}
			messagePos.x += console->maxFileNameWidth + (console->showLineNumbers ? 0 : 1);
			messageMaxWidth -= console->maxFileNameWidth + (console->showLineNumbers ? 0 : 1);
			addPadding = true;
		}
		else
		{
			sameFileNameBegin = drawRec.y + drawRec.height + CSL_LINE_SPACING;
		}
		
		// +==============================+
		// |    Draw File Line Numbers    |
		// +==============================+
		RcBindFontEx(CSL_LINE_NUM_FONT, CSL_LINE_NUM_FONT_SIZE, FontStyle_Default, Alignment_Left);
		if (console->showLineNumbers)
		{
			v2 lineNumStrSize = FontMeasureNtString(lineNumStr, CSL_LINE_NUM_FONT, CSL_LINE_NUM_FONT_SIZE);
			if (!nextLineNumIsSame || !nextFileIsSame)
			{
				rec lineNumRec = NewRec(messagePos.x, sameLineNumBegin, console->maxLineNumWidth, drawRec.y + drawRec.height - sameLineNumBegin);
				if (RecIntersects(lineNumRec, console->viewRec))
				{
					rec viewport = rc->viewport;
					rec usableRec = RecOverlap(lineNumRec, viewport);
					
					r32 fontCenteringY = -RcLineHeight()/2 + RcMaxExtendUp();
					v2 lineNumStrPos = NewVec2(lineNumRec.x + lineNumRec.width/2 - lineNumStrSize.width/2, lineNumRec.y + lineNumRec.height/2 + fontCenteringY);
					if (lineNumStrPos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { lineNumStrPos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendDown(); }
					if (lineNumStrPos.y < usableRec.y + 2 + RcMaxExtendUp()) { lineNumStrPos.y = usableRec.y + 2 + RcMaxExtendUp(); }
					if (lineNumStrPos.y < lineNumRec.y + RcMaxExtendUp()) { lineNumStrPos.y = lineNumRec.y + RcMaxExtendUp(); }
					if (lineNumStrPos.y > lineNumRec.y + lineNumRec.height - RcMaxExtendDown()) { lineNumStrPos.y = lineNumRec.y + lineNumRec.height - RcMaxExtendDown(); }
					lineNumStrPos = Vec2Round(lineNumStrPos);
					
					Color_t backColor = fileNameBackColor;
					if (console->fullyOpaque && IsInsideRec(usableRec, RenderMousePos)) { backColor = ColorDarken(backColor, 16); }
					RcDrawRectangle(lineNumRec, backColor);
					RcSetViewport(usableRec);
					RcDrawNtString(lineNumStr, lineNumStrPos, fileNameColor);
					RcSetViewport(viewport);
				}
				sameLineNumBegin = drawRec.y + drawRec.height + CSL_LINE_SPACING;
			}
			messagePos.x += console->maxLineNumWidth+1;
			messageMaxWidth -= console->maxLineNumWidth+1;
			addPadding = true;
		}
		else
		{
			sameLineNumBegin = drawRec.y + drawRec.height + CSL_LINE_SPACING;
		}
		
		// +==============================+
		// |     Draw Function Names      |
		// +==============================+
		RcBindFontEx(CSL_FUNC_NAME_FONT, CSL_FUNC_NAME_FONT_SIZE, FontStyle_Default, Alignment_Left);
		if (funcName != nullptr && console->showFuncNames)
		{
			v2 funcNameSize = RcMeasureNtString(funcName);
			if (!nextFuncIsSame || !nextFileIsSame)
			{
				rec funcNameRec = NewRec(messagePos.x, sameFuncNameBegin, console->maxFuncNameWidth, drawRec.y + drawRec.height - sameFuncNameBegin);
				if (RecIntersects(funcNameRec, console->viewRec))
				{
					rec viewport = rc->viewport;
					rec usableRec = RecOverlap(funcNameRec, viewport);
					
					r32 fontCenteringY = -RcLineHeight()/2 + RcMaxExtendUp();
					v2 functionNamePos = NewVec2(funcNameRec.x + funcNameRec.width/2 - funcNameSize.width/2, funcNameRec.y + funcNameRec.height/2 + fontCenteringY);
					if (functionNamePos.y > usableRec.y + usableRec.height - 2 - RcMaxExtendDown()) { functionNamePos.y = usableRec.y + usableRec.height - 2 - RcMaxExtendDown(); }
					if (functionNamePos.y < usableRec.y + 2 + RcMaxExtendUp()) { functionNamePos.y = usableRec.y + 2 + RcMaxExtendUp(); }
					if (functionNamePos.y < funcNameRec.y + RcMaxExtendUp()) { functionNamePos.y = funcNameRec.y + RcMaxExtendUp(); }
					if (functionNamePos.y > funcNameRec.y + funcNameRec.height - RcMaxExtendDown()) { functionNamePos.y = funcNameRec.y + funcNameRec.height - RcMaxExtendDown(); }
					functionNamePos = Vec2Round(functionNamePos);
					
					RcDrawRectangle(funcNameRec, fileNameBackColor);
					RcSetViewport(usableRec);
					RcDrawNtString(funcName, functionNamePos, fileNameColor);
					RcSetViewport(viewport);
				}
				sameFuncNameBegin = drawRec.y + drawRec.height + CSL_LINE_SPACING;
			}
			messagePos.x += console->maxFuncNameWidth+1;
			messageMaxWidth -= console->maxFuncNameWidth+1;
			addPadding = true;
		}
		else
		{
			sameFuncNameBegin = drawRec.y + drawRec.height + CSL_LINE_SPACING;
		}
		
		if (addPadding) { messagePos.x += 4; messageMaxWidth -= 4; }
		
		// +==============================+
		// |      Draw Line Message       |
		// +==============================+
		if (messageStr != nullptr && RecIntersects(drawRec, console->viewRec))
		{
			Assert(messageStr[line->messageLength] == '\0');
			messagePos = Vec2Round(messagePos);
			u16 fontStyle = FontStyle_Default;
			if (IsFlagSet(line->flags, DbgFlag_RawText)) { fontStyle |= FontStyle_RawData; }
			RcBindFontEx(CSL_FONT, CSL_FONT_SIZE, fontStyle, Alignment_Left);
			
			FontFlowExtra_t flowExtra = {};
			if (doSelectionColoring && lIndex >= selMinLine && lIndex <= selMaxLine)
			{
				flowExtra.doSelection = true;
				flowExtra.selChangesColor = true;
				flowExtra.selColor = MonokaiDarkGray;
				flowExtra.selStart = 0;
				flowExtra.selEnd = line->messageLength;
				if (lIndex == selMinLine) { flowExtra.selStart = (i32)selMinIndex; }
				if (lIndex == selMaxLine) { flowExtra.selEnd = (i32)selMaxIndex; }
			}
			
			RcDrawNtString(messageStr, messagePos, textColor, console->lineWrap ? messageMaxWidth : 0, nullptr, &flowExtra);
		}
		
		textPos.y += line->drawRec.height + CSL_LINE_SPACING;
		line = line->next;
		lIndex++;
		TempPopMark();
	}
	RcFinishBatches(&resources->fontShader);
	
	RcSetDepth(0.85f);
	
	#if 0
	// +==============================+
	// |      Draw Hover Cursor       |
	// +==============================+
	if (console->mouseHoverLine >= 0 && console->mouseHoverIndex >= 0)
	{
		Assert(hoverLine != nullptr);
		v2 cursorPos = viewOrigin + console->mouseHoverCursorPos;
		RcBindFontEx(CSL_FONT, CSL_FONT_SIZE, FontStyle_Default, Alignment_Left);
		rec cursorRec = NewRec(cursorPos.x, cursorPos.y - RcMaxExtendUp(), 1, RcLineHeight());
		RcDrawRectangle(cursorRec, ColorTransparent(MonokaiWhite, Oscillate(0, 1, 1000)));
	}
	#endif
	
	// +==============================+
	// |    Draw Selection Cursor     |
	// +==============================+
	if (console->selEndLine >= 0)
	{
		v2 cursorPos = viewOrigin + console->selCursorPos;
		RcBindFontEx(CSL_FONT, CSL_FONT_SIZE, FontStyle_Default, Alignment_Left);
		rec cursorRec = NewRec(cursorPos.x, cursorPos.y - RcMaxExtendUp(), 1, RcLineHeight() + CSL_LINE_SPACING);
		RcDrawRectangle(cursorRec, ColorLerp(MonokaiDarkGray, MonokaiWhite, Oscillate(0, 1, 1000)));
	}
	
	RcSetViewport(oldViewport);
	RcBindShader(&resources->mainShader);
	RcSetDepth(0.5f);
	
	RcDrawRectangle(console->scrollGutterRec, ColorTransparent(MonokaiDarkGray, consoleAlpha));
	RcDrawRectangle(console->scrollBarRec, MonokaiWhite);
	
	// +==============================+
	// |  Draw New Content Indicator  |
	// +==============================+
	if (!console->scrollToEnd && console->numLines > 0)
	{
		u64 timeSinceLastLine = TimeSince(console->lastLine->time);
		if (timeSinceLastLine < 1000)
		{
			r32 animPercent = (r32)timeSinceLastLine / 1000;
			rec indicatorRec = NewRec(console->viewRec.x, console->viewRec.y + console->viewRec.height - 20, console->viewRec.width, 20);
			indicatorRec.width += console->scrollGutterRec.width;
			RcDrawGradient(indicatorRec, ColorTransparent(MonokaiLightGray, (1-animPercent) * consoleAlpha), NewColor(Color_TransparentWhite), Dir2_Up);
			// RcDrawRectangle(indicatorRec, ColorTransparent(MonokaiLightGray, (1-animPercent) * consoleAlpha));
		}
	}
	
	// +==============================+
	// |     Draw Drop Down Menu      |
	// +==============================+
	if (console->dropDownAnimProgress >= 0.0f)
	{
		if (console->fullyOpaque) { RcDrawRectangle(console->dropDownRec, MonokaiGray2); }
		
		RcSetViewport(console->dropDownRec);
		
		if (console->clearButton.bounds.width >= 10)
		{
			ButtonDraw(&console->clearButton);
		}
		
		if (console->fullyOpaque)
		{
			Color_t checkboxColor = MonokaiWhite;
			RcBindFontEx(CSL_FONT, CSL_FONT_SIZE, FontStyle_Default, Alignment_Left);
			
			RcDrawBorderedRec(console->lineWrapCheckboxRec, console->lineWrap ? checkboxColor : Transparent, checkboxColor, 1);
			RcDrawNtString("Line Wrap", console->lineWrapCheckboxRec.topLeft + console->lineWrapCheckboxRec.size + NewVec2(5, 0), checkboxColor);
			
			RcDrawBorderedRec(console->gutterCheckboxRec, console->showGutterNumbers ? checkboxColor : Transparent, checkboxColor, 1);
			RcDrawNtString("Gutter", console->gutterCheckboxRec.topLeft + console->gutterCheckboxRec.size + NewVec2(5, 0), checkboxColor);
			
			RcDrawBorderedRec(console->fileNamesCheckboxRec, console->showFileNames ? checkboxColor : Transparent, checkboxColor, 1);
			RcDrawNtString("File Names", console->fileNamesCheckboxRec.topLeft + console->fileNamesCheckboxRec.size + NewVec2(5, 0), checkboxColor);
			
			RcDrawBorderedRec(console->lineNumsCheckboxRec, console->showLineNumbers ? checkboxColor : Transparent, checkboxColor, 1);
			RcDrawNtString("Line Numbers", console->lineNumsCheckboxRec.topLeft + console->lineNumsCheckboxRec.size + NewVec2(5, 0), checkboxColor);
			
			RcDrawBorderedRec(console->funcNamesCheckboxRec, console->showFuncNames ? checkboxColor : Transparent, checkboxColor, 1);
			RcDrawNtString("Function Names", console->funcNamesCheckboxRec.topLeft + console->funcNamesCheckboxRec.size + NewVec2(5, 0), checkboxColor);
		}
		
		RcSetViewport(oldViewport);
	}
	
	ButtonDraw(&console->dropDownButton);
	// RcDrawRectangle(console->dropDownButton.bounds, MonokaiRed);
	
	RcPopState();
}

char* DbgConsoleCrashDump(DbgConsole_t* console, u32* resultSizeOut, const char* crashFunction, const char* crashFilename, int crashLineNumber, const char* crashExpressionStr, const RealTime_t* realTime, u64 programTime)
{
	char* result = nullptr;
	u32 resultSize = 0;
	if (resultSizeOut != nullptr) { *resultSizeOut = 0; }
	
	bool isDebugOutputCorrupted = false;
	if (console == nullptr || app == nullptr || console->fifoSpace == nullptr || console->firstLine == nullptr) { isDebugOutputCorrupted = true; }
	else
	{
		DbgConsoleLine_t* line = console->firstLine;
		while (line != nullptr)
		{
			if ((u8*)line < console->fifoSpace) { isDebugOutputCorrupted = true; break; }
			if ((u8*)line + sizeof(DbgConsoleLine_t) > console->fifoSpace + console->fifoSize) { isDebugOutputCorrupted = true; break; }
			u32 totalSize = sizeof(DbgConsoleLine_t) + line->filePathLength+1 + line->funcNameLength+1 + line->messageLength+1;
			if ((u8*)line + totalSize > console->fifoSpace + console->fifoSize) { isDebugOutputCorrupted = true; break; }
			line = line->next;
		}
	}
	
	for (u8 pass = 0; pass < 2; pass++)
	{
		u32 numChars = 0;
		
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "|  App Crash Dump v%u.%u(%5u)  |\n", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
		TwoPassPrint(&numChars, result, resultSize, "+==============================+\n");
		TwoPassPrint(&numChars, result, resultSize, "Time: %s %s %s %u at %u:%u:%u%s\n", GetDayOfWeekStr((DayOfWeek_t)realTime->dayOfWeek), GetMonthStr((Month_t)realTime->month), GetDayOfMonthString(realTime->day), realTime->year, Convert24HourTo12Hour(realTime->hour), realTime->minute, realTime->second, IsPostMeridian(realTime->hour) ? "pm" : "am");
		TwoPassPrint(&numChars, result, resultSize, "Failed Assertion in %s:%u function %s: (%s) is false\n\n", crashFilename, crashLineNumber, crashFunction, crashExpressionStr);
		
		if (!isDebugOutputCorrupted)
		{
			DbgConsoleLine_t* line = console->firstLine;
			while (line != nullptr)
			{
				const char* message = GetDbgLineMessage(line);
				const char* filePath = GetDbgLineFilePath(line);
				const char* funcName = GetDbgLineFuncName(line);
				u64 programStartTimestamp = realTime->timestamp;
				if (programStartTimestamp > (programTime/1000)) { programStartTimestamp -= (programTime/1000); }
				else { programStartTimestamp = 0; }
				u64 lineTimestamp = programStartTimestamp + (line->time/1000);
				u64 milliseconds = (line->time%1000);
				if (milliseconds >= (programTime%1000)) { milliseconds -= (programTime%1000); }
				else { milliseconds += 1000 - (programTime%1000); }
				milliseconds = (milliseconds % 1000);
				RealTime_t lineRealTime = {}; GetRealTime(lineTimestamp, true, &lineRealTime); //TODO: This calculation is totally wrong, time is in ms and relative to program time
				TwoPassPrint(&numChars, result, resultSize, "%s %s %s %u at %2u:%02u:%02u.%03u%s ", GetDayOfWeekStr((DayOfWeek_t)lineRealTime.dayOfWeek), GetMonthStr((Month_t)lineRealTime.month), GetDayOfMonthString(lineRealTime.day), lineRealTime.year, Convert24HourTo12Hour(lineRealTime.hour), lineRealTime.minute, lineRealTime.second, (u16)milliseconds, IsPostMeridian(lineRealTime.hour) ? "pm" : "am");
				TwoPassPrint(&numChars, result, resultSize, "%3u %.*s:%u %.*s [%s] - ", line->gutterNumber, line->filePathLength, filePath, line->fileLineNumber, line->funcNameLength, funcName, GetDbgLevelStr(line->dbgLevel));
				TwoPassPrint(&numChars, result, resultSize, "%.*s\n", line->messageLength, message);
				line = line->next;
			}
		}
		else
		{
			TwoPassPrint(&numChars, result, resultSize, "[Debug Output is Corrupted]\n");
		}
		
		if (pass == 0)
		{
			if (numChars == 0) { return nullptr; }
			resultSize = numChars;
			result = (char*)malloc(numChars+1);
			if (result == nullptr) { return nullptr; }
		}
		else
		{
			if (numChars != resultSize) { free(result); return nullptr; }
			result[resultSize] = '\0';
		}
	}
	if (resultSizeOut != nullptr) { *resultSizeOut = resultSize; }
	return result;
}
