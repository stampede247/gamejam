/*
File:   app_palette.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_PALETTE_H
#define _APP_PALETTE_H

union PalColor_t
{
	Color_t colors[4];
	struct
	{
		Color_t primary;   //target1
		Color_t secondary; //target4
		Color_t light;     //target2
		Color_t dark;      //target3
	};
	struct
	{
		Color_t color0;
		Color_t color1;
		Color_t color2;
		Color_t color3;
	};
	struct
	{
		Color_t c0;
		Color_t c1;
		Color_t c2;
		Color_t c3;
	};
	struct
	{
		Color_t target1;
		Color_t target4;
		Color_t target2;
		Color_t target3;
	};
	struct
	{
		Color_t dress;
		Color_t trim;
		Color_t pin;
		Color_t shoes;
	};
};

#define NUM_PALETTE_COLORS 32
struct ColorPalette_t
{
	Texture_t texture;
	union
	{
		PalColor_t colors[NUM_PALETTE_COLORS];
		struct
		{
			PalColor_t target;
			PalColor_t unused1;
			PalColor_t unused2;
			PalColor_t unused3;
			PalColor_t unused4;
			PalColor_t unused5;
			PalColor_t unused6;
			PalColor_t unused7;
			PalColor_t unused8;
			PalColor_t unused9;
			PalColor_t unused10;
			PalColor_t unused11;
			PalColor_t unused12;
			PalColor_t unused13;
			PalColor_t unused14;
			PalColor_t unused15;
			PalColor_t unused16;
			PalColor_t unused17;
			PalColor_t unused18;
			PalColor_t unused19;
			PalColor_t unused20;
			PalColor_t unused21;
			PalColor_t unused22;
			PalColor_t unused23;
			PalColor_t unused24;
			PalColor_t unused25;
			PalColor_t unused26;
			PalColor_t unused27;
			PalColor_t unused28;
			PalColor_t unused29;
			PalColor_t unused30;
			PalColor_t unused31;
		};
	};
};

#endif //  _APP_PALETTE_H
