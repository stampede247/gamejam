/*
File:   app_console.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_CONSOLE_H
#define _APP_CONSOLE_H

typedef enum
{
	DbgFlags_None         = 0x00,
	DbgFlag_Inverted      = 0x01,
	DbgFlag_UserInput     = 0x02,
	DbgFlag_PlatformLayer = 0x04,
	DbgFlag_Box2D         = 0x08,
	DbgFlag_RawText       = 0x10,
	// DbgFlag_Something = 0x20,
	// DbgFlag_Something = 0x40,
	// DbgFlag_Something = 0x80,
} DbgFlags_t;

struct DbgConsoleLine_t
{
	DbgConsoleLine_t* prev;
	DbgConsoleLine_t* next;
	
	u32 filePathLength;
	u32 funcNameLength;
	u32 messageLength;
	
	u8 flags;
	u64 time;
	DbgLevel_t dbgLevel;
	u32 fileLineNumber;
	u32 gutterNumber;
	
	rec drawRec;
	r32 gutterNumWidth;
	r32 fileNameWidth;
	r32 lineNumWidth;
	r32 funcNameWidth;
	r32 messageOffsetX;
};

struct DbgConsole_t
{
	bool isOpen;
	bool openLarge;
	bool fullyOpaque;
	bool dropDownOpen;
	r32 openAnimProgress;
	r32 dropDownAnimProgress;
	
	rec drawRec;
	rec viewRec;
	rec scrollGutterRec;
	rec scrollBarRec;
	rec dropDownRec;
	rec lineWrapCheckboxRec;
	rec gutterCheckboxRec;
	rec fileNamesCheckboxRec;
	rec lineNumsCheckboxRec;
	rec funcNamesCheckboxRec;
	
	bool lineWrap;
	bool showGutterNumbers;
	bool showFileNames;
	bool showLineNumbers;
	bool showFuncNames;
	bool scrollToEnd;
	v2 scroll;
	v2 scrollGoto;
	bool remeasureContent;
	r32 maxFileNameWidth;
	r32 maxLineNumWidth;
	r32 maxFuncNameWidth;
	r32 maxGutterNumWidth;
	v2 contentSize;
	u32 recallIndex;
	char* tempRecallBuffer;
	
	TextBox_t inputBox;
	Button_t sendButton;
	Button_t dropDownButton;
	Button_t clearButton;
	
	u32 fifoSize;
	u32 fifoUsage;
	u8* fifoSpace;
	
	u32 numLines;
	u32 nextGutterNumber;
	DbgConsoleLine_t* firstLine;
	DbgConsoleLine_t* lastLine;
	
	i32 mouseHoverLine;
	i32 mouseHoverIndex;
	v2 mouseHoverCursorPos;
	bool mouseStartedOverText;
	
	bool selectionChanged;
	i32 selStartLine;
	u32 selStartIndex;
	i32 selEndLine;
	u32 selEndIndex;
	bool selRecAddToEnd;
	bool selRecAddToBeginning;
	DynArray_t selRecs; //these rectangles are in relative view space
	v2 selCursorPos;
};

DbgConsoleLine_t* DbgConsoleOutput(DbgConsole_t* console, u8 flags, const char* filePath, u32 lineNumber, const char* funcName, DbgLevel_t dbgLevel, const char* message);

#endif //  _APP_CONSOLE_H
