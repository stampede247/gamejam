/*
File:   app_structs.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _APP_STRUCTS_H
#define _APP_STRUCTS_H

typedef enum
{
	SoundTransition_Immediate,
	SoundTransition_FadeOut,
	SoundTransition_FadeIn,
	SoundTransition_FadeOutFadeIn,
	SoundTransition_CrossFade,
} SoundTransition_t;

struct Texture_t
{
	GLuint id;
	bool isValid;
	
	union
	{
		v2i size;
		struct { i32 width, height; };
	};
};

struct Sound_t
{
	bool isValid;
	ALuint id;
	ALenum format;
	ALsizei frequency;
	u32 channelCount;
	u32 sampleCount;
};

struct SoundInstance_t
{
	bool playing;
	u64 startTime;
	ALuint id;
	ALuint soundId;
};

struct SpriteSheet_t
{
	Texture_t texture;
	
	union
	{
		v2i padding;
		struct { i32 padX, padY; };
	};
	union
	{
		v2i frameSize;
		struct { i32 frameX, frameY; };
	};
	union
	{
		v2i innerFrameSize;
		struct { i32 innerX, innerY; };
	};
	union
	{
		v2i sheetSize;
		v2i numFrames;
		struct { i32 framesX, framesY; };
	};
};

struct Shader_t
{
	bool isValid;
	
	GLuint vertId;
	GLuint fragId;
	GLuint programId;
	
	GLuint vertexArray;
	
	struct
	{
		//Attributes
		GLint positionAttrib;
		GLint colorAttrib;
		GLint texCoordAttrib;
		
		//Uniform Locations
		GLint worldMatrix;
		GLint viewMatrix;
		GLint projectionMatrix;
		
		GLint texture;
		GLint textureSize;
		GLint sourceRectangle;
		GLint maskRectangle;
		
		GLint altTexture;
		GLint altTextureSize;
		GLint useAltTexture;
		GLint altSourceRectangle;
		GLint gradientEnabled;
		
		GLint primaryColor;
		GLint secondaryColor;
		GLint replaceColor1;
		GLint replaceColor2;
		GLint replaceColor3;
		GLint replaceColor4;
		
		GLint circleRadius;
		GLint circleInnerRadius;
		GLint vignette;
		
		GLint time;
		GLint saturation;
		GLint brightness;
		
		GLint value0;
		GLint value1;
		GLint value2;
		GLint value3;
		GLint value4;
	} locations;
};

struct VertexBuffer_t
{
	bool isDynamic;
	GLuint id;
	u32 numVertices;
};

struct Vertex_t
{
	union
	{
		v3 position;
		struct { r32 x, y, z; };
	};
	union
	{
		v4 color;
		struct { r32 r, g, b, a; };
	};
	union
	{
		v2 texCoord;
		struct { r32 tX, tY; };
	};
};

struct FrameBuffer_t
{
	GLuint id;
	GLuint depthBuffId;
	Texture_t texture;
	bool hasTransparency;
	bool hasStencil;
};

union TextLocation_t
{
	v2i vec;
	struct
	{
		i32 lineIndex;
		i32 charIndex;
	};
};

inline TextLocation_t NewTextLocation(i32 lineIndex, i32 charIndex)
{
	TextLocation_t result = {};
	result.lineIndex = lineIndex;
	result.charIndex = charIndex;
	return result;
}

typedef enum
{
	DisplayBtnShape_Rectangle = 0x00,
	DisplayBtnShape_Circle,
	DisplayBtnShape_Texture,
	DisplayBtnShape_AnalogStick,
} DisplayBtnShape_t;

struct DisplayBtn_t
{
	bool isGamepad;
	bool isAlt;
	Buttons_t btn;
	GamepadButtons_t gamepadBtn;
	rec drawRec;
	DisplayBtnShape_t shape;
};

enum
{
	PhysFlag_Static      = 0x01,
	PhysFlag_Active      = 0x02,
	PhysFlag_InUse       = 0x04,
	PhysFlag_Rectangular = 0x08,
};

struct PhysicsObject_t
{
	u8 flags;
	b2Body* body;
	Color_t color;
	v2 size;
};

#endif //  _APP_STRUCTS_H
