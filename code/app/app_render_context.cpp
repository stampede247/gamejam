/*
File:   app_render_context.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** The render context is a simple structure that helps us keep track of settings that were
	** sent to the graphics card through OpenGL
*/

void InitializeRenderContext()
{
	Assert(rc != nullptr);
	ClearPointer(rc);
	
	Vertex_t squareVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 0.0f} },
		{  {0.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 1.0f} },
		
		{  {0.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 1.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 0.0f} },
		{  {1.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 1.0f} },
	};
	rc->squareBuffer = CreateVertexBuffer(squareVertices, ArrayCount(squareVertices));
	
	Vertex_t rightTriangleVertices[] =
	{
		{  {0.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 0.0f} },
		{  {1.0f, 0.0f, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 0.0f} },
		{  {0.0f, 1.0f, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 1.0f} },
	};
	rc->rightTriangleBuffer = CreateVertexBuffer(rightTriangleVertices, ArrayCount(rightTriangleVertices));
	
	r32 triangleHeight = SqrtR32(3)/2.0f;
	Vertex_t equilTriangleVertices[] =
	{
		{  { 0.0f, 0.0f,           0.0f}, NewVec4(1, 1, 1, 1), {0.5f, 0.0f} },
		{  { 0.5f, triangleHeight, 0.0f}, NewVec4(1, 1, 1, 1), {1.0f, 1.0f} },
		{  {-0.5f, triangleHeight, 0.0f}, NewVec4(1, 1, 1, 1), {0.0f, 1.0f} },
	};
	rc->equilTriangleBuffer = CreateVertexBuffer(equilTriangleVertices, ArrayCount(equilTriangleVertices));
	
	TempPushMark();
	{
		u32 gradientWidth = 256;
		u8* gradientTextureData = PushArray(TempArena, u8, gradientWidth*4);
		for (u32 pIndex = 0; pIndex < gradientWidth; pIndex++)
		{
			u8* red   = &gradientTextureData[pIndex*4 + 0];
			u8* green = &gradientTextureData[pIndex*4 + 1];
			u8* blue  = &gradientTextureData[pIndex*4 + 2];
			u8* alpha = &gradientTextureData[pIndex*4 + 3];
			u8 value = (u8)pIndex;
			*red = value; *green = value; *blue = value;
			*alpha = 255;
		}
		rc->gradientTexture = CreateTexture(gradientTextureData, gradientWidth, 1, false, false);
	}
	TempPopMark();
	
	Color_t textureData = White;
	rc->dotTexture = CreateTexture((u8*)&textureData, 1, 1);
	
	DynamicVertexBufferExpand(&rc->dynamicBuffer, RC_MAX_DYNAMIC_VERTICES);
	
	rc->numPushedStates = 0;
}

// +--------------------------------------------------------------+
// |                    State Change Functions                    |
// +--------------------------------------------------------------+
void RcUpdateShader()
{
	if (rc->boundShader != nullptr)
	{
		if (rc->boundBuffer != nullptr)
		{
			glBindVertexArray(rc->boundShader->vertexArray);
			glBindBuffer(GL_ARRAY_BUFFER, rc->boundBuffer->id);
			glVertexAttribPointer(rc->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
			glVertexAttribPointer(rc->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
			glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
		}
		else
		{
			glBindVertexArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		
		glUniformMatrix4fv(rc->boundShader->locations.worldMatrix,      1, GL_FALSE, &rc->worldMatrix.values[0][0]);
		glUniformMatrix4fv(rc->boundShader->locations.viewMatrix,       1, GL_FALSE, &rc->viewMatrix.values[0][0]);
		glUniformMatrix4fv(rc->boundShader->locations.projectionMatrix, 1, GL_FALSE, &rc->projectionMatrix.values[0][0]);
		
		glActiveTexture(GL_TEXTURE0);
		if (rc->boundTexture != nullptr)
		{
			glBindTexture(GL_TEXTURE_2D, rc->boundTexture->id);
			glUniform1i(rc->boundShader->locations.texture, 0);
			glUniform2f(rc->boundShader->locations.textureSize, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		glUniform4f(rc->boundShader->locations.sourceRectangle, rc->sourceRectangle.x, rc->sourceRectangle.y, rc->sourceRectangle.width, rc->sourceRectangle.height);
		glUniform4f(rc->boundShader->locations.maskRectangle, rc->maskRectangle.x, rc->maskRectangle.y, rc->maskRectangle.width, rc->maskRectangle.height);
		
		glActiveTexture(GL_TEXTURE1);
		if (rc->boundAltTexture != nullptr)
		{
			glBindTexture(GL_TEXTURE_2D, rc->boundAltTexture->id);
			glUniform1i(rc->boundShader->locations.altTexture, 1);
			glUniform2f(rc->boundShader->locations.altTextureSize, (r32)rc->boundAltTexture->width, (r32)rc->boundAltTexture->height);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		glUniform1i(rc->boundShader->locations.useAltTexture, rc->useAltTexture ? 1 : 0);
		glUniform4f(rc->boundShader->locations.altSourceRectangle, rc->altSourceRectangle.x, rc->altSourceRectangle.y, rc->altSourceRectangle.width, rc->altSourceRectangle.height);
		glUniform1i(rc->boundShader->locations.gradientEnabled, rc->gradientEnabled ? 1 : 0);
		
		v4 colorVec = NewVec4(rc->primaryColor);
		glUniform4f(rc->boundShader->locations.primaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		colorVec = NewVec4(rc->secondaryColor);
		glUniform4f(rc->boundShader->locations.secondaryColor, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		colorVec = NewVec4(rc->replaceColor1);
		glUniform4f(rc->boundShader->locations.replaceColor1, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		colorVec = NewVec4(rc->replaceColor2);
		glUniform4f(rc->boundShader->locations.replaceColor2, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		colorVec = NewVec4(rc->replaceColor3);
		glUniform4f(rc->boundShader->locations.replaceColor3, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		colorVec = NewVec4(rc->replaceColor4);
		glUniform4f(rc->boundShader->locations.replaceColor4, colorVec.r, colorVec.g, colorVec.b, colorVec.a);
		
		glUniform1f(rc->boundShader->locations.circleRadius, rc->circleRadius);
		glUniform1f(rc->boundShader->locations.circleInnerRadius, rc->circleInnerRadius);
		glUniform2f(rc->boundShader->locations.vignette, rc->vigRadius, rc->vigSmoothness);
		
		glUniform1f(rc->boundShader->locations.time, rc->time);
		glUniform1f(rc->boundShader->locations.saturation, rc->saturation);
		glUniform1f(rc->boundShader->locations.brightness, rc->brightness);
		
		glUniform1f(rc->boundShader->locations.value0, rc->value0);
		glUniform1f(rc->boundShader->locations.value1, rc->value1);
		glUniform1f(rc->boundShader->locations.value2, rc->value2);
		glUniform1f(rc->boundShader->locations.value3, rc->value3);
		glUniform1f(rc->boundShader->locations.value4, rc->value4);
	}
}
void RcBindShader(const Shader_t* shaderPntr, bool forceUpdate = false)
{
	if (rc->boundShader != shaderPntr || forceUpdate)
	{
		rc->boundShader = shaderPntr;
		if (rc->boundShader == nullptr)
		{
			glUseProgram(0);
		}
		else
		{
			glUseProgram(rc->boundShader->programId);
			RcUpdateShader();
		}
	}
}

void RcSetWorldMatrix(const mat4& worldMatrix)
{
	rc->worldMatrix = worldMatrix;
	if (rc->boundShader != nullptr)
	{
		glUniformMatrix4fv(rc->boundShader->locations.worldMatrix, 1, GL_FALSE, &worldMatrix.values[0][0]);
	}
}
void RcSetViewMatrix(const mat4& viewMatrix)
{
	rc->viewMatrix = viewMatrix;
	if (rc->boundShader != nullptr)
	{
		glUniformMatrix4fv(rc->boundShader->locations.viewMatrix, 1, GL_FALSE, &viewMatrix.values[0][0]);
	}
}
void RcSetProjectionMatrix(const mat4& projectionMatrix)
{
	rc->projectionMatrix = projectionMatrix;
	if (rc->boundShader != nullptr)
	{
		glUniformMatrix4fv(rc->boundShader->locations.projectionMatrix, 1, GL_FALSE, &projectionMatrix.values[0][0]);
	}
}

void RcSetViewport(rec viewport, bool round = true)
{
	rc->viewport = viewport;
	if (round)
	{
		rc->viewport.topLeft = Vec2Round(rc->viewport.topLeft);
		rc->viewport.size = Vec2Round(rc->viewport.size);
	}
	
	bool flipYAxis = (rc->boundFrameBuffer == nullptr);
	reci openglRec = NewReci(
		(i32)rc->viewport.x,
		0,
		(i32)(rc->viewport.width),
		(i32)(rc->viewport.height)
	);
	if (rc->boundFrameBuffer != nullptr)
	{
		openglRec.y = (i32)(rc->viewport.y);
	}
	else
	{
		openglRec.y = (i32)(RenderArea.height - (rc->viewport.y + rc->viewport.height));	
	}
	glViewport(openglRec.x, openglRec.y, openglRec.width, openglRec.height);
	
	mat4 projMatrix = Mat4_Identity;
	projMatrix = Mat4Scale(NewVec3(2.0f/rc->viewport.width, (flipYAxis ? -1 : 1) * 2.0f/rc->viewport.height, 1.0f));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-rc->viewport.width/2.0f, -rc->viewport.height/2.0f, 0.0f)));
	projMatrix = Mat4Multiply(projMatrix, Mat4Translate(NewVec3(-rc->viewport.x, -rc->viewport.y, 0.0f)));
	RcSetProjectionMatrix(projMatrix);
}

void RcSetDepth(r32 depth)
{
	rc->depth = depth;
}

void RcBindFrameBuffer(const FrameBuffer_t* frameBuffer)
{
	if (rc->boundFrameBuffer == frameBuffer) { return; }
	rc->boundFrameBuffer = frameBuffer;
	
	if (frameBuffer == nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer->id);
	}
	RcSetViewport(rc->viewport);
}
void RcBindBuffer(const VertexBuffer_t* vertBufferPntr)
{
	if (rc->boundBuffer == vertBufferPntr) { return; }
	rc->boundBuffer = vertBufferPntr;
	
	if (vertBufferPntr == nullptr)
	{
		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else
	{
		glBindVertexArray(rc->boundShader->vertexArray);
		glBindBuffer(GL_ARRAY_BUFFER, vertBufferPntr->id);
		glVertexAttribPointer(rc->boundShader->locations.positionAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)0);
		glVertexAttribPointer(rc->boundShader->locations.colorAttrib,    4, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)sizeof(v3));
		glVertexAttribPointer(rc->boundShader->locations.texCoordAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex_t), (void*)(sizeof(v3)+sizeof(v4)));
	}
}
void RcBindTexture(const Texture_t* texturePntr)
{
	if (rc->boundTexture == texturePntr) { return; }
	rc->boundTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE0);
	if (texturePntr == nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, rc->dotTexture.id);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, rc->boundTexture->id);
		glUniform1i(rc->boundShader->locations.texture, 0);
		glUniform2f(rc->boundShader->locations.textureSize, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height);
	}
}
void RcBindAltTexture(const Texture_t* texturePntr)
{
	rc->boundAltTexture = texturePntr;
	
	glActiveTexture(GL_TEXTURE1);
	if (texturePntr == nullptr)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, rc->boundAltTexture->id);
		glUniform1i(rc->boundShader->locations.altTexture, 1);
		glUniform2f(rc->boundShader->locations.altTextureSize, (r32)rc->boundAltTexture->width, (r32)rc->boundAltTexture->height);
	}
}
void RcBindFont(Font_t* fontPntr)
{
	rc->boundFont = fontPntr;
}

void RcDrawBufferTriangles(u32 numVertices = 0)
{
	Assert(rc->boundBuffer != nullptr);
	if (numVertices == 0) { numVertices = rc->boundBuffer->numVertices; }
	glDrawArrays(GL_TRIANGLES, 0, numVertices);
}

void RcSetFontSize(r32 fontSize)
{
	rc->fontSize = fontSize;
}
void RcSetFontStyle(u16 fontStyle)
{
	rc->fontStyle = fontStyle;
}
void RcSetFontAlignment(Alignment_t alignment)
{
	rc->fontAlignment = alignment;
}
void RcBindFontEx(Font_t* fontPntr, r32 fontSize, u16 fontStyle, Alignment_t alignment)
{
	RcBindFont(fontPntr);
	RcSetFontSize(fontSize);
	RcSetFontStyle(fontStyle);
	RcSetFontAlignment(alignment);
}
void RcDefaultFontSettings()
{
	if (rc->boundFont != nullptr && rc->boundFont->numBakes > 0)
	{
		FontBake_t* fontBake = &rc->boundFont->bakes[0];
		RcSetFontSize(fontBake->size);
		RcSetFontStyle(fontBake->styleFlags);
	}
	else
	{
		RcSetFontSize(16);
		RcSetFontStyle(FontStyle_Default);
	}
	RcSetFontAlignment(Alignment_Left);
}
void RcBindFontDef(Font_t* fontPntr)
{
	RcBindFont(fontPntr);
	RcDefaultFontSettings();
}

void RcSetColor(Color_t color)
{
	if (rc->primaryColor.value != color.value)
	{
		rc->primaryColor = color;
		if (rc->boundShader != nullptr)
		{
			glUniform4f(rc->boundShader->locations.primaryColor, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
		}
	}
}
void RcSetSecondaryColor(Color_t color)
{
	if (rc->secondaryColor.value != color.value)
	{
		rc->secondaryColor = color;
		if (rc->boundShader != nullptr)
		{
			glUniform4f(rc->boundShader->locations.secondaryColor, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
		}
	}
}

void RcSetReplaceColor1(Color_t color)
{
	rc->replaceColor1 = color;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.replaceColor1, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
	}
}
void RcSetReplaceColor2(Color_t color)
{
	rc->replaceColor2 = color;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.replaceColor2, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
	}
}
void RcSetReplaceColor3(Color_t color)
{
	rc->replaceColor3 = color;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.replaceColor3, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
	}
}
void RcSetReplaceColor4(Color_t color)
{
	rc->replaceColor4 = color;
	if (rc->boundShader != nullptr)
	{
		glUniform4f(rc->boundShader->locations.replaceColor4, color.r / 255.f, color.g / 255.f, color.b / 255.f, color.a / 255.f);
	}
}
void RcSetReplaceColors(PalColor_t color)
{
	RcSetReplaceColor1(color.primary);
	RcSetReplaceColor2(color.light);
	RcSetReplaceColor3(color.dark);
	RcSetReplaceColor4(color.secondary);
}
void RcDefaultReplaceColors()
{
	RcSetReplaceColor1(plt->target.primary);
	RcSetReplaceColor2(plt->target.light);
	RcSetReplaceColor3(plt->target.dark);
	RcSetReplaceColor4(plt->target.secondary);
}

void RcSetSourceRectangle(rec sourceRectangle, bool round = true)
{
	if (round)
	{
		sourceRectangle.topLeft = Vec2Round(sourceRectangle.topLeft);
		sourceRectangle.size = Vec2Round(sourceRectangle.size);
	}
	if (rc->sourceRectangle != sourceRectangle)
	{
		rc->sourceRectangle = sourceRectangle;
		if (rc->boundShader != nullptr)
		{
			glUniform4f(rc->boundShader->locations.sourceRectangle, rc->sourceRectangle.x, rc->sourceRectangle.y, rc->sourceRectangle.width, rc->sourceRectangle.height);
		}
	}
}
void RcDefaultSourceRectangle()
{
	if (rc->boundTexture != nullptr)
	{
		RcSetSourceRectangle(NewRec(0, 0, (r32)rc->boundTexture->width, (r32)rc->boundTexture->height));
	}
	else
	{
		RcSetSourceRectangle(NewRec(0, 0, 0, 0));
	}
}

void RcSetMaskRectangle(rec maskRectangle, bool round = true)
{
	if (round)
	{
		maskRectangle.topLeft = Vec2Round(maskRectangle.topLeft);
		maskRectangle.size = Vec2Round(maskRectangle.size);
	}
	if (rc->maskRectangle != maskRectangle)
	{
		rc->maskRectangle = maskRectangle;
		if (rc->boundShader != nullptr)
		{
			glUniform4f(rc->boundShader->locations.maskRectangle, maskRectangle.x, maskRectangle.y, maskRectangle.width, maskRectangle.height);
		}
	}
}
void RcDisableMaskRectangle()
{
	RcSetMaskRectangle(NewRec(0, 0, 0, 0));
}

void RcSetAltTextureEnabled(bool useAltTexture)
{
	if (rc->useAltTexture != useAltTexture)
	{
		rc->useAltTexture = useAltTexture;
		if (rc->boundShader != nullptr)
		{
			glUniform1i(rc->boundShader->locations.useAltTexture, useAltTexture ? 1 : 0);
		}
	}
}
void RcEnableAltTexture()
{
	RcSetAltTextureEnabled(true);
}
void RcDisableAltTexture()
{
	RcSetAltTextureEnabled(false);
}

void RcSetAltSourceRectangle(rec sourceRectangle, bool round = true)
{
	if (round)
	{
		sourceRectangle.topLeft = Vec2Round(sourceRectangle.topLeft);
		sourceRectangle.size = Vec2Round(sourceRectangle.size);
	}
	if (rc->altSourceRectangle != sourceRectangle)
	{
		rc->altSourceRectangle = sourceRectangle;
		if (rc->boundShader != nullptr)
		{
			glUniform4f(rc->boundShader->locations.altSourceRectangle, rc->altSourceRectangle.x, rc->altSourceRectangle.y, rc->altSourceRectangle.width, rc->altSourceRectangle.height);
		}
	}
}
void RcDefaultAltSourceRectangle()
{
	if (rc->boundAltTexture != nullptr)
	{
		RcSetAltSourceRectangle(NewRec(0, 0, (r32)rc->boundAltTexture->width, (r32)rc->boundAltTexture->height));
	}
	else
	{
		RcSetAltSourceRectangle(NewRec(0, 0, 0, 0));
	}
}

void RcSetGradientEnabled(bool gradientEnabled)
{
	if (rc->gradientEnabled != gradientEnabled)
	{
		rc->gradientEnabled = gradientEnabled;
		if (rc->boundShader != nullptr)
		{
			glUniform1i(rc->boundShader->locations.gradientEnabled, gradientEnabled ? 1 : 0);
		}
	}
}

void RcSetCircleRadius(r32 radius, r32 innerRadius = 0.0f)
{
	if (rc->circleRadius != radius || rc->circleInnerRadius != innerRadius)
	{
		rc->circleRadius = radius;
		rc->circleInnerRadius = innerRadius;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.circleRadius, radius);
			glUniform1f(rc->boundShader->locations.circleInnerRadius, innerRadius);
		}
	}
}

void RcEnableVignette(r32 radius, r32 smoothness)
{
	if (rc->vigRadius != radius || rc->vigSmoothness != smoothness)
	{
		rc->vigRadius = radius;
		rc->vigSmoothness = smoothness;
		if (rc->boundShader != nullptr)
		{
			glUniform2f(rc->boundShader->locations.vignette, radius, smoothness);
		}
	}
}
void RcDisableVignette()
{
	RcEnableVignette(0, 0);
}

void RcSetTime(r32 time)
{
	if (rc->time != time)
	{
		rc->time = time;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.time, time);
		}
	}
}

void RcSetSaturation(r32 saturation)
{
	if (rc->saturation != saturation)
	{
		rc->saturation = saturation;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.saturation, saturation);
		}
	}
}

void RcSetBrightness(r32 brightness)
{
	if (rc->brightness != brightness)
	{
		rc->brightness = brightness;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.brightness, brightness);
		}
	}
}

void RcSetShaderValue0(r32 value)
{
	if (rc->value0 != value)
	{
		rc->value0 = value;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.value0, value);
		}
	}
}
void RcSetShaderValue1(r32 value)
{
	if (rc->value1 != value)
	{
		rc->value1 = value;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.value1, value);
		}
	}
}
void RcSetShaderValue2(r32 value)
{
	if (rc->value2 != value)
	{
		rc->value2 = value;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.value2, value);
		}
	}
}
void RcSetShaderValue3(r32 value)
{
	if (rc->value3 != value)
	{
		rc->value3 = value;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.value3, value);
		}
	}
}
void RcSetShaderValue4(r32 value)
{
	if (rc->value4 != value)
	{
		rc->value4 = value;
		if (rc->boundShader != nullptr)
		{
			glUniform1f(rc->boundShader->locations.value4, value);
		}
	}
}
void RcSetShaderValue(u8 index, r32 value)
{
	switch (index)
	{
		 case 0: RcSetShaderValue0(value); break;
		 case 1: RcSetShaderValue1(value); break;
		 case 2: RcSetShaderValue2(value); break;
		 case 3: RcSetShaderValue3(value); break;
		 case 4: RcSetShaderValue4(value); break;
		 default: PrintLine_E("Invalid shader index passed to RcSetShaderValue: %u", index); break;
	}
}

// +--------------------------------------------------------------+
// |                   Font Batching Functions                    |
// +--------------------------------------------------------------+
bool RcStartFontBatch(const FontBake_t* batchFontBake, MemoryArena_t* vertsMemArena, u32 numGlyphsExpected = FONT_BATCH_BASE_CHUNK_SIZE)
{
	Assert(batchFontBake != nullptr);
	Assert(vertsMemArena != nullptr);
	if (rc->numBatchingFonts >= MAX_BATCHING_FONTS) { return false; }
	rc->batchingFontBakes[rc->numBatchingFonts] = batchFontBake;
	if (rc->batchingVerts[rc->numBatchingFonts].items == nullptr)
	{
		CreateDynamicArray(&rc->batchingVerts[rc->numBatchingFonts], vertsMemArena, sizeof(FontBatchVertex_t), FONT_BATCH_CHUNK_SIZE, numGlyphsExpected);
	}
	rc->numBatchingFonts++;
	return true;
}
bool RcBatchGlyph(const FontBake_t* fontBake, v2 position, Color_t color, rec sourceRec, r32 scale)
{
	for (u32 bIndex = 0; bIndex < rc->numBatchingFonts; bIndex++)
	{
		const FontBake_t* batchFontBake = rc->batchingFontBakes[bIndex];
		DynArray_t* vertsArray = &rc->batchingVerts[bIndex];
		if (batchFontBake == fontBake)
		{
			DynArrayExpand(vertsArray, vertsArray->length+1);
			FontBatchVertex_t* newVertex = DynArrayAdd(vertsArray, FontBatchVertex_t);
			Assert(newVertex != nullptr);
			ClearPointer(newVertex);
			newVertex->position = NewVec3(position.x, position.y, rc->depth);
			newVertex->color = NewVec4(color);
			newVertex->sourceRec = sourceRec;
			newVertex->scale = scale;
			return true;
		}
		// else
		// {
		// 	PrintLine_W("fontBake: %p != batchFontBake: %p", fontBake, batchFontBake);
		// }
	}
	return false;
}
void RcFinishBatches(const Shader_t* shader)
{
	const Shader_t* oldShader = rc->boundShader;
	for (u32 bIndex = 0; bIndex < rc->numBatchingFonts; bIndex++)
	{
		const FontBake_t* batchFontBake = rc->batchingFontBakes[bIndex];
		DynArray_t* vertsArray = &rc->batchingVerts[bIndex];
		if (vertsArray->length > 0)
		{
			TempPushMark();
			u32 numVerts = vertsArray->length * 6;
			Vertex_t* verts = PushArray(TempArena, Vertex_t, numVerts);
			if (verts != nullptr)
			{
				for (u32 vIndex = 0; vIndex < vertsArray->length; vIndex++)
				{
					FontBatchVertex_t* fontVert = DynArrayGet(vertsArray, FontBatchVertex_t, vIndex);
					Assert(fontVert != nullptr);
					Vertex_t* vertsPntr = &verts[6*vIndex];
					rec sourceRec = fontVert->sourceRec;
					r32 scale = fontVert->scale;
					v2 textureSize = NewVec2(batchFontBake->texture.size);
					
					//top left
					vertsPntr[0].position.x = fontVert->position.x;
					vertsPntr[0].position.y = fontVert->position.y;
					vertsPntr[0].position.z = fontVert->position.z;
					vertsPntr[0].color = fontVert->color;
					vertsPntr[0].texCoord.x = sourceRec.x;
					vertsPntr[0].texCoord.y = sourceRec.y;
					//top right
					vertsPntr[1].position.x = fontVert->position.x + sourceRec.width*scale;
					vertsPntr[1].position.y = fontVert->position.y;
					vertsPntr[1].position.z = fontVert->position.z;
					vertsPntr[1].color = fontVert->color;
					vertsPntr[1].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[1].texCoord.y = sourceRec.y;
					//bottom left
					vertsPntr[2].position.x = fontVert->position.x;
					vertsPntr[2].position.y = fontVert->position.y + sourceRec.height*scale;
					vertsPntr[2].position.z = fontVert->position.z;
					vertsPntr[2].color = fontVert->color;
					vertsPntr[2].texCoord.x = sourceRec.x;
					vertsPntr[2].texCoord.y = sourceRec.y + sourceRec.height;
					
					//bottom left
					vertsPntr[3].position.x = fontVert->position.x;
					vertsPntr[3].position.y = fontVert->position.y + sourceRec.height*scale;
					vertsPntr[3].position.z = fontVert->position.z;
					vertsPntr[3].color = fontVert->color;
					vertsPntr[3].texCoord.x = sourceRec.x;
					vertsPntr[3].texCoord.y = sourceRec.y + sourceRec.height;
					//top right
					vertsPntr[4].position.x = fontVert->position.x + sourceRec.width*scale;
					vertsPntr[4].position.y = fontVert->position.y;
					vertsPntr[4].position.z = fontVert->position.z;
					vertsPntr[4].color = fontVert->color;
					vertsPntr[4].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[4].texCoord.y = sourceRec.y;
					//bottom right
					vertsPntr[5].position.x = fontVert->position.x + sourceRec.width*scale;
					vertsPntr[5].position.y = fontVert->position.y + sourceRec.height*scale;
					vertsPntr[5].position.z = fontVert->position.z;
					vertsPntr[5].color = fontVert->color;
					vertsPntr[5].texCoord.x = sourceRec.x + sourceRec.width;
					vertsPntr[5].texCoord.y = sourceRec.y + sourceRec.height;
				}
				
				VertexBuffer_t* vertBuffer = &rc->batchVertBuffers[bIndex];
				DynamicVertexBufferExpand(vertBuffer, numVerts, FONT_BATCH_BASE_CHUNK_SIZE);
				DynamicVertexBufferFill(vertBuffer, verts, numVerts);
				
				TempPopMark();
				
				RcBindShader(shader);
				RcBindTexture(&batchFontBake->texture);
				RcDefaultSourceRectangle();
				RcSetColor(White);
				RcSetWorldMatrix(Mat4_Identity);
				RcBindBuffer(vertBuffer);
				RcDrawBufferTriangles(numVerts);
			}
			else 
			{
				TempPopMark();
				PrintLine_E("Failed to allocated vertices for font bake of %u glyphs", vertsArray->length);
				Assert(false);
			}
			
			rc->numBatchedGlyphs += vertsArray->length;
		}
		
		// DestroyDynamicArray(vertsArray);
		vertsArray->length = 0;
		rc->batchingFontBakes[bIndex] = nullptr;
	}
	rc->numBatchingFonts = 0;
	RcBindShader(oldShader);
}

// +--------------------------------------------------------------+
// |                        Begin Function                        |
// +--------------------------------------------------------------+
void RcBegin(const Shader_t* defaultShader, rec defaultViewport, Font_t* defaultFont, FrameBuffer_t* defaultFramebuffer = nullptr, bool altBlendMode = false)
{
	RcBindFrameBuffer(defaultFramebuffer);
	RcSetViewport(defaultViewport);
	RcBindBuffer(nullptr);
	
	// glBlendEquation(GL_FUNC_ADD);
	if (altBlendMode) { glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA); }
	else { glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); }
	glEnable(GL_BLEND);
	
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GEQUAL, 0.1f);
	
	rc->depth = 1.0f;
	
	rc->boundFont = defaultFont;
	RcDefaultFontSettings();
	
	rc->worldMatrix = Mat4_Identity;
	rc->viewMatrix = Mat4_Identity;
	
	rc->boundTexture = &rc->dotTexture;
	rc->sourceRectangle = NewRec(0, 0, 1, 1);
	rc->maskRectangle = NewRec(0, 0, 0, 0);
	
	rc->boundAltTexture = nullptr;
	rc->useAltTexture = false;
	rc->altSourceRectangle = NewRec(0, 0, 0, 0);
	rc->gradientEnabled = false;
	
	rc->primaryColor = White;
	rc->secondaryColor = White;
	rc->replaceColor1 = plt->target.primary;
	rc->replaceColor2 = plt->target.light;
	rc->replaceColor3 = plt->target.dark;
	rc->replaceColor4 = plt->target.secondary;
	
	rc->circleRadius = 0.0f;
	rc->circleInnerRadius = 0.0f;
	rc->vigRadius = 0.0f;
	rc->vigSmoothness = 0.0f;
	
	rc->time = 0.0f;
	rc->saturation = 1.0f;
	rc->brightness = 1.0f;
	
	rc->value0 = 0.0f;
	rc->value1 = 0.0f;
	rc->value2 = 0.0f;
	rc->value3 = 0.0f;
	rc->value4 = 0.0f;
	
	RcBindShader(defaultShader, true);
}


// +--------------------------------------------------------------+
// |                        Other Actions                         |
// +--------------------------------------------------------------+
void RcStartStencilDrawing()
{
	glEnable(GL_STENCIL_TEST);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE); //don't draw to the color buffer
	glDepthMask(GL_FALSE); //don't draw to the depth buffer
	glStencilMask(0xFF); //
	glStencilFunc(GL_NEVER, 1, 0xFF);
	glStencilOp(GL_REPLACE, GL_KEEP, GL_KEEP);
}
void RcUseStencil(bool inverseMask = false)
{
	glEnable(GL_STENCIL_TEST);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDepthMask(GL_TRUE);
	glStencilMask(0x00);
	if (inverseMask)
	{
		glStencilFunc(GL_GREATER, 1, 0xFF);
	}
	else
	{
		glStencilFunc(GL_EQUAL, 1, 0xFF);
	}
}
void RcDisableStencil()
{
	glDisable(GL_STENCIL_TEST);
}

void RcClearColorBuffer(Color_t clearColor)
{
	v4 colorVec = NewVec4(clearColor);
	glClearColor(colorVec.x, colorVec.y, colorVec.z, colorVec.w);
	glClear(GL_COLOR_BUFFER_BIT);
}
void RcClearDepthBuffer(r32 clearDepth)
{
	glClearDepth(clearDepth);
	glClear(GL_DEPTH_BUFFER_BIT);
}
void RcClear(Color_t clearColor, r32 clearDepth)
{
	v4 colorVec = NewVec4(clearColor);
	glClearColor(colorVec.x, colorVec.y, colorVec.z, colorVec.w);
	glClearDepth(clearDepth);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
}
void RcClearStencilBuffer(u32 stencilValue)
{
	glClearStencil(stencilValue);
	glClear(GL_STENCIL_BUFFER_BIT);
}

// +--------------------------------------------------------------+
// |                           Push/Pop                           |
// +--------------------------------------------------------------+
void RcPushState()
{
	Assert(rc->numPushedStates < MAX_RENDER_CONTEXT_PUSHES);
	
	for (u32 sIndex = rc->numPushedStates; sIndex > 0; sIndex--)
	{
		memcpy(&rc->pushedStates[sIndex], &rc->pushedStates[sIndex-1], sizeof(RenderContextState_t));
	}
	
	RenderContextState_t* newState = &rc->pushedStates[0];
	
	newState->viewport           = rc->viewport;
	newState->depth              = rc->depth;
	newState->boundShader        = rc->boundShader;
	newState->boundFrameBuffer   = rc->boundFrameBuffer;
	newState->boundBuffer        = rc->boundBuffer;
	
	newState->boundFont          = rc->boundFont;
	newState->fontSize           = rc->fontSize;
	newState->fontStyle          = rc->fontStyle;
	newState->fontAlignment      = rc->fontAlignment;
	
	newState->worldMatrix        = rc->worldMatrix;
	newState->viewMatrix         = rc->viewMatrix;
	newState->projectionMatrix   = rc->projectionMatrix;
	
	newState->boundTexture       = rc->boundTexture;
	newState->sourceRectangle    = rc->sourceRectangle;
	newState->maskRectangle      = rc->maskRectangle;
	
	newState->boundAltTexture    = rc->boundAltTexture;
	newState->useAltTexture      = rc->useAltTexture;
	newState->altSourceRectangle = rc->altSourceRectangle;
	newState->gradientEnabled    = rc->gradientEnabled;
	
	newState->primaryColor       = rc->primaryColor;
	newState->secondaryColor     = rc->secondaryColor;
	newState->replaceColor1      = rc->replaceColor1;
	newState->replaceColor2      = rc->replaceColor2;
	newState->replaceColor3      = rc->replaceColor3;
	newState->replaceColor4      = rc->replaceColor4;
	
	newState->circleRadius       = rc->circleRadius;
	newState->circleInnerRadius  = rc->circleInnerRadius;
	newState->vigRadius          = rc->vigRadius;
	newState->vigSmoothness      = rc->vigSmoothness;
	
	newState->time               = rc->time;
	newState->saturation         = rc->saturation;
	newState->brightness         = rc->brightness;
	
	newState->value0             = rc->value0;
	newState->value1             = rc->value1;
	newState->value2             = rc->value2;
	newState->value3             = rc->value3;
	newState->value4             = rc->value4;
	
	rc->numPushedStates++;
}

void RcPopState()
{
	Assert(rc->numPushedStates > 0);
	
	RenderContextState_t* oldState = &rc->pushedStates[0];
	
	if (rc->viewport != oldState->viewport) { RcSetViewport(oldState->viewport, false); }
	if (rc->depth != oldState->depth) { RcSetDepth(oldState->depth); }
	if (rc->boundShader != oldState->boundShader) { RcBindShader(oldState->boundShader); }
	if (rc->boundFrameBuffer != oldState->boundFrameBuffer) { RcBindFrameBuffer(oldState->boundFrameBuffer); }
	if (rc->boundBuffer != oldState->boundBuffer) { RcBindBuffer(oldState->boundBuffer); }
	
	if (rc->boundFont != oldState->boundFont) { RcBindFont(oldState->boundFont); }
	if (rc->fontSize != oldState->fontSize) { RcSetFontSize(oldState->fontSize); }
	if (rc->fontStyle != oldState->fontStyle) { RcSetFontStyle(oldState->fontStyle); }
	if (rc->fontAlignment != oldState->fontAlignment) { RcSetFontAlignment(oldState->fontAlignment); }
	
	if (rc->worldMatrix != oldState->worldMatrix) { RcSetWorldMatrix(oldState->worldMatrix); }
	if (rc->viewMatrix != oldState->viewMatrix) { RcSetViewMatrix(oldState->viewMatrix); }
	if (rc->projectionMatrix != oldState->projectionMatrix) { RcSetProjectionMatrix(oldState->projectionMatrix); }
	
	if (rc->boundTexture != oldState->boundTexture) { RcBindTexture(oldState->boundTexture); }
	if (rc->sourceRectangle != oldState->sourceRectangle) { RcSetSourceRectangle(oldState->sourceRectangle, false); }
	if (rc->maskRectangle != oldState->maskRectangle) { RcSetMaskRectangle(oldState->maskRectangle, false); }
	
	if (rc->boundAltTexture != oldState->boundAltTexture) { RcBindAltTexture(oldState->boundAltTexture); }
	if (rc->useAltTexture != oldState->useAltTexture) { RcSetAltTextureEnabled(oldState->useAltTexture); }
	if (rc->altSourceRectangle != oldState->altSourceRectangle) { RcSetAltSourceRectangle(oldState->altSourceRectangle, false); }
	if (rc->gradientEnabled != oldState->gradientEnabled) { RcSetGradientEnabled(oldState->gradientEnabled); }
	
	if (rc->primaryColor.value != oldState->primaryColor.value) { RcSetColor(oldState->primaryColor); }
	if (rc->secondaryColor.value != oldState->secondaryColor.value) { RcSetSecondaryColor(oldState->secondaryColor); }
	if (rc->replaceColor1.value != oldState->replaceColor1.value) { RcSetSecondaryColor(oldState->replaceColor1); }
	if (rc->replaceColor2.value != oldState->replaceColor2.value) { RcSetSecondaryColor(oldState->replaceColor2); }
	if (rc->replaceColor3.value != oldState->replaceColor3.value) { RcSetSecondaryColor(oldState->replaceColor3); }
	if (rc->replaceColor4.value != oldState->replaceColor4.value) { RcSetSecondaryColor(oldState->replaceColor4); }
	
	if (rc->circleRadius != oldState->circleRadius || rc->circleInnerRadius != oldState->circleInnerRadius) { RcSetCircleRadius(oldState->circleRadius, oldState->circleInnerRadius); }
	if (rc->vigRadius != oldState->vigRadius || rc->vigSmoothness != oldState->vigSmoothness) { if (oldState->vigRadius > 0) { RcEnableVignette(oldState->vigRadius, oldState->vigSmoothness); } else { RcDisableVignette(); } }
	
	if (rc->time != oldState->time) { RcSetTime(oldState->time); }
	if (rc->saturation != oldState->saturation) { RcSetSaturation(oldState->saturation); }
	if (rc->brightness != oldState->brightness) { RcSetBrightness(oldState->brightness); }
	
	if (rc->value0 != oldState->value0) { RcSetShaderValue0(oldState->value0); }
	if (rc->value1 != oldState->value1) { RcSetShaderValue1(oldState->value1); }
	if (rc->value2 != oldState->value2) { RcSetShaderValue2(oldState->value2); }
	if (rc->value3 != oldState->value3) { RcSetShaderValue3(oldState->value3); }
	if (rc->value4 != oldState->value4) { RcSetShaderValue4(oldState->value4); }
	
	for (u32 sIndex = 0; sIndex+1 < MAX_RENDER_CONTEXT_PUSHES; sIndex++)
	{
		memcpy(&rc->pushedStates[sIndex], &rc->pushedStates[sIndex+1], sizeof(RenderContextState_t));
	}
	rc->numPushedStates--;
}
