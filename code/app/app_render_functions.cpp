/*
File:   app_render_functions.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds all the Rc functions that help us render various sorts
	** of geometry to the screen using the RenderContext_t
*/

void RcDrawTexturedRec(rec rectangle, Color_t color)
{
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Scale2(rectangle.width, rectangle.height));       //Scale
	Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x, rectangle.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}
void RcDrawTexturedRec(rec rectangle, Color_t color, rec sourceRectangle)
{
	RcSetSourceRectangle(sourceRectangle);
	RcBindTexture(rc->boundTexture);
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Scale2(rectangle.width, rectangle.height));       //Scale
	Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x, rectangle.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	glDrawArrays(GL_TRIANGLES, 0, rc->squareBuffer.numVertices);
}

void RcDrawSpriteEx(const Texture_t* spritePntr, rec sourceRectangle, v2 topLeft, v2 origin, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	RcBindTexture(spritePntr);
	RcSetSourceRectangle(sourceRectangle);
	RcSetColor(color);
	v2 spriteSize = sourceRectangle.size * scale;
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Scale2(spriteSize.x, spriteSize.y));                          //Scale
	Mat4Transform(worldMatrix, Mat4Translate3(-origin.x * scale, -origin.y * scale, rc->depth)); //Center
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                                           //Rotation
	Mat4Transform(worldMatrix, Mat4Translate(topLeft));                                          //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}
void RcDrawSpriteOffset(const Texture_t* spritePntr, v2 topLeft, v2 origin, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	RcDrawSpriteEx(spritePntr, NewRec(Vec2_Zero, NewVec2(spritePntr->size)), topLeft, origin, color, scale, rotation);
}
void RcDrawSprite(const Texture_t* spritePntr, v2 topLeft, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	RcDrawSpriteEx(spritePntr, NewRec(Vec2_Zero, NewVec2(spritePntr->size)), topLeft, Vec2_Zero, color, scale, rotation);
}
void RcDrawSpriteCentered(const Texture_t* spritePntr, v2 center, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	RcDrawSpriteEx(spritePntr, NewRec(Vec2_Zero, NewVec2(spritePntr->size)), center, NewVec2(spritePntr->size)/2, color, scale, rotation);
}

void RcDrawRectangle(rec rectangle, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Scale2(rectangle.width, rectangle.height));       //Scale
	Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x, rectangle.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

void RcDrawRotatedRectangle(v2 center, v2 size, r32 rotation, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(-0.5f, -0.5f));                   //center
	Mat4Transform(worldMatrix, Mat4Scale2(size.width, size.height));            //scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                          //rotate
	Mat4Transform(worldMatrix, Mat4Translate3(center.x, center.y, rc->depth));  //position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

void RcDrawTriangle(v2 point, r32 rotation, r32 height, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	r32 triangleHeight = SqrtR32(3)/2;
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4RotateZ(Pi32/2.0f + rotation));                       //Rotation
	Mat4Transform(worldMatrix, Mat4Scale2(height/triangleHeight, height/triangleHeight)); //Scale
	Mat4Transform(worldMatrix, Mat4Translate3(point.x, point.y, rc->depth));              //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->equilTriangleBuffer);
	RcDrawBufferTriangles();
}

void RcDrawTriangleBordered(v2 point, r32 rotation, r32 height, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	v2 normDirVec = NewVec2(CosR32(rotation), SinR32(rotation));
	r32 pointDist = Vec2Length(NewVec2(borderWidth * (1 + CosR32(ToRadians(60))), borderWidth * SinR32(ToRadians(60))));
	RcDrawTriangle(point + normDirVec*pointDist, rotation, height + borderWidth + pointDist, borderColor);
	RcDrawTriangle(point, rotation, height, color);
}

void RcDrawBorderedRec(rec rectangle, Color_t backgroundColor, Color_t borderColor, r32 borderWidth = 1.0f)
{
	RcDrawRectangle(rectangle, backgroundColor);
	
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y, rectangle.width, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y, borderWidth, rectangle.height), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y + rectangle.height - borderWidth, rectangle.width, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + rectangle.width - borderWidth, rectangle.y, borderWidth, rectangle.height), borderColor);
}

void RcDrawBorderedRecClipped(rec rectangle, Color_t backgroundColor, Color_t borderColor, r32 borderWidth = 1.0f)
{
	RcDrawRectangle(RecDeflate(rectangle, borderWidth), backgroundColor);
	
	RcDrawRectangle(NewRec(rectangle.x + borderWidth, rectangle.y, rectangle.width - borderWidth*2, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x, rectangle.y + borderWidth, borderWidth, rectangle.height - borderWidth*2), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + borderWidth, rectangle.y + rectangle.height - borderWidth, rectangle.width - borderWidth*2, borderWidth), borderColor);
	RcDrawRectangle(NewRec(rectangle.x + rectangle.width - borderWidth, rectangle.y + borderWidth, borderWidth, rectangle.height - borderWidth*2), borderColor);
}

void RcDrawGradient(rec rectangle, Color_t color1, Color_t color2, Dir2_t direction)
{
	RcBindTexture(&rc->gradientTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color1);
	RcSetSecondaryColor(color2);
	RcSetGradientEnabled(true);
	
	mat4 worldMatrix = Mat4_Identity;
	switch (direction)
	{
		case Dir2_Right:
		default:
		{
			Mat4Transform(worldMatrix, Mat4Scale2(rectangle.width, rectangle.height));       //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x, rectangle.y, rc->depth)); //Position
		} break;
		
		case Dir2_Left:
		{
			Mat4Transform(worldMatrix, Mat4Scale2(-rectangle.width, rectangle.height));                        //Scale
			Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x + rectangle.width, rectangle.y, rc->depth)); //Position
		} break;
		
		case Dir2_Down:
		{
			Mat4Transform(worldMatrix, Mat4Scale2(rectangle.height, rectangle.width));                         //Scale
			Mat4Transform(worldMatrix, Mat4RotateZ(Pi32/2));                                                   //Rotation
			Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x + rectangle.width, rectangle.y, rc->depth)); //Position
		} break;
		
		case Dir2_Up:
		{	
			Mat4Transform(worldMatrix, Mat4Scale2(-rectangle.height, rectangle.width));                                           //Scale
			Mat4Transform(worldMatrix, Mat4RotateZ(Pi32/2));                                                                      //Rotation
			Mat4Transform(worldMatrix, Mat4Translate3(rectangle.x + rectangle.width, rectangle.y + rectangle.height, rc->depth)); //Position
		} break;
	};
	RcSetWorldMatrix(worldMatrix);
	
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
	
	RcSetGradientEnabled(false);
}

void RcDrawLine(v2 p1, v2 p2, r32 thickness, Color_t color)
{
	RcBindTexture(&rc->dotTexture);
	RcDefaultSourceRectangle();
	RcSetColor(color);
	r32 length = Vec2Length(p2 - p1);
	r32 rotation = AtanR32(p2.y - p1.y, p2.x - p1.x); 
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(0.0f, -0.5f));           //Centering
	Mat4Transform(worldMatrix, Mat4Scale2(length, thickness));         //Scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                 //Rotation
	Mat4Transform(worldMatrix, Mat4Translate3(p1.x, p1.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

void RcDrawCircle(v2 center, r32 radius, Color_t color)
{
	RcSetCircleRadius(1.0f, 0.0f);
	RcDrawRectangle(NewRec(center.x - radius, center.y - radius, radius*2, radius*2), color);
	RcSetCircleRadius(0.0f, 0.0f);
}

void RcDrawDonut(v2 center, r32 radius, r32 innerRadius, Color_t color)
{
	r32 realInnerRadius = ClampR32(innerRadius / radius, 0.0f, 1.0f);
	RcSetCircleRadius(1.0f, realInnerRadius);
	RcDrawRectangle(NewRec(center.x - radius, center.y - radius, radius*2, radius*2), color);
	RcSetCircleRadius(0.0f, 0.0f);
}

void RcDrawSheetFrame(const SpriteSheet_t* spriteSheet, v2i frame, rec drawRec, Color_t color, Dir2_t rotation = Dir2_Down)
{
	RcBindTexture(&spriteSheet->texture);
	rec sourceRec = NewRec(NewVec2(Vec2iMultiply(spriteSheet->frameSize, frame) + spriteSheet->padding), NewVec2(spriteSheet->innerFrameSize));
	RcSetSourceRectangle(sourceRec);
	RcSetColor(color);
	mat4 worldMatrix = Mat4_Identity;
	if (rotation == Dir2_Left)
	{
		Mat4Transform(worldMatrix, Mat4RotateZ(Pi32/2));                                             //Rotation
		Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height));                       //Scale
		Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x + drawRec.width, drawRec.y, rc->depth)); //Position
	}
	else if (rotation == Dir2_Right)
	{
		Mat4Transform(worldMatrix, Mat4RotateZ(-Pi32/2));                                             //Rotation
		Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height));                        //Scale
		Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x, drawRec.y + drawRec.height, rc->depth)); //Position
	}
	else if (rotation == Dir2_Up)
	{
		Mat4Transform(worldMatrix, Mat4RotateZ(Pi32));                                                                //Rotation
		Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height));                                        //Scale
		Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x + drawRec.width, drawRec.y + drawRec.height, rc->depth)); //Position
	}
	else
	{
		Mat4Transform(worldMatrix, Mat4Scale2(drawRec.width, drawRec.height));       //Scale
		Mat4Transform(worldMatrix, Mat4Translate3(drawRec.x, drawRec.y, rc->depth)); //Position
	}
	RcSetWorldMatrix(worldMatrix);
	RcBindBuffer(&rc->squareBuffer);
	RcDrawBufferTriangles();
}

void RcDrawBezierCurve3(v2 start, v2 control, v2 end, Color_t color, u32 numVerts, r32 thickness = 1.0f, bool drawCircles = false, bool roundedEnds = false)
{
	for (u32 vIndex = 0; vIndex < numVerts; vIndex++)
	{
		r32 t1 = (r32)vIndex * (1.0f/numVerts);
		r32 t2 = (r32)(vIndex+1) * (1.0f/numVerts);
		
		r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
		r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
		v2 pos1 = NewVec2(xPos1, yPos1);
		
		r32 xPos2 = ((1 - t2)*(1 - t2)*start.x) + (2*t2*(1 - t2)*control.x) + t2*t2*end.x;
		r32 yPos2 = ((1 - t2)*(1 - t2)*start.y) + (2*t2*(1 - t2)*control.y) + t2*t2*end.y;
		v2 pos2 = NewVec2(xPos2, yPos2);
		
		if (drawCircles)
		{
			if (vIndex == 0 && roundedEnds)
			{
				RcDrawCircle(pos1, thickness/2, color);
			}
			if (vIndex+1 < numVerts || roundedEnds)
			{
				RcDrawCircle(pos2, thickness/2, color);
			}
		}
		RcDrawLine(pos1, pos2, thickness, color);
	}
}

void RcDrawBezierArrow3(v2 start, v2 control, v2 end, u32 numVerts, r32 arrowSize, Color_t color)
{
	r32 t1 = (r32)(numVerts-1) * (1.0f/numVerts);
	r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
	r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
	v2 pos1 = NewVec2(xPos1, yPos1);
	v2 normal = Vec2Normalize(end - pos1);
	r32 endDirection = AtanR32(end.y - pos1.y, end.x - pos1.x);
	
	RcDrawTriangle(end + normal*(arrowSize/2), endDirection, arrowSize, color);
}

void RcDrawBezierArrowBordered3(v2 start, v2 control, v2 end, u32 numVerts, r32 arrowSize, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	r32 t1 = (r32)(numVerts-1) * (1.0f/numVerts);
	r32 xPos1 = ((1 - t1)*(1 - t1)*start.x) + (2*t1*(1 - t1)*control.x) + t1*t1*end.x;
	r32 yPos1 = ((1 - t1)*(1 - t1)*start.y) + (2*t1*(1 - t1)*control.y) + t1*t1*end.y;
	v2 pos1 = NewVec2(xPos1, yPos1);
	v2 normal = Vec2Normalize(end - pos1);
	r32 endDirection = AtanR32(end.y - pos1.y, end.x - pos1.x);
	
	RcDrawTriangleBordered(end + normal*(arrowSize/2), endDirection, arrowSize, color, borderColor, borderWidth);
}

void RcDrawBezierCurve4(v2 start, v2 control1, v2 control2, v2 end, Color_t color, u32 numVerts, r32 thickness = 1.0f, bool drawCircles = false, bool roundedEnds = false)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierCurve3(start, control1, midPoint, color, numVerts, thickness, drawCircles, roundedEnds);
	RcDrawBezierCurve3(midPoint, control2, end, color, numVerts, thickness, drawCircles, roundedEnds);
}

void RcDrawBezierArrow4(v2 start, v2 control1, v2 control2, v2 end, u32 numVerts, r32 arrowSize, Color_t color)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierArrow3(midPoint, control2, end, numVerts, arrowSize, color);
}

void RcDrawBezierArrowBordered4(v2 start, v2 control1, v2 control2, v2 end, u32 numVerts, r32 arrowSize, Color_t color, Color_t borderColor, r32 borderWidth = 1.0f)
{
	v2 midPoint = (control1 + control2) / 2;
	RcDrawBezierArrowBordered3(midPoint, control2, end, numVerts, arrowSize, color, borderColor, borderWidth);
}

void RcDrawLoadingBar(rec barRec, r32 fillPercent, Color_t fillColor, Color_t backgroundColor, bool drawPercentageString = true)
{
	RcDrawRectangle(barRec, backgroundColor);
	
	rec fillRec = barRec;
	fillRec.width *= fillPercent;
	if (fillPercent < 0) { fillRec.x += barRec.width; }
	RcDrawRectangle(fillRec, fillColor);
	rec divideRec = fillRec;
	divideRec.x += divideRec.width;
	divideRec.width = 1;
	RcDrawRectangle(divideRec, NewColor(Color_Black));
	
	char* percentString = ArenaPrint(TempArena, "%.0f%%", (r32)RoundR32(fillPercent*100));
	RcDrawNtString(percentString, barRec.topLeft + NewVec2(barRec.width+2, barRec.height), White, 1.0f);
}

void RcDrawConnectionArc(v2 p1, v2 p2, v2 arcUp, r32 arcCoefficient, u32 numVerts, r32 thickness, Color_t color)
{
	if (p1 == p2) { return; }
	v2 perp = Vec2PerpLeft(p2 - p1);
	if (Vec2Dot(perp, arcUp) < 0) { perp = -perp; }
	r32 arcLength = Vec2Length(p2 - p1);
	perp = perp / arcLength;
	r32 arcHeight = arcLength * arcCoefficient;
	RcDrawBezierCurve3(p1, p1 + (p2 - p1)/2 + perp*arcHeight, p2, color, numVerts, thickness, false, false);
}

void RcDrawVerticesEx(u32 numVertices, const Vertex_t* vertices, v2 position, v2 origin, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	if (numVertices < 3) { return; }
	Assert(numVertices <= RC_MAX_DYNAMIC_VERTICES);
	
	RcSetColor(color);
	
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(-origin.x, -origin.y));              //Centering
	Mat4Transform(worldMatrix, Mat4Scale2(scale, scale));                          //Scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                             //Rotation
	Mat4Transform(worldMatrix, Mat4Translate3(position.x, position.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	
	DynamicVertexBufferFill(&rc->dynamicBuffer, vertices, numVertices);
	RcBindBuffer(&rc->dynamicBuffer);
	RcDrawBufferTriangles(numVertices);
}
void RcDrawVertices(u32 numVertices, const Vertex_t* vertices)
{
	RcDrawVerticesEx(numVertices, vertices, Vec2_Zero, Vec2_Zero, White);
}

void RcDrawPolygonOutline(u32 numPolyVertices, const Vertex_t* polyVerts, v2 position, v2 origin, Color_t color, r32 lineThickness = 1.0f, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	Assert(polyVerts != nullptr);
	for (u32 vIndex = 0; vIndex < numPolyVertices; vIndex++)
	{
		const Vertex_t* vert1 = &polyVerts[vIndex];
		const Vertex_t* vert2 = &polyVerts[(vIndex+1) % numPolyVertices];
		v2 pos1 = Vec2Rotate(NewVec2(vert1->position.x, vert1->position.y) - origin, rotation) * scale + position;
		v2 pos2 = Vec2Rotate(NewVec2(vert2->position.x, vert2->position.y) - origin, rotation) * scale + position;
		RcDrawLine(pos1, pos2, lineThickness, ColorMultiply(NewColor(vert1->color), color));
	}
}

bool RcDrawConvexPolygonEx(u32 numPolyVertices, const Vertex_t* polyVerts, v2 position, v2 origin, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	if (numPolyVertices < 3)
	{
		RcDrawPolygonOutline(numPolyVertices, polyVerts, position, origin, color, 1.0f, scale, rotation);
		return false;
	}
	
	RcSetColor(color);
	
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(-origin.x, -origin.y));              //Centering
	Mat4Transform(worldMatrix, Mat4Scale2(scale, scale));                          //Scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                             //Rotation
	Mat4Transform(worldMatrix, Mat4Translate3(position.x, position.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	
	u32 numTriangles = numPolyVertices - 2; //fan triangulation produces n-2 triangles
	u32 numVertices = numTriangles*3;
	TempPushMark();
	Vertex_t* vertices = TempArray(Vertex_t, numVertices);
	for (u32 tIndex = 0; tIndex < numTriangles; tIndex++)
	{
		vertices[tIndex*3 + 0] = polyVerts[0];
		vertices[tIndex*3 + 1] = polyVerts[1 + tIndex];
		vertices[tIndex*3 + 2] = polyVerts[2 + tIndex];
	}
	RcDrawVerticesEx(numVertices, vertices, position, origin, color, scale, rotation);
	TempPopMark();
	return true;
}
bool RcDrawConvexPolygon(u32 numPolyVertices, const Vertex_t* polyVerts)
{
	return RcDrawConvexPolygonEx(numPolyVertices, polyVerts, Vec2_Zero, Vec2_Zero, White);
}

bool RcDrawPolygonEx(u32 numPolyVertices, const Vertex_t* polyVerts, v2 position, v2 origin, Color_t color, r32 scale = 1.0f, r32 rotation = 0.0f)
{
	if (numPolyVertices < 3)
	{
		RcDrawPolygonOutline(numPolyVertices, polyVerts, position, origin, color, 1.0f, scale, rotation);
		return false;
	}
	
	RcSetColor(color);
	
	mat4 worldMatrix = Mat4_Identity;
	Mat4Transform(worldMatrix, Mat4Translate2(-origin.x, -origin.y));              //Centering
	Mat4Transform(worldMatrix, Mat4Scale2(scale, scale));                          //Scale
	Mat4Transform(worldMatrix, Mat4RotateZ(rotation));                             //Rotation
	Mat4Transform(worldMatrix, Mat4Translate3(position.x, position.y, rc->depth)); //Position
	RcSetWorldMatrix(worldMatrix);
	
	TempPushMark();
	v2* polyPositions = TempArray(v2, numPolyVertices);
	for (u32 vIndex = 0; vIndex < numPolyVertices; vIndex++) { polyPositions[vIndex] = NewVec2(polyVerts[vIndex].position.x, polyVerts[vIndex].position.y); }
	u32 numIndices = 0;
	u32* indices = TraingulatePolygonEars(TempArena, numPolyVertices, polyPositions, &numIndices);
	if (indices == nullptr)
	{
		TempPopMark();
		RcDrawPolygonOutline(numPolyVertices, polyVerts, position, origin, color, 1.0f, scale, rotation);
		return false;
	}
	Vertex_t* vertices = TempArray(Vertex_t, numIndices);
	Assert(vertices != nullptr);
	for (u32 iIndex = 0; iIndex < numIndices; iIndex++)
	{
		Assert(indices[iIndex] < numPolyVertices);
		vertices[iIndex] = polyVerts[indices[iIndex]];
	}
	RcDrawVerticesEx(numIndices, vertices, position, origin, color, scale, rotation);
	TempPopMark();
	return true;
}
bool RcDrawPolygon(u32 numPolyVertices, const Vertex_t* polyVerts)
{
	return RcDrawPolygonEx(numPolyVertices, polyVerts, Vec2_Zero, Vec2_Zero, White);
}
