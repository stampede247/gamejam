/*
File:   app_input.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Holds helper functions that the game can use to decipher
	** keyboard, mouse, and controller input in various ways
*/

// +--------------------------------------------------------------+
// |                Pressed, Released, Down Macros                |
// +--------------------------------------------------------------+
#define ButtonPressedNoHandling(button)  ((appInput->buttons[button].isDown && appInput->buttons[button].transCount > 0) || appInput->buttons[button].transCount >= 2)
#define ButtonReleasedNoHandling(button) ((!appInput->buttons[button].isDown && appInput->buttons[button].transCount > 0) || appInput->buttons[button].transCount >= 2)
#define ButtonDownNoHandling(button)     (appInput->buttons[button].isDown)
#define ButtonRepeatedNoHandling(button) (appInput->buttons[button].repeatCount > 0 || ButtonPressed(button))

#define ButtonPressed(button)  (app->buttonHandled[button] == false && ButtonPressedNoHandling(button))
#define ButtonReleased(button) (app->buttonHandled[button] == false && ButtonReleasedNoHandling(button))
#define ButtonDown(button)     (app->buttonHandled[button] == false && ButtonDownNoHandling(button))
#define ButtonRepeated(button) (app->buttonHandled[button] == false && ButtonRepeatedNoHandling(button))

#define GamepadPressedNoHandling(padIndex, button)  ((appInput->gamepads[padIndex].buttons[button].isDown && appInput->gamepads[padIndex].buttons[button].transCount > 0) || appInput->gamepads[padIndex].buttons[button].transCount >= 2)
#define GamepadReleasedNoHandling(padIndex, button) ((!appInput->gamepads[padIndex].buttons[button].isDown && appInput->gamepads[padIndex].buttons[button].transCount > 0) || appInput->gamepads[padIndex].buttons[button].transCount >= 2)
#define GamepadDownNoHandling(padIndex, button)     (appInput->gamepads[padIndex].buttons[button].isDown)

#define GamepadPressed(padIndex, button)  (app->gamepadHandled[padIndex][button] == false && GamepadPressedNoHandling(padIndex, button))
#define GamepadReleased(padIndex, button) (app->gamepadHandled[padIndex][button] == false && GamepadReleasedNoHandling(padIndex, button))
#define GamepadDown(padIndex, button)     (app->gamepadHandled[padIndex][button] == false && GamepadDownNoHandling(padIndex, button))

#define MouseScrolledNoHandling() (appInput->scrollDelta != Vec2_Zero)
#define MouseScrolled()           (!app->mouseScrollHandled && MouseScrolledNoHandling())

#define MouseMovedNoHandling() (appInput->mouseMoved)
#define MouseMoved()           (!app->mouseMoveHandled && MouseMovedNoHandling())

#define IsMouseOverOther() (app->mouseOverOther)

#define ClickedOnRec(rectangle) (ButtonReleasedUnhandled(MouseButton_Left) && IsInsideRec(rectangle, RenderMousePos) && IsInsideRec(rectangle, RenderMouseStartPos))

// +--------------------------------------------------------------+
// |                           GameBtn                            |
// +--------------------------------------------------------------+
void HandleButton_(const char* funcName, Buttons_t button)
{
	Assert(button < Buttons_NumButtons);
	app->buttonHandled[button] = true;
	#if DEBUG
	app->buttonHandledFunc[button] = funcName;
	#endif
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_ButtonTransition && event->button == button)
		{
			app->inputEventHandled[eIndex] = true;
			#if DEBUG
			app->inputEventHandledFunc[eIndex] = funcName;
			#endif
		}
	}
}
void HandleGamepad_(const char* funcName, u32 padIndex, GamepadButtons_t button)
{
	Assert(padIndex < MAX_NUM_GAMEPADS);
	Assert(button < Gamepad_NumButtons);
	app->gamepadHandled[padIndex][button] = true;
	#if DEBUG
	app->gamepadHandledFunc[padIndex][button] = funcName;
	#endif
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_GamepadTransition && event->gamepadBtn == button && event->gamepadIndex == padIndex)
		{
			app->inputEventHandled[eIndex] = true;
			#if DEBUG
			app->inputEventHandledFunc[eIndex] = funcName;
			#endif
		}
	}
}
void HandleGamepadAll_(const char* funcName, GamepadButtons_t button)
{
	Assert(button < Gamepad_NumButtons);
	for (u32 padIndex = 0; padIndex < MAX_NUM_GAMEPADS; padIndex++)
	{
		app->gamepadHandled[padIndex][button] = true;
		#if DEBUG
		app->gamepadHandledFunc[padIndex][button] = funcName;
		#endif
	}
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_GamepadTransition && event->gamepadBtn == button)
		{
			app->inputEventHandled[eIndex] = true;
			#if DEBUG
			app->inputEventHandledFunc[eIndex] = funcName;
			#endif
		}
	}
}
void HandleMouseScroll_(const char* funcName)
{
	app->mouseScrollHandled = true;
	#if DEBUG
	app->mouseScrollHandledFunc = funcName;
	#endif
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_MouseScrolled)
		{
			app->inputEventHandled[eIndex] = true;
			#if DEBUG
			app->inputEventHandledFunc[eIndex] = funcName;
			#endif
		}
	}
}
void HandleMouseMove_(const char* funcName)
{
	app->mouseMoveHandled = true;
	#if DEBUG
	app->mouseMoveHandledFunc = funcName;
	#endif
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* event = &appInput->inputEvents[eIndex];
		if (event->type == InputEventType_MouseMoved)
		{
			app->inputEventHandled[eIndex] = true;
			#if DEBUG
			app->inputEventHandledFunc[eIndex] = funcName;
			#endif
		}
	}
}
void MouseOverNamed(const char* name)
{
	app->mouseOverOther = true;
	app->mouseOverOtherName = name;
}
bool IsMouseOver(const char* name)
{
	if (!app->mouseOverOther) { return false; }
	if (name == nullptr && app->mouseOverOtherName == nullptr) { return true; }
	if (name == nullptr || app->mouseOverOtherName == nullptr) { return false; }
	return (strcmp(name, app->mouseOverOtherName) == 0);
}

#define HandleButton(button)            HandleButton_(__func__, button)
#define HandleGamepad(padIndex, button) HandleGamepad_(__func__, padIndex, button)
#define HandleGamepadAll(button)        HandleGamepadAll_(__func__, button)
#define HandleMouseScroll()             HandleMouseScroll_(__func__)
#define HandleMouseMove()               HandleMouseMove_(__func__)
#define MouseOverMe()                   MouseOverNamed(__func__)

void AppInputClearHandled()
{
	app->mouseScrollHandled = false;
	app->mouseMoveHandled = false;
	#if DEBUG
	app->mouseScrollHandledFunc = nullptr;
	app->mouseMoveHandledFunc = nullptr;
	#endif
	app->mouseOverOther = false;
	app->mouseOverOtherName = nullptr;
	
	for (u32 eIndex = 0; eIndex < MAX_NUM_INPUT_EVENTS; eIndex++)
	{
		app->inputEventHandled[eIndex] = false;
		#if DEBUG
		app->inputEventHandledFunc[eIndex] = nullptr;
		#endif
	}
	
	for (u32 bIndex = 0; bIndex < Buttons_NumButtons; bIndex++)
	{
		app->buttonHandled[bIndex] = false;
		#if DEBUG
		app->buttonHandledFunc[bIndex] = nullptr;
		#endif
	}
	for (u32 padIndex = 0; padIndex < MAX_NUM_GAMEPADS; padIndex++)
	{
		for (u32 bIndex = 0; bIndex < Gamepad_NumButtons; bIndex++)
		{
			app->gamepadHandled[padIndex][bIndex] = false;
			#if DEBUG
			app->gamepadHandledFunc[padIndex][bIndex] = nullptr;
			#endif
		}
	}
}

// +--------------------------------------------------------------+
// |                Sequential Oriented Functions                 |
// +--------------------------------------------------------------+
bool ButtonPressedSeq(Buttons_t button, u8 flags = 0x00, bool repeated = false, bool unhandled = true)
{
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* inputEvent = &appInput->inputEvents[eIndex];
		if (inputEvent->type == InputEventType_ButtonTransition && inputEvent->button == button && IsFlagSet(inputEvent->flags, BtnTransFlag_Pressed))
		{
			if ((flags == 0xFF || (inputEvent->flags & BtnTransFlag_Modifiers) == flags) && (repeated || !IsFlagSet(inputEvent->flags, BtnTransFlag_Repeat)) && (!unhandled || !app->inputEventHandled[eIndex]))
			{
				return true;
			}
		}
	}
	return false;
}
bool ButtonReleasedSeq(Buttons_t button, u8 flags = 0x00, bool unhandled = true)
{
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* inputEvent = &appInput->inputEvents[eIndex];
		if (inputEvent->type == InputEventType_ButtonTransition && inputEvent->button == button && !IsFlagSet(inputEvent->flags, BtnTransFlag_Pressed))
		{
			if ((flags == 0xFF || (inputEvent->flags & BtnTransFlag_Modifiers) == flags) && (!unhandled || !app->inputEventHandled[eIndex]))
			{
				return true;
			}
		}
	}
	return false;
}

const InputEvent_t* HandleButtonPressSeq(const char* funcName, Buttons_t button, u8 flags = 0x00, bool repeated = false)
{
	if (button < Buttons_NumButtons)
	{
		app->buttonHandled[button] = true;
		#if DEBUG
		app->buttonHandledFunc[button] = funcName;
		#endif
	}
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* inputEvent = &appInput->inputEvents[eIndex];
		if (inputEvent->type == InputEventType_ButtonTransition && inputEvent->button == button && IsFlagSet(inputEvent->flags, BtnTransFlag_Pressed))
		{
			if ((flags == 0xFF || (inputEvent->flags & BtnTransFlag_Modifiers) == flags) && (repeated || !IsFlagSet(inputEvent->flags, BtnTransFlag_Repeat)) && !app->inputEventHandled[eIndex])
			{
				app->inputEventHandled[eIndex] = true;
				#if DEBUG
				app->inputEventHandledFunc[eIndex] = funcName;
				#endif
				return inputEvent;
			}
		}
	}
	return nullptr;
}
const InputEvent_t* HandleButtonReleaseSeq(const char* funcName, Buttons_t button, u8 flags = 0x00)
{
	if (button < Buttons_NumButtons)
	{
		app->buttonHandled[button] = true;
		#if DEBUG
		app->buttonHandledFunc[button] = funcName;
		#endif
	}
	
	for (u32 eIndex = 0; eIndex < appInput->numInputEvents; eIndex++)
	{
		const InputEvent_t* inputEvent = &appInput->inputEvents[eIndex];
		if (inputEvent->type == InputEventType_ButtonTransition && inputEvent->button == button && !IsFlagSet(inputEvent->flags, BtnTransFlag_Pressed))
		{
			if ((flags == 0xFF || (inputEvent->flags & BtnTransFlag_Modifiers) == flags) && !app->inputEventHandled[eIndex])
			{
				app->inputEventHandled[eIndex] = true;
				#if DEBUG
				app->inputEventHandledFunc[eIndex] = funcName;
				#endif
				return inputEvent;
			}
		}
	}
	return nullptr;
}

// +--------------------------------------------------------------+
// |                      Focused UI Element                      |
// +--------------------------------------------------------------+
bool IsUiFocused(const void* object)
{
	return (app->focusPntr == object);
}

void SetUiFocus(const void* object)
{
	//TODO: Anything that needs to know it lost focus can be called here
	if (object != app->focusPntr)
	{
		// PrintLine_D("Focusing %p", object);
		app->focusPntr = object;
	}
}

// +--------------------------------------------------------------+
// |                       GameBtn Mapping                        |
// +--------------------------------------------------------------+
Buttons_t GetGameBtnKeyboard(GameBtn_t gameBtn, bool alternate = false)
{
	Assert(gameBtn < GameBtn_NumButtons);
	if (!alternate)
	{
		switch (gameBtn)
		{
			case GameBtn_Accept:     return Button_Z;
			case GameBtn_Cancel:     return Button_X;
			case GameBtn_Start:      return Button_Escape;
			case GameBtn_Right:      return Button_Right;
			case GameBtn_Left:       return Button_Left;
			case GameBtn_Up:         return Button_Up;
			case GameBtn_Down:       return Button_Down;
			case GameBtn_ViewRight:  return Button_D;
			case GameBtn_ViewLeft:   return Button_A;
			case GameBtn_ViewUp:     return Button_W;
			case GameBtn_ViewDown:   return Button_S;
			// case GameBtn_Options:    return Button_A;
			// case GameBtn_Sample:     return Button_A;
			// case GameBtn_RightTrig:  return Button_A;
			// case GameBtn_LeftTrig:   return Button_A;
			// case GameBtn_RightBmp:   return Button_A;
			// case GameBtn_LeftBmp:    return Button_A;
			// case GameBtn_Select:     return Button_A;
			// case GameBtn_Undo:       return Button_A;
			// case GameBtn_Redo:       return Button_A;
			default: return Buttons_NumButtons;
		}
	}
	else
	{
		switch (gameBtn)
		{
			// case GameBtn_Accept:    return Button_A;
			// case GameBtn_Cancel:    return Button_A;
			// case GameBtn_Options:   return Button_A;
			// case GameBtn_Sample:    return Button_A;
			// case GameBtn_RightTrig: return Button_A;
			// case GameBtn_LeftTrig:  return Button_A;
			// case GameBtn_RightBmp:  return Button_A;
			// case GameBtn_LeftBmp:   return Button_A;
			// case GameBtn_Start:     return Button_A;
			// case GameBtn_Select:    return Button_A;
			// case GameBtn_Undo:      return Button_A;
			// case GameBtn_Redo:      return Button_A;
			// case GameBtn_Down:      return Button_A;
			// case GameBtn_Up:        return Button_A;
			// case GameBtn_Left:      return Button_A;
			// case GameBtn_Right:     return Button_A;
			// case GameBtn_ViewDown:  return Button_A;
			// case GameBtn_ViewUp:    return Button_A;
			// case GameBtn_ViewLeft:  return Button_A;
			// case GameBtn_ViewRight: return Button_A;
			default: return Buttons_NumButtons;
		}
	}
}
GamepadButtons_t GetGameBtnGamepad(GameBtn_t gameBtn, bool alternate)
{
	Assert(gameBtn < GameBtn_NumButtons);
	if (!alternate)
	{
		switch (gameBtn)
		{
			case GameBtn_Accept:    return Gamepad_A;
			case GameBtn_Cancel:    return Gamepad_B;
			case GameBtn_Options:   return Gamepad_X;
			case GameBtn_Sample:    return Gamepad_Y;
			case GameBtn_RightTrig: return Gamepad_RightTrigger;
			case GameBtn_LeftTrig:  return Gamepad_LeftTrigger;
			case GameBtn_RightBmp:  return Gamepad_RightBumper;
			case GameBtn_LeftBmp:   return Gamepad_LeftBumper;
			case GameBtn_Start:     return Gamepad_Start;
			case GameBtn_Select:    return Gamepad_Back;
			case GameBtn_Undo:      return Gamepad_LeftStick;
			case GameBtn_Redo:      return Gamepad_RightStick;
			case GameBtn_Down:      return Gamepad_lsDown;
			case GameBtn_Up:        return Gamepad_lsUp;
			case GameBtn_Left:      return Gamepad_lsLeft;
			case GameBtn_Right:     return Gamepad_lsRight;
			case GameBtn_ViewDown:  return Gamepad_rsDown;
			case GameBtn_ViewUp:    return Gamepad_rsUp;
			case GameBtn_ViewLeft:  return Gamepad_rsLeft;
			case GameBtn_ViewRight: return Gamepad_rsRight;
			default: return Gamepad_NumButtons;
		}
	}
	else
	{
		switch (gameBtn)
		{
			// case GameBtn_Accept:    return Gamepad_A;
			// case GameBtn_Cancel:    return Gamepad_A;
			// case GameBtn_Options:   return Gamepad_A;
			// case GameBtn_Sample:    return Gamepad_A;
			// case GameBtn_RightTrig: return Gamepad_A;
			// case GameBtn_LeftTrig:  return Gamepad_A;
			// case GameBtn_RightBmp:  return Gamepad_A;
			// case GameBtn_LeftBmp:   return Gamepad_A;
			// case GameBtn_Start:     return Gamepad_A;
			// case GameBtn_Select:    return Gamepad_A;
			// case GameBtn_Undo:      return Gamepad_A;
			// case GameBtn_Redo:      return Gamepad_A;
			case GameBtn_Down:      return Gamepad_Down;
			case GameBtn_Up:        return Gamepad_Up;
			case GameBtn_Left:      return Gamepad_Left;
			case GameBtn_Right:     return Gamepad_Right;
			// case GameBtn_ViewDown:  return Gamepad_A;
			// case GameBtn_ViewUp:    return Gamepad_A;
			// case GameBtn_ViewLeft:  return Gamepad_A;
			// case GameBtn_ViewRight: return Gamepad_A;
			default: return Gamepad_NumButtons;
		}
	}
}
GameBtn_t GetGameBtnForButton(Buttons_t button, bool includeMain = true, bool includeAlternate = true)
{
	GameBtn_t result = GameBtn_NumButtons;
	for (u32 gIndex = 0; gIndex < GameBtn_NumButtons; gIndex++)
	{
		GameBtn_t gameBtn = (GameBtn_t)gIndex;
		Buttons_t keyboardBtn1 = includeMain ? GetGameBtnKeyboard(gameBtn, false) : Buttons_NumButtons;
		Buttons_t keyboardBtn2 = includeAlternate ? GetGameBtnKeyboard(gameBtn, true) : Buttons_NumButtons;
		if (keyboardBtn1 < Buttons_NumButtons && keyboardBtn1 == button) { result = gameBtn; break; }
		if (keyboardBtn2 < Buttons_NumButtons && keyboardBtn2 == button) { result = gameBtn; break; }
	}
	return result;
}
GameBtn_t GetGameBtnForGamepadBtn(GamepadButtons_t button, bool includeMain = true, bool includeAlternate = true)
{
	GameBtn_t result = GameBtn_NumButtons;
	for (u32 gIndex = 0; gIndex < GameBtn_NumButtons; gIndex++)
	{
		GameBtn_t gameBtn = (GameBtn_t)gIndex;
		
		GamepadButtons_t gamepadBtn1 = includeMain ? GetGameBtnGamepad(gameBtn, false) : Gamepad_NumButtons;
		GamepadButtons_t gamepadBtn2 = includeAlternate ? GetGameBtnGamepad(gameBtn, true) : Gamepad_NumButtons;
		if (gamepadBtn1 < Gamepad_NumButtons && gamepadBtn1 == button) { result = gameBtn; break; }
		if (gamepadBtn2 < Gamepad_NumButtons && gamepadBtn2 == button) { result = gameBtn; break; }
	}
	return result;
}
bool IsGameBtnModifierSensitive(GameBtn_t gameBtn)
{
	Assert(gameBtn < GameBtn_NumButtons);
	switch (gameBtn)
	{
		case GameBtn_Accept:    return true;
		case GameBtn_Cancel:    return true;
		case GameBtn_Options:   return true;
		case GameBtn_Sample:    return true;
		case GameBtn_RightTrig: return true;
		case GameBtn_LeftTrig:  return true;
		case GameBtn_RightBmp:  return true;
		case GameBtn_LeftBmp:   return true;
		// case GameBtn_Start:     return true;
		// case GameBtn_Select:    return true;
		case GameBtn_Undo:      return true;
		case GameBtn_Redo:      return true;
		// case GameBtn_Down:      return true;
		// case GameBtn_Up:        return true;
		// case GameBtn_Left:      return true;
		// case GameBtn_Right:     return true;
		case GameBtn_ViewDown:  return true;
		case GameBtn_ViewUp:    return true;
		case GameBtn_ViewLeft:  return true;
		case GameBtn_ViewRight: return true;
		default: return false;
	}
}

Dir2_t GetGameBtnDir2(GameBtn_t gameBtn)
{
	switch (gameBtn)
	{
		case GameBtn_Down:  return Dir2_Down;
		case GameBtn_Up:    return Dir2_Up;
		case GameBtn_Left:  return Dir2_Left;
		case GameBtn_Right: return Dir2_Right;
		default: return Dir2_None;
	}
}
GameBtn_t GetGameBtnByDir2(Dir2_t direction)
{
	switch (direction)
	{
		case Dir2_Down:  return GameBtn_Down;
		case Dir2_Up:    return GameBtn_Up;
		case Dir2_Left:  return GameBtn_Left;
		case Dir2_Right: return GameBtn_Right;
		default: return GameBtn_NumButtons;
	}
}

bool GameBtnPressed(GameBtn_t gameBtn, bool unhandled = true, u8 padIndex = 0xFF, bool allowRepeats = false)
{
	Assert(gameBtn < GameBtn_NumButtons);
	Assert(padIndex == 0xFF || padIndex < MAX_NUM_GAMEPADS);
	
	if (padIndex == 0 || padIndex == 0xFF)
	{
		Buttons_t keyboardBtn1 = GetGameBtnKeyboard(gameBtn, false);
		Buttons_t keyboardBtn2 = GetGameBtnKeyboard(gameBtn, true);
		if (!IsGameBtnModifierSensitive(gameBtn) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
		{
			if (keyboardBtn1 != Buttons_NumButtons && (ButtonPressed(keyboardBtn1) || (allowRepeats && ButtonRepeated(keyboardBtn1))) && (!unhandled || app->buttonHandled[keyboardBtn1] == false))
			{
				return true;
			}
			if (keyboardBtn2 != Buttons_NumButtons && (ButtonPressed(keyboardBtn2) || (allowRepeats && ButtonRepeated(keyboardBtn2))) && (!unhandled || app->buttonHandled[keyboardBtn2] == false))
			{
				return true;
			}
		}
	}
	
	GamepadButtons_t gamepadBtn1 = GetGameBtnGamepad(gameBtn, false);
	GamepadButtons_t gamepadBtn2 = GetGameBtnGamepad(gameBtn, true);
	if (padIndex == 0xFF)
	{
		for (u8 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
		{
			if (appInput->gamepads[pIndex].isConnected)
			{
				if (gamepadBtn1 != Gamepad_NumButtons && GamepadPressed(pIndex, gamepadBtn1) && (!unhandled || app->gamepadHandled[pIndex][gamepadBtn1] == false))
				{
					return true;
				}
				if (gamepadBtn2 != Gamepad_NumButtons && GamepadPressed(pIndex, gamepadBtn2) && (!unhandled || app->gamepadHandled[pIndex][gamepadBtn2] == false))
				{
					return true;
				}
			}
		}
	}
	else
	{
		if (appInput->gamepads[padIndex].isConnected)
		{
			if (gamepadBtn1 != Gamepad_NumButtons && GamepadPressed(padIndex, gamepadBtn1) && (!unhandled || app->gamepadHandled[padIndex][gamepadBtn1] == false))
			{
				return true;
			}
			if (gamepadBtn2 != Gamepad_NumButtons && GamepadPressed(padIndex, gamepadBtn2) && (!unhandled || app->gamepadHandled[padIndex][gamepadBtn2] == false))
			{
				return true;
			}
		}
	}
	
	return false;
}

bool GameBtnReleased(GameBtn_t gameBtn, bool unhandled = true, u8 padIndex = 0xFF)
{
	Assert(gameBtn < GameBtn_NumButtons);
	Assert(padIndex == 0xFF || padIndex < MAX_NUM_GAMEPADS);
	
	if (padIndex == 0 || padIndex == 0xFF)
	{
		Buttons_t keyboardBtn1 = GetGameBtnKeyboard(gameBtn, false);
		Buttons_t keyboardBtn2 = GetGameBtnKeyboard(gameBtn, true);
		if (!IsGameBtnModifierSensitive(gameBtn) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
		{
			if (keyboardBtn1 != Buttons_NumButtons && ButtonReleased(keyboardBtn1) && (!unhandled || app->buttonHandled[keyboardBtn1] == false))
			{
				return true;
			}
			if (keyboardBtn2 != Buttons_NumButtons && ButtonReleased(keyboardBtn2) && (!unhandled || app->buttonHandled[keyboardBtn2] == false))
			{
				return true;
			}
		}
	}
	
	GamepadButtons_t gamepadBtn1 = GetGameBtnGamepad(gameBtn, false);
	GamepadButtons_t gamepadBtn2 = GetGameBtnGamepad(gameBtn, true);
	if (padIndex == 0xFF)
	{
		for (u8 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
		{
			if (appInput->gamepads[pIndex].isConnected)
			{
				if (gamepadBtn1 != Gamepad_NumButtons && GamepadReleased(pIndex, gamepadBtn1) && (!unhandled || app->gamepadHandled[pIndex][gamepadBtn1] == false))
				{
					return true;
				}
				if (gamepadBtn2 != Gamepad_NumButtons && GamepadReleased(pIndex, gamepadBtn2) && (!unhandled || app->gamepadHandled[pIndex][gamepadBtn2] == false))
				{
					return true;
				}
			}
		}
	}
	else
	{
		if (appInput->gamepads[padIndex].isConnected)
		{
			if (gamepadBtn1 != Gamepad_NumButtons && GamepadReleased(padIndex, gamepadBtn1) && (!unhandled || app->gamepadHandled[padIndex][gamepadBtn1] == false))
			{
				return true;
			}
			if (gamepadBtn2 != Gamepad_NumButtons && GamepadReleased(padIndex, gamepadBtn2) && (!unhandled || app->gamepadHandled[padIndex][gamepadBtn2] == false))
			{
				return true;
			}
		}
	}
	
	return false;
}

bool GameBtnDown(GameBtn_t gameBtn, bool unhandled = true, u8 padIndex = 0xFF)
{
	Assert(gameBtn < GameBtn_NumButtons);
	Assert(padIndex == 0xFF || padIndex < MAX_NUM_GAMEPADS);
	
	if (padIndex == 0 || padIndex == 0xFF)
	{
		Buttons_t keyboardBtn1 = GetGameBtnKeyboard(gameBtn, false);
		Buttons_t keyboardBtn2 = GetGameBtnKeyboard(gameBtn, true);
		if (!IsGameBtnModifierSensitive(gameBtn) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
		{
			if (keyboardBtn1 != Buttons_NumButtons && ButtonDown(keyboardBtn1) && (!unhandled || app->buttonHandled[keyboardBtn1] == false))
			{
				return true;
			}
			if (keyboardBtn2 != Buttons_NumButtons && ButtonDown(keyboardBtn2) && (!unhandled || app->buttonHandled[keyboardBtn2] == false))
			{
				return true;
			}
		}
	}
	
	GamepadButtons_t gamepadBtn1 = GetGameBtnGamepad(gameBtn, false);
	GamepadButtons_t gamepadBtn2 = GetGameBtnGamepad(gameBtn, true);
	if (padIndex == 0xFF)
	{
		for (u8 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
		{
			if (appInput->gamepads[pIndex].isConnected)
			{
				if (gamepadBtn1 != Gamepad_NumButtons && GamepadDown(pIndex, gamepadBtn1) && (!unhandled || app->gamepadHandled[pIndex][gamepadBtn1] == false))
				{
					return true;
				}
				if (gamepadBtn2 != Gamepad_NumButtons && GamepadDown(pIndex, gamepadBtn2) && (!unhandled || app->gamepadHandled[pIndex][gamepadBtn2] == false))
				{
					return true;
				}
			}
		}
	}
	else
	{
		if (appInput->gamepads[padIndex].isConnected)
		{
			if (gamepadBtn1 != Gamepad_NumButtons && GamepadDown(padIndex, gamepadBtn1) && (!unhandled || app->gamepadHandled[padIndex][gamepadBtn1] == false))
			{
				return true;
			}
			if (gamepadBtn2 != Gamepad_NumButtons && GamepadDown(padIndex, gamepadBtn2) && (!unhandled || app->gamepadHandled[padIndex][gamepadBtn2] == false))
			{
				return true;
			}
		}
	}
	
	return false;
}

void HandleGameBtn(GameBtn_t gameBtn, u8 padIndex = 0xFF)
{
	Assert(gameBtn < GameBtn_NumButtons);
	Assert(padIndex == 0xFF || padIndex < MAX_NUM_GAMEPADS);
	
	if (padIndex == 0 || padIndex == 0xFF)
	{
		Buttons_t keyboardBtn1 = GetGameBtnKeyboard(gameBtn, false);
		Buttons_t keyboardBtn2 = GetGameBtnKeyboard(gameBtn, true);
		if (keyboardBtn1 != Buttons_NumButtons) { HandleButton(keyboardBtn1); }
		if (keyboardBtn2 != Buttons_NumButtons) { HandleButton(keyboardBtn2); }
	}
	
	GamepadButtons_t gamepadBtn1 = GetGameBtnGamepad(gameBtn, false);
	GamepadButtons_t gamepadBtn2 = GetGameBtnGamepad(gameBtn, true);
	if (padIndex == 0xFF)
	{
		for (u8 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
		{
			if (appInput->gamepads[pIndex].isConnected)
			{
				if (gamepadBtn1 != Gamepad_NumButtons) { HandleGamepad(pIndex, gamepadBtn1); }
				if (gamepadBtn2 != Gamepad_NumButtons) { HandleGamepad(pIndex, gamepadBtn2); }
			}
		}
	}
	else
	{
		if (appInput->gamepads[padIndex].isConnected)
		{
			if (gamepadBtn1 != Gamepad_NumButtons) { HandleGamepad(padIndex, gamepadBtn1); }
			if (gamepadBtn2 != Gamepad_NumButtons) { HandleGamepad(padIndex, gamepadBtn2); }
		}
	}
}

v2 GetGameBtnVec(bool rightStick, bool normalize = false, bool clampToOne = false, u8 padIndex = 0xFF)
{
	Assert(padIndex == 0xFF || padIndex < MAX_NUM_GAMEPADS);
	v2 result = Vec2_Zero;
	
	v2 keyboardVec = Vec2_Zero;
	if (padIndex == 0 || padIndex == 0xFF)
	{
		if (rightStick)
		{
			Buttons_t downBtn1  = GetGameBtnKeyboard(GameBtn_ViewDown,  false);
			Buttons_t downBtn2  = GetGameBtnKeyboard(GameBtn_ViewDown,  true);
			Buttons_t upBtn1    = GetGameBtnKeyboard(GameBtn_ViewUp,    false);
			Buttons_t upBtn2    = GetGameBtnKeyboard(GameBtn_ViewUp,    true);
			Buttons_t leftBtn1  = GetGameBtnKeyboard(GameBtn_ViewLeft,  false);
			Buttons_t leftBtn2  = GetGameBtnKeyboard(GameBtn_ViewLeft,  true);
			Buttons_t rightBtn1 = GetGameBtnKeyboard(GameBtn_ViewRight, false);
			Buttons_t rightBtn2 = GetGameBtnKeyboard(GameBtn_ViewRight, true);
			
			if (!IsGameBtnModifierSensitive(GameBtn_ViewDown) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (downBtn1  != Buttons_NumButtons && ButtonDown(downBtn1))  { keyboardVec.y += 1; }
				if (downBtn2  != Buttons_NumButtons && ButtonDown(downBtn2))  { keyboardVec.y += 1; }
			}
			if (!IsGameBtnModifierSensitive(GameBtn_ViewUp) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (upBtn1    != Buttons_NumButtons && ButtonDown(upBtn1))    { keyboardVec.y -= 1; }
				if (upBtn2    != Buttons_NumButtons && ButtonDown(upBtn2))    { keyboardVec.y -= 1; }
			}
			if (!IsGameBtnModifierSensitive(GameBtn_ViewLeft) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (leftBtn1  != Buttons_NumButtons && ButtonDown(leftBtn1))  { keyboardVec.x -= 1; }
				if (leftBtn2  != Buttons_NumButtons && ButtonDown(leftBtn2))  { keyboardVec.x -= 1; }
			}
			if (!IsGameBtnModifierSensitive(GameBtn_ViewRight) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (rightBtn1 != Buttons_NumButtons && ButtonDown(rightBtn1)) { keyboardVec.x += 1; }
				if (rightBtn2 != Buttons_NumButtons && ButtonDown(rightBtn2)) { keyboardVec.x += 1; }
			}
		}
		else
		{
			Buttons_t downBtn1  = GetGameBtnKeyboard(GameBtn_Down,  false);
			Buttons_t downBtn2  = GetGameBtnKeyboard(GameBtn_Down,  true);
			Buttons_t upBtn1    = GetGameBtnKeyboard(GameBtn_Up,    false);
			Buttons_t upBtn2    = GetGameBtnKeyboard(GameBtn_Up,    true);
			Buttons_t leftBtn1  = GetGameBtnKeyboard(GameBtn_Left,  false);
			Buttons_t leftBtn2  = GetGameBtnKeyboard(GameBtn_Left,  true);
			Buttons_t rightBtn1 = GetGameBtnKeyboard(GameBtn_Right, false);
			Buttons_t rightBtn2 = GetGameBtnKeyboard(GameBtn_Right, true);
			
			if (!IsGameBtnModifierSensitive(GameBtn_Down) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (downBtn1  != Buttons_NumButtons && ButtonDown(downBtn1))  { keyboardVec.y += 1; }
				if (downBtn2  != Buttons_NumButtons && ButtonDown(downBtn2))  { keyboardVec.y += 1; }
			}
			if (!IsGameBtnModifierSensitive(GameBtn_Up) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (upBtn1    != Buttons_NumButtons && ButtonDown(upBtn1))    { keyboardVec.y -= 1; }
				if (upBtn2    != Buttons_NumButtons && ButtonDown(upBtn2))    { keyboardVec.y -= 1; }
			}
			if (!IsGameBtnModifierSensitive(GameBtn_Left) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (leftBtn1  != Buttons_NumButtons && ButtonDown(leftBtn1))  { keyboardVec.x -= 1; }
				if (leftBtn2  != Buttons_NumButtons && ButtonDown(leftBtn2))  { keyboardVec.x -= 1; }
			}
			if (!IsGameBtnModifierSensitive(GameBtn_Right) || (!ButtonDown(Button_Control) && !ButtonDown(Button_Alt)))
			{
				if (rightBtn1 != Buttons_NumButtons && ButtonDown(rightBtn1)) { keyboardVec.x += 1; }
				if (rightBtn2 != Buttons_NumButtons && ButtonDown(rightBtn2)) { keyboardVec.x += 1; }
			}
		}
	}
	
	v2 controllerVec = Vec2_Zero;
	if (padIndex == 0xFF)
	{
		for (u8 pIndex = 0; pIndex < MAX_NUM_GAMEPADS; pIndex++)
		{
			if (appInput->gamepads[pIndex].isConnected)
			{
				if (rightStick)
				{
					controllerVec += NewVec2(appInput->gamepads[pIndex].rightStick.x, -appInput->gamepads[pIndex].rightStick.y);
				}
				else
				{
					controllerVec += NewVec2(appInput->gamepads[pIndex].leftStick.x, -appInput->gamepads[pIndex].leftStick.y);
				}
			}
		}
	}
	else
	{
		if (appInput->gamepads[padIndex].isConnected)
		{
			if (rightStick)
			{
				controllerVec = NewVec2(appInput->gamepads[padIndex].rightStick.x, -appInput->gamepads[padIndex].rightStick.y);
			}
			else
			{
				controllerVec = NewVec2(appInput->gamepads[padIndex].leftStick.x, -appInput->gamepads[padIndex].leftStick.y);
			}
			if (Vec2Length(controllerVec) < GAME_BTN_ANALOG_DEADZONE) { controllerVec = Vec2_Zero; }
		}
	}
	
	if (controllerVec != Vec2_Zero)
	{
		result = controllerVec;
	}
	else
	{
		result = keyboardVec;
	}
	
	//TODO: Implement dpadLike
	if (normalize)
	{
		result = Vec2Normalize(result);
	}
	else if (clampToOne && Vec2Length(result) > 1.0f)
	{
		result = Vec2Normalize(result);
	}
	return result;
}

v2i GetKeyboardButtonSheetFrame(Buttons_t button)
{
	switch (button)
	{
		case Button_Tilde:        return NewVec2i( 0, 27);
		case Button_1:            return NewVec2i( 1, 27);
		case Button_2:            return NewVec2i( 2, 27);
		case Button_3:            return NewVec2i( 3, 27);
		case Button_4:            return NewVec2i( 4, 27);
		case Button_5:            return NewVec2i( 5, 27);
		case Button_6:            return NewVec2i( 6, 27);
		case Button_7:            return NewVec2i( 7, 27);
		case Button_8:            return NewVec2i( 8, 27);
		case Button_9:            return NewVec2i( 9, 27);
		case Button_0:            return NewVec2i(10, 27);
		case Button_Minus:        return NewVec2i(11, 27);
		case Button_Plus:         return NewVec2i(12, 27);
		case Button_Backspace:    return NewVec2i(13, 27);
		
		case Button_Tab:          return NewVec2i( 1, 28);
		case Button_Q:            return NewVec2i( 1, 28);
		case Button_W:            return NewVec2i( 2, 28);
		case Button_E:            return NewVec2i( 3, 28);
		case Button_R:            return NewVec2i( 4, 28);
		case Button_T:            return NewVec2i( 5, 28);
		case Button_Y:            return NewVec2i( 6, 28);
		case Button_U:            return NewVec2i( 7, 28);
		case Button_I:            return NewVec2i( 8, 28);
		case Button_O:            return NewVec2i( 9, 28);
		case Button_P:            return NewVec2i(10, 28);
		case Button_OpenBracket:  return NewVec2i(11, 28);
		case Button_CloseBracket: return NewVec2i(12, 28);
		case Button_Pipe:         return NewVec2i(13, 28);
		
		case Button_CapsLock:     return NewVec2i( 0, 29);
		case Button_A:            return NewVec2i( 1, 29);
		case Button_S:            return NewVec2i( 2, 29);
		case Button_D:            return NewVec2i( 3, 29);
		case Button_F:            return NewVec2i( 4, 29);
		case Button_G:            return NewVec2i( 5, 29);
		case Button_H:            return NewVec2i( 6, 29);
		case Button_J:            return NewVec2i( 7, 29);
		case Button_K:            return NewVec2i( 8, 29);
		case Button_L:            return NewVec2i( 9, 29);
		case Button_Colon:        return NewVec2i(10, 29);
		case Button_Quote:        return NewVec2i(11, 29);
		case Button_Enter:        return NewVec2i(12, 29);
		
		case Button_Shift:        return NewVec2i( 0, 30);
		case Button_Z:            return NewVec2i( 2, 30);
		case Button_X:            return NewVec2i( 3, 30);
		case Button_C:            return NewVec2i( 4, 30);
		case Button_V:            return NewVec2i( 5, 30);
		case Button_B:            return NewVec2i( 6, 30);
		case Button_N:            return NewVec2i( 7, 30);
		case Button_M:            return NewVec2i( 8, 30);
		case Button_Comma:        return NewVec2i( 9, 30);
		case Button_Period:       return NewVec2i(10, 30);
		case Button_QuestionMark: return NewVec2i(11, 30);
		
		case Button_Control:      return NewVec2i( 0, 31);
		case Button_Alt:          return NewVec2i( 2, 31);
		case Button_Space:        return NewVec2i( 6, 31);
		
		case Button_Up:           return NewVec2i(14, 30);
		case Button_Left:         return NewVec2i(13, 31);
		case Button_Down:         return NewVec2i(14, 31);
		case Button_Right:        return NewVec2i(15, 31);
		
		case Button_Insert:       return NewVec2i(14, 27);
		case Button_Home:         return NewVec2i(15, 27);
		case Button_PageUp:       return NewVec2i(16, 27);
		case Button_Delete:       return NewVec2i(14, 28);
		case Button_End:          return NewVec2i(15, 28);
		case Button_PageDown:     return NewVec2i(16, 28);
		
		case Button_Num0:         return NewVec2i(17, 31);
		case Button_Num1:         return NewVec2i(17, 30);
		case Button_Num2:         return NewVec2i(18, 30);
		case Button_Num3:         return NewVec2i(19, 30);
		case Button_Num4:         return NewVec2i(17, 29);
		case Button_Num5:         return NewVec2i(18, 29);
		case Button_Num6:         return NewVec2i(19, 29);
		case Button_Num7:         return NewVec2i(17, 28);
		case Button_Num8:         return NewVec2i(18, 28);
		case Button_Num9:         return NewVec2i(19, 28);
		case Button_NumLock:      return NewVec2i(17, 27);
		case Button_NumDivide:    return NewVec2i(18, 27);
		case Button_NumMultiply:  return NewVec2i(19, 27);
		case Button_NumSubtract:  return NewVec2i(20, 27);
		case Button_NumAdd:       return NewVec2i(20, 28);
		case Button_NumPeriod:    return NewVec2i(19, 31);
		
		default: return NewVec2i(9, 31);
	}
}
