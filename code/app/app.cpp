/*
File:   app.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** This is the main compilation file used to make the application. Every other file that
	** is used by the application must be included from this file
	** This is also where we define the main exported functions for the application DLL
*/

#define RESET_APPLICATION              false
#define USE_ASSERT_FAILURE_FUNCTION    true
#define MEMORY_ARENA_META_INFO_ENABLED DEBUG
#define MAX_CRASH_DUMPS                4

#include "plat/plat_interface.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image_write.h"
#define STB_TRUETYPE_IMPLEMENTATION
#include "stb/stb_truetype.h"

#include "mylib/my_tempMemory.h"
#include "mylib/my_tempMemory.cpp"

#include "mylib/my_linkedList.h"
#include "mylib/my_boundedStrList.h"
#include "mylib/my_dynamicArray.h"
#include "mylib/my_triangulation.h"

// +--------------------------------------------------------------+
// |                  Platform Global Variables                   |
// +--------------------------------------------------------------+
const PlatformInfo_t* platform = nullptr;
const AppInput_t* appInput = nullptr;
AppOutput_t* appOutput = nullptr;

// +--------------------------------------------------------------+
// |                     Application Includes                     |
// +--------------------------------------------------------------+
#include "app/app_version.h"
#include "app/app_defines.h"
#if USE_BOX2D
#include <Box2D/Box2D.h>
#endif
#include "app/app_structs.h"
#include "app/app_font.h"
#include "app/app_ogg.h"
#include "app/app_input.h"
#include "app/app_palette.h"
#include "app/app_dynamic_color.h"
#include "app/app_resources.h"
#include "app/app_render_context.h"
#include "app/app_button.h"
#include "app/app_text_box.h"
#include "app/app_states.h"
#include "app/app_console.h"

// +==============================+
// |            Menus             |
// +==============================+
#include "settings_menu/settings_menu.h"

// +==============================+
// |          App States          |
// +==============================+
#include "game/game.h"
#include "main_menu/main_menu.h"
#include "mess/mess.h"

#include "app/app.h"

// +--------------------------------------------------------------+
// |                 Application Global Variables                 |
// +--------------------------------------------------------------+
AppData_t*          app      = nullptr;
MainMenuData_t*     mainMenu = nullptr;
GameData_t*         game     = nullptr;
MessData_t*         mess     = nullptr;
SettingsMenuData_t* settMenu = nullptr;

MemoryArena_t* mainHeap = nullptr;
MemoryArena_t* staticHeap = nullptr;
MemoryArena_t* platArena = nullptr;
AppResources_t* resources = nullptr;
RenderContext_t* rc = nullptr;
const ColorPalette_t* plt = nullptr;
rec RenderArea = {};
v2 RenderMousePos = {};
v2 RenderMouseStartLeft = {};
v2 RenderMouseStartRight = {};
u64 ProgramTime = 0;
r32 ElapsedMs = 0;
RealTime_t SystemTime = {};
RealTime_t LocalTime = {};

// +--------------------------------------------------------------+
// |                      Box2D Source Files                      |
// +--------------------------------------------------------------+
#if USE_BOX2D
void* my_b2Alloc(i32 size);
void my_b2Free(void* mem);
void my_b2Log(const char* line);
#include <Box2D/Box2D.cpp> //must be included AFTER mylib/my_tempMemory.h
#endif //USE_BOX2D

// +--------------------------------------------------------------+
// |                   Application Source Files                   |
// +--------------------------------------------------------------+
#include "app/app_func_defs.h"
#include "app/app_debug.cpp"
#include "app/app_helpers.cpp"
#include "app/app_input.cpp"
#include "app/app_font.cpp"
#include "app/app_wav.cpp"
#include "app/app_ogg.cpp"
#include "app/app_loading_functions.cpp"
#include "app/app_palette.cpp"
#include "app/app_resources.cpp"
#include "app/app_sound.cpp"
#include "app/app_render_context.cpp"
#include "app/app_render_functions.cpp"
#include "app/app_font_flow.cpp"
#include "app/app_button.cpp"
#include "app/app_text_box.cpp"
#include "app/app_console.cpp"
#include "app/app_input_display.cpp"


// +==============================+
// |            Menus             |
// +==============================+
#include "settings_menu/settings_menu.cpp"

// +==============================+
// |          App States          |
// +==============================+
#include "game/game.cpp"
#include "main_menu/main_menu.cpp"
#include "mess/mess.cpp"

#include "app/app_states.cpp"
#include "app/app_functions.cpp"

#include "app/app_console_commands.cpp"
#include "app/app_console_command_parser.cpp"

// +--------------------------------------------------------------+
// |                       App Get Version                        |
// +--------------------------------------------------------------+
// Version_t App_GetVersion(bool* resetApplication)
EXPORT AppGetVersion_DEFINITION(App_GetVersion)
{
	Version_t version = {
		APP_VERSION_MAJOR,
		APP_VERSION_MINOR,
		APP_VERSION_BUILD,
	};
	
	if (resetApplication != nullptr)
	{
		*resetApplication = RESET_APPLICATION;
	}
	
	return version;
}

// +--------------------------------------------------------------+
// |                   App Get Startup Options                    |
// +--------------------------------------------------------------+
// void App_GetStartupOptions(const StartupInfo_t* StartupInfo, StartupOptions_t* StartupOptions)
EXPORT AppGetStartupOptions_DEFINITION(App_GetStartupOptions)
{
	Assert_(StartupInfo != nullptr);
	Assert_(StartupOptions != nullptr);
	
	StartupOptions->windowSize             = NewVec2i(960, 540);
	StartupOptions->minWindowSize          = NewVec2i(200, 100);
	StartupOptions->maxWindowSize          = NewVec2i(-1, -1);
	StartupOptions->windowPosition         = NewVec2i(0, 0);
	StartupOptions->fullscreen             = false;
	StartupOptions->fullscreenMonitorIndex = StartupInfo->primaryMonitorIndex;
	StartupOptions->topmostWindow          = (false && DEBUG);
	StartupOptions->decoratedWindow        = true;
	StartupOptions->resizableWindow        = true;
	StartupOptions->forceAspectRatio       = false;
	StartupOptions->aspectRatio            = NewVec2i(16, 9);
	StartupOptions->antialiasingNumSamples = 8;
	StartupOptions->temporaryMemorySize    = Megabytes(16);
	StartupOptions->permanantMemorySize    = Megabytes(1);
	
	StartupOptions->loadingColor = VisBlueGray;
	BufferPrint(StartupOptions->iconFilePaths[0], SPRITES_FOLDER "icon16.png");
	BufferPrint(StartupOptions->iconFilePaths[1], SPRITES_FOLDER "icon24.png");
	BufferPrint(StartupOptions->iconFilePaths[2], SPRITES_FOLDER "icon32.png");
	BufferPrint(StartupOptions->iconFilePaths[3], SPRITES_FOLDER "icon64.png");
	BufferPrint(StartupOptions->loadingImagePath, SPRITES_FOLDER "loading.png");
}

// +--------------------------------------------------------------+
// |                        App Initialize                        |
// +--------------------------------------------------------------+
// void App_Initialize(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput)
EXPORT AppInitialize_DEFINITION(App_Initialize)
{
	AppEntryPoint(PlatformInfo, AppMemory, AppInput, nullptr);
	
	// +==================================+
	// |          Memory Arenas           |
	// +==================================+
	ClearPointer(app);
	u32 staticHeapSize = AppMemory->permanantSize - sizeof(AppData_t);
	InitializeMemoryArenaHeap(&app->staticHeap, app + 1, staticHeapSize);
	InitializeMemoryArenaExternal(&app->platArena, platform->AllocateMemory, platform->FreeMemory);
	InitializeMemoryArenaTemp(&app->tempArena, AppMemory->transientPntr, AppMemory->transientSize, TRANSIENT_MAX_NUMBER_MARKS);
	InitializeMemoryArenaGrowingHeap(&app->mainHeap, platform->AllocateMemory, GROWING_HEAP_PAGE_SIZE, 1, GROWING_HEAP_MAX_NUM_PAGES);
	MemoryArenaAddMetaArena(&app->mainHeap, &app->platArena);
	
	u32 dbgFifoSize = CSL_FIFO_SPACE_SIZE;
	void* dbgFifoSpace = ArenaPush(&app->mainHeap, dbgFifoSize);
	AppDebugOuputInit();
	DbgConsoleInit(&app->dbgConsole, dbgFifoSpace, dbgFifoSize);
	platform->UpdateLoadingBar(0.10f);
	
	// FillFunctionTable(&app->funcTable, &app->mainHeap);
	
	TempPushMark();
	{
		PrintLine_I("Initializing App v%u.%u(%u)...", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
		PrintLine_D("Static Heap:   %u bytes", staticHeapSize);
		PrintLine_D("Temp Arena:    %u bytes", AppMemory->transientSize);
		
		// +================================+
		// |    External Initializations    |
		// +================================+
		LoadColorPalette(&app->colorPalette, PALETTE_FILE_PATH);
		platform->WatchFile(PALETTE_FILE_PATH);
		InitializeRenderContext();
		WriteLine_I("Loading Resources...");
		LoadAllResources(true);
		platform->UpdateLoadingBar(0.80f);
		
		app->allowAppStateChanges = true;
		PushAppState(INITIAL_APP_STATE);
		app->allowAppStateChanges = false;
		
		app->mainFrameBuffer = CreateFrameBuffer(Vec2Roundi(RenderArea.size), false, true);
		app->secondaryBuffer = CreateFrameBuffer(Vec2Roundi(RenderArea.size), false, true);
		
		CreateKeyboardBtnRecs();
		CreateGamepadBtnRecs();
	}
	TempPopMark();
	
	//Test longer loading times manually
	// Sleep(1500);
	
	WriteLine_I("Initialization Done!");
	
	app->appInitTempHighWaterMark = ArenaGetHighWaterMark(TempArena);
	ArenaResetHighWaterMark(TempArena);
	app->initialized = true;
}

// +--------------------------------------------------------------+
// |                         App Reloaded                         |
// +--------------------------------------------------------------+
// void App_Reloaded(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppReloaded_DEFINITION(App_Reloaded)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr);
	
	WriteLine_W("App Reloaded");
	
	//TODO: Any function pointers that need to be remapped?
}

// +--------------------------------------------------------------+
// |                         File Changed                         |
// +--------------------------------------------------------------+
// void App_FileChanged(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const char* filePath)
EXPORT AppFileChanged_DEFINITION(App_FileChanged)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr);
	TempPushMark();
	
	u32 resourceIndex = 0;
	#if DEVELOPER
	if (strcmp(filePath, PALETTE_FILE_PATH) == 0)
	{
		WriteLine_I("Reloading color palette");
		LoadColorPalette(&app->colorPalette, PALETTE_FILE_PATH);
	}
	else if (IsFileResource(filePath, &resourceIndex))
	{
		ResourceType_t type = GetResourceType(resourceIndex);
		PrintLine_I("Reloading %s: \"%s\"", GetResourceTypeStr(type), GetFileNamePart(filePath));
		LoadResource(false, resourceIndex);
	}
	else
	{
		PrintLine_W("Unknown file changed: \"%s\"", filePath);
	}
	#else
	PrintLine_W("File changed in release mode? \"%s\"", filePath);
	#endif
	
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0);
}

// +--------------------------------------------------------------+
// |                          App Update                          |
// +--------------------------------------------------------------+
// void App_Update(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory, const AppInput_t* AppInput, AppOutput_t* AppOutput)
EXPORT AppUpdate_DEFINITION(App_Update)
{
	AppEntryPoint(PlatformInfo, AppMemory, AppInput, AppOutput);
	TempPushMark();
	appOutput->cursorType = Cursor_Default;
	AppInputClearHandled();
	Assert(app->initialized);
	
	CreateKeyboardBtnRecs();
	CreateGamepadBtnRecs();
	
	// +==============================+
	// | Clear Button Handled Arrays  |
	// +==============================+
	for (u32 bIndex = 0; bIndex < ArrayCount(app->buttonHandled); bIndex++)
	{
		app->buttonHandled[bIndex] = false;
	}
	for (u32 pIndex = 0; pIndex < ArrayCount(app->gamepadHandled); pIndex++)
	{
		for (u32 bIndex = 0; bIndex < ArrayCount(app->gamepadHandled[0]); bIndex++)
		{
			app->gamepadHandled[pIndex][bIndex] = false;
		}
	}
	
	// +==============================+
	// |     Update Debug Console     |
	// +==============================+
	DbgConsoleClaimMouse(&app->dbgConsole);
	DbgConsoleUpdateRecs(&app->dbgConsole);
	DbgConsoleUpdate(&app->dbgConsole);
	
	// // +==============================+
	// // |      Go to Mess Hotkey       |
	// // +==============================+
	// if (ButtonPressedUnhandled(Button_Tilde))
	// {
	// 	HandleButton(Button_Tilde);
	// 	PushAppState(AppState_Mess);
	// }
	// +==============================+
	// |     Toggle Debug Overlay     |
	// +==============================+
	if (ButtonPressed(Button_Pipe))
	{
		HandleButton(Button_Pipe);
		app->showDebugOverlay = !app->showDebugOverlay;
		PrintLine_D("%s Debug Overlay", app->showDebugOverlay ? "Showing" : "Hiding");
	}
	// +==============================+
	// |    Toggle Framerate Graph    |
	// +==============================+
	if (ButtonPressed(Button_F7))
	{
		HandleButton(Button_F7);
		if (ButtonDown(Button_Shift))
		{
			app->showMemoryUsage = !app->showMemoryUsage;
			PrintLine_D("%s Memory Usage", app->showMemoryUsage ? "Showing" : "Hiding");
		}
		else
		{
			app->showFramerateGraph = !app->showFramerateGraph;
			PrintLine_D("%s Framerate Graph", app->showFramerateGraph ? "Showing" : "Hiding");
		}
	}
	// +==============================+
	// |     Toggle Debug Console     |
	// +==============================+
	if (ButtonPressedSeq(Button_F6) || ButtonPressedSeq(Button_F6, BtnTransFlag_Shift))
	{
		const InputEvent_t* inputEvent = HandleButtonPressSeq("DebugConsole", Button_F6);
		if (inputEvent == nullptr) { inputEvent = HandleButtonPressSeq("DebugConsole", Button_F6, BtnTransFlag_Shift); }
		if (IsFlagSet(inputEvent->flags, BtnTransFlag_Shift))
		{
			if (!app->dbgConsole.isOpen || !app->dbgConsole.openLarge)
			{
				if (app->dbgConsole.isOpen) { app->dbgConsole.openAnimProgress /= 3; }
				app->dbgConsole.isOpen = true;
				app->dbgConsole.openLarge = true;
				
			}
			else { app->dbgConsole.isOpen = false; }
		}
		else
		{
			app->dbgConsole.isOpen = !app->dbgConsole.isOpen;
			if (app->dbgConsole.isOpen) { app->dbgConsole.openLarge = false; }
		}
		if (app->dbgConsole.isOpen)
		{
			app->dbgConsole.fullyOpaque = true;
		}
		else
		{
			if (IsUiFocused(&app->dbgConsole.inputBox)) { SetUiFocus(nullptr); }
		}
		
		// PrintLine_D("%s Debug Console%s", app->dbgConsole.isOpen ? "Showing" : "Hiding", app->dbgConsole.openLarge ? " (Large)" : "");
	}
	
	// +==============================+
	// |     Reload Sprite Sheets     |
	// +==============================+
	if (ButtonPressed(Button_F5) && !ButtonDown(Button_Alt))
	{
		// LoadNewMapAnims(); TODO: Revert back to just this once automatic file-reloading has been fixed
		PrintLine_I("Reloading %u resources", NUM_RESOURCES);
		// LoadAllResources(false);
		for (u32 rIndex = 0; rIndex < NUM_RESOURCES; rIndex++)
		{
			if (ButtonDown(Button_Shift))
			{
				// if (GetResourceType(rIndex) == ResourceType_Shader)
				// {
				// 	PrintLine_I("Reloading shader %u \"%s\"", rIndex, GetResourceFilePath(rIndex));
				// 	LoadResource(false, rIndex);
				// }
				if (GetResourceType(rIndex) == ResourceType_Sound)
				{
					PrintLine_I("Reloading sound %u \"%s\"", rIndex, GetResourceFilePath(rIndex));
					LoadResource(false, rIndex);
				}
				if (GetResourceType(rIndex) == ResourceType_Font)
				{
					PrintLine_I("Reloading font %u \"%s\"", rIndex, GetResourceFilePath(rIndex));
					LoadResource(false, rIndex);
				}
			}
			else
			{
				if (GetResourceType(rIndex) == ResourceType_SpriteSheet)
				{
					PrintLine_I("Reloading sheet %u \"%s\"", rIndex, GetResourceFilePath(rIndex));
					LoadResource(false, rIndex);
				}
				if (GetResourceType(rIndex) == ResourceType_Texture)
				{
					PrintLine_I("Reloading texture %u \"%s\"", rIndex, GetResourceFilePath(rIndex));
					LoadResource(false, rIndex);
				}
			}
		}
	}
	
	// +==============================+
	// |     Record Elapsed Time      |
	// +==============================+
	for (u32 eIndex = FRAMERATE_GRAPH_WIDTH-1; eIndex > 0; eIndex--)
	{
		app->elapsedTimesGraphValues[eIndex] = app->elapsedTimesGraphValues[eIndex-1];
		app->updateElapsedTimesGraphValues[eIndex] = app->updateElapsedTimesGraphValues[eIndex-1];
	}
	app->elapsedTimesGraphValues[0] = (r32)appInput->elapsedMs;
	app->updateElapsedTimesGraphValues[0] = (r32)appInput->lastUpdateElapsedMs;
	
	#if AUDIO_ENABLED
	UpdateSndInstances();
	#endif
	
	if (RenderArea.width > 0 && RenderArea.height > 0 && RenderArea.size != NewVec2(app->mainFrameBuffer.texture.size))
	{
		DestroyFrameBuffer(&app->mainFrameBuffer);
		DestroyFrameBuffer(&app->secondaryBuffer);
		app->mainFrameBuffer = CreateFrameBuffer(Vec2Roundi(RenderArea.size), false, true);
		app->secondaryBuffer = CreateFrameBuffer(Vec2Roundi(RenderArea.size), false, true);
	}
	
	rc->numBatchedGlyphs = 0;
	rc->numDrawnGlyphs = 0;
	
	// +==========================================+
	// | Update and Render AppStates and AppMenus |
	// +==========================================+
	{
		if (CurrentAppState() != AppState_None) { UpdateAppState(CurrentAppState(), CurrentAppMenu()); }
		if (CurrentAppMenu() != AppMenu_None) { UpdateAppMenu(CurrentAppMenu(), CurrentAppState(), AppMenu_None); }
		
		if (app->numActiveStates > 0)
		{
			u32 startAppState = 0;
			for (u32 sIndex = app->numActiveStates; sIndex > 0; sIndex--)
			{
				if (DoesAppStateCoverBelow(app->activeStates[sIndex-1], (sIndex >= 2) ? app->activeStates[sIndex-2] : AppState_None))
				{
					startAppState = sIndex-1;
					break;
				}
			}
			for (u32 sIndex = startAppState; sIndex < app->numActiveStates; sIndex++)
			{
				RenderAppState(app->activeStates[sIndex], &app->mainFrameBuffer, (sIndex+1 < app->numActiveStates) ? app->activeStates[sIndex+1] : AppState_None, CurrentAppMenu());
			}
		}
		
		for (u32 mIndex = 0; mIndex < app->numActiveMenus; mIndex++)
		{
			RenderAppMenu(app->activeMenus[mIndex], &app->mainFrameBuffer, CurrentAppState(), (mIndex+1 < app->numActiveMenus) ? app->activeMenus[mIndex+1] : AppMenu_None);
		}
		
		if (app->appStateChanged)
		{
			switch (app->appStateChange.type)
			{
				 case AppStateChangeType_Push:   PushAppState_(&app->appStateChange);   break;
				 case AppStateChangeType_Pop:    PopAppState_(&app->appStateChange);    break;
				 case AppStateChangeType_Change: ChangeAppState_(&app->appStateChange); break;
				 default: Assert(false); break;
			}
			app->appStateChanged = false;
		}
		if (app->appMenuChanged)
		{
			switch (app->appMenuChange.type)
			{
				 case AppStateChangeType_Push:   PushAppMenu_(&app->appMenuChange);   break;
				 case AppStateChangeType_Pop:    PopAppMenu_(&app->appMenuChange);    break;
				 case AppStateChangeType_Change: ChangeAppMenu_(&app->appMenuChange); break;
				 default: Assert(false); break;
			}
			app->appMenuChanged = false;
		}
	}
	
	RcBegin(&resources->mainShader, RenderArea, &resources->pixelFont, nullptr);
	RcClear(VisLightPurple, 1.0f);
	RcBindTexture(&app->mainFrameBuffer.texture);
	RcDrawTexturedRec(RenderArea, White);
	
	// +--------------------------------------------------------------+
	// |                  Render Debug Overlay Items                  |
	// +--------------------------------------------------------------+
	{
		// +==============================+
		// |     Render Debug Console     |
		// +==============================+
		DbgConsoleRender(&app->dbgConsole);
		glDisable(GL_DEPTH_TEST);
		
		// +==============================+
		// |    Render Framerate Graph    |
		// +==============================+
		if (app->showFramerateGraph)
		{
			rec graphRec = NewRec(10, 10, FRAMERATE_GRAPH_WIDTH*5, 100);
			r32 maxElapsedTime = 25.0f;
			for (u32 eIndex = 0; eIndex < FRAMERATE_GRAPH_WIDTH; eIndex++)
			{
				r32 elapsedTime = app->elapsedTimesGraphValues[eIndex];
				r32 updateElapsedTime = app->updateElapsedTimesGraphValues[eIndex];
				if (elapsedTime+2 > maxElapsedTime) { maxElapsedTime = elapsedTime+2; }
				if (updateElapsedTime+2 > maxElapsedTime) { maxElapsedTime = updateElapsedTime+2; }
			}
			
			r32 vertScale = (graphRec.height / maxElapsedTime);
			u32 numVertTicks = (u32)MaxI32(0, FloorR32(maxElapsedTime)) / FRAMERATE_VERT_TICK_VAL;
			r32 vertTickOffset = FRAMERATE_VERT_TICK_VAL * vertScale;
			u32 numTicksToSkip = MaxU32(1, numVertTicks / 5);
			u32 numHorTicks = FRAMERATE_GRAPH_WIDTH;
			r32 horTickOffset = graphRec.width / numHorTicks;
			
			RcBindFont(&resources->pixelFont);
			RcDrawBorderedRec(graphRec, ColorTransparent(NewColor(Color_Black), 0.5f), VisWhite);
			
			r32 target1 = graphRec.y + graphRec.height - (1000.0f / 60.0f) * vertScale;
			if (target1 > graphRec.y)
			{
				RcDrawRectangle(NewRec(graphRec.x+1, target1, graphRec.width-2, 1), VisGreen);
			}
			
			r32 target2 = graphRec.y + graphRec.height - (1000.0f / 30.0f) * vertScale;
			if (target2 > graphRec.y)
			{
				RcDrawRectangle(NewRec(graphRec.x+1, target2, graphRec.width-2, 1), VisBlue);
			}
			
			for (u32 vIndex = 1; vIndex <= numVertTicks; vIndex += numTicksToSkip)
			{
				u32 msValue = vIndex * FRAMERATE_VERT_TICK_VAL;
				r32 yPos = graphRec.y + graphRec.height - (vertTickOffset * vIndex);
				rec tickRec = NewRec(graphRec.x + graphRec.width - 2, yPos, 2, 1);
				v2 textPos = NewVec2(tickRec.x + tickRec.width + 1, tickRec.y + RcLineHeight()/2 - RcMaxExtendDown());
				RcDrawRectangle(tickRec, VisWhite);
				RcPrintString(textPos, VisWhite, "%u", msValue);
			}
			for (u32 hIndex = 1; hIndex < numHorTicks; hIndex+=2)
			{
				u32 tValue = hIndex;
				r32 xPos = graphRec.x + horTickOffset*hIndex;
				rec tickRec = NewRec(xPos, graphRec.y + graphRec.height - 2, 1, 2);
				RcDrawRectangle(tickRec, VisWhite);
			}
			
			v2 lastPos = Vec2_Zero;
			v2 lastUpdatePos = Vec2_Zero;
			for (u32 eIndex = 0; eIndex < FRAMERATE_GRAPH_WIDTH; eIndex++)
			{
				r32 elapsedTime = app->elapsedTimesGraphValues[eIndex];
				r32 xPos = graphRec.x + (graphRec.width / FRAMERATE_GRAPH_WIDTH) * eIndex;
				r32 yPos = graphRec.y + graphRec.height - (vertScale * elapsedTime);
				v2 newPos = NewVec2(xPos, yPos);
				
				r32 updateElapsedTime = app->updateElapsedTimesGraphValues[eIndex];
				xPos = graphRec.x + (graphRec.width / FRAMERATE_GRAPH_WIDTH) * eIndex;
				yPos = graphRec.y + graphRec.height - (vertScale * updateElapsedTime);
				v2 newUpdatePos = NewVec2(xPos, yPos);
				
				if (eIndex > 0)
				{
					RcDrawLine(lastPos, newPos, 1.0f, VisRed);
					RcDrawLine(lastUpdatePos, newUpdatePos, 1.0f, VisYellow);
				}
				lastPos = newPos;
				lastUpdatePos = newUpdatePos;
			}
		}
		
		// +==============================+
		// |     Render Memory Usage      |
		// +==============================+
		if (app->showMemoryUsage)
		{
			MemoryArena_t* arenas[3] = { mainHeap, &app->staticHeap, &app->tempArena };
			const char* arenaNames[3] = { "Main", "Static", "Temp" };
			
			v2 drawPos = NewVec2(5, 200);
			v2 drawSize = NewVec2(25, RenderArea.height-5 - drawPos.y);
			for (u32 aIndex = 0; aIndex < ArrayCount(arenas); aIndex++)
			{
				MemoryArena_t* arena = arenas[aIndex];
				const char* arenaName = arenaNames[aIndex];
				
				RcBindFont(&resources->debugFont);
				r32 lineHeight = RcLineHeight();
				r32 maxExtendUp = RcMaxExtendUp();
				r32 maxExtendDown = RcMaxExtendDown();
				
				bool drawBasic = true;
				r32 basicFillPercent = (arena->size > 0) ? ((r32)arena->used / (r32)arena->size) : 0.0f;
				r32 subFillPercent = 0.0f;
				if (arena->type == MemoryArenaType_Temp)
				{
					u32 highWaterMark = ArenaGetHighWaterMark(arena);
					basicFillPercent = (r32)highWaterMark / (r32)arena->size;
					subFillPercent = (r32)app->appInitTempHighWaterMark / (r32)arena->size;
				}
				else if (arena->type == MemoryArenaType_GrowingHeap)
				{
					Assert(arena->base != nullptr);
					GrowingHeapHeader_t* mainHeader = (GrowingHeapHeader_t*)arena->base;
					GrowingHeapPageHeader_t* pageHeader = mainHeader->firstPage;
					v2 namePos = drawPos + NewVec2(0, -maxExtendDown - lineHeight*2);
					v2 percentStrPos = namePos + NewVec2(0, lineHeight);
					v2 pagesStrPos = namePos + NewVec2(0, lineHeight*2);
					
					u32 numPages = 0;
					bool drawName = false;
					while (pageHeader != nullptr)
					{
						r32 fillPercent = (r32)pageHeader->used / (r32)pageHeader->size;
						rec drawRec = NewRec(drawPos, drawSize);
						rec fillRec = drawRec;
						fillRec.height = (r32)FloorR32(fillRec.height * fillPercent);
						bool insideDrawRec = IsInsideRec(drawRec, RenderMousePos);
						bool insideFillRec = IsInsideRec(fillRec, RenderMousePos);
						Color_t backColor = VisBlue;
						Color_t fillColor = VisOrange;
						Color_t borderColor = VisWhite;
						if (insideDrawRec)
						{
							backColor = VisLightBlue;
							drawName = true;
							basicFillPercent = fillPercent;
						}
						if (insideFillRec) { fillColor = VisYellow; }
						
						RcDrawRectangle(drawRec, backColor);
						RcDrawRectangle(fillRec, fillColor);
						RcDrawBorderedRec(drawRec, Transparent, borderColor, 1);
						
						drawPos.x += drawRec.width + 5;
						numPages++;
						pageHeader = pageHeader->next;
					}
					
					if (drawName)
					{
						char* percentStr = TempPrint("%.1f%%", basicFillPercent*100);
						char* pagesStr = TempPrint("%u/%u", numPages, mainHeader->maxNumPages);
						// v2 RcDrawNtString(const char* nullTermString, v2 position, Color_t color, r32 maxWidth = 0, FontFlowInfo_t* flowInfo = nullptr, FontFlowExtra_t* flowExtra = nullptr)
						RcDrawNtString(percentStr, percentStrPos, VisWhite);
						RcDrawNtString(pagesStr, pagesStrPos, VisWhite);
						RcDrawNtString(arenaName, namePos, VisWhite);
					}
					
					drawBasic = false;	
				}
				
				if (drawBasic)
				{
					rec drawRec = NewRec(drawPos, drawSize);
					bool insideDrawRec = IsInsideRec(drawRec, RenderMousePos);
					Color_t backColor = VisBlue;
					Color_t borderColor = VisWhite;
					
					if (insideDrawRec) { backColor = VisLightBlue; }
					
					RcDrawRectangle(drawRec, backColor);
					if (subFillPercent > 0)
					{
						rec subFillRec = drawRec;
						subFillRec.height = (r32)FloorR32(subFillRec.height * subFillPercent);
						bool insideSubFillRec = IsInsideRec(subFillRec, RenderMousePos);
						Color_t subFillColor = VisPurple;
						if (insideSubFillRec) { subFillColor = VisLightPurple; }
						RcDrawRectangle(subFillRec, subFillColor);
					}
					if (basicFillPercent > 0)
					{
						rec fillRec = drawRec;
						fillRec.height = (r32)FloorR32(fillRec.height * basicFillPercent);
						bool insideFillRec = IsInsideRec(fillRec, RenderMousePos);
						Color_t fillColor = VisOrange;
						if (insideFillRec) { fillColor = VisYellow; }
						RcDrawRectangle(fillRec, fillColor);
					}
					RcDrawBorderedRec(drawRec, Transparent, borderColor, 1);
					
					if (insideDrawRec)
					{
						v2 namePos = drawRec.topLeft + NewVec2(0, -maxExtendDown - lineHeight*2);
						v2 percentStrPos = namePos + NewVec2(0, lineHeight);
						v2 sizeStrPos = namePos + NewVec2(0, lineHeight*2);
						if (arena->size > 0)
						{
							char* percentStr = TempPrint("%.1f%%", basicFillPercent*100);
							RcDrawNtString(percentStr, percentStrPos, VisWhite);
							RcDrawNtString(FormattedSizeStr(arena->size), sizeStrPos, VisWhite);
						}
						else { namePos.y += lineHeight*2; }
						RcDrawNtString(arenaName, namePos, VisWhite);
					}
					
					drawPos.x += drawRec.width + 5;
				}
			}
		}
		
		// +==============================+
		// |     Render Debug Overlay     |
		// +==============================+
		if (app->showDebugOverlay)
		{
			RcBindFont(&resources->debugFont);
			rec overlayRec = RecDeflate(RenderArea, 10);
			RcDrawBorderedRec(overlayRec, ColorTransparent(NewColor(Color_Black), 0.5f), VisWhite, 1);
			v2 textPos = overlayRec.topLeft + NewVec2(5, 5 + RcMaxExtendUp());
			
			RcPrintString(textPos, White, "Mouse Over: %s", app->mouseOverOtherName);
			textPos.y += RcLineHeight();
			RcPrintString(textPos, White, "Mouse Inside: %s", appInput->mouseInsideWindow ? "True" : "False");
			textPos.y += RcLineHeight();
			RcPrintString(textPos, White, "Num Drawn Glyphs: %u", rc->numDrawnGlyphs);
			textPos.y += RcLineHeight();
			RcPrintString(textPos, White, "Num Batched Glyphs: %u", rc->numBatchedGlyphs);
			textPos.y += RcLineHeight();
		}
		glEnable(GL_DEPTH_TEST);
	}
	
	// +==============================+
	// |     Update Window Title      |
	// +==============================+
	if (DEBUG)
	{
		BufferPrint(appOutput->windowTitle, "GAME_JAM_DEBUG");
	}
	else if (DEVELOPER)
	{
		BufferPrint(appOutput->windowTitle, "GAME_JAM_DEV v%u.%u(%03u)", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
	}
	else
	{
		BufferPrint(appOutput->windowTitle, "Game Jam");
	}
	
	// +==============================+
	// |  Temp Arena Update Loop Pop  |
	// +==============================+
	TempPopMark();
	Assert(ArenaNumMarks(TempArena) == 0);
	if (ButtonPressed(Button_T) && ButtonDown(Button_Control)) { ArenaResetHighWaterMark(TempArena); }
}

// +--------------------------------------------------------------+
// |                         App Closing                          |
// +--------------------------------------------------------------+
// void App_Closing(const PlatformInfo_t* PlatformInfo, const AppMemory_t* AppMemory)
EXPORT AppClosing_DEFINITION(App_Closing)
{
	AppEntryPoint(PlatformInfo, AppMemory, nullptr, nullptr);
	WriteLine_E("Application closing!");
	
	//TODO: Anything we need to do before closing?
}

#if (ASSERTIONS_ACTIVE && USE_ASSERT_FAILURE_FUNCTION)
//This function is declared in my_assert.h and needs to be implemented by us for a debug build to compile successfully
void AssertFailure(const char* function, const char* filename, int lineNumber, const char* expressionStr)
{
	u32 fileNameStart = 0;
	for (u32 cIndex = 0; filename[cIndex] != '\0'; cIndex++)
	{
		if (filename[cIndex] == '\\' || filename[cIndex] == '/')
		{
			fileNameStart = cIndex+1;
		}
	}
	
	#if CRASH_DUMP_ASSERTS
	if (platform != nullptr && platform->WriteEntireFile != nullptr && app != nullptr)
	{
		if (app->numCrashDumps < MAX_CRASH_DUMPS)
		{
			u32 crashDumpSize = 0;
			char* crashDump = nullptr;
			crashDump = DbgConsoleCrashDump(&app->dbgConsole, &crashDumpSize,
				function, &filename[fileNameStart], lineNumber, expressionStr,
				&SystemTime, ProgramTime
			);
			
			if (crashDump != nullptr)
			{
				char* crashFilename = MallocPrint("crash_%u-%u-%u_%u-%u-%u.txt", LocalTime.year, LocalTime.month+1, LocalTime.day+1, LocalTime.hour, LocalTime.minute, LocalTime.second);
				if (crashFilename == nullptr) { crashFilename = (char*)"crash.txt"; }
				platform->WriteEntireFile(crashFilename, crashDump, crashDumpSize);
				app->numCrashDumps++;
				PrintLine_E("Saved crash dump to \"%s\"", crashFilename);
				free(crashDump);
				free(crashFilename);
			}
			else
			{
				WriteLine_E("No crash dump generated");
			}
		}
	}
	else
	{
		WriteLine_E("platform not available for assertion failure!");
	}
	#else
	if (platform != nullptr)
	{
		platform->DebugPrint(__func__, __FILE__, __LINE__, DbgLevel_Error, true,
			"Assertion Failure! %s in \"%s\" line %d: (%s) is not true", function, &filename[fileNameStart], lineNumber, expressionStr
		);
	}
	#endif
}
#endif
