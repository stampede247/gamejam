/*
File:   game_physics.cpp
Author: Taylor Robbins
Date:   07\24\2019
Description: 
	** Holds functions that help us create and keep track of physics bodies in the world
*/

PhysicsObject_t* AllocatePhysicsObject()
{
	PhysicsObject_t* result = nullptr;
	for (u32 bIndex = 0; bIndex < game->physBodies.length; bIndex++)
	{
		PhysicsObject_t* physObject = DynArrayGet(&game->physBodies, PhysicsObject_t, bIndex);
		if (!IsFlagSet(physObject->flags, PhysFlag_InUse))
		{
			result = physObject;
			break;
		}
	}
	if (result == nullptr) { result = DynArrayAdd(&game->physBodies, PhysicsObject_t); }
	if (result != nullptr)
	{
		ClearPointer(result);
		FlagSet(result->flags, PhysFlag_InUse);
	}
	return result;
}

PhysicsObject_t* CreatePhysicsRectangle(rec bodyRec, Color_t color, bool fixed = false, r32 density = 1.0f, r32 friction = 0.3f)
{
	PhysicsObject_t* result = AllocatePhysicsObject();
	if (result == nullptr) { return nullptr; }
	
	FlagSet(result->flags, PhysFlag_Active);
	if (fixed) { FlagSet(result->flags, PhysFlag_Static); }
	result->color = color;
	FlagSet(result->flags, PhysFlag_Rectangular);
	result->size = bodyRec.size;
	
	b2BodyDef bodyDef;
	bodyDef.type = fixed ? b2_staticBody : b2_dynamicBody;
	bodyDef.position.Set(bodyRec.x + bodyRec.width/2, bodyRec.y + bodyRec.height/2);
	result->body = game->physWorld.CreateBody(&bodyDef);
	b2PolygonShape boxShape;
	boxShape.SetAsBox(bodyRec.width/2, bodyRec.height/2);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &boxShape;
	fixtureDef.density = fixed ? 0.0f : density;
	fixtureDef.friction = friction;
	result->body->CreateFixture(&fixtureDef);
	
	return result;
}

PhysicsObject_t* CreatePhysicsCircle(v2 center, r32 radius, Color_t color, bool fixed = false, r32 density = 1.0f, r32 friction = 0.3f)
{
	PhysicsObject_t* result = AllocatePhysicsObject();
	if (result == nullptr) { return nullptr; }
	
	FlagSet(result->flags, PhysFlag_Active);
	if (fixed) { FlagSet(result->flags, PhysFlag_Static); }
	result->color = color;
	
	b2BodyDef bodyDef;
	bodyDef.type = fixed ? b2_staticBody : b2_dynamicBody;
	bodyDef.position.Set(center.x, center.y);
	result->body = game->physWorld.CreateBody(&bodyDef);
	b2CircleShape circleShape = b2CircleShape();
	circleShape.m_radius = radius;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circleShape;
	fixtureDef.density = fixed ? 0.0f : density;
	fixtureDef.friction = friction;
	result->body->CreateFixture(&fixtureDef);
	
	return result;
}

PhysicsObject_t* CreatePhysicsCapsule(rec bodyRec, Color_t color, bool fixed = false, r32 density = 1.0f, r32 friction = 0.3f)
{
	PhysicsObject_t* result = AllocatePhysicsObject();
	if (result == nullptr) { return nullptr; }
	
	FlagSet(result->flags, PhysFlag_Active);
	if (fixed) { FlagSet(result->flags, PhysFlag_Static); }
	result->color = color;
	
	b2BodyDef bodyDef;
	bodyDef.type = fixed ? b2_staticBody : b2_dynamicBody;
	bodyDef.position.Set(bodyRec.x + bodyRec.width/2, bodyRec.y + bodyRec.height/2);
	result->body = game->physWorld.CreateBody(&bodyDef);
	{
		b2PolygonShape boxShape;
		boxShape.SetAsBox(bodyRec.width/2, bodyRec.height/2);
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &boxShape;
		fixtureDef.density = fixed ? 0.0f : density;
		fixtureDef.friction = friction;
		result->body->CreateFixture(&fixtureDef);
	}
	{
		b2CircleShape circleShape = b2CircleShape();
		circleShape.m_p = b2Vec2(0, -bodyRec.height/2);
		circleShape.m_radius = bodyRec.width/2;
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &circleShape;
		fixtureDef.density = fixed ? 0.0f : density;
		fixtureDef.friction = friction;
		result->body->CreateFixture(&fixtureDef);
	}
	{
		b2CircleShape circleShape = b2CircleShape();
		circleShape.m_p = b2Vec2(0, bodyRec.height/2);
		circleShape.m_radius = bodyRec.width/2;
		b2FixtureDef fixtureDef;
		fixtureDef.shape = &circleShape;
		fixtureDef.density = fixed ? 0.0f : density;
		fixtureDef.friction = friction;
		result->body->CreateFixture(&fixtureDef);
	}
	
	return result;
}

void DrawPhysicsObject(PhysicsObject_t* obj, r32 scale, v2 viewPos)
{
	v2 bodyPos = NewVec2(obj->body->GetPosition().x, obj->body->GetPosition().y);
	r32 bodyRotation = obj->body->GetAngle();
	
	if (IsFlagSet(obj->flags, PhysFlag_Rectangular))
	{
		RcDrawRotatedRectangle((bodyPos - viewPos) * scale, obj->size * scale, bodyRotation, obj->color);
	}
	else
	{
		b2Fixture* fixture = obj->body->GetFixtureList();
		while (fixture != nullptr)
		{
			b2Shape* shape = fixture->GetShape();
			b2Shape::Type shapeType = fixture->GetType();
			switch (shapeType)
			{
				case b2Shape::e_polygon:
				{
					b2PolygonShape* polyShape = (b2PolygonShape*)shape;
					v2 centroid = NewVec2(polyShape->m_centroid.x, polyShape->m_centroid.y);
					for (u32 vIndex = 0; (i32)vIndex < polyShape->m_count; vIndex++)
					{
						v2 vertex = NewVec2(polyShape->m_vertices[vIndex].x, polyShape->m_vertices[vIndex].y);
						v2 prevVertex = NewVec2(polyShape->m_vertices[polyShape->m_count-1].x, polyShape->m_vertices[polyShape->m_count-1].y);
						if (vIndex > 0) { prevVertex = NewVec2(polyShape->m_vertices[vIndex-1].x, polyShape->m_vertices[vIndex-1].y); }
						
						vertex = GameTransformPolyVertex(viewPos, scale, bodyPos, bodyRotation, centroid, vertex);
						prevVertex = GameTransformPolyVertex(viewPos, scale, bodyPos, bodyRotation, centroid, prevVertex);
						RcDrawLine(prevVertex, vertex, 2, obj->color);
					}
				} break;
				
				case b2Shape::e_circle:
				{
					b2CircleShape* circleShape = (b2CircleShape*)shape;
					r32 circleRadius = circleShape->m_radius;
					v2 circleCenter = NewVec2(circleShape->m_p.x, circleShape->m_p.y);
					circleCenter = Vec2Rotate(circleCenter, bodyRotation);
					RcDrawCircle((bodyPos + circleCenter - viewPos) * scale, circleRadius * scale, obj->color);
					RcDrawLine((bodyPos + circleCenter - viewPos) * scale, (bodyPos + circleCenter + NewVec2(CosR32(bodyRotation), SinR32(bodyRotation)) * circleRadius - viewPos) * scale, 1, ColorDarken(obj->color, 50));
				} break;
			}
			fixture = fixture->GetNext();
		}
	}
}
