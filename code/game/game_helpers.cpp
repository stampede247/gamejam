/*
File:   game_helpers.cpp
Author: Taylor Robbins
Date:   07\21\2019
Description: 
	** Holds some helper functions that are used various game_ files
*/

v2 GameTransformPolyVertex(v2 viewPos, r32 physScale, v2 bodyPos, r32 bodyRot, v2 centroid, v2 vertex)
{
	v2 result = vertex;
	result -= centroid;
	result = Vec2Rotate(result, bodyRot);
	result += bodyPos;
	result -= viewPos;
	result = result * physScale;
	return result;
}
