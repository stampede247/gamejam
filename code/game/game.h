/*
File:   game.h
Author: Taylor Robbins
Date:   07\14\2019
*/

#ifndef _GAME_H
#define _GAME_H

struct GameData_t
{
	bool initialized;
	
	b2World physWorld;
	DynArray_t physBodies;
};

#endif //  _GAME_H
