/*
File:   game.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** None 
*/

#include "game/game_helpers.cpp"
#include "game/game_physics.cpp"

// +--------------------------------------------------------------+
// |                       Initialize Game                        |
// +--------------------------------------------------------------+
void InitializeGame()
{
	Assert(game != nullptr);
	
	CreateDynamicArray(&game->physBodies, mainHeap, sizeof(PhysicsObject_t));
	
	b2Vec2 gravityVec(0, 9.8f);
	b2World* world = new (&game->physWorld) b2World(gravityVec);
	
	CreatePhysicsRectangle(NewRec(-50, 0, 100, 20), VisGreen, true);
	CreatePhysicsRectangle(NewRec(0, -4, 2, 2), VisPurple);
	
	game->initialized = true;
}

// +--------------------------------------------------------------+
// |                          Start Game                          |
// +--------------------------------------------------------------+
bool StartGame(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	Assert(game->initialized);
	
	
	
	return true;
}

// +--------------------------------------------------------------+
// |                      Deinitialize Game                       |
// +--------------------------------------------------------------+
void DeinitializeGame()
{
	Assert(game != nullptr);
	Assert(game->initialized == true);
	
	DestroyDynamicArray(&game->physBodies);
	game->physWorld.~b2World();
	
	game->initialized = false;
	ClearPointer(game);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesGameCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                         Game Update                          |
// +--------------------------------------------------------------+
void UpdateGame(AppMenu_t appMenuAbove)
{
	Assert(game != nullptr);
	Assert(game->initialized);
	
	bool mouseOverUI = !appInput->mouseInsideWindow;
	
	// +--------------------------------------------------------------+
	// |                         Update Game                          |
	// +--------------------------------------------------------------+
	if (appMenuAbove == AppMenu_None)
	{
		r32 physTimeStep = (1.0f / 60.0f) * (r32)(appInput->elapsedMs / (1000/60.0f)); //60Hz
		game->physWorld.Step(physTimeStep, 8, 3); //8 velocity iterations, 3 position iterations
		
		r32 physScale = 50;
		v2 viewPos = (-RenderArea.size/2) / physScale;
		v2 worldPos = (RenderMousePos / physScale) + viewPos;
		
		if (ButtonPressed(MouseButton_Left) && appInput->mouseInsideWindow)
		{
			HandleButton(MouseButton_Left);
			CreatePhysicsCapsule(NewRecCentered(worldPos, NewVec2(2, 1)), VisOrange);
		}
		
		if (ButtonPressed(MouseButton_Right) && appInput->mouseInsideWindow)
		{
			HandleButton(MouseButton_Right);
			CreatePhysicsCircle(worldPos, 1, VisYellow);
		}
	}
}

// +--------------------------------------------------------------+
// |                         Game Render                          |
// +--------------------------------------------------------------+
void RenderGame(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	Color_t color1 = ColorFromHSV((i32)(ProgramTime*180) % 360, 1.0f, 1.0f);
	Color_t color2 = ColorFromHSV((i32)(ProgramTime*180 + 125) % 360, 1.0f, 1.0f);
	r32 sinValue = (SinR32((ProgramTime/355.0f)*6.0f) + 1.0f) / 2.0f;
	
	RcBegin(&resources->pixelShader, RenderArea, &resources->pixelFont, renderBuffer);
	RcClear(VisBlueGray, 1.0f);
	RcSetReplaceColors(plt->target);
	
	// +==============================+
	// |       UI Render Setup        |
	// +==============================+
	RcClearDepthBuffer(1.0f);
	RcBindShader(&resources->mainShader);
	RcSetViewport(RenderArea);
	RcSetViewMatrix(Mat4_Identity);
	RcSetDepth(0.1f);
	
	r32 physScale = 50;
	v2 viewPos = (-RenderArea.size/2) / physScale;
	
	for (u32 bIndex = 0; bIndex < game->physBodies.length; bIndex++)
	{
		PhysicsObject_t* obj = DynArrayGet(&game->physBodies, PhysicsObject_t, bIndex);
		if (IsFlagSet(obj->flags, PhysFlag_InUse))
		{
			DrawPhysicsObject(obj, physScale, viewPos);
		}
	}
	
	// +================================+
	// |       Draw Debug Overlay       |
	// +================================+
	if (app->showDebugOverlay)
	{
		rec overlayRec = NewRec(10, 10, (r32)RenderArea.width - 10*2, (r32)RenderArea.height - 10*2);
		RcDrawBorderedRec(overlayRec, ColorTransparent({Color_Black}, 0.5f), ColorTransparent({Color_White}, 0.5f));
		
		v2 textPos = NewVec2(overlayRec.x + 5, overlayRec.y + 5 + RcMaxExtendUp());
		r32 lineHeight = RcLineHeight();
		Color_t textColor = White;
		RcBindFont(&resources->debugFont);
		
		// RcPrintString(textPos, {Color_White}, 1.0f, "AppData Size: %u/%u (%.3f%%)",
		// 	sizeof(AppData_t), AppMemory->permanantSize,
		// 	(r32)sizeof(AppData_t) / (r32)AppMemory->permanantSize * 100.0f);
		// textPos.y += resources->debugFont.lineHeight;
		
		RcPrintString(textPos, textColor, "Main Heap: %s/%s (%.3f%%)",
			FormattedSizeStr(app->mainHeap.used), FormattedSizeStr(app->mainHeap.size),
			(r32)app->mainHeap.used / (r32)app->mainHeap.size * 100.0f);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Temp Init: %s/%s (%.3f%%)",
			FormattedSizeStr(app->appInitTempHighWaterMark), FormattedSizeStr(TempArena->size),
			(r32)app->appInitTempHighWaterMark / (r32)TempArena->size * 100.0f);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Temp Update: %s/%s (%.3f%%) %u marks",
			FormattedSizeStr(ArenaGetHighWaterMark(TempArena)), FormattedSizeStr(TempArena->size),
			(r32)ArenaGetHighWaterMark(TempArena) / (r32)TempArena->size * 100.0f, ArenaNumMarks(TempArena));
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Program Time: %lu", ProgramTime);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Time Delta: %.2f", appInput->timeDelta);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "System Timestamp: %ld", appInput->systemTime.timestamp);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "System Time: %s", FormattedTimeStr(appInput->systemTime));
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Local Timestamp: %ld", appInput->localTime.timestamp);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Local Time: %s", FormattedTimeStr(appInput->localTime));
		textPos.y += lineHeight;
		
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Framerate: %.0lf", appInput->avgFramerate);
		textPos.y += lineHeight;
		
		RcPrintString(textPos, textColor, "Elapsed Time: %.1fms", appInput->avgElapsedMs);
		textPos.y += lineHeight;
		
		//Sound Instance List
		textPos = NewVec2(overlayRec.x + 5 + 500, overlayRec.y + 5 + RcMaxExtendUp());
		
		u32 numActiveInstances = 0;
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				numActiveInstances++;
			}
		}
		RcPrintString(textPos, textColor, "%u instance%s", numActiveInstances, (numActiveInstances == 1) ? "" : "s");
		textPos.y += lineHeight;
		for (u32 sIndex = 0; sIndex < MAX_SOUND_INSTANCES; sIndex++)
		{
			SoundInstance_t* instance = &app->soundInstances[sIndex];
			if (instance->playing)
			{
				const char* sndName = nullptr;
				for (u32 rIndex = 0; rIndex < NUM_RESOURCE_SOUNDS; rIndex++)
				{
					if (instance->soundId == resources->sounds[rIndex].id)
					{
						sndName = GetFileNamePart(GetSoundFilePath(rIndex));
						break;
					}
				}
				for (u32 rIndex = 0; rIndex < NUM_RESOURCE_MUSICS; rIndex++)
				{
					if (instance->soundId == resources->musics[rIndex].id)
					{
						sndName = GetFileNamePart(GetMusicFilePath(rIndex));
						break;
					}
				}
				if (sndName == nullptr) { sndName = TempPrint("%u", instance->soundId); }
				RcPrintString(textPos, textColor, "[%u] %s %s", sIndex, sndName, FormattedMilliseconds(TimeSince(instance->startTime)));
				textPos.y += lineHeight;
			}
		}
		
		RenderCurrentGamepadInputDisplay(RenderArea, ColorTransparent(VisWhite, 0.5f), ColorTransparent(VisWhite, 0.5f), ColorTransparent(0.0f), ColorTransparent(0.0f));
	}
}
