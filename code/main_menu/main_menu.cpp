/*
File:   main_menu.cpp
Author: Taylor Robbins
Date:   07\14\2019
Description: 
	** Handles the Main Menu (the start point for the game)
*/

// +--------------------------------------------------------------+
// |                     Initialize Main Menu                     |
// +--------------------------------------------------------------+
void InitializeMainMenu()
{
	Assert(mainMenu != nullptr);
	
	
	mainMenu->initialized = true;
}

// +--------------------------------------------------------------+
// |                       Start Main Menu                        |
// +--------------------------------------------------------------+
bool StartMainMenu(AppState_t oldAppState, TransInfoType_t transInfoType, void* transInfoPntr)
{
	
	return true;
}

// +--------------------------------------------------------------+
// |                    Deinitialize Main Menu                    |
// +--------------------------------------------------------------+
void DeinitializeMainMenu()
{
	Assert(mainMenu != nullptr);
	Assert(mainMenu->initialized == true);
	
	mainMenu->initialized = false;
	ClearPointer(mainMenu);
}

// +--------------------------------------------------------------+
// |                       Does Cover Below                       |
// +--------------------------------------------------------------+
bool DoesMainMenuCoverBelow(AppState_t belowState)
{
	return true;
}

// +--------------------------------------------------------------+
// |                       Update Main Menu                       |
// +--------------------------------------------------------------+
void UpdateMainMenu(AppMenu_t appMenuAbove)
{
	Assert(mainMenu != nullptr);
	Assert(mainMenu->initialized);
	
	bool appMenuOpen = (appMenuAbove != AppMenu_None);
	
	if (!appMenuOpen)
	{
		
	}
}

// +--------------------------------------------------------------+
// |                       Render Main Menu                       |
// +--------------------------------------------------------------+
void RenderMainMenu(FrameBuffer_t* renderBuffer, AppState_t appStateAbove, AppMenu_t appMenuAbove)
{
	// +==============================+
	// |         Render Setup         |
	// +==============================+
	{
		RcBegin(&resources->mainShader, RenderArea, &resources->pixelFont, renderBuffer);
		RcClear(VisBlueGray, 1.0f);
	}
	
	// +==============================+
	// |     Draw Version Number      |
	// +==============================+
	#if MM_SHOW_VERSION_NUMS
	{
		r32 textScale = 2.0f;
		char* appVersionStr = TempPrint("App v%u.%u(%u)", APP_VERSION_MAJOR, APP_VERSION_MINOR, APP_VERSION_BUILD);
		char* platVersionStr = TempPrint("Plat v%u.%u(%u)", platform->version.major, platform->version.minor, platform->version.build);
		r32 maxWidth = MaxR32(RcMeasureNtString(appVersionStr).width * textScale, RcMeasureNtString(platVersionStr).width * textScale);
		v2 textPos = NewVec2(RenderArea.width - 5 - maxWidth, 5 + RcMaxExtendUp() * textScale);
		r32 advanceY = RcLineHeight() * textScale + 5;
		Color_t textColor = VisWhite;
		Color_t embossColor = VisBlueGray;
		RcBindFont(&resources->pixelFont);
		
		RcDrawNtString(appVersionStr, textPos + NewVec2(0, textScale), embossColor, textScale);
		RcDrawNtString(appVersionStr, textPos, textColor, textScale);
		textPos.y += advanceY;
		
		RcDrawNtString(platVersionStr, textPos + NewVec2(0, textScale), embossColor, textScale);
		RcDrawNtString(platVersionStr, textPos, textColor, textScale);
		textPos.y += advanceY;
		
		if (app->showDebugOverlay)
		{
			#if DEBUG
			RcDrawNtString("DEBUG", textPos + NewVec2(0, textScale), embossColor, textScale);
			RcDrawNtString("DEBUG", textPos, textColor, textScale);
			textPos.y += advanceY;
			#endif
			
			#if DEVELOPER
			RcDrawNtString("DEVELOPER", textPos + NewVec2(0, textScale), embossColor, textScale);
			RcDrawNtString("DEVELOPER", textPos, textColor, textScale);
			textPos.y += advanceY;
			#endif
			
			#if AUDIO_ENABLED
			RcDrawNtString("AUDIO_ENABLED", textPos + NewVec2(0, textScale), embossColor, textScale);
			RcDrawNtString("AUDIO_ENABLED", textPos, textColor, textScale);
			textPos.y += advanceY;
			#endif
			
			#if ASSERTIONS_ACTIVE
			RcDrawNtString("ASSERTIONS", textPos + NewVec2(0, textScale), embossColor, textScale);
			RcDrawNtString("ASSERTIONS", textPos, textColor, textScale);
			textPos.y += advanceY;
			#endif
			
			#if CRASH_DUMP_ASSERTS
			RcDrawNtString("CRASH_DUMP", textPos + NewVec2(0, textScale), embossColor, textScale);
			RcDrawNtString("CRASH_DUMP", textPos, textColor, textScale);
			textPos.y += advanceY;
			#endif
		}
	}
	#endif
	
	// +==============================+
	// |        Debug Overlay         |
	// +==============================+
	if (app->showDebugOverlay)
	{
		r32 fontScale = 1.0f;
		v2 textPos = NewVec2(5, 5 + RcMaxExtendUp() * fontScale);
		r32 advanceY = RcLineHeight() * fontScale + 2;
		RcBindFont(&resources->pixelFont);
		
		
	}
}
