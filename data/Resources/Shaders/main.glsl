//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2 TextureSize;
uniform vec4 SourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = (SourceRectangle.xy + (inTexCoord * SourceRectangle.zw)) / TextureSize;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;
uniform vec2 TextureSize;

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;

uniform bool      GradientEnabled;
uniform float     CircleRadius;
uniform float     CircleInnerRadius;
uniform float     Saturation;
uniform float     Brightness;

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;

layout(location = 0) out vec4 frag_colour;

vec3 czm_saturation(vec3 rgb, float adjustment)
{
    // Algorithm from Chapter 16 of OpenGL Shading Language
    const vec3 W = vec3(0.2125, 0.7154, 0.0721);
    vec3 intensity = vec3(dot(rgb, W));
    return mix(intensity, rgb, adjustment);
}

void main()
{
	vec4 sampleColor = texture(Texture, fSampleCoord);
	
	sampleColor = fColor * sampleColor;
	
	if (GradientEnabled)
	{
		float gray = dot(sampleColor.rgb, vec3(0.299, 0.587, 0.114));
		frag_colour = mix(PrimaryColor.rgba, SecondaryColor.rgba, gray);
	}
	else
	{
		frag_colour = PrimaryColor * sampleColor;
	}
	
	if (CircleRadius != 0)
	{
		float distFromCenter = length(fTexCoord - vec2(0.5, 0.5)) * 2;
		float smoothDelta = fwidth(distFromCenter);
		frag_colour.a *= smoothstep(CircleRadius, CircleRadius-smoothDelta, distFromCenter);
		if (CircleInnerRadius != 0)
		{
			frag_colour.a *= smoothstep(CircleInnerRadius, CircleInnerRadius+smoothDelta, distFromCenter);
		}
	}
	
	frag_colour.rgb = czm_saturation(frag_colour.rgb, Saturation);
	frag_colour.rgb *= Brightness;
	
	if (frag_colour.a < 0.1 && !GradientEnabled) { discard; }
}