//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec4 SourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;
uniform vec2 TextureSize;

uniform vec4 PrimaryColor;
uniform float Value0; // 0 = horizontal pass   1 = vertical pass   2 = diagonal1   3 = diagonal2
uniform float Value1; // Blur Distance (in pixels)

in vec2 fTexCoord;
in vec2 fSampleCoord;

layout(location = 0) out vec4 frag_colour;

vec4 SampleAvgHorizontal(sampler2D source, vec2 sourceSize, vec2 coordinate, float distance)
{
	vec2 initialCoord = vec2(coordinate.x - distance/2, coordinate.y);
	vec4 result = vec4(0, 0, 0, 0);
	int count = 0;
	for (float i = 0; i <= distance+1.1; i++)
	{
		result += texture(source, vec2(initialCoord.x + i, initialCoord.y) / sourceSize);
		count++;
	}
	return result / count;
}

vec4 SampleAvgVertical(sampler2D source, vec2 sourceSize, vec2 coordinate, float distance)
{
	vec2 initialCoord = vec2(coordinate.x, coordinate.y - distance/2);
	vec4 result = vec4(0, 0, 0, 0);
	int count = 0;
	for (float i = 0; i <= distance+1.1; i++)
	{
		result += texture(source, vec2(initialCoord.x, initialCoord.y + i) / sourceSize);
		count++;
	}
	return result / count;
}

vec4 SampleAvgDiagonal1(sampler2D source, vec2 sourceSize, vec2 coordinate, float distance)
{
	vec2 initialCoord = vec2(coordinate.x - distance/2, coordinate.y - distance/2);
	vec4 result = vec4(0, 0, 0, 0);
	int count = 0;
	for (float i = 0; i <= distance+1.1; i++)
	{
		result += texture(source, vec2(initialCoord.x + i, initialCoord.y + i) / sourceSize);
		count++;
	}
	return result / count;
}
vec4 SampleAvgDiagonal2(sampler2D source, vec2 sourceSize, vec2 coordinate, float distance)
{
	vec2 initialCoord = vec2(coordinate.x + distance/2, coordinate.y + distance/2);
	vec4 result = vec4(0, 0, 0, 0);
	int count = 0;
	for (float i = 0; i <= distance+1.1; i++)
	{
		result += texture(source, vec2(initialCoord.x - i, initialCoord.y - i) / sourceSize);
		count++;
	}
	return result / count;
}
void main()
{
	vec4 sampleColor;
	if (Value1 > 0.5)
	{
		if (Value0 > 2.5)
		{
			sampleColor = SampleAvgDiagonal2(Texture, TextureSize, fSampleCoord, round(Value1)*2);
		}
		else if (Value0 > 1.5)
		{
			sampleColor = SampleAvgDiagonal1(Texture, TextureSize, fSampleCoord, round(Value1)*2);
		}
		else if (Value0 > 0.5)
		{
			sampleColor = SampleAvgVertical(Texture, TextureSize, fSampleCoord, round(Value1)*2);
		}
		else
		{
			sampleColor = SampleAvgHorizontal(Texture, TextureSize, fSampleCoord, round(Value1)*2);
		}
	}
	else
	{
		sampleColor = texture(Texture, fSampleCoord / TextureSize);
	}
	frag_colour = sampleColor * PrimaryColor;
}