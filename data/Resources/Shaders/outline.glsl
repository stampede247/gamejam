//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

// uniform vec2 TextureSize;
uniform vec4 SourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;
uniform vec2 TextureSize;

uniform vec4 PrimaryColor;
uniform vec4 SecondaryColor;

in vec2 fTexCoord;
in vec2 fSampleCoord;

layout(location = 0) out vec4 frag_colour;

void main()
{
	vec4 sampleColor = texture(Texture, fSampleCoord / TextureSize);
	
	if (sampleColor.a < 0.5)
	{
		frag_colour = SecondaryColor;
	}
	else if (texture(Texture, vec2(fSampleCoord.x - 1, fSampleCoord.y    ) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 1, fSampleCoord.y    ) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x,     fSampleCoord.y - 1) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x,     fSampleCoord.y + 1) / TextureSize).a < 0.5 ||
	         
	         texture(Texture, vec2(fSampleCoord.x - 1, fSampleCoord.y - 1) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 1, fSampleCoord.y - 1) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x - 1, fSampleCoord.y + 1) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 1, fSampleCoord.y + 1) / TextureSize).a < 0.5 ||
	         
	         texture(Texture, vec2(fSampleCoord.x - 2, fSampleCoord.y - 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x - 1, fSampleCoord.y - 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 0, fSampleCoord.y - 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 1, fSampleCoord.y - 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 2, fSampleCoord.y - 2) / TextureSize).a < 0.5 ||
	         
	         texture(Texture, vec2(fSampleCoord.x - 2, fSampleCoord.y - 1) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x - 2, fSampleCoord.y + 0) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x - 2, fSampleCoord.y + 1) / TextureSize).a < 0.5 ||
	         
	         texture(Texture, vec2(fSampleCoord.x + 2, fSampleCoord.y - 1) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 2, fSampleCoord.y + 0) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 2, fSampleCoord.y + 1) / TextureSize).a < 0.5 ||
	         
	         texture(Texture, vec2(fSampleCoord.x - 2, fSampleCoord.y + 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x - 1, fSampleCoord.y + 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 0, fSampleCoord.y + 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 1, fSampleCoord.y + 2) / TextureSize).a < 0.5 ||
	         texture(Texture, vec2(fSampleCoord.x + 2, fSampleCoord.y + 2) / TextureSize).a < 0.5)
	{
		frag_colour = PrimaryColor;// * sampleColor;
	}
	else
	{
		frag_colour = SecondaryColor;
	}
}