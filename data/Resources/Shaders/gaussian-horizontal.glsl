//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec4 SourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;
uniform vec2 TextureSize;

uniform vec4 PrimaryColor;
uniform float Value0; // sigma, higher value means more blur, good values: (9x9: 3-5, 7x7: 2.5-4, 5x5: 2-3.5)

in vec2 fTexCoord;
in vec2 fSampleCoord;

layout(location = 0) out vec4 frag_colour;

const float pi = 3.14159265f;
const float numPixelsPerSide = 6.0f; //13x13
// const float numPixelsPerSide = 4.0f; //9x9
const vec2 blurMultiplyVec = vec2(1.0f, 0.0f); //For Horizontal Pass

void main()
{
	//NOTE: Value0 is sigma
	
	vec3 incrementalGaussian;
	incrementalGaussian.x = 1.0f / (sqrt(2.0f * pi) * Value0);
	incrementalGaussian.y = exp(-0.5f / (Value0*Value0)); 
	incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;
	
	vec4 avgValue = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	float coefficientSum = 0.0f;
	
	avgValue += texture2D(Texture, fTexCoord) * incrementalGaussian.x;
	coefficientSum += incrementalGaussian.x;
	incrementalGaussian.xy *= incrementalGaussian.yz;
	
	for (float i = 1.0f; i <= numPixelsPerSide; i++)
	{
		//TextureSize.x for Horizontal pass
		avgValue += texture2D(Texture, fTexCoord - i * blurMultiplyVec / TextureSize.x) * incrementalGaussian.x;
		avgValue += texture2D(Texture, fTexCoord + i * blurMultiplyVec / TextureSize.x) * incrementalGaussian.x;
		coefficientSum += 2 * incrementalGaussian.x;
		incrementalGaussian.xy *= incrementalGaussian.yz;
	}
	
	frag_colour = avgValue / coefficientSum;
	frag_colour *= PrimaryColor;
}