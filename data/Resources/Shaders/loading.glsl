//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2 TextureSize;
uniform vec4 SourceRectangle;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = (SourceRectangle.xy + (inTexCoord * SourceRectangle.zw)) / TextureSize;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;

uniform vec4 PrimaryColor;

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;

layout(location = 0) out vec4 frag_colour;

void main()
{
	vec4 sampleColor = texture(Texture, fSampleCoord);
	frag_colour = PrimaryColor * fColor * sampleColor;
	// frag_colour = vec4(1, 0, 0, 1);
}