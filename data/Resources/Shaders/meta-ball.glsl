//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform vec4      PrimaryColor;
uniform vec4      SecondaryColor;

uniform float MetaBallValues[10*10];

in vec4 fColor;
in vec2 fTexCoord;

layout(location = 0) out vec4 frag_colour;

void main()
{
	float signedValue = 0.0f;
	int secondaryColor = 0;
	for (int x = 0; x < 10; x++)
	{
		for (int y = 0; y < 10; y++)
		{
			float ballRadius = MetaBallValues[y*10 + x] / (10*2);
			vec2 ballLocation = vec2(x / 10.0, y / 10.0) - fTexCoord;
			float distSquared = (ballLocation.x*ballLocation.x + ballLocation.y*ballLocation.y);
			if (distSquared < ballRadius*ballRadius) { secondaryColor = 1; }
			// if (distSquared <= ballRadius*ballRadius) { signedValue = 1; }
			float newValue = (1 / distSquared) * ballRadius*ballRadius;
			if (newValue > 0.05) { signedValue += newValue; }
			// float value = (ballRadius*ballRadius - distSquared) / (ballRadius*ballRadius);
			// signedValue += value*value;
		}
	}
	
	// if (secondaryColor != 0)
	// {
	// 	frag_colour = fColor * SecondaryColor;
	// }
	// else if (signedValue >= 1)
	// {
	// 	frag_colour = fColor * PrimaryColor;
	// }
	// else
	// {
	// 	discard;// frag_colour = fColor * SecondaryColor;
	// }
	
	if (signedValue > 1) { frag_colour = SecondaryColor; }
	else if (signedValue > 0.5)
	{
		float mixAmount = (signedValue - 0.5) / (1 - 0.5);
		frag_colour = mix(PrimaryColor, SecondaryColor, sqrt(mixAmount));
	}
	else { discard; }
}