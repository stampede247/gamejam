//======================== VERTEX_SHADER ========================

#version 330

uniform mat4 WorldMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

uniform vec2 TextureSize;
uniform vec4 SourceRectangle;
uniform vec4 MaskRectangle;
uniform vec2 ShiftVec;

in vec3 inPosition;
in vec4 inColor;
in vec2 inTexCoord;

out vec4 fColor;
out vec2 fTexCoord;
out vec2 fSampleCoord;
out vec2 fMaskCoord;

void main()
{
	fColor = inColor;
	fTexCoord = inTexCoord;
	fSampleCoord = SourceRectangle.xy + (inTexCoord * SourceRectangle.zw);
	fMaskCoord = MaskRectangle.xy + (inTexCoord * MaskRectangle.zw);
	//NOTE: For some reason our sampling is pulling 1 too many pixels from the top row of the texture
	//      I think this is probably due to rounding errors so we just add a small value here to fix it
	//      We should somehow tune this value so it works for scales >= 100
	fSampleCoord.y += 0.01;
	fMaskCoord.y += 0.01;
	mat4 transformMatrix = ProjectionMatrix * (ViewMatrix * WorldMatrix);
	gl_Position = transformMatrix * vec4(inPosition, 1.0);
}

//======================== FRAGMENT_SHADER ========================

#version 330

uniform sampler2D Texture;
uniform vec2      TextureSize;
uniform vec4      SourceRectangle;
uniform vec4      MaskRectangle;
uniform vec2      ShiftVec;

uniform vec4      PrimaryColor;
uniform vec4      ReplaceColor1;
uniform vec4      ReplaceColor2;
uniform vec4      ReplaceColor3;
uniform vec4      ReplaceColor4;
// uniform float     Saturation;

in vec4 fColor;
in vec2 fTexCoord;
in vec2 fSampleCoord;
in vec2 fMaskCoord;

layout(location = 0) out vec4 frag_colour;

// vec3 czm_saturation(vec3 rgb, float adjustment)
// {
//     // Algorithm from Chapter 16 of OpenGL Shading Language
//     const vec3 W = vec3(0.2125, 0.7154, 0.0721);
//     vec3 intensity = vec3(dot(rgb, W));
//     return mix(intensity, rgb, adjustment);
// }

void main()
{
	vec4 targetColor1 = vec4(240.0/255.0, 110.0/255.0, 170.0/255.0, 1);
	vec4 targetColor2 = vec4(246.0/255.0, 172.0/255.0, 205.0/255.0, 1);
	vec4 targetColor3 = vec4(236.0/255.0,   0.0/255.0, 140.0/255.0, 1);
	vec4 targetColor4 = vec4(240.0/255.0, 136.0/255.0, 184.0/255.0, 1);
	
	// if (PrimaryColor.a < 0.1)
	// {
	// 	discard;
	// }
	
	vec2 realSampleCoord = fSampleCoord;
	if (ShiftVec.x != 0 || ShiftVec.y != 0)
	{
		realSampleCoord = realSampleCoord + ShiftVec;
		if (realSampleCoord.x > SourceRectangle.x + SourceRectangle.z) { discard; }//realSampleCoord.x -= SourceRectangle.z; }
		if (realSampleCoord.x < SourceRectangle.x)                     { discard; }//realSampleCoord.x += SourceRectangle.z; }
		if (realSampleCoord.y > SourceRectangle.y + SourceRectangle.w) { discard; }//realSampleCoord.y -= SourceRectangle.w; }
		if (realSampleCoord.y < SourceRectangle.y)                     { discard; }//realSampleCoord.y += SourceRectangle.w; }
		// realSampleCoord.x = round(realSampleCoord.x);
		// realSampleCoord.y = round(realSampleCoord.y);
	}
	vec4 sampleColor = texture(Texture, realSampleCoord / TextureSize);
	
	if (MaskRectangle.z > 0 && MaskRectangle.w > 0)
	{
		vec2 realMaskCoord = fMaskCoord + ShiftVec;
		// realMaskCoord.x = ceil(realMaskCoord.x);
		// realMaskCoord.y = ceil(realMaskCoord.y);
		vec4 maskSampleColor = texture(Texture, realMaskCoord / TextureSize);
		// if (maskSampleColor.a < 0.5f) { discard; }
		sampleColor.a *= maskSampleColor.a;
	}
	
	// if (PrimaryColor.a < 0.75)
	// {
	// 	if (realSampleCoord.x > SourceRectangle.x + 0.99 &&
	// 		realSampleCoord.x < SourceRectangle.x + SourceRectangle.z - 0.99 &&
	// 		realSampleCoord.y > SourceRectangle.y + 0.99 &&
	// 		realSampleCoord.y < SourceRectangle.y + SourceRectangle.w - 0.99)
	// 	{
	// 		vec4 sampleLeft  = texture(Texture, (realSampleCoord + vec2(-1, 0)) / TextureSize);
	// 		vec4 sampleRight = texture(Texture, (realSampleCoord + vec2(1, 0)) / TextureSize);
	// 		vec4 sampleUp    = texture(Texture, (realSampleCoord + vec2(0, -1)) / TextureSize);
	// 		vec4 sampleDown  = texture(Texture, (realSampleCoord + vec2(0, 1)) / TextureSize);
	// 		if (sampleLeft.a > 0.5 && sampleRight.a > 0.5 && sampleUp.a > 0.5 && sampleDown.a > 0.5)
	// 		{
	// 			discard;
	// 		}
	// 	}
	// }
	
	if (sampleColor.a > 250/256.0f)
	{
		if (abs(sampleColor.r - targetColor1.r) < 1/256.0f &&
			abs(sampleColor.g - targetColor1.g) < 1/256.0f &&
			abs(sampleColor.b - targetColor1.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor1;
		}
		else if (abs(sampleColor.r - targetColor2.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor2.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor2.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor2;
		}
		else if (abs(sampleColor.r - targetColor3.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor3.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor3.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor3;
		}
		else if (abs(sampleColor.r - targetColor4.r) < 1/256.0f &&
			     abs(sampleColor.g - targetColor4.g) < 1/256.0f &&
			     abs(sampleColor.b - targetColor4.b) < 1/256.0f)
		{
			sampleColor = ReplaceColor4;
		}
	}
	frag_colour = PrimaryColor * fColor * sampleColor;
	// frag_colour.rgb = czm_saturation(frag_colour.rgb, Saturation);
	// frag_colour.rgb = frag_colour.rgb * frag_colour.a;
	
	if (frag_colour.a < 0.1)
	{
		discard;
	}
}